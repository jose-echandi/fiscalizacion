<?php

/* FiscalizacionBundle:Fiscalizacion:mis.html.twig */
class __TwigTemplate_9eb483c93c55ab01c5ce8b333de854ad8144bafce0f8c53a8fdbbb05888fa2d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle:Fiscalizacion:index.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle:Fiscalizacion:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = array())
    {
        // line 3
        echo "a Centros Hípicos y operadoras</em>
";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Fiscalizacion:mis.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,);
    }
}
