<?php

/* FiscalizacionBundle:Fiscalizacion:index.html.twig */
class __TwigTemplate_fd281c5790f339b94a1299bf2bacef1d64a43c74cfa2bfcf9f8d8372ec530c5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'titulo' => array($this, 'block_titulo'),
            'preheader' => array($this, 'block_preheader'),
            'precols' => array($this, 'block_precols'),
            'preactions' => array($this, 'block_preactions'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        echo " 
<div class=\"col-md-11\">
    <div class=\"block-separator col-sm-11\"></div>

    <h1>Lista de Fiscalizacion ";
        // line 7
        $this->displayBlock('titulo', $context, $blocks);
        echo "</h1>

    ";
        // line 9
        if ((!array_key_exists("hidden_button", $context))) {
            // line 10
            echo "    ";
            if (($this->env->getExtension('security')->isGranted("ROLE_GERENTE") || $this->env->getExtension('security')->isGranted("ROLE_FISCAL"))) {
                // line 11
                echo "    <a href=\"";
                echo $this->env->getExtension('routing')->getPath("fiscalizacion_fiscalizar");
                echo "\" class=\"btn btn-primary\">
    Fiscalizar
    </a>
    ";
            }
            // line 15
            echo "    <a href=\"";
            echo $this->env->getExtension('routing')->getPath("fiscalizacion_citados");
            echo "\" class=\"btn btn-default\">
    Ver Citados
    </a>
    <a href=\"";
            // line 18
            echo $this->env->getExtension('routing')->getPath("fiscalizacion_multados");
            echo "\" class=\"btn btn-default\">
    Ver Multados
    </a>
    ";
        }
        // line 22
        echo "    
    <div class=\"left\">
            ";
        // line 24
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))) > 0)) {
            // line 25
            echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
            // line 26
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            echo "
                </article>
            ";
        }
        // line 29
        echo "        </div>
    <br/><br>
    ";
        // line 31
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities"))) > 0)) {
            // line 32
            echo "        ";
            echo twig_include($this->env, $context, "ExportBundle::iconslink.html.twig", array("pdf" => ("exportpdf_fiscalizacion_" . (isset($context["tipo"]) ? $context["tipo"] : $this->getContext($context, "tipo"))), "xcel" => ("exportxls_fiscalizacion_" . (isset($context["tipo"]) ? $context["tipo"] : $this->getContext($context, "tipo")))));
            echo "
        <table class=\"table table-condensed table-striped\">
            <thead>
            <tr>
                <th>No</th>
                ";
            // line 37
            $this->displayBlock('preheader', $context, $blocks);
            // line 38
            echo "                <th>D. Comercial</th>
                <th>RIF</th>
                <th>Providencia</th>
                <th>Responsable</th>
                <th>Cedula</th>
                <th>Cargo</th>
                <th>Estatus</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
            // line 49
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 50
                echo "            ";
                $context["est"] = (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centro", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centro"), $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "operadora"))) : ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "operadora")));
                // line 51
                echo "            <tr>
                <td><a href=\"";
                // line 52
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fiscalizacion_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : $this->getContext($context, "loop")), "index"), "html", null, true);
                echo "</a></td>
                ";
                // line 53
                $this->displayBlock('precols', $context, $blocks);
                // line 54
                echo "                <td>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["est"]) ? $context["est"] : $this->getContext($context, "est")), "denominacionComercial"), "html", null, true);
                echo "</td>
                <td>";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["est"]) ? $context["est"] : $this->getContext($context, "est")), "persJuridica"), "html", null, true);
                echo "-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["est"]) ? $context["est"] : $this->getContext($context, "est")), "rif"), "html", null, true);
                echo "</td>
                <td>";
                // line 56
                echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "providencia", array(), "any", false, true), "num", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "providencia", array(), "any", false, true), "num"), "-")) : ("-")), "html", null, true);
                echo "</td>
                <td>";
                // line 57
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "responsable"), "html", null, true);
                echo "</td>
                <td>";
                // line 58
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cedula"), "html", null, true);
                echo "</td>
                <td>";
                // line 59
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cargo"), "html", null, true);
                echo "</td>
                <td>";
                // line 60
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "estatus"), "html", null, true);
                echo " </td>
                <td>
                    ";
                // line 62
                $this->displayBlock('preactions', $context, $blocks);
                // line 63
                echo "                    <a class=\"btn btn-info btn-sm\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fiscalizacion_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\">Ver</a>

                </td>
            </tr>
        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "        </tbody>
    </table>
        
        ";
        } else {
            // line 72
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>No se encontraron resultados</h5></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 80
        echo "</div>
";
    }

    // line 7
    public function block_titulo($context, array $blocks = array())
    {
    }

    // line 37
    public function block_preheader($context, array $blocks = array())
    {
    }

    // line 53
    public function block_precols($context, array $blocks = array())
    {
    }

    // line 62
    public function block_preactions($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Fiscalizacion:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 62,  233 => 53,  228 => 37,  223 => 7,  218 => 80,  208 => 72,  202 => 68,  182 => 63,  180 => 62,  175 => 60,  171 => 59,  167 => 58,  163 => 57,  159 => 56,  153 => 55,  148 => 54,  146 => 53,  140 => 52,  137 => 51,  134 => 50,  117 => 49,  104 => 38,  102 => 37,  93 => 32,  91 => 31,  87 => 29,  81 => 26,  78 => 25,  76 => 24,  72 => 22,  65 => 18,  58 => 15,  50 => 11,  47 => 10,  45 => 9,  40 => 7,  32 => 3,  31 => 3,  28 => 2,);
    }
}
