<?php

/* FiscalizacionBundle::base.html.twig */
class __TwigTemplate_fe4f77098f0e8a1ef9bf46e5cffe608a48ad165e31e2b18f9e47a5104c8be0ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/genstyles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"print\">

    ";
        // line 8
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "e1efe2f_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e1efe2f_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/e1efe2f_build_standalone_1.css");
            // line 11
            echo "    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
    ";
        } else {
            // asset "e1efe2f"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e1efe2f") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/e1efe2f.css");
            echo "    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
    ";
        }
        unset($context["asset_url"]);
        // line 13
        echo "
";
    }

    // line 17
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 18
        echo "
    ";
        // line 19
        $this->displayParentBlock("foot_script_assetic", $context, $blocks);
        echo "

    ";
        // line 21
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "f06b435_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f06b435_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/f06b435_bootstrap-datetimepicker_1.js");
            // line 24
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "f06b435"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f06b435") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/f06b435.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        // line 26
        echo "
    ";
        // line 27
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "8eb30f8_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8eb30f8_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/8eb30f8_bootstrap-datetimepicker.es_1.js");
            // line 30
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "8eb30f8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8eb30f8") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/8eb30f8.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        // line 32
        echo "

    <script type=\"text/javascript\">
        \$(function(){

            \$('[data-provider=\"datepicker\"]').datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                minView: 'month',
                pickerPosition: 'bottom-left',
                todayBtn: true,
                startView: 'month',
                language: 'es'
            });

            \$('[data-provider=\"datetimepicker\"]').datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy hh:ii',
                language: 'fr',
                pickerPosition: 'bottom-left',
                todayBtn: true,
                language: 'es'
            });

            \$('[data-provider=\"timepicker\"]').datetimepicker({
                autoclose: true,
                format: 'hh:ii',
                formatViewType: 'time',
                maxView: 'day',
                minView: 'hour',
                pickerPosition: 'bottom-left',
                startView: 'day',
                language: 'es'
            });

            // Restore value from hidden input
            \$('input[type=hidden]', '.date').each(function(){
                if(\$(this).val()) {
                    \$(this).parent().datetimepicker('setValue');
                }
            });

        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 32,  105 => 30,  101 => 27,  98 => 26,  84 => 24,  80 => 21,  75 => 19,  69 => 17,  64 => 13,  46 => 8,  41 => 6,  37 => 5,  29 => 3,  238 => 62,  233 => 53,  228 => 37,  223 => 7,  218 => 80,  208 => 72,  202 => 68,  182 => 63,  180 => 62,  175 => 60,  171 => 59,  167 => 58,  163 => 57,  159 => 56,  153 => 55,  148 => 54,  146 => 53,  140 => 52,  137 => 51,  134 => 50,  117 => 49,  104 => 38,  102 => 37,  93 => 32,  91 => 31,  87 => 29,  81 => 26,  78 => 25,  76 => 24,  72 => 18,  65 => 18,  58 => 15,  50 => 11,  47 => 10,  45 => 9,  40 => 7,  32 => 4,  31 => 3,  28 => 2,);
    }
}
