<?php

namespace Proxies\__CG__\Sunahip\FiscalizacionBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Citacion extends \Sunahip\FiscalizacionBundle\Entity\Citacion implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'id', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'fecha', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'incumple', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'renovacion', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'juego', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'explota', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'permiso', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'otros', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'fiscalizacion');
        }

        return array('__isInitialized__', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'id', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'fecha', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'incumple', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'renovacion', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'juego', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'explota', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'permiso', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'otros', '' . "\0" . 'Sunahip\\FiscalizacionBundle\\Entity\\Citacion' . "\0" . 'fiscalizacion');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Citacion $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setFecha($fecha)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFecha', array($fecha));

        return parent::setFecha($fecha);
    }

    /**
     * {@inheritDoc}
     */
    public function getFecha()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFecha', array());

        return parent::getFecha();
    }

    /**
     * {@inheritDoc}
     */
    public function setIncumple($incumple)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIncumple', array($incumple));

        return parent::setIncumple($incumple);
    }

    /**
     * {@inheritDoc}
     */
    public function getIncumple()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIncumple', array());

        return parent::getIncumple();
    }

    /**
     * {@inheritDoc}
     */
    public function setRenovacion($renovacion)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRenovacion', array($renovacion));

        return parent::setRenovacion($renovacion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRenovacion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRenovacion', array());

        return parent::getRenovacion();
    }

    /**
     * {@inheritDoc}
     */
    public function setJuego($juego)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setJuego', array($juego));

        return parent::setJuego($juego);
    }

    /**
     * {@inheritDoc}
     */
    public function getJuego()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getJuego', array());

        return parent::getJuego();
    }

    /**
     * {@inheritDoc}
     */
    public function setExplota($explota)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExplota', array($explota));

        return parent::setExplota($explota);
    }

    /**
     * {@inheritDoc}
     */
    public function getExplota()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExplota', array());

        return parent::getExplota();
    }

    /**
     * {@inheritDoc}
     */
    public function setPermiso($permiso)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPermiso', array($permiso));

        return parent::setPermiso($permiso);
    }

    /**
     * {@inheritDoc}
     */
    public function getPermiso()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPermiso', array());

        return parent::getPermiso();
    }

    /**
     * {@inheritDoc}
     */
    public function setOtros($otros)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOtros', array($otros));

        return parent::setOtros($otros);
    }

    /**
     * {@inheritDoc}
     */
    public function getOtros()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOtros', array());

        return parent::getOtros();
    }

    /**
     * {@inheritDoc}
     */
    public function setFiscalizacion(\Sunahip\FiscalizacionBundle\Entity\Fiscalizacion $fiscalizacion = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFiscalizacion', array($fiscalizacion));

        return parent::setFiscalizacion($fiscalizacion);
    }

    /**
     * {@inheritDoc}
     */
    public function getFiscalizacion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFiscalizacion', array());

        return parent::getFiscalizacion();
    }

}
