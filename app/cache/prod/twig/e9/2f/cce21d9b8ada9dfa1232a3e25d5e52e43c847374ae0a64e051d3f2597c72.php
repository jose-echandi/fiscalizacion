<?php

/* CentrohipicoBundle:DataCentrohipico:show.html.twig */
class __TwigTemplate_e92fcce21d9b8ada9dfa1232a3e25d5e52e43c847374ae0a64e051d3f2597c72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style>
.ui-widget-header {
    background: transparent !important;
    color: transparent !important;
    border: none !important;
}
.ui-widget-content {
    background: transparent !important;
    color: transparent !important;
    border: none !important;
}    
</style>

<table class=\"record_properties table table-condensed col-lg-12 col-md-12 col-sm-12\" id=\"tabla_reporte\">
    <thead>
            <tr>
                <th colspan=\"2\"> Denominacion Comercial: ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
        echo "</th>
            </tr>
    </thead>
    <tbody>
        <tr>
            <td style=\"width: 60%; text-align: right;\"> RIF </td>
            <th>
                ";
        // line 24
        if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "empresa", array(), "any", true, true)) {
            // line 25
            echo "                    ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "empresa"), "persJuridica"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "empresa"), "rif"), "html", null, true);
            echo "
                ";
        }
        // line 27
        echo "            </th>
        </tr>
        <tr>
            <td style=\"width: 60%; text-align: right;\"> Fecha Registro </td>
            <th>";
        // line 31
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaRegistro"), "d-m-Y"), "html", null, true);
        echo "</th>
        </tr>
   </tbody>
</table>
<div id=\"tabs\">
      <ul>
        ";
        // line 38
        echo "        <li><a href=\"#dataoper\" data-toggle=\"tab\">Establecimiento</a></li>
        <li><a href=\"#licencia\" data-toggle=\"tab\">Licencia</a></li>
      </ul>
    ";
        // line 42
        echo "    ";
        // line 43
        echo "        ";
        // line 44
        echo "                ";
        // line 45
        echo "                    ";
        // line 46
        echo "                ";
        // line 47
        echo "        ";
        // line 48
        echo "        ";
        // line 49
        echo "        ";
        // line 50
        echo "            ";
        // line 51
        echo "                ";
        // line 52
        echo "                ";
        // line 53
        echo "                ";
        // line 54
        echo "                ";
        // line 55
        echo "            ";
        // line 56
        echo "            ";
        // line 57
        echo "                ";
        // line 58
        echo "                ";
        // line 59
        echo "                ";
        // line 60
        echo "                ";
        // line 61
        echo "            ";
        // line 62
        echo "            ";
        // line 63
        echo "                ";
        // line 64
        echo "                ";
        // line 65
        echo "                        ";
        // line 66
        echo "                    ";
        // line 67
        echo "                ";
        // line 68
        echo "                ";
        // line 69
        echo "                ";
        // line 70
        echo "                        ";
        // line 71
        echo "                    ";
        // line 72
        echo "                ";
        // line 73
        echo "            ";
        // line 74
        echo "            ";
        // line 75
        echo "                ";
        // line 76
        echo "                ";
        // line 77
        echo "                        ";
        // line 78
        echo "                    ";
        // line 79
        echo "                ";
        // line 80
        echo "                ";
        // line 81
        echo "                ";
        // line 82
        echo "                        ";
        // line 83
        echo "                    ";
        // line 84
        echo "                ";
        // line 85
        echo "            ";
        // line 86
        echo "            ";
        // line 87
        echo "                ";
        // line 88
        echo "                ";
        // line 89
        echo "                ";
        // line 90
        echo "                ";
        // line 91
        echo "            ";
        // line 92
        echo "            ";
        // line 93
        echo "                ";
        // line 94
        echo "                ";
        // line 95
        echo "                ";
        // line 96
        echo "                ";
        // line 97
        echo "            ";
        // line 98
        echo "            ";
        // line 99
        echo "                ";
        // line 100
        echo "                ";
        // line 101
        echo "                ";
        // line 102
        echo "                ";
        // line 103
        echo "            ";
        // line 104
        echo "            ";
        // line 105
        echo "                ";
        // line 106
        echo "                ";
        // line 107
        echo "                ";
        // line 108
        echo "                ";
        // line 109
        echo "            ";
        // line 110
        echo "            ";
        // line 111
        echo "                ";
        // line 112
        echo "                ";
        // line 113
        echo "            ";
        // line 114
        echo "        ";
        // line 115
        echo "        ";
        // line 116
        echo "    ";
        // line 117
        echo "    ";
        // line 118
        echo "    <div id=\"dataoper\"  class=\"col-md-12\">
        <table class=\"table table-condensed col-lg-12 col-md-12 col-sm-12\" id=\"tabla_reporte2\">
        <thead>
            <tr>
                <th colspan=\"4\" >DATOS DEL ESTABLECIMIENTO - INFORMACION SOBRE EL LUGAR DE EXPLOTACION DEL JUEGO</th>
            </tr>
        </thead>
        <tbody>
           <tr>
                <td>Denominación Comercial:</td>
                <th colspan=\"3\">";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Nombre de la Empresa o persona natural/</td>
                <th>";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "propietarioLocal"), "html", null, true);
        echo "</th>
                <td>Rif</td>
                <th>";
        // line 134
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Nombre:</td>
                <th>";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nombre"), "html", null, true);
        echo "</th>
                <td>Apellido:</td>
                <th>";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "apellido"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                 <td>Estado:</td>
                
                <th>";
        // line 145
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "estado")) > 0)) {
            // line 146
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "estado"), "nombre"), "html", null, true);
            echo "
                    ";
        }
        // line 148
        echo "                </th>
                <td>Ciudad:</td>
                <th>";
        // line 150
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ciudad")) > 0)) {
            // line 151
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ciudad"), "html", null, true);
            echo "
                    ";
        }
        // line 153
        echo "                </th>
            </tr>
            <tr>
                <td>Municipio:</td>
                <th>";
        // line 157
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "municipio")) > 0)) {
            // line 158
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "municipio"), "nombre"), "html", null, true);
            echo "
                    ";
        }
        // line 160
        echo "                </th>
                <td>Urbanización/Sector:</td>
                <th>";
        // line 162
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "urbanSector"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Av/Calle/Carrera:</td>
                <th>";
        // line 166
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "avCalleCarrera"), "html", null, true);
        echo "</th>
                <td>Edif/Casa:</td>
                <th>";
        // line 168
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "edifCasa"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Oficina/Apto/No:</td>
                <th>";
        // line 172
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ofcAptoNum"), "html", null, true);
        echo "</th>
                <td>Punto Referencia</td>
                <th>";
        // line 174
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "puntoReferencia"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Teléfono Fijo:</td>
                <th>";
        // line 178
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfFijo"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tflFijo"), "html", null, true);
        echo "</th>
                <td>Teléfono Celular</td>
                <th>";
        // line 180
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfCelular"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tflCelular"), "html", null, true);
        echo "</th>
            </tr>
        </tbody>
    </table>
    </div>
    <div id=\"licencia\"  class=\"col-md-12\">
        <table id=\"tabla_reporte3\" class=\"table table-condensed col-lg-12 col-md-12 col-sm-12\">
            <thead>
                <tr>
                    <th colspan=\"4\" > Detalles de Licencia </th>
                </tr>  
            </thead>
                <tr id=\"table_header\">
                    <td><a href=\"#\">Nº Licencia</a></td>
                    <td><a href=\"#\">Clase de Licencia</a></td>
                    <td><a href=\"#\">Fecha de Vencimiento</a></td>
                    <td><a href=\"#\">Estatus</a></td>
                </tr>
             <tbody>
             ";
        // line 199
        if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob", array(), "any", true, true)) {
            // line 200
            echo "              ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"));
            foreach ($context['_seq'] as $context["_key"] => $context["lic"]) {
                echo "  
                <tr>
                    <td class=\"text-center\">";
                // line 202
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "numLicencia"), "html", null, true);
                echo "</td>
                    <td class=\"text-center\">";
                // line 203
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "clasfLicencia"), "html", null, true);
                echo "</td>
                    <td class=\"text-center\">";
                // line 204
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "fechaVencimiento"), "html", null, true);
                echo "</td>
                    <td class=\"text-center\">";
                // line 205
                echo ((($this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "vigente") == 1)) ? ("Vigente") : ("Vencida"));
                echo "</td>
                </tr>
             ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 208
            echo "             ";
            if (twig_test_empty($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"))) {
                // line 209
                echo "                 <tr><td colspan=\"4\">Sin Registros</td></tr>
             ";
            }
            // line 211
            echo "             ";
        }
        // line 212
        echo "          </tbody>
        </table>
    </div>
</div>
<script type=\"text/javascript\" >
    \$(document).ready(function(){
        \$('#tabs').tabs();
        \$(\"#dataoper #datalegal\").show();
    });
</script>  
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataCentrohipico:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 212,  413 => 211,  409 => 209,  406 => 208,  397 => 205,  393 => 204,  389 => 203,  385 => 202,  377 => 200,  375 => 199,  351 => 180,  344 => 178,  337 => 174,  332 => 172,  325 => 168,  320 => 166,  313 => 162,  309 => 160,  303 => 158,  301 => 157,  295 => 153,  289 => 151,  287 => 150,  283 => 148,  277 => 146,  275 => 145,  267 => 140,  262 => 138,  253 => 134,  248 => 132,  241 => 128,  229 => 118,  227 => 117,  225 => 116,  223 => 115,  221 => 114,  219 => 113,  217 => 112,  215 => 111,  213 => 110,  211 => 109,  209 => 108,  207 => 107,  205 => 106,  203 => 105,  201 => 104,  199 => 103,  197 => 102,  195 => 101,  193 => 100,  191 => 99,  189 => 98,  187 => 97,  185 => 96,  183 => 95,  181 => 94,  179 => 93,  177 => 92,  175 => 91,  173 => 90,  171 => 89,  169 => 88,  167 => 87,  165 => 86,  163 => 85,  161 => 84,  159 => 83,  157 => 82,  155 => 81,  153 => 80,  151 => 79,  149 => 78,  147 => 77,  145 => 76,  143 => 75,  141 => 74,  139 => 73,  137 => 72,  135 => 71,  133 => 70,  131 => 69,  129 => 68,  127 => 67,  125 => 66,  123 => 65,  121 => 64,  119 => 63,  117 => 62,  115 => 61,  113 => 60,  111 => 59,  109 => 58,  107 => 57,  105 => 56,  103 => 55,  101 => 54,  99 => 53,  97 => 52,  95 => 51,  93 => 50,  91 => 49,  89 => 48,  87 => 47,  85 => 46,  83 => 45,  81 => 44,  79 => 43,  77 => 42,  72 => 38,  63 => 31,  57 => 27,  49 => 25,  47 => 24,  37 => 17,  19 => 1,);
    }
}
