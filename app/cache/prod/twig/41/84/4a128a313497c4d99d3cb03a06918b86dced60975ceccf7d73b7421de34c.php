<?php

/* CentrohipicoBundle:DataOperadora:noreplegal.html.twig */
class __TwigTemplate_41844a128a313497c4d99d3cb03a06918b86dced60975ceccf7d73b7421de34c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-12\">
    <div class=\"alert alert-warning\">
        No hay un <strong>Representante Legal</strong> creado. Puede crear en <a href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("operadora_new");
        echo "\" class=\"btn btn-default\">Crear Representante Legal</a>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataOperadora:noreplegal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 6,  31 => 4,  28 => 3,);
    }
}
