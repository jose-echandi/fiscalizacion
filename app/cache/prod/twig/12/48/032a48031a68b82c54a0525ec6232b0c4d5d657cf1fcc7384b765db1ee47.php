<?php

/* ReporteBundle:Filtro:ingresos.html.twig */
class __TwigTemplate_1248032a48031a68b82c54a0525ec6232b0c4d5d657cf1fcc7384b765db1ee47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<li>
    Ingresos por procesamiento:
    <input type=\"radio\" name=\"status\" value=\"PROCESAMIENTO\" ";
        // line 3
        if (array_key_exists("status", $context)) {
            echo " ";
            if (((isset($context["status"]) ? $context["status"] : null) == "Solicitada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo ">
</li>
<li>
    Ingresos por otorgamiento:
    <input type=\"radio\" name=\"status\" value=\"OTORGAMIENTO\" ";
        // line 7
        if (array_key_exists("status", $context)) {
            echo " ";
            if (((isset($context["status"]) ? $context["status"] : null) == "Solicitada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo ">
</li>
<li>
    Ingresos por aportes:
    <input type=\"radio\" name=\"status\" value=\"APORTE_MENSUAL\" ";
        // line 11
        if (array_key_exists("status", $context)) {
            echo " ";
            if (((isset($context["status"]) ? $context["status"] : null) == "Solicitada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo ">
</li>
<li>
    Ingresos por multas:
    <input type=\"radio\" name=\"status\" value=\"MULTAS\" ";
        // line 15
        if (array_key_exists("status", $context)) {
            echo " ";
            if (((isset($context["status"]) ? $context["status"] : null) == "Solicitada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo ">
</li>";
    }

    public function getTemplateName()
    {
        return "ReporteBundle:Filtro:ingresos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 15,  49 => 11,  36 => 7,  23 => 3,  19 => 1,);
    }
}
