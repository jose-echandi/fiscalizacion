<?php

/* CentrohipicoBundle:DataOperadora:info.html.twig */
class __TwigTemplate_1220056dc48c9316b0ec6af41bea4c877ddad3e583377a02f59ced0a75f23ee8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style>
    .ui-widget-header {
        background: transparent !important;
        color: transparent !important;
        border: none !important;
    }
    .ui-widget-content {
        background: transparent !important;
        color: transparent !important;
        border: none !important;
    }    
</style>

<table class=\"record_properties table table-condensed col-lg-12 col-md-12 col-sm-12\" id=\"tabla_reporte\">
    <thead>
            <tr>
                <th colspan=\"2\"> Denominacion Comercial: ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
        echo "</th>
            </tr>
    </thead>
    <tbody>
        <tr>
            <td style=\"width: 60%; text-align: right;\"> RIF </td>
            <th>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
        echo "</th>
        </tr>
        <tr>
            <td style=\"width: 60%; text-align: right;\"> Fecha Registro </td>
            <th>";
        // line 27
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaRegistro"), "d-m-Y"), "html", null, true);
        echo "</th>
        </tr>
   </tbody>
</table>
<div id=\"tabs\">
    <ul>
        <li><a href=\"#tabs-1\">Básicos</a></li>
        <li><a href=\"#tabs-2\">Rep Legal</a></li>
        <li><a href=\"#tabs-3\">Oficina</a></li>
        <li><a href=\"#tabs-4\">Solicitudes</a></li>
        <li><a href=\"#tabs-5\">Pagos</a></li>
        ";
        // line 39
        echo "        <li><a href=\"#tabs-7\">Licencias</a></li>
    </ul>
    <div id=\"tabs-1\">
        <table id=\"tabla_reporte2\">
            <tr id=\"table_header2\">
                <td colspan=\"4\"><b>Datos del Solicitante</b></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>RIF:</td>
                <td><strong>";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "persJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "rif"), "html", null, true);
        echo "</strong></td>

            </tr>
            <tr>
                <td>Nombre:</td>
                <td><strong>";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "nombre"), "html", null, true);
        echo "</strong></td>
                <td>Apellido</td>
                <td><strong>";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "apellido"), "html", null, true);
        echo "</strong></td>
            </tr>
            <tr>
                <td>Entidad Federal:</td>
                <td><strong>";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getEstado"), "nombre"), "html", null, true);
        echo "</strong></td>
                <td>Ciudad:</td>
                <td><strong>";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "ciudad"), "html", null, true);
        echo "</strong></td>
            </tr>
            <tr>
                <td>Parroquia:</td>
                <td><strong>";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getParroquia"), "nombre"), "html", null, true);
        echo "</strong></td>
                <td>Urbanización/Sector:</td>
                <td><strong>";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "urbanizacion"), "html", null, true);
        echo "</strong></td>
            </tr>
            <tr>
                <td>Oficina/Apto/No.:</td>
                <td><strong>";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "apartamento"), "html", null, true);
        echo "</strong></td>
                <td>Punto de Referencia:</td>
                <td><strong>";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "referencia"), "html", null, true);
        echo "</strong></td>
            </tr>
            <tr>
                <td>Código Postal:</td>
                <td><strong>";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getCodigoPostal"), "html", null, true);
        echo "</strong></td>
                <td>No. Fax:</td>
                <td>
                    <strong>";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getCodFax"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "fax"), "html", null, true);
        echo "</strong>
                </td>
            </tr>
            <tr>
                <td>No. Teléfono Fijo:</td>
                <td>";
        // line 87
        if ((($this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getCodTelefonoLocal") != "") && ($this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getTelefonoLocal") != ""))) {
            // line 88
            echo "                    <strong>(";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getCodTelefonoLocal"), "html", null, true);
            echo ") ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getTelefonoLocal"), "html", null, true);
            echo "</strong>
                    ";
        }
        // line 90
        echo "                </td>
                <td>No. Teléfono Celular:</td>
                <td>";
        // line 92
        if ((($this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getCodTelefonoMovil") != "") && ($this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getTelefonoMovil") != ""))) {
            // line 93
            echo "                    <strong>(";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getCodTelefonoMovil"), "html", null, true);
            echo ") ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getTelefonoMovil"), "html", null, true);
            echo "</strong>
                    ";
        }
        // line 95
        echo "                </td>
            </tr>
            <tr>
                <td>Correo Electrónico:</td>
                <td><strong><a href=\"#\">";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getUsuario"), "email"), "html", null, true);
        echo "</a></strong></td>
                <td>Página Web:</td>

                <td><strong><a href=\"#\">";
        // line 102
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["basicos"]) ? $context["basicos"] : null), "getSitioWeb"), "html", null, true);
        echo "</a></strong></td>
            </tr>
        </table>
    </div>
    <div id=\"tabs-2\">
        ";
        // line 107
        if (array_key_exists("legal", $context)) {
            // line 108
            echo "        <table id=\"tabla_reporte2\">
            <tr id=\"table_header2\">
                <td colspan=\"4\"><b>Datos del Apoderado o Representante Legal</b></td>
            </tr>
            <tr>
                <td>Nombre:</td>
                <td><strong>";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "nombre"), "html", null, true);
            echo "</strong></td>
                <td>Apellido</td>
                <td><strong>";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "apellido"), "html", null, true);
            echo "</strong></td>
            </tr>
            <tr>
                <td>Entidad Federal:</td>
                <td><strong>";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "getEstado"), "nombre"), "html", null, true);
            echo "</strong></td>
                <td>Ciudad:</td>
                <td><strong>";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "ciudad"), "html", null, true);
            echo "</strong></td>
            </tr>
            <tr>
                <td>Parroquia:</td>
                <td><strong>";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "getParroquia"), "nombre"), "html", null, true);
            echo "</strong></td>
                <td>Urbanización/Sector:</td>
                <td><strong>";
            // line 128
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "urbanSector"), "html", null, true);
            echo "</strong></td>
            </tr>
            <tr>
                <td>Avenida/Calle/Carrera:</td>
                <td><strong>";
            // line 132
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "avCalleCarrera"), "html", null, true);
            echo "</strong></td>
                <td>Edificio/Casa:</td>
                <td><strong>";
            // line 134
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "edifCasa"), "html", null, true);
            echo "</strong></td>
            </tr>
            <tr>
                <td>Oficina/Apto/No.:</td>
                <td><strong>";
            // line 138
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "ofcAptoNum"), "html", null, true);
            echo "</strong></td>
                <td>Punto de Referencia:</td>
                <td><strong>";
            // line 140
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "puntoReferencia"), "html", null, true);
            echo "</strong></td>
            </tr>
            <tr>
                <td>Código Postal:</td>
                <td><strong>";
            // line 144
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "codigoPostal"), "html", null, true);
            echo "</strong></td>
                <td>No. Fax:</td>
                <td><strong>";
            // line 146
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "codFax"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "fax"), "html", null, true);
            echo "</strong></td>
            </tr>
            <tr>
                <td>No. Teléfono Fijo:</td>
                <td>";
            // line 150
            if ((($this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "codTlfFijo") != "") && ($this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "tlfFijo") != ""))) {
                // line 151
                echo "                    <strong>(";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "codTlfFijo"), "html", null, true);
                echo ") ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "tlfFijo"), "html", null, true);
                echo "</strong>
                    ";
            }
            // line 153
            echo "                </td>
                <td>No. Teléfono Celular:</td>
                <td>";
            // line 155
            if ((($this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "codTlfCelular") != "") && ($this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "tlfCelular") != ""))) {
                // line 156
                echo "                    <strong>(";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "codTlfCelular"), "html", null, true);
                echo ") ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "tlfCelular"), "html", null, true);
                echo "</strong>
                    ";
            }
            // line 158
            echo "                </td>
            </tr>
        </table>
        ";
        } else {
            // line 162
            echo "            Sin información
        ";
        }
        // line 164
        echo "    </div>
    <div id=\"tabs-3\">
        <table id=\"tabla_reporte2\">
            <tr id=\"table_header2\">
                <td colspan=\"4\"><b>Oficina Principal</b></td>
            </tr>
            <tr>
                <td colspan=\"2\">Denominación Comercial</td>
                <td colspan=\"2\"><b>";
        // line 172
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
        echo "</b></td>
            </tr>
            <tr>
                <td colspan=\"2\">Nombre:</td>                
                <td colspan=\"2\">";
        // line 176
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "propietarioLocal"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <td>Entidad Federal:</td>
                <td><strong>";
        // line 180
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getEstado"), "nombre"), "html", null, true);
        echo "</strong></td>
                <td>Ciudad:</td>
                <td><strong>";
        // line 182
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ciudad"), "html", null, true);
        echo "</strong></td>
            </tr>
            <tr>
                <td>Parroquia:</td>
                <td><strong>";
        // line 186
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getParroquia"), "nombre"), "html", null, true);
        echo "</strong></td>
                <td>Urbanización/Sector:</td>
                <td><strong>";
        // line 188
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "urbanSector"), "html", null, true);
        echo "</strong></td>
            </tr>
            <tr>
                <td>Avenida/Calle/Carrera:</td>
                <td><strong>";
        // line 192
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "avCalleCarrera"), "html", null, true);
        echo "</strong></td>
                <td>Edificio/Casa:</td>
                <td><strong>";
        // line 194
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "edifCasa"), "html", null, true);
        echo "</strong></td>
            </tr>
            <tr>
                <td>Oficina/Apto/No.:</td>
                <td><strong>";
        // line 198
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ofcAptoNum"), "html", null, true);
        echo "</strong></td>
                <td>Punto de Referencia:</td>
                <td><strong>";
        // line 200
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "puntoReferencia"), "html", null, true);
        echo "</strong></td>
            </tr>
            <tr>
                <td>Código Postal:</td>
                <td><strong>";
        // line 204
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codigoPostal"), "html", null, true);
        echo "</strong></td>
                <td>No. Fax:</td>
                <td><strong>";
        // line 206
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codFax"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fax"), "html", null, true);
        echo "</strong></td>
            </tr>
            <tr>
                <td>No. Teléfono Fijo:</td>
                <td>";
        // line 210
        if ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfFijo") != "") && ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tlfFijo") != ""))) {
            // line 211
            echo "                    <strong>(";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfFijo"), "html", null, true);
            echo ") ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tlfFijo"), "html", null, true);
            echo "</strong>
                    ";
        }
        // line 213
        echo "                </td>
                <td>No. Teléfono Celular:</td>
                <td>";
        // line 215
        if ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfCelular") != "") && ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tlfCelular") != ""))) {
            // line 216
            echo "                    <strong>(";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfCelular"), "html", null, true);
            echo ") ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tlfCelular"), "html", null, true);
            echo "</strong>
                    ";
        }
        // line 218
        echo "                </td>
            </tr>
        </table>
    </div>
    <div id=\"tabs-4\">
        ";
        // line 223
        if (array_key_exists("licence", $context)) {
            // line 224
            echo "        <table id=\"tabla_reporte2\" class=\"table table-condensed table-striped\">
            <thead>
                <tr id=\"table_header2\">
                    <th>Nº Solicitud</th>
                    <th>Tipo de Autorización</th>
                    <th>Fecha</th>
                    <th>Estatus</th>
                </tr>
            </thead>
            <tbody>
                ";
            // line 234
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["licenses"]) ? $context["licenses"] : null));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["licence"]) {
                // line 235
                echo "                <tr>
                    <td>";
                // line 236
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["licence"]) ? $context["licence"] : null), "codsolicitud"), "html", null, true);
                echo "</td>
                    <td>";
                // line 237
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["licence"]) ? $context["licence"] : null), "ClasLicencia"), "html", null, true);
                echo "</td>
                    <td>";
                // line 238
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["licence"]) ? $context["licence"] : null), "fechaSolicitud"), "d/m/Y"), "html", null, true);
                echo "</td>
                    <td>";
                // line 239
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["licence"]) ? $context["licence"] : null), "status"), "html", null, true);
                echo "</td>
                </tr>
                ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 242
                echo "                <tr>
                    <td colspan=\"5\">No hay Registros</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['licence'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 246
            echo "            </tbody>
        </table>
        ";
        } else {
            // line 249
            echo "            Sin información
        ";
        }
        // line 251
        echo "    </div>
    <div id=\"tabs-5\">
        <table id=\"tabla_reporte2\" class=\"table table-condensed table-striped\">
            <thead>
                <tr id=\"table_header2\">
                    <th>Nº</th>
                    <th>Tipo Pago</th>
                    <th>Fecha Pago</th>
                    <th>Monto</th>
                    <th>Estatus</th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 264
        if (array_key_exists("payments", $context)) {
            // line 265
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["payments"]) ? $context["payments"] : null));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["payment"]) {
                // line 266
                echo "                <tr>
                    <td>
                        ";
                // line 268
                if ((!(null === $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "getSolicitud")))) {
                    // line 269
                    echo "                            ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "getSolicitud"), "codsolicitud"), "html", null, true);
                    echo "
                        ";
                }
                // line 271
                echo "                    </td>
                    <td>";
                // line 272
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "tipoPago"), "html", null, true);
                echo "</td>
                    <td>";
                // line 273
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "fechaDeposito"), "d/m/Y"), "html", null, true);
                echo "</td>
                    <td>";
                // line 274
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "monto"), 2, ",", "."), "html", null, true);
                echo "</td>
                    <td>";
                // line 275
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "status"), "html", null, true);
                echo "</td>
                </tr>
                ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 278
                echo "                <tr>
                    <td colspan=\"5\">No hay Registros</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 282
            echo "            ";
        }
        // line 283
        echo "            </tbody>
        </table>
    </div>
    <div id=\"tabs-7\">
        ";
        // line 287
        if (array_key_exists("lic", $context)) {
            // line 288
            echo "        <table id=\"tabla_reporte2\" class=\"table table-condensed table-striped\">
            <thead>
                <tr id=\"table_header2\">
                    <th>Nº Licencia</th>
                    <th>Clasificación</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Vencimiento</th>
                </tr>
            </thead>
            <tbody>
            ";
            // line 298
            if (array_key_exists("licensesAprob", $context)) {
                // line 299
                echo "                ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["licensesAprob"]) ? $context["licensesAprob"] : null));
                $context['_iterated'] = false;
                foreach ($context['_seq'] as $context["_key"] => $context["lic"]) {
                    // line 300
                    echo "                <tr>
                    <td>";
                    // line 301
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "getSolicitud"), "numLicenciaAdscrita"), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 302
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "getSolicitud"), "ClasLicencia"), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 303
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "fechaInicio"), "d/m/Y"), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 304
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "fechaFin"), "d/m/Y"), "html", null, true);
                    echo "</td>
                </tr>
                ";
                    $context['_iterated'] = true;
                }
                if (!$context['_iterated']) {
                    // line 307
                    echo "                <tr>
                    <td colspan=\"4\">No hay Registros</td>
                </tr>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lic'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 311
                echo "            ";
            }
            // line 312
            echo "            </tbody>
        </table>
        ";
        } else {
            // line 315
            echo "            Sin información
        ";
        }
        // line 317
        echo "    </div>
</div>

<script type=\"text/javascript\" >
    \$(document).ready(function(){
        \$('#tabs').tabs();
    });
</script> ";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataOperadora:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  637 => 317,  633 => 315,  628 => 312,  625 => 311,  616 => 307,  608 => 304,  604 => 303,  600 => 302,  596 => 301,  593 => 300,  587 => 299,  585 => 298,  573 => 288,  571 => 287,  565 => 283,  562 => 282,  553 => 278,  545 => 275,  541 => 274,  537 => 273,  533 => 272,  530 => 271,  524 => 269,  522 => 268,  518 => 266,  512 => 265,  510 => 264,  495 => 251,  491 => 249,  486 => 246,  477 => 242,  469 => 239,  465 => 238,  461 => 237,  457 => 236,  454 => 235,  449 => 234,  437 => 224,  435 => 223,  428 => 218,  420 => 216,  418 => 215,  414 => 213,  406 => 211,  404 => 210,  395 => 206,  390 => 204,  383 => 200,  378 => 198,  371 => 194,  366 => 192,  359 => 188,  354 => 186,  347 => 182,  342 => 180,  335 => 176,  328 => 172,  318 => 164,  314 => 162,  308 => 158,  300 => 156,  298 => 155,  294 => 153,  286 => 151,  284 => 150,  275 => 146,  270 => 144,  263 => 140,  258 => 138,  251 => 134,  246 => 132,  239 => 128,  234 => 126,  227 => 122,  222 => 120,  215 => 116,  210 => 114,  202 => 108,  200 => 107,  192 => 102,  186 => 99,  180 => 95,  172 => 93,  170 => 92,  166 => 90,  158 => 88,  156 => 87,  146 => 82,  140 => 79,  133 => 75,  128 => 73,  121 => 69,  116 => 67,  109 => 63,  104 => 61,  97 => 57,  92 => 55,  82 => 50,  69 => 39,  55 => 27,  46 => 23,  37 => 17,  19 => 1,);
    }
}
