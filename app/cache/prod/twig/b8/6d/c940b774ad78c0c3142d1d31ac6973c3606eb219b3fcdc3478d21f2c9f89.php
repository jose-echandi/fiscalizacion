<?php

/* FOSUserBundle:ChangePassword:changePassword.html.twig */
class __TwigTemplate_b86dc940b774ad78c0c3142d1d31ac6973c3606eb219b3fcdc3478d21f2c9f89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script' => array($this, 'block_foot_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content_content($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"page-header\">
        <h1>Cambiar contraseña</h1>
    </div>

    <form action=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo "
          id=\"form-change-password\" method=\"POST\" class=\"fos_user_change_password form-horizontal\" role=\"form\">
       ";
        // line 30
        echo "        <div  class=\"form-group col-sm-9\">
                <section>
                        <div class=\"input\">";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "current_password"), 'row', array("attr" => array("maxlength" => "8")));
        // line 33
        echo "</div>
                </section>
                <section>
                        <div class=\"input\">";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "plainPassword"), "first"), 'row', array("attr" => array("maxlength" => "8")));
        // line 37
        echo "</div>
                </section>
                <section>
                        <div class=\"input\">";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "plainPassword"), "second"), 'row', array("attr" => array("maxlength" => "8")));
        // line 41
        echo "</div>
                </section>
        </div>
                    ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
        <div class=\"form-group\">
            <div class=\"col-sm-8\">
                <button type=\"submit\" class=\"btn btn-primary pull-right\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change_password.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "</button>
            </div>
        </div>
    </form>
";
    }

    // line 52
    public function block_foot_script($context, array $blocks = array())
    {
        // line 53
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/user/js/Password/validaPassword.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 53,  85 => 52,  76 => 47,  70 => 44,  65 => 41,  63 => 40,  58 => 37,  56 => 36,  51 => 33,  49 => 32,  45 => 30,  38 => 9,  32 => 5,  29 => 4,);
    }
}
