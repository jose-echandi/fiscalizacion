<?php

/* SolicitudesCitasBundle:DataSolicitudes:generada.html.twig */
class __TwigTemplate_f1ae278b025b7a4a40b2b7b2ce5427b364d0ce52b724e642e6f6caee4ee7dfe6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SolicitudesCitasBundle::solicitud_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"contendor\">
        <br/>
\t<div class=\"tit_principal\">Solicitud Centro Hípico Generada</div>
         <br /><br /><br />
         
             <table id=\"tabla_reporte2\">
                 <tbody>
                       <tr id=\"table_header2\">
                            <td>Nº Localizador</td>
                            <td>Denominación</td>
                            <td>Fecha Solicitud </td>
                            <td>Fecha de Cita  &nbsp;</td>
                            <td>Tipo de Autorización</td>
                            <td>Pago Procesamiento</td>
                            <td>Pago Otorgamiento</td>
\t               </tr>
                        <tr>
                            <td class=\"text-center bg-white\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codsolicitud"), "html", null, true);
        echo "</td>
                            <td class=\"text-center bg-white\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "denominacionComercial"), "html", null, true);
        echo "</td>
                            <td class=\"text-center bg-white\">";
        // line 22
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaSolicitud"), "d-m-Y"), "html", null, true);
        echo "</td>
                            <td class=\"text-center bg-white\">";
        // line 23
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "fechaSolicitud"), "d-m-Y"), "html", null, true);
        echo "</td>
                            <td class=\"text-center bg-white\">";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ClasLicencia"), "clasfLicencia"), "html", null, true);
        echo "</td>
                            <td class=\"text-center bg-white\">";
        // line 25
        echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ClasLicencia"), "solicitudUt") * twig_number_format_filter($this->env, (isset($context["unidad_tributaria"]) ? $context["unidad_tributaria"] : null), 2, ",", ".")), "html", null, true);
        echo " Bs</td>
                            <td class=\"text-center bg-white\">";
        // line 26
        echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ClasLicencia"), "otorgamientoUt") * twig_number_format_filter($this->env, (isset($context["unidad_tributaria"]) ? $context["unidad_tributaria"] : null), 2, ",", ".")), "html", null, true);
        echo " Bs</td>
                         </tr>
\t\t</tbody>
             </table>              
       <br/><br/>
      <div class=\"col-md-2 col-md-offset-6\"> 
          <a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("solicitudes_list");
        echo "\" class=\"btn btn-warning\"><i class=\"fa fa-list\"></i> Ir a Solicitudes</a>
      </div> 
      ";
        // line 36
        echo "                  
      <div class=\"col-md-2\"> 
          <a href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("solicitudes_printGen", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "tipo" => "1")), "html", null, true);
        echo "\" target=\"_blank\" class=\"btn btn-info\"><i class=\"fa fa-print\"></i> Imprimir</a>
          ";
        // line 40
        echo "      </div>                  
 </div>   
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:generada.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 40,  92 => 38,  88 => 36,  83 => 32,  66 => 24,  62 => 23,  58 => 22,  54 => 21,  50 => 20,  36 => 4,  31 => 3,  28 => 2,  78 => 40,  74 => 26,  70 => 25,  52 => 23,  48 => 22,  44 => 21,  22 => 2,  19 => 1,);
    }
}
