<?php

/* CentrohipicoBundle:DataCentrohipico:CHtable_form.html.twig */
class __TwigTemplate_c21690c6b40174ebaba499ed7bf64bd23b75c9642ef62187258e7a405bdc4588 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        echo "<table id=\"tabla_reporte2\" class=\"tabla_reporte2\">\t
    <tbody>
      <tr id=\"table_header2\"><td colspan=\"4\">Datos del Establecimiento</td></tr>
      ";
        // line 10
        if ((!(isset($context["hidden"]) ? $context["hidden"] : null))) {
            // line 11
            echo "      <tr>
          <td colspan=\"2\" style=\"width: 50%;\">";
            // line 12
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "empresa"), 'label', array("label_attr" => array("class" => "text-left col-md-12")));
            echo "</td>
          <td colspan=\"2\" style=\"width: 50%;\">
              ";
            // line 14
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "empresa"), 'widget', array("attr" => array("class" => " ")));
            echo "
              ";
            // line 15
            if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "empresa", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
                // line 16
                echo "                  <span class=\"help-block \">
                    ";
                // line 17
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "empresa"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
                echo "
                </span>
              ";
            }
            // line 20
            echo "          </td>
      </tr>
      ";
        }
        // line 23
        echo "    <tr>
        <td colspan=\"2\" style=\"width: 50%;\">";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "denominacionComercial"), 'label', array("label_attr" => array("class" => "text-left col-md-12")));
        echo "</td>
        <td colspan=\"2\" style=\"width: 50%;\">
            ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "denominacionComercial"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
            ";
        // line 27
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "denominacionComercial", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 28
            echo "            <span class=\"help-block \">
                ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "denominacionComercial"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
            </span>
            ";
        }
        // line 32
        echo "            
        </td>
    </tr>\t\t\t
    <tr>
             <td colspan=\"2\" style=\"width: 50%;\">";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estatusLocal"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td colspan=\"2\" style=\"width: 50%;\">
                      ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estatusLocal"), 'widget', array("attr" => array("class" => " ")));
        echo "
                            ";
        // line 39
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estatusLocal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 40
            echo "                            <span class=\"help-block \">
                                ";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estatusLocal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 44
        echo "             </td>
    </tr>
    <tr>
             <td colspan=\"2\" style=\"width: 50%;\">";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "clasificacionLocal"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td colspan=\"2\" style=\"width: 50%;\">
                 ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "clasificacionLocal"), 'widget', array("attr" => array("class" => " ")));
        echo "
                            ";
        // line 50
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "clasificacionLocal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 51
            echo "                            <span class=\"help-block \">
                                ";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "clasificacionLocal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 55
        echo "             </td>
    </tr>\t
    <tr>
             <td colspan=\"2\" style=\"width: 50%;\">";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "propietarioLocal"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
             <td colspan=\"2\" style=\"width: 50%;\">
                 ";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "propietarioLocal"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                            ";
        // line 61
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "propietarioLocal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 62
            echo "                            <span class=\"help-block \">
                                ";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "propietarioLocal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 66
        echo "             </td>
    </tr>\t
    <tr>
             <td>";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
             <td>";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), 'widget', array("attr" => array("class" => " ")));
        echo "
                            ";
        // line 71
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 72
            echo "                            <span class=\"help-block \">
                                ";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 76
        echo "                 
             </td>
             <td>";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
             <td>";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), 'widget', array("attr" => array("class" => " ")));
        echo "
                            ";
        // line 80
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 81
            echo "                            <span class=\"help-block \">
                                ";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 85
        echo "                 
             </td>
             
    </tr>
    <tr>
             <td>";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>
                  ";
        // line 92
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tipoci"), 'widget', array("attr" => array("class" => " input-mini")));
        echo "
                  - ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), 'widget', array("attr" => array("class" => " ")));
        echo "
                    ";
        // line 94
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 95
            echo "                    <span class=\"help-block \">
                        ";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 99
        echo "                 
             </td>
             <td>";
        // line 101
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rifDueno"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>
                 ";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "persJuridicaDueno"), 'widget', array("attr" => array("class" => "input-mini ")));
        echo "
                  - ";
        // line 104
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rifDueno"), 'widget', array("attr" => array("class" => " ")));
        echo "
                    ";
        // line 105
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rifDueno", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 106
            echo "                    <span class=\"help-block \">
                        ";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rifDueno"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 110
        echo "                 
             </td>

    </tr>
    <tr>
             <td>";
        // line 115
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 116
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'widget', array("attr" => array("class" => "estados", "style" => "width:160px !important")));
        echo "
                    ";
        // line 117
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 118
            echo "                    <span class=\"help-block \">
                        ";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 122
        echo "                
             </td>
             <td><label class=\"col-md-12 control-label text-left required\" for=\"municipios\">Municipio <span class=\"asterisk\">*</span></label></td>
             <td>
                ";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), 'widget', array("attr" => array("class" => "municipio")));
        echo "
                ";
        // line 127
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 128
            echo "                <span class=\"help-block \">
                    ";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                </span>
                ";
        }
        // line 131
        echo "                 
             </td>

    </tr>
    <tr>
            <td>";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 137
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 138
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 139
            echo "                    <span class=\"help-block \">
                        ";
            // line 140
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 143
        echo "             </td>
             <td><label class=\"col-md-12 control-label text-left required\" for=\"parroquia\">Parroquia <span class=\"asterisk\">*</span></label></td>
             <td>
                ";
        // line 146
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), 'widget', array("attr" => array("class" => "parroquia")));
        echo "
                ";
        // line 147
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 148
            echo "                <span class=\"help-block \">
                    ";
            // line 149
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                </span>
                ";
        }
        // line 151
        echo "  
             </td>
    </tr>
    <tr>
             <td>";
        // line 155
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 156
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 157
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 158
            echo "                    <span class=\"help-block \">
                        ";
            // line 159
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 162
        echo "             </td>
             <td>";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>
                 ";
        // line 165
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 166
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 167
            echo "                    <span class=\"help-block \">
                        ";
            // line 168
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 171
        echo "                
             </td>
    </tr>
    <tr>
             <td>";
        // line 175
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 176
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 177
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 178
            echo "                    <span class=\"help-block \">
                        ";
            // line 179
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 182
        echo "                
                 
             </td>
             <td>";
        // line 185
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 186
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 187
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 188
            echo "                    <span class=\"help-block \">
                        ";
            // line 189
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 192
        echo "                
                 
             </td>
    </tr>
    <tr>
             <td>";
        // line 197
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 198
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 199
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 200
            echo "                    <span class=\"help-block \">
                        ";
            // line 201
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 204
        echo "                
             </td>
             <td>";
        // line 206
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 207
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
                    ";
        // line 208
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 209
            echo "                    <span class=\"help-block \">
                        ";
            // line 210
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 213
        echo "                
             </td>
    </tr>
    <tr>
             <td>";
        // line 217
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 218
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 219
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 220
            echo "                    <span class=\"help-block \">
                        ";
            // line 221
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 224
        echo "             </td>
        <td>";
        // line 225
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
        <td>";
        // line 226
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codFax"), 'widget', array("attr" => array("class" => " col-md-4")));
        echo "
            ";
        // line 227
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
            ";
        // line 228
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 229
            echo "                <span class=\"help-block \">
                        ";
            // line 230
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
            ";
        }
        // line 233
        echo "        </td>
    </tr>
    <tr>
             <td>";
        // line 236
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 237
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codTlfFijo"), 'widget', array("attr" => array("class" => " col-md-4")));
        echo "
                 ";
        // line 238
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
                    ";
        // line 239
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 240
            echo "                    <span class=\"help-block \">
                        ";
            // line 241
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 244
        echo "             </td>
             <td>";
        // line 245
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 246
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codTlfCelular"), 'widget', array("attr" => array("class" => " col-md-4")));
        echo "
                 ";
        // line 247
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
                    ";
        // line 248
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 249
            echo "                    <span class=\"help-block \">
                        ";
            // line 250
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 253
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 256
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td colspan=\"3\">";
        // line 257
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 258
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 259
            echo "                    <span class=\"help-block \">
                        ";
            // line 260
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 263
        echo "             </td>
    </tr>
    ";
        // line 268
        echo "  </tbody>
</table>
   ";
        // line 270
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token"), 'widget');
        echo "
 <div class=\"block-separator col-md-12\"></div>
 ";
        // line 273
        echo " ";
        // line 274
        echo " <script type=\"text/javascript\" >
 \$(document).ready(function() {

     \$(\"#sunahip_centrohipicobundle_datacentrohipico_empresa\").removeAttr(\"multiple\");

    \$(\".estados\").change(function() {
        estado = \$(this).val();
        ";
        // line 282
        echo "        \$('.parroquia').val('');
          var Rmunicipio=Routing.generate('municipios', {estado_id: estado||0});
          getSelect(Rmunicipio,'.municipio',\"Municipio\");
          \$(\".municipio\").val('');

    });
    
    \$(\".municipio\").change(function() {
            municipio = \$(this).val();
            \$(\".parroquia\").val('');
            var Rparroquia=Routing.generate('parroquias', {municipio_id: municipio||0});
            getSelect(Rparroquia,'.parroquia',\"Parroquia\");
        });
    });

    \$('.estados').change();
    \$('.municipio').change();


    function sFormCH(){
        var datach=\$(\"#form_dch\").serialize();
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataCentrohipico:CHtable_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  651 => 282,  642 => 274,  640 => 273,  635 => 270,  631 => 268,  627 => 263,  621 => 260,  618 => 259,  616 => 258,  612 => 257,  608 => 256,  603 => 253,  597 => 250,  594 => 249,  592 => 248,  588 => 247,  584 => 246,  580 => 245,  577 => 244,  571 => 241,  568 => 240,  566 => 239,  562 => 238,  558 => 237,  554 => 236,  549 => 233,  543 => 230,  540 => 229,  538 => 228,  534 => 227,  530 => 226,  526 => 225,  523 => 224,  517 => 221,  514 => 220,  512 => 219,  508 => 218,  504 => 217,  498 => 213,  492 => 210,  489 => 209,  487 => 208,  483 => 207,  479 => 206,  475 => 204,  469 => 201,  466 => 200,  464 => 199,  460 => 198,  456 => 197,  449 => 192,  443 => 189,  440 => 188,  438 => 187,  434 => 186,  430 => 185,  425 => 182,  419 => 179,  416 => 178,  414 => 177,  410 => 176,  406 => 175,  400 => 171,  394 => 168,  391 => 167,  389 => 166,  385 => 165,  380 => 163,  377 => 162,  371 => 159,  368 => 158,  366 => 157,  362 => 156,  358 => 155,  352 => 151,  346 => 149,  343 => 148,  341 => 147,  337 => 146,  332 => 143,  326 => 140,  323 => 139,  321 => 138,  317 => 137,  313 => 136,  306 => 131,  300 => 129,  297 => 128,  295 => 127,  291 => 126,  285 => 122,  279 => 119,  276 => 118,  274 => 117,  270 => 116,  266 => 115,  259 => 110,  253 => 107,  250 => 106,  248 => 105,  244 => 104,  240 => 103,  235 => 101,  231 => 99,  225 => 96,  222 => 95,  220 => 94,  216 => 93,  212 => 92,  207 => 90,  200 => 85,  194 => 82,  191 => 81,  189 => 80,  185 => 79,  181 => 78,  177 => 76,  171 => 73,  168 => 72,  166 => 71,  162 => 70,  158 => 69,  153 => 66,  147 => 63,  144 => 62,  142 => 61,  138 => 60,  133 => 58,  128 => 55,  122 => 52,  119 => 51,  117 => 50,  113 => 49,  108 => 47,  103 => 44,  97 => 41,  94 => 40,  92 => 39,  88 => 38,  83 => 36,  77 => 32,  71 => 29,  68 => 28,  66 => 27,  62 => 26,  57 => 24,  54 => 23,  43 => 17,  40 => 16,  38 => 15,  34 => 14,  26 => 11,  24 => 10,  19 => 7,  111 => 52,  90 => 34,  85 => 33,  82 => 32,  70 => 25,  65 => 22,  58 => 18,  51 => 13,  49 => 20,  45 => 11,  37 => 5,  35 => 4,  32 => 3,  29 => 12,);
    }
}
