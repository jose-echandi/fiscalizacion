<?php

/* SolicitudesCitasBundle:DataSolicitudes:editCita.html.twig */
class __TwigTemplate_e34ccb94bb831fb389249c3fba5e41fdf52fa0e2011dc21060d84798b0dbb232 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SolicitudesCitasBundle::solicitud_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"contendor\">
        <div class=\"block-separator col-md-12\"></div>
\t<div class=\"tit_principal\">Modificar Fecha de Cita</div>
\t\t<br /><br />
                  <div class=\"col-md-12\">
                     <div class=\"col-md-4\"> 
                      <table id=\"tabla_reporte2\">
                          <tr id=\"table_header2\">
                              <td >Solicitud</td>
                          </tr>
                          <tr>
                              <td class=\"text-center\">
                                  ";
        // line 15
        if ($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "operadora")) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "operadora"), "denominacionComercial"), "html", null, true);
        }
        // line 16
        echo "                                  ";
        if ($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "centroHipico")) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "centroHipico"), "denominacionComercial"), "html", null, true);
        }
        // line 17
        echo "                              </td>
                          </tr>
                          <tr>
                              <td class=\"text-center\">
                                  ";
        // line 21
        if ($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "operadora")) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "operadora"), "persJuridica"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "operadora"), "rif"), "html", null, true);
            echo " ";
        }
        // line 22
        echo "                                  ";
        if ($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "centroHipico")) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "centroHipico"), "persJuridica"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "centroHipico"), "rif"), "html", null, true);
        }
        // line 23
        echo "                              </td>
                          </tr>
                      </table>
                     </div>  
                     <div class=\"block-separator col-md-12\"></div>
                     <div class=\"col-md-4\"> 
                      <table id=\"tabla_reporte2\">
                          <tr id=\"table_header2\">
                              <td >Su cita Actual</td>
                          </tr>
                          <tr>
                              <td class=\"text-center\">
                                  ";
        // line 35
        if ($this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "cita"), "fechaSolicitud")) {
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "cita"), "fechaSolicitud"), "d-m-Y"), "html", null, true);
        }
        // line 36
        echo "                                  <input type=\"hidden\" id=\"citaid\" name=\"citaid\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "cita"), "id"), "html", null, true);
        echo "\"/>
                                  <input type=\"hidden\" id=\"fecha_actual\" value=\"";
        // line 37
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "cita"), "fechaSolicitud"), "d/m/Y"), "html", null, true);
        echo "\" />                                         
                              </td>
                          </tr>
                      </table>
                     </div> 
                  </div>
                  ";
        // line 44
        echo "                  <div class=\"col-md-12\">
                     <h3>Cambiar D&iacute;a de Cita</h3>
                            <div class=\"col-md-2\"><label for=\"date\">Fecha:</label> <br/>
                              <input type=\"text\" id=\"date\" name=\"datacita\" placeholder=\"Fecha dd/mm/aaaa\" class=\"\" size=\"13px;\" readonly/> <div id=\"error\"></div>
                            </div>
                           <div class=\"col-md-4 alert-warning\" >
                               <p style=\"margin-top: 10px;\">Debe seleccionar alguno de los Dias Activos.</p>
                           </div>
                            <div class=\"col-md-4 \"><div id=\"datepick\"></div> </div>
                           <div class=\"block-separator col-md-12\"></div>                 
                           <div class=\"col-md-4 col-md-offset-8 text-right\">
                                   <a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"btn btn-warning btn-sm\">Cancelar</a>
                                   <a href=\"#\" class=\"btn btn-primary btn-sm\" onclick=\"ActualizaCita();\">Actualizar Cita</a> 
                           </div>
                  </div>
                  <div class=\"block-separator col-md-12\"></div>
    </div>
<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Notificación</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"col-md-12 alert text-left\" id=\"myMessage\">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 ";
    }

    // line 81
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 82
        echo "        ";
        $this->displayParentBlock("foot_script_assetic", $context, $blocks);
        echo "
<script src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/js/solfun.js"), "html", null, true);
        echo "\"></script>        
  <script type=\"text/javascript\">
        \$(document).ready(function() {
               ActiveCalendar();
           });
           
 function ActualizaCita(){
          var idsol='";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["datasolicitud"]) ? $context["datasolicitud"] : null), "id"), "html", null, true);
        echo "';
          var arrdate=\$( \"#date\" ).val().split(\"/\");
          var dia = parseInt(arrdate[0]) - 1;
          var datets = Date.parse(arrdate[2]+\"-\"+arrdate[1]+\"-\"+dia);
          if(\$( \"#date\" ).val() == \$( \"#fecha_actual\" ).val()) {
             \$(\"#myMessage\").html(\"La fecha de la cita es igual a la fecha previa solicitada. Seleccione otro día\");
             \$(\"#myModal\").modal(\"show\");
             return false;
          }
          
          if(isNaN(datets)) {
             //Dialog('<div class=\"col-md-12 has-error\"> Debe Seleccionar una Fecha para la Cita </div>','ERROR');
             \$(\"#myMessage\").html(\"Debe Seleccionar una Fecha para la Cita\");
             \$(\"#myModal\").modal(\"show\");
             return false;  
            }
            //Dialog('<div class=\"col-md-12 \"> Procesando Su cita Espere.. </div>','Procesando');
            \$(\"#myMessage\").html(\"Espere un momento mientras se envian los Datos\");
            \$(\"#myModal\").modal(\"show\");
            
           ///{id}/actualiza-cita/{fecha}
          var path=Routing.generate('solicitudes_updatecita',{id:idsol,fecha:datets},true);
          //alert(path);
          window.location.href=path;
          //\$(location).href(path);
    }
    
  
  </script>
 
 ";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:editCita.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 90,  162 => 83,  157 => 82,  154 => 81,  125 => 55,  112 => 44,  103 => 37,  98 => 36,  93 => 35,  79 => 23,  71 => 22,  63 => 21,  57 => 17,  51 => 16,  46 => 15,  32 => 3,  29 => 2,);
    }
}
