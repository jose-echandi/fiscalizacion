<?php

/* ExportBundle::export_pdf.html.twig */
class __TwigTemplate_7f35239380d6506dda0744df80cb3fcdef26144d3272bc8e4ac1eed8aec40908 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["num"] = array(0 => "cero", 1 => "uno", 2 => "dos", 3 => "tres", 4 => "cuatro", 5 => "cinco", 6 => "seis", 7 => "siete", 8 => "ocho", 9 => "nueve", 10 => "diez", 11 => "once", 12 => "doce", 13 => "trece", 14 => "catorce", 15 => "quince", 16 => "dieciseis", 17 => "diecisiete", 18 => "dieciocho", 19 => "diecinueve", 20 => "veinte", 21 => "veintiuno", 22 => "veintidos", 23 => "veintitres", 24 => "veinticuatro", 25 => "veinticinco", 26 => "veintiseis", 27 => "veintisiete", 28 => "veintiocho", 29 => "veintinueve", 30 => "treinta", 31 => "treintaiuno");
        // line 5
        echo "
";
        // line 6
        $context["mes"] = array(0 => "", 1 => "enero", 2 => "febrero", 3 => "marzo", 4 => "abril", 5 => "mayo", 6 => "junio", 7 => "julio", 8 => "agosto", 9 => "septiembre", 10 => "octubre", 11 => "noviembre", 12 => "diciembre");
        // line 7
        echo "<!DOCTYPE html>
<html> 
    <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
       <style type=\"text/css\">
       h1{text-align:center}
       .center{text-align:center}
       @page {
            header: html_myheader;
            footer: html_myfooter;
        }
        #content{padding:3ex}
       </style>
    </head>
<body>
<htmlpageheader name=\"myheader\">
  <img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/newtemplate/images/cintillo.png"), "html", null, true);
        echo "\" height=\"53\" width=\"1140\" alt=\"Gobierno Bolivariano\"> 
    <div id=\"header\" class=\"row-fluid\">
        Registro Nacional de <span>Licencias Hípicas</span>
    </div><br/>
</htmlpageheader>


<htmlpagefooter name=\"myfooter\">
<div class=\"center\">
Edificio Sede del Hipódromo la Rinconada – INH, piso 5. Parroquia Coche - Caracas, D.C. Teléfono (0212) 395-35-11 /
681-33-31. <a href=\"www.sunahip.gob.ve\">www.sunahip.gob.ve</a></div>
</htmlpagefooter>

<div id=\"content\">
";
        // line 37
        $this->displayBlock('content_content', $context, $blocks);
        // line 38
        echo "</div>
</body>
</html>";
    }

    // line 37
    public function block_content_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "ExportBundle::export_pdf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 37,  64 => 38,  62 => 37,  45 => 23,  27 => 7,  25 => 6,  22 => 5,  20 => 1,  147 => 35,  144 => 34,  139 => 18,  136 => 17,  129 => 39,  114 => 37,  112 => 34,  108 => 33,  104 => 32,  100 => 31,  94 => 30,  90 => 29,  86 => 28,  82 => 27,  79 => 26,  76 => 25,  59 => 24,  53 => 20,  51 => 17,  37 => 6,  33 => 4,  30 => 3,);
    }
}
