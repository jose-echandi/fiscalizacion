<?php

/* FiscalizacionBundle::modal.html.twig */
class __TwigTemplate_82ea7746cccc87b7c03c1809ac35aeb5898b6b2ac6672deaeb923516ed619562 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle::base.html.twig");

        $this->blocks = array(
            'body_start' => array($this, 'block_body_start'),
            'header' => array($this, 'block_header'),
            'page_header' => array($this, 'block_page_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'content_content' => array($this, 'block_content_content'),
            'footer' => array($this, 'block_footer'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_start($context, array $blocks = array())
    {
    }

    // line 4
    public function block_header($context, array $blocks = array())
    {
    }

    // line 5
    public function block_page_header($context, array $blocks = array())
    {
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/genstyles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"print\">

    ";
        // line 12
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "e1efe2f_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e1efe2f_0") : $this->env->getExtension('assets')->getAssetUrl("css/e1efe2f_build_standalone_1.css");
            // line 15
            echo "    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
    ";
        } else {
            // asset "e1efe2f"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e1efe2f") : $this->env->getExtension('assets')->getAssetUrl("css/e1efe2f.css");
            echo "    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\">
    ";
        }
        unset($context["asset_url"]);
        // line 17
        echo "    <style type=\"text/css\">body{background: #fff}
    .modal.modal-wide .modal-dialog {
  width: 90%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}
</style>
    ";
    }

    // line 27
    public function block_content($context, array $blocks = array())
    {
        // line 28
        echo "<div class=\"container\">
    <div clas=\"row\">
        ";
        // line 30
        $this->displayBlock('content_content', $context, $blocks);
        // line 31
        echo "    </div>
</div>        
";
    }

    // line 30
    public function block_content_content($context, array $blocks = array())
    {
    }

    // line 35
    public function block_footer($context, array $blocks = array())
    {
    }

    // line 38
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 39
        echo "
    ";
        // line 40
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "f06b435_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f06b435_0") : $this->env->getExtension('assets')->getAssetUrl("js/f06b435_bootstrap-datetimepicker_1.js");
            // line 43
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "f06b435"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f06b435") : $this->env->getExtension('assets')->getAssetUrl("js/f06b435.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        // line 45
        echo "
    ";
        // line 46
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "8eb30f8_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8eb30f8_0") : $this->env->getExtension('assets')->getAssetUrl("js/8eb30f8_bootstrap-datetimepicker.es_1.js");
            // line 49
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "8eb30f8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8eb30f8") : $this->env->getExtension('assets')->getAssetUrl("js/8eb30f8.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        // line 51
        echo "
    <script type=\"text/javascript\">
        \$(function(){
            \$('[data-provider=\"datepicker\"]').datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                minView: 'month',
                pickerPosition: 'bottom-left',
                todayBtn: true,
                startView: 'month',
                language: 'es'
            });

            \$('[data-provider=\"datetimepicker\"]').datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy hh:ii',
                language: 'fr',
                pickerPosition: 'bottom-left',
                todayBtn: true
            });

            \$('[data-provider=\"timepicker\"]').datetimepicker({
                autoclose: true,
                format: 'hh:ii',
                formatViewType: 'time',
                maxView: 'day',
                minView: 'hour',
                pickerPosition: 'bottom-left',
                startView: 'day'
            });

            // Restore value from hidden input
            \$('input[type=hidden]', '.date').each(function(){
                if(\$(this).val()) {
                    \$(this).parent().datetimepicker('setValue');
                }
            });

        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle::modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 51,  153 => 49,  149 => 46,  146 => 45,  132 => 43,  128 => 40,  125 => 39,  122 => 38,  117 => 35,  112 => 30,  106 => 31,  104 => 30,  100 => 28,  97 => 27,  85 => 17,  71 => 15,  67 => 12,  62 => 10,  58 => 9,  50 => 7,  45 => 5,  40 => 4,  82 => 33,  63 => 18,  60 => 17,  53 => 8,  43 => 6,  39 => 5,  35 => 3,  32 => 3,  29 => 2,);
    }
}
