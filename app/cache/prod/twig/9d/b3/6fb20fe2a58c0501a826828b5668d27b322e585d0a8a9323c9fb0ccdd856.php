<?php

/* ExportBundle::iconslink.html.twig */
class __TwigTemplate_9db36fb20fe2a58c0501a826828b5668d27b322e585d0a8a9323c9fb0ccdd856 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-md-2 col-md-offset-10\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath((isset($context["pdf"]) ? $context["pdf"] : null));
        echo "\" target='_blank'><i class=\"icon-pdf\"></i></a>&nbsp;<a href=\"";
        echo $this->env->getExtension('routing')->getPath((isset($context["xcel"]) ? $context["xcel"] : null));
        echo "\" target='_blank'><i class=\"icon-excel\"></i></a></div>
";
    }

    public function getTemplateName()
    {
        return "ExportBundle::iconslink.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
