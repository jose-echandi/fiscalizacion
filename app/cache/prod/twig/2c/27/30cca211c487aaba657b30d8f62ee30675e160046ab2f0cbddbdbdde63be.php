<?php

/* FiscalizacionBundle:Pagos:form.html.twig */
class __TwigTemplate_2c2730cca211c487aaba657b30d8f62ee30675e160046ab2f0cbddbdbdde63be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle::modal.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle::modal.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "<div>
    ";
        // line 4
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
        ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
        ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
        <div class=\"block-separator col-sm-12\"></div>
        <div class=\"col-md-12 form-group btn-group\">
             <div style=\"float: right\">
                 <button type=\"submit\" class=\"btn btn-primary btn-sm\">Pagar</button>
             </div>
        </div>
    ";
        // line 13
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
</div>
";
    }

    // line 17
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 18
        echo "    ";
        $this->displayParentBlock("foot_script_assetic", $context, $blocks);
        echo "
<script type=\"text/javascript\">
    \$(document).ready(function(){
        \$(\".numeric\").keydown(function(e) {
            if (\$.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }

            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        var Options={
            autoSize: true,
            dateFormat: 'dd/mm/yy',
            maxDate: '";
        // line 33
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
        echo "'
        };
        \$(\".datePago\").each(function(index,elem){
          \$(this).datepicker(Options); 
        });   
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Pagos:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 33,  63 => 18,  60 => 17,  53 => 13,  43 => 6,  39 => 5,  35 => 4,  32 => 3,  29 => 2,);
    }
}
