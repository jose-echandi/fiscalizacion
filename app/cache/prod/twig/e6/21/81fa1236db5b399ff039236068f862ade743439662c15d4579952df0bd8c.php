<?php

/* FiscalizacionBundle:Pagos:todos.html.twig */
class __TwigTemplate_e62181fa1236db5b399ff039236068f862ade743439662c15d4579952df0bd8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle:Pagos:index.html.twig");

        $this->blocks = array(
            'addHeader' => array($this, 'block_addHeader'),
            'addCol' => array($this, 'block_addCol'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle:Pagos:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_addHeader($context, array $blocks = array())
    {
        // line 3
        echo "    <th>F. de Pago</th>
    <th>&nbsp;</th>
";
    }

    // line 7
    public function block_addCol($context, array $blocks = array())
    {
        // line 8
        echo "    <td>
    ";
        // line 9
        if ((!(null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaDeposito")))) {
            // line 10
            echo "    ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaDeposito"), "d/m/Y"), "html", null, true);
            echo "
    ";
        } else {
            // line 12
            echo "        -
    ";
        }
        // line 14
        echo "    </td>
    <td>
    ";
        // line 16
        if ((!(null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getArchivoAdjunto")))) {
            // line 17
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getArchivoAdjunto"), "CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig"), "html", null, true);
            echo "
    ";
        }
        // line 19
        echo "    </td>
";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Pagos:todos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 19,  62 => 17,  60 => 16,  56 => 14,  52 => 12,  46 => 10,  44 => 9,  41 => 8,  38 => 7,  32 => 3,  29 => 2,);
    }
}
