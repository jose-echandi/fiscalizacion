<?php

/* LicenciaBundle:AdmClasfLicencias:show.html.twig */
class __TwigTemplate_04a59bd938da5a7f69f669296cc7a626789cf35daf42e49888aa2b2ee8b76de6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/genstyles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"print\">

";
    }

    // line 9
    public function block_content_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"tit_principal\">Clasificación de Licencia</div>

    <div class=\"col-md-12\">
        <table class=\"record_properties table table-condensed\">
            <thead>
                <tr>
                    <th colspan=\"2\" >Datos de la Licencia</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Clasificación de Licencia</th>
                    <td>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasfLicencia"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th>Tipo de Operadora</th>
                    <td>";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "admTiposLicencias"), "tipoLicencia"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th>Recaudos</th>
                    <td>
                        <ul>
                        ";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "admRecaudosLicencias"));
        foreach ($context['_seq'] as $context["_key"] => $context["recaudo"]) {
            // line 34
            echo "                            <li>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["recaudo"]) ? $context["recaudo"] : null), "recaudo"), "html", null, true);
            echo "</li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recaudo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>Juegos Explotados</th>
                    <td>
                        <ul>
                        ";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "admJuegosExplotados"));
        foreach ($context['_seq'] as $context["_key"] => $context["juego"]) {
            // line 44
            echo "                            <li>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["juego"]) ? $context["juego"] : null), "juego"), "html", null, true);
            echo "</li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['juego'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>Valor Solicitud (UT)</th>
                    <td>";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "solicitudUt"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th>Valor Otorgamiento (UT)</th>
                    <td>";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "otorgamientoUt"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th>Código Generación Licencia</th>
                    <td>";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codLicencia"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th>Requiere Operadora</th>
                    <td>";
        // line 63
        echo ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "hasOperadora") == 1)) ? ("Requiere") : ("No Requiere"));
        echo "</td>
                </tr>
                <tr>
                    <th>Requiere Hipódromo</th>
                    <td>";
        // line 67
        echo ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "hasHipodromo") == 1)) ? ("Requiere") : ("No Requiere"));
        echo "</td>
                </tr>
                <tr>
                    <th>Estatus</th>
                    <td>";
        // line 71
        echo ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == 1)) ? ("Activo") : ("Inactivo"));
        echo "</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class=\"col-md-6 col-md-offset-2 form-group btn-group\">
        <div class=\"col-md-6\" style=\"text-align:center\"><a href=\"";
        // line 77
        echo $this->env->getExtension('routing')->getPath("admclasflicencias");
        echo "\" class=\"btn btn-primary btn-sm \">Regresar</a></div>
        ";
        // line 80
        echo "        <div class=\"col-md-6\" style=\"text-align:center\"><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admclasflicencias_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
        echo "\" class=\"btn btn-success btn-sm \">Modificar</a></div>
    </div>

    <!-- Modal -->
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Eliminar el Registro</h4>
                </div>
                <div class=\"modal-body\">
                    Realmente desea eliminar el registro <b>\"";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasfLicencia"), "html", null, true);
        echo "\"</b>?
                </div>
                <div class=\"modal-footer\">
                    ";
        // line 95
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start');
        echo "
                    ";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : null), "submit"), 'widget');
        echo "
                    ";
        // line 97
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
        echo "
                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "LicenciaBundle:AdmClasfLicencias:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 97,  198 => 96,  194 => 95,  188 => 92,  172 => 80,  168 => 77,  159 => 71,  152 => 67,  145 => 63,  138 => 59,  131 => 55,  124 => 51,  117 => 46,  108 => 44,  104 => 43,  95 => 36,  86 => 34,  82 => 33,  73 => 27,  66 => 23,  51 => 10,  48 => 9,  41 => 5,  37 => 4,  32 => 3,  29 => 2,);
    }
}
