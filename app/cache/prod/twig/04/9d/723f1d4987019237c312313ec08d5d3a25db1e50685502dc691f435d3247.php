<?php

/* MopaBootstrapBundle:Form:fields.html.twig */
class __TwigTemplate_049d723f1d4987019237c312313ec08d5d3a25db1e50685502dc691f435d3247 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("form_div_layout.html.twig");

        $this->blocks = array(
            'button_attributes' => array($this, 'block_button_attributes'),
            'button_widget' => array($this, 'block_button_widget'),
            'button_row' => array($this, 'block_button_row'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'form_widget_compound' => array($this, 'block_form_widget_compound'),
            'form_tabs' => array($this, 'block_form_tabs'),
            'tabs_widget' => array($this, 'block_tabs_widget'),
            'form_tab' => array($this, 'block_form_tab'),
            'collection_widget' => array($this, 'block_collection_widget'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'datetime_widget' => array($this, 'block_datetime_widget'),
            'percent_widget' => array($this, 'block_percent_widget'),
            'money_widget' => array($this, 'block_money_widget'),
            'file_widget' => array($this, 'block_file_widget'),
            'form_legend' => array($this, 'block_form_legend'),
            'form_label' => array($this, 'block_form_label'),
            'help_label' => array($this, 'block_help_label'),
            'help_label_tooltip' => array($this, 'block_help_label_tooltip'),
            'help_label_popover' => array($this, 'block_help_label_popover'),
            'form_actions_widget' => array($this, 'block_form_actions_widget'),
            'form_actions_row' => array($this, 'block_form_actions_row'),
            'form_rows_visible' => array($this, 'block_form_rows_visible'),
            'form_row' => array($this, 'block_form_row'),
            'form_message' => array($this, 'block_form_message'),
            'form_help' => array($this, 'block_form_help'),
            'form_widget_add_btn' => array($this, 'block_form_widget_add_btn'),
            'form_widget_remove_btn' => array($this, 'block_form_widget_remove_btn'),
            'collection_button' => array($this, 'block_collection_button'),
            'label_asterisk' => array($this, 'block_label_asterisk'),
            'widget_addon' => array($this, 'block_widget_addon'),
            'form_errors' => array($this, 'block_form_errors'),
            'error_type' => array($this, 'block_error_type'),
            'widget_form_group_start' => array($this, 'block_widget_form_group_start'),
            'help_widget_popover' => array($this, 'block_help_widget_popover'),
            'widget_form_group_end' => array($this, 'block_widget_form_group_end'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_button_attributes($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => ("btn " . (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "")) : ("")))));
        // line 6
        echo "    ";
        $this->displayParentBlock("button_attributes", $context, $blocks);
        echo "
";
    }

    // line 9
    public function block_button_widget($context, array $blocks = array())
    {
        // line 10
        ob_start();
        // line 11
        echo "    ";
        if (twig_test_empty((isset($context["label"]) ? $context["label"] : null))) {
            // line 12
            echo "        ";
            $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : null));
            // line 13
            echo "    ";
        }
        // line 14
        echo "    <button type=\"";
        echo twig_escape_filter($this->env, ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : null), "button")) : ("button")), "html", null, true);
        echo "\" ";
        $this->displayBlock("button_attributes", $context, $blocks);
        echo ">
    ";
        // line 15
        if ((!twig_test_empty((isset($context["icon"]) ? $context["icon"] : null)))) {
            // line 16
            echo "            ";
            echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon((isset($context["icon"]) ? $context["icon"] : null), ((array_key_exists("icon_inverted", $context)) ? (_twig_default_filter((isset($context["icon_inverted"]) ? $context["icon_inverted"] : null), false)) : (false)));
            echo "
    ";
        }
        // line 18
        echo "    ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
        echo "</button>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 22
    public function block_button_row($context, array $blocks = array())
    {
        // line 23
        echo "    ";
        ob_start();
        // line 24
        echo "        ";
        if ((array_key_exists("button_offset", $context) && (!twig_test_empty((isset($context["button_offset"]) ? $context["button_offset"] : null))))) {
            // line 25
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("for" => (isset($context["id"]) ? $context["id"] : null), "class" => (isset($context["button_offset"]) ? $context["button_offset"] : null)));
            // line 26
            echo "            <div class=\"form-group\">
                <div ";
            // line 27
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : null), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : null), "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
                ";
            // line 28
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
            echo "
                </div>
            </div>
        ";
        } else {
            // line 32
            echo "            <div>
                ";
            // line 33
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
            echo "
            </div>
        ";
        }
        // line 36
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 41
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        // line 42
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => (((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "")) : ("")) . " ") . (isset($context["widget_form_control_class"]) ? $context["widget_form_control_class"] : null))));
        // line 43
        echo "    ";
        $this->displayParentBlock("choice_widget_collapsed", $context, $blocks);
        echo "
";
    }

    // line 46
    public function block_textarea_widget($context, array $blocks = array())
    {
        // line 47
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : null), "text")) : ("text"));
        // line 48
        echo "    ";
        if ((((isset($context["type"]) ? $context["type"] : null) != "hidden") && ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null), null)) : (null)))) || (!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), null)) : (null))))))) {
            // line 49
            echo "    <div class=\"input-group\">
        ";
            // line 50
            if ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null), null)) : (null))))) {
                // line 51
                echo "            ";
                $context["widget_addon"] = (isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null);
                // line 52
                echo "            ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 54
            echo "    ";
        }
        // line 55
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => (((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "")) : ("")) . " ") . (isset($context["widget_form_control_class"]) ? $context["widget_form_control_class"] : null))));
        // line 56
        echo "    ";
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        echo "
    ";
        // line 57
        if ((((isset($context["type"]) ? $context["type"] : null) != "hidden") && ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null), null)) : (null)))) || (!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), null)) : (null))))))) {
            // line 58
            echo "        ";
            if ((!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), null)) : (null))))) {
                // line 59
                echo "        ";
                $context["widget_addon"] = (isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null);
                // line 60
                echo "        ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 62
            echo "    </div>
    ";
        }
    }

    // line 66
    public function block_form_widget_simple($context, array $blocks = array())
    {
        // line 67
        ob_start();
        // line 68
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : null), "text")) : ("text"));
        // line 69
        echo "    ";
        if ((((isset($context["type"]) ? $context["type"] : null) != "hidden") && ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null), null)) : (null)))) || (!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), null)) : (null))))))) {
            // line 70
            echo "    <div class=\"input-group\">
        ";
            // line 71
            if ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null), null)) : (null))))) {
                // line 72
                echo "            ";
                $context["widget_addon"] = (isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null);
                // line 73
                echo "            ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 75
            echo "    ";
        }
        // line 76
        echo "    ";
        if ((!((array_key_exists("widget_remove_btn", $context)) ? (_twig_default_filter((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : null), null)) : (null)))) {
            // line 77
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "")) : ("")) . " not-removable")));
            // line 78
            echo "    ";
        }
        // line 79
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => (((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "")) : ("")) . " ") . (isset($context["widget_form_control_class"]) ? $context["widget_form_control_class"] : null))));
        // line 80
        echo "    ";
        if (((isset($context["static_text"]) ? $context["static_text"] : null) === true)) {
            // line 81
            echo "        <p class=\"form-control-static\">";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "</p>
    ";
        } else {
            // line 83
            echo "        ";
            $this->displayParentBlock("form_widget_simple", $context, $blocks);
            echo "
    ";
        }
        // line 85
        echo "    ";
        if ((((isset($context["type"]) ? $context["type"] : null) != "hidden") && ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null), null)) : (null)))) || (!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), null)) : (null))))))) {
            // line 86
            echo "        ";
            if ((!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), null)) : (null))))) {
                // line 87
                echo "        ";
                $context["widget_addon"] = (isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null);
                // line 88
                echo "        ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
        ";
            }
            // line 90
            echo "    </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 95
    public function block_form_widget_compound($context, array $blocks = array())
    {
        // line 96
        ob_start();
        // line 97
        echo "    ";
        if (($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") == null)) {
            // line 98
            echo "        ";
            if ((isset($context["render_fieldset"]) ? $context["render_fieldset"] : null)) {
                echo "<fieldset>";
            }
            // line 99
            echo "        ";
            if ((isset($context["show_legend"]) ? $context["show_legend"] : null)) {
                $this->displayBlock("form_legend", $context, $blocks);
            }
            // line 100
            echo "    ";
        }
        // line 101
        echo "
    ";
        // line 102
        if ($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "tabbed")) {
            // line 103
            echo "        ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'tabs');
            echo "
        <div class=\"tab-content\">
    ";
        }
        // line 106
        echo "
    ";
        // line 107
        $this->displayBlock("form_rows_visible", $context, $blocks);
        echo "

    ";
        // line 109
        if ($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "tabbed")) {
            // line 110
            echo "        </div>
    ";
        }
        // line 112
        echo "
    ";
        // line 113
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "

    ";
        // line 115
        if (($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") == null)) {
            // line 116
            echo "        ";
            if ((isset($context["render_fieldset"]) ? $context["render_fieldset"] : null)) {
                echo "</fieldset>";
            }
            // line 117
            echo "    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 121
    public function block_form_tabs($context, array $blocks = array())
    {
        // line 122
        if ($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "tabsView", array(), "any", true, true)) {
            // line 123
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "tabsView"), 'widget');
            echo "
";
        }
    }

    // line 127
    public function block_tabs_widget($context, array $blocks = array())
    {
        // line 128
        ob_start();
        // line 129
        echo "<ul class=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "attr"), "class"), "html", null, true);
        echo "\">
    ";
        // line 130
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "tabs"));
        foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
            // line 131
            echo "        <li";
            if ($this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "active")) {
                echo " class=\"active\"";
            }
            echo ">
            <a data-toggle=\"tab\" href=\"#";
            // line 132
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "id"), "html", null, true);
            echo "\">
                ";
            // line 133
            if ($this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "icon")) {
                echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "icon"));
            }
            // line 134
            echo "                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "label"), array(), $this->getAttribute((isset($context["tab"]) ? $context["tab"] : null), "translation_domain")), "html", null, true);
            echo "
            </a>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 138
        echo "</ul>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 142
    public function block_form_tab($context, array $blocks = array())
    {
        // line 143
        echo "    ";
        $context["tab_attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => ((("tab-pane" . (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "tab_active")) ? (" active") : (""))) . " ") . (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "")) : (""))), "id" => (isset($context["id"]) ? $context["id"] : null)));
        // line 144
        echo "    <div";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tab_attr"]) ? $context["tab_attr"] : null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : null), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : null), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
        ";
        // line 145
        $this->displayBlock("form_widget", $context, $blocks);
        echo "
    </div>
";
    }

    // line 149
    public function block_collection_widget($context, array $blocks = array())
    {
        // line 150
        ob_start();
        // line 151
        echo "    ";
        if (array_key_exists("prototype", $context)) {
            // line 152
            echo "        ";
            $context["prototype_markup"] = $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["prototype"]) ? $context["prototype"] : null), 'row');
            // line 153
            echo "        ";
            $context["data_prototype_name"] = (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "name", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "name"), "__name__")) : ("__name__"));
            // line 154
            echo "        ";
            $context["data_prototype_label"] = (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "label", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "form", array(), "any", false, true), "vars", array(), "any", false, true), "prototype", array(), "any", false, true), "vars", array(), "any", false, true), "label"), "__name__label__")) : ("__name__label__"));
            // line 155
            echo "        ";
            $context["widget_form_group_attr"] = twig_array_merge(twig_array_merge((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), array("data-prototype" => (isset($context["prototype_markup"]) ? $context["prototype_markup"] : null), "data-prototype-name" => (isset($context["data_prototype_name"]) ? $context["data_prototype_name"] : null), "data-prototype-label" => (isset($context["data_prototype_label"]) ? $context["data_prototype_label"] : null))), (isset($context["attr"]) ? $context["attr"] : null));
            // line 160
            echo "    ";
        }
        // line 161
        echo "    ";
        // line 162
        echo "\t";
        if ((twig_in_filter("collection", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "block_prefixes")) && $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true))) {
            // line 163
            echo "\t\t";
            $context["widget_form_group_attr"] = twig_array_merge((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), array("class" => (((($this->getAttribute((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), "class"), "row")) : ("row")) . " ") . $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"))));
            // line 164
            echo "\t";
        }
        // line 165
        echo "    ";
        // line 166
        echo "    ";
        $context["widget_form_group_attr"] = twig_array_merge((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), array("id" => (("collection" . (isset($context["id"]) ? $context["id"] : null)) . "_form_group"), "class" => ((($this->getAttribute((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), "class") . " collection-items ") . (isset($context["id"]) ? $context["id"] : null)) . "_form_group")));
        // line 167
        echo "
    <div ";
        // line 168
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : null), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : null), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
    ";
        // line 170
        echo "    ";
        if (((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "value")) == 0) && array_key_exists("prototype", $context))) {
            // line 171
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["prototype_names"]) ? $context["prototype_names"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["name"]) {
                // line 172
                echo "            ";
                echo strtr((isset($context["prototype_markup"]) ? $context["prototype_markup"] : null), array("__name__" => (isset($context["name"]) ? $context["name"] : null)));
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['name'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 174
            echo "    ";
        }
        // line 175
        echo "    ";
        $this->displayBlock("form_widget", $context, $blocks);
        echo "
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 180
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        // line 181
        ob_start();
        // line 182
        echo "    ";
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : null), array("class" => (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class"), "")) : (""))));
        // line 183
        echo "    ";
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : null), array("class" => (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class") . " ") . ((((isset($context["widget_type"]) ? $context["widget_type"] : null) != "")) ? ((((((isset($context["multiple"]) ? $context["multiple"] : null)) ? ("checkbox") : ("radio")) . "-") . (isset($context["widget_type"]) ? $context["widget_type"] : null))) : ("")))));
        // line 184
        echo "    ";
        if ((isset($context["expanded"]) ? $context["expanded"] : null)) {
            // line 185
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), (isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null))) : ((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null)))));
            // line 186
            echo "    ";
        }
        // line 187
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 188
            echo "        ";
            if (((isset($context["widget_type"]) ? $context["widget_type"] : null) != "inline")) {
                // line 189
                echo "        <div class=\"";
                echo (((isset($context["multiple"]) ? $context["multiple"] : null)) ? ("checkbox") : ("radio"));
                echo "\">
        ";
            }
            // line 191
            echo "            <label";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : null), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : null), "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
                ";
            // line 192
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["child"]) ? $context["child"] : null), 'widget', array("horizontal_label_class" => (isset($context["horizontal_label_class"]) ? $context["horizontal_label_class"] : null), "horizontal_input_wrapper_class" => (isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "attr" => array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class"), "")) : ("")))));
            echo "
                ";
            // line 193
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getAttribute((isset($context["child"]) ? $context["child"] : null), "vars"), "label"), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
            echo "
            </label>
        ";
            // line 195
            if (((isset($context["widget_type"]) ? $context["widget_type"] : null) != "inline")) {
                // line 196
                echo "        </div>
        ";
            }
            // line 198
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 202
    public function block_checkbox_widget($context, array $blocks = array())
    {
        // line 203
        ob_start();
        // line 204
        if (((!((isset($context["label"]) ? $context["label"] : null) === false)) && twig_test_empty((isset($context["label"]) ? $context["label"] : null)))) {
            // line 205
            echo "    ";
            $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : null));
        }
        // line 207
        if ((($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"), "vars"), "block_prefixes")))) {
            // line 208
            echo "    <div class=\"checkbox\">
";
        }
        // line 210
        echo "
";
        // line 211
        if (((($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"), "vars"), "block_prefixes"))) && (isset($context["label_render"]) ? $context["label_render"] : null))) {
            // line 212
            echo "    <label ";
            if ((!(isset($context["horizontal"]) ? $context["horizontal"] : null))) {
                echo "class=\"checkbox-inline\"";
            }
            echo ">
";
        }
        // line 214
        echo "        <input type=\"checkbox\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "\"";
        }
        if ((isset($context["checked"]) ? $context["checked"] : null)) {
            echo " checked=\"checked\"";
        }
        echo "/>
";
        // line 215
        if ((($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"), "vars"), "block_prefixes")))) {
            // line 216
            echo "    ";
            if ((isset($context["label_render"]) ? $context["label_render"] : null)) {
                // line 217
                echo "        ";
                if (twig_in_filter((isset($context["widget_checkbox_label"]) ? $context["widget_checkbox_label"] : null), array(0 => "both", 1 => "widget"))) {
                    // line 218
                    echo "            ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
                    echo "
        ";
                }
                // line 220
                echo "    </label>
    ";
            }
        }
        // line 223
        echo "
";
        // line 224
        if ((($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") != null) && !twig_in_filter("choice", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"), "vars"), "block_prefixes")))) {
            // line 225
            echo "    </div>
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 230
    public function block_date_widget($context, array $blocks = array())
    {
        // line 231
        ob_start();
        // line 232
        if (((isset($context["widget"]) ? $context["widget"] : null) == "single_text")) {
            // line 233
            echo "    ";
            if (array_key_exists("datepicker", $context)) {
                // line 234
                echo "        ";
                $context["widget_addon_icon"] = (($this->getAttribute((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), "icon", array(), "any", true, true)) ? ($this->getAttribute((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), "icon")) : ("calendar"));
                // line 235
                echo "        <div data-provider=\"datepicker\" class=\"input-group date\" data-date=\"";
                echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
                echo "\" data-link-field=\"";
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
                echo "\" data-link-format=\"yyyy-mm-dd\">
            <input ";
                // line 236
                if ((!((isset($context["widget_form_control_class"]) ? $context["widget_form_control_class"] : null) === false))) {
                    echo "class=\"";
                    echo twig_escape_filter($this->env, (isset($context["widget_form_control_class"]) ? $context["widget_form_control_class"] : null), "html", null, true);
                    echo "\" ";
                }
                echo "type=\"text\" ";
                if ((isset($context["read_only"]) ? $context["read_only"] : null)) {
                    echo " readonly=\"readonly\"";
                }
                if ((isset($context["disabled"]) ? $context["disabled"] : null)) {
                    echo " disabled=\"disabled\"";
                }
                if ((isset($context["required"]) ? $context["required"] : null)) {
                    echo " required=\"required\"";
                }
                if (twig_in_filter("placeholder", twig_get_array_keys_filter((isset($context["attr"]) ? $context["attr"] : null)))) {
                    echo " placeholder=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "placeholder", array(), "array"), "html", null, true);
                    echo "\"";
                }
                echo ">
            <input type=\"hidden\" value=\"";
                // line 237
                echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
                echo "\" ";
                $this->displayBlock("widget_attributes", $context, $blocks);
                echo ">
            <span class=\"input-group-addon\">";
                // line 238
                echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon((isset($context["widget_addon_icon"]) ? $context["widget_addon_icon"] : null));
                echo "</span>
        </div>
    ";
            } else {
                // line 241
                echo "        ";
                $this->displayBlock("form_widget_simple", $context, $blocks);
                echo "
    ";
            }
        } else {
            // line 244
            echo "    ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "inline")) : ("inline"))));
            // line 245
            echo "    \t<div class=\"row\">
        ";
            // line 246
            echo strtr((isset($context["date_pattern"]) ? $context["date_pattern"] : null), array("{{ year }}" =>             // line 247
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "year"), 'widget', array("attr" => array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class"), "")) : ("")) . "")), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "col-sm-3")) : ("col-sm-3")))), "{{ month }}" =>             // line 248
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "month"), 'widget', array("attr" => array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class"), "")) : ("")) . "")), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "col-sm-3")) : ("col-sm-3")))), "{{ day }}" =>             // line 249
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "day"), 'widget', array("attr" => array("class" => ((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class"), "")) : ("")) . "")), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "col-sm-3")) : ("col-sm-3"))))));
            // line 250
            echo "
        </div>
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 256
    public function block_time_widget($context, array $blocks = array())
    {
        // line 257
        ob_start();
        // line 258
        if (((isset($context["widget"]) ? $context["widget"] : null) == "single_text")) {
            // line 259
            echo "    ";
            if (array_key_exists("timepicker", $context)) {
                // line 260
                echo "        ";
                $context["widget_addon_icon"] = (($this->getAttribute((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), "icon", array(), "any", true, true)) ? ($this->getAttribute((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), "icon")) : ("time"));
                // line 261
                echo "        <div data-provider=\"timepicker\" class=\"input-group date\" data-date=\"";
                echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
                echo "\" data-link-field=\"";
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
                echo "\" data-link-format=\"hh:ii\">
            <input class=\"form-control\" type=\"text\" ";
                // line 262
                if ((isset($context["read_only"]) ? $context["read_only"] : null)) {
                    echo " readonly=\"readonly\"";
                }
                if ((isset($context["disabled"]) ? $context["disabled"] : null)) {
                    echo " disabled=\"disabled\"";
                }
                if ((isset($context["required"]) ? $context["required"] : null)) {
                    echo " required=\"required\"";
                }
                if (twig_in_filter("placeholder", twig_get_array_keys_filter((isset($context["attr"]) ? $context["attr"] : null)))) {
                    echo " placeholder=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "placeholder", array(), "array"), "html", null, true);
                    echo "\"";
                }
                echo ">
            <input type=\"hidden\" value=\"";
                // line 263
                echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
                echo "\" ";
                $this->displayBlock("widget_attributes", $context, $blocks);
                echo ">
            <span class=\"input-group-addon\">";
                // line 264
                echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon((isset($context["widget_addon_icon"]) ? $context["widget_addon_icon"] : null));
                echo "</span>
        </div>
    ";
            } else {
                // line 267
                echo "        ";
                $this->displayBlock("form_widget_simple", $context, $blocks);
                echo "
    ";
            }
        } else {
            // line 270
            echo "    ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "")) : (""))));
            // line 271
            echo "    ";
            ob_start();
            // line 272
            echo "    ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hour"), 'widget', array("attr" => array("size" => "1"), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "col-sm-2")) : ("col-sm-2"))));
            echo "
    ";
            // line 273
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "minute"), 'widget', array("attr" => array("size" => "1"), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "col-sm-2")) : ("col-sm-2"))));
            echo "
    ";
            // line 274
            if ((isset($context["with_seconds"]) ? $context["with_seconds"] : null)) {
                // line 275
                echo "        :";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "second"), 'widget', array("attr" => array("size" => "1"), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "col-sm-2")) : ("col-sm-2"))));
                echo "
    ";
            }
            // line 277
            echo "    ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 282
    public function block_datetime_widget($context, array $blocks = array())
    {
        // line 283
        ob_start();
        // line 284
        echo "    ";
        if (((isset($context["widget"]) ? $context["widget"] : null) == "single_text")) {
            // line 285
            echo "        ";
            if (array_key_exists("datetimepicker", $context)) {
                // line 286
                echo "            ";
                $context["widget_addon_icon"] = (($this->getAttribute((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), "icon", array(), "any", true, true)) ? ($this->getAttribute((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), "icon")) : ("th"));
                // line 287
                echo "            <div data-provider=\"datetimepicker\" class=\"input-group date\" data-date=\"";
                if ((isset($context["value"]) ? $context["value"] : null)) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "Y-m-d H:i"), "html", null, true);
                }
                echo "\" data-link-field=\"";
                echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
                echo "\" data-link-format=\"yyyy-mm-dd hh:ii\">
                <input class=\"form-control\" type=\"text\" ";
                // line 288
                if ((isset($context["read_only"]) ? $context["read_only"] : null)) {
                    echo " readonly=\"readonly\"";
                }
                if ((isset($context["disabled"]) ? $context["disabled"] : null)) {
                    echo " disabled=\"disabled\"";
                }
                if ((isset($context["required"]) ? $context["required"] : null)) {
                    echo " required=\"required\"";
                }
                if (twig_in_filter("placeholder", twig_get_array_keys_filter((isset($context["attr"]) ? $context["attr"] : null)))) {
                    echo " placeholder=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "placeholder", array(), "array"), "html", null, true);
                    echo "\"";
                }
                echo ">
                <input type=\"hidden\" value=\"";
                // line 289
                if ((isset($context["value"]) ? $context["value"] : null)) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "Y-m-d H:i"), "html", null, true);
                }
                echo "\" ";
                $this->displayBlock("widget_attributes", $context, $blocks);
                echo ">
                <span class=\"input-group-addon\">";
                // line 290
                echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon((isset($context["widget_addon_icon"]) ? $context["widget_addon_icon"] : null));
                echo "</span>
            </div>
        ";
            } else {
                // line 293
                echo "            ";
                $this->displayBlock("form_widget_simple", $context, $blocks);
                echo "
        ";
            }
            // line 295
            echo "    ";
        } else {
            // line 296
            echo "            ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "")) : (""))));
            // line 297
            echo "            <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
                ";
            // line 298
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "date"), 'errors');
            echo "
                ";
            // line 299
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "time"), 'errors');
            echo "
                ";
            // line 300
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "date"), 'widget', array("attr" => array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class"), "")) : (""))), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "col-sm-3")) : ("col-sm-3"))));
            echo "
                ";
            // line 301
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "time"), 'widget', array("attr" => array("class" => (($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "widget_class"), "")) : (""))), "horizontal_input_wrapper_class" => ((array_key_exists("horizontal_input_wrapper_class", $context)) ? (_twig_default_filter((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "col-sm-2")) : ("col-sm-2"))));
            echo "
            </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 307
    public function block_percent_widget($context, array $blocks = array())
    {
        // line 308
        ob_start();
        // line 309
        echo "    ";
        $context["widget_addon_append"] = twig_array_merge((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), array("text" => (($this->getAttribute((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), "text", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), "text"), "%")) : ("%"))));
        // line 310
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 314
    public function block_money_widget($context, array $blocks = array())
    {
        // line 315
        ob_start();
        // line 316
        echo "    ";
        $context["widget_addon_prepend"] = ((((((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null) != false) || ((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null) == null)) && ((isset($context["money_pattern"]) ? $context["money_pattern"] : null) != "{{ widget }}"))) ? (array("text" => strtr((isset($context["money_pattern"]) ? $context["money_pattern"] : null), array("{{ widget }}" => "")))) : (((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null), null)) : (null))));
        // line 317
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 321
    public function block_file_widget($context, array $blocks = array())
    {
        // line 322
        ob_start();
        // line 323
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : null), "file")) : ("file"));
        // line 324
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => (((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class"), "")) : ("")) . " ") . (isset($context["widget_form_control_class"]) ? $context["widget_form_control_class"] : null))));
        // line 325
        echo "    ";
        if ((!(null === ((array_key_exists("widget_addon_prepend", $context)) ? (_twig_default_filter((isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null), null)) : (null))))) {
            // line 326
            echo "        ";
            $context["widget_addon"] = (isset($context["widget_addon_prepend"]) ? $context["widget_addon_prepend"] : null);
            // line 327
            echo "        ";
            $this->displayBlock("widget_addon", $context, $blocks);
            echo "
    ";
        }
        // line 329
        echo "<input type=\"";
        echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
        echo "\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo "/>
";
        // line 330
        if ((((isset($context["type"]) ? $context["type"] : null) != "hidden") && (!(null === (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "type"), null)) : (null)))))) {
            // line 331
            echo "    ";
            if ((!(null === ((array_key_exists("widget_addon_append", $context)) ? (_twig_default_filter((isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null), null)) : (null))))) {
                // line 332
                echo "        ";
                $context["widget_addon"] = (isset($context["widget_addon_append"]) ? $context["widget_addon_append"] : null);
                // line 333
                echo "        ";
                $this->displayBlock("widget_addon", $context, $blocks);
                echo "
    ";
            }
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 341
    public function block_form_legend($context, array $blocks = array())
    {
        // line 342
        ob_start();
        // line 343
        echo "    ";
        if (twig_test_empty((isset($context["label"]) ? $context["label"] : null))) {
            // line 344
            echo "        ";
            $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : null));
            // line 345
            echo "    ";
        }
        // line 346
        echo "    <";
        echo twig_escape_filter($this->env, (isset($context["legend_tag"]) ? $context["legend_tag"] : null), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
        echo "</";
        echo twig_escape_filter($this->env, (isset($context["legend_tag"]) ? $context["legend_tag"] : null), "html", null, true);
        echo ">
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 350
    public function block_form_label($context, array $blocks = array())
    {
        // line 351
        if ((!twig_in_filter("checkbox", (isset($context["block_prefixes"]) ? $context["block_prefixes"] : null)) || twig_in_filter((isset($context["widget_checkbox_label"]) ? $context["widget_checkbox_label"] : null), array(0 => "label", 1 => "both")))) {
            // line 352
            ob_start();
            // line 353
            echo "    ";
            if ((!((isset($context["label"]) ? $context["label"] : null) === false))) {
                // line 354
                echo "        ";
                if (twig_test_empty((isset($context["label"]) ? $context["label"] : null))) {
                    // line 355
                    echo "            ";
                    $context["label"] = $this->env->getExtension('form')->humanize((isset($context["name"]) ? $context["name"] : null));
                    // line 356
                    echo "        ";
                }
                // line 357
                echo "        ";
                if ((!(isset($context["compound"]) ? $context["compound"] : null))) {
                    // line 358
                    echo "            ";
                    $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : null), array("for" => (isset($context["id"]) ? $context["id"] : null)));
                    // line 359
                    echo "        ";
                }
                // line 360
                echo "        ";
                $context["label_attr_class"] = "";
                // line 361
                echo "        ";
                if ((isset($context["horizontal"]) ? $context["horizontal"] : null)) {
                    // line 362
                    echo "            ";
                    $context["label_attr_class"] = (("control-label " . (isset($context["label_attr_class"]) ? $context["label_attr_class"] : null)) . (isset($context["horizontal_label_class"]) ? $context["horizontal_label_class"] : null));
                    // line 363
                    echo "        ";
                }
                // line 364
                echo "        ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : null), array("class" => ((((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class"), "")) : ("")) . " ") . (isset($context["label_attr_class"]) ? $context["label_attr_class"] : null)) . (((isset($context["required"]) ? $context["required"] : null)) ? (" required") : (" optional")))));
                // line 365
                echo "        <label";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : null));
                foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                    echo " ";
                    echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : null), "html", null, true);
                    echo "=\"";
                    echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : null), "html", null, true);
                    echo "\"";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo ">
        ";
                // line 366
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
                // line 367
                $this->displayBlock("label_asterisk", $context, $blocks);
                echo "
        ";
                // line 368
                if (((twig_in_filter("collection", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "block_prefixes")) && ((array_key_exists("widget_add_btn", $context)) ? (_twig_default_filter((isset($context["widget_add_btn"]) ? $context["widget_add_btn"] : null), null)) : (null))) && ($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "allow_add") == true))) {
                    // line 369
                    echo "            ";
                    $this->displayBlock("form_widget_add_btn", $context, $blocks);
                    echo "
        ";
                }
                // line 371
                echo "        ";
                if ((isset($context["help_label"]) ? $context["help_label"] : null)) {
                    // line 372
                    echo "            ";
                    $this->displayBlock("help_label", $context, $blocks);
                    echo "
        ";
                }
                // line 374
                echo "        ";
                if ($this->getAttribute((isset($context["help_label_tooltip"]) ? $context["help_label_tooltip"] : null), "title")) {
                    // line 375
                    echo "            ";
                    $this->displayBlock("help_label_tooltip", $context, $blocks);
                    echo "
        ";
                }
                // line 377
                echo "        ";
                if ($this->getAttribute((isset($context["help_label_popover"]) ? $context["help_label_popover"] : null), "title")) {
                    // line 378
                    echo "            ";
                    $this->displayBlock("help_label_popover", $context, $blocks);
                    echo "
        ";
                }
                // line 380
                echo "        </label>
    ";
            }
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        }
    }

    // line 386
    public function block_help_label($context, array $blocks = array())
    {
        // line 387
        echo "    <span class=\"help-block\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["help_label"]) ? $context["help_label"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
        echo "</span>
";
    }

    // line 390
    public function block_help_label_tooltip($context, array $blocks = array())
    {
        // line 391
        echo "    <span class=\"help-block\">
        <a href=\"#\" data-toggle=\"tooltip\" data-placement=\"";
        // line 392
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["help_label_tooltip"]) ? $context["help_label_tooltip"] : null), "placement"), "html", null, true);
        echo "\" data-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["help_label_tooltip"]) ? $context["help_label_tooltip"] : null), "title"), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
        echo "\">
            ";
        // line 393
        if ((!($this->getAttribute((isset($context["help_label_tooltip"]) ? $context["help_label_tooltip"] : null), "icon") === false))) {
            // line 394
            echo "                ";
            echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getAttribute((isset($context["help_label_tooltip"]) ? $context["help_label_tooltip"] : null), "icon"));
            echo "
            ";
        }
        // line 396
        echo "            ";
        if ((!($this->getAttribute((isset($context["help_label_tooltip"]) ? $context["help_label_tooltip"] : null), "text") === null))) {
            // line 397
            echo "            ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["help_label_tooltip"]) ? $context["help_label_tooltip"] : null), "text"), "html", null, true);
            echo "
            ";
        }
        // line 399
        echo "        </a>
    </span>
";
    }

    // line 403
    public function block_help_label_popover($context, array $blocks = array())
    {
        // line 404
        echo "    <span class=\"help-block\">
        <a href=\"#\" data-toggle=\"popover\" data-trigger=\"hover\" data-placement=\"";
        // line 405
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["help_label_popover"]) ? $context["help_label_popover"] : null), "placement"), "html", null, true);
        echo "\" data-title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["help_label_popover"]) ? $context["help_label_popover"] : null), "title"), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
        echo "\" data-content=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["help_label_popover"]) ? $context["help_label_popover"] : null), "content"), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
        echo "\" >
            ";
        // line 406
        if ((!($this->getAttribute((isset($context["help_label_popover"]) ? $context["help_label_popover"] : null), "icon") === false))) {
            // line 407
            echo "                ";
            echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getAttribute((isset($context["help_label_popover"]) ? $context["help_label_popover"] : null), "icon"));
            echo "
            ";
        }
        // line 409
        echo "            ";
        if ((!($this->getAttribute((isset($context["help_label_popover"]) ? $context["help_label_popover"] : null), "text") === null))) {
            // line 410
            echo "            ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["help_label_popover"]) ? $context["help_label_popover"] : null), "text"), "html", null, true);
            echo "
            ";
        }
        // line 412
        echo "        </a>
    </span>
";
    }

    // line 416
    public function block_form_actions_widget($context, array $blocks = array())
    {
        // line 417
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "children"));
        foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
            // line 418
            echo "        ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["button"]) ? $context["button"] : null), 'widget');
            echo "&nbsp; ";
            // line 419
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 423
    public function block_form_actions_row($context, array $blocks = array())
    {
        // line 424
        echo "    ";
        $this->displayBlock("button_row", $context, $blocks);
        echo "
";
    }

    // line 427
    public function block_form_rows_visible($context, array $blocks = array())
    {
        // line 428
        ob_start();
        // line 429
        echo "    ";
        if ($this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors')) {
            // line 430
            echo "        <div class=\"symfony-form-errors\">
            ";
            // line 431
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
            echo "
        </div>
    ";
        }
        // line 434
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 435
            echo "        ";
            if (!twig_in_filter("hidden", $this->getAttribute($this->getAttribute((isset($context["child"]) ? $context["child"] : null), "vars"), "block_prefixes"))) {
                echo " ";
                // line 436
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["child"]) ? $context["child"] : null), 'row');
                echo "
        ";
            }
            // line 438
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 442
    public function block_form_row($context, array $blocks = array())
    {
        // line 443
        ob_start();
        // line 444
        echo "    ";
        if (twig_in_filter("tab", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "block_prefixes"))) {
            // line 445
            echo "        ";
            $this->displayBlock("form_tab", $context, $blocks);
            echo "
    ";
        } else {
            // line 447
            echo "        ";
            $this->displayBlock("widget_form_group_start", $context, $blocks);
            echo "

        ";
            // line 449
            $context["show_horizontal_wrapper"] = ((isset($context["horizontal"]) ? $context["horizontal"] : null) && (!((!(null === $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"))) && twig_in_filter("collection", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"), "vars"), "block_prefixes")))));
            // line 450
            echo "
        ";
            // line 451
            if (((isset($context["horizontal"]) ? $context["horizontal"] : null) && (!(isset($context["label_render"]) ? $context["label_render"] : null)))) {
                // line 452
                echo "            ";
                $context["horizontal_input_wrapper_class"] = (((isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null) . " ") . (isset($context["horizontal_label_offset_class"]) ? $context["horizontal_label_offset_class"] : null));
                // line 453
                echo "        ";
            }
            // line 454
            echo "
        ";
            // line 455
            if ((isset($context["show_horizontal_wrapper"]) ? $context["show_horizontal_wrapper"] : null)) {
                // line 456
                echo "        <div class=\"";
                echo twig_escape_filter($this->env, (isset($context["horizontal_input_wrapper_class"]) ? $context["horizontal_input_wrapper_class"] : null), "html", null, true);
                echo "\">
        ";
            }
            // line 458
            echo "
        ";
            // line 459
            echo $this->env->getExtension('translator')->trans((isset($context["widget_prefix"]) ? $context["widget_prefix"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null));
            echo " ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget', $context);
            echo " ";
            echo $this->env->getExtension('translator')->trans((isset($context["widget_suffix"]) ? $context["widget_suffix"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null));
            echo "

        ";
            // line 461
            $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : null), "text")) : ("text"));
            // line 462
            echo "        ";
            if (((isset($context["type"]) ? $context["type"] : null) != "hidden")) {
                // line 463
                echo "        ";
                $this->displayBlock("form_message", $context, $blocks);
                echo "
        ";
            }
            // line 465
            echo "
        ";
            // line 466
            if ((isset($context["show_horizontal_wrapper"]) ? $context["show_horizontal_wrapper"] : null)) {
                // line 467
                echo "        </div>
        ";
            }
            // line 469
            echo "
        ";
            // line 470
            if (((((!(null === $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"))) && twig_in_filter("collection", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"), "vars"), "block_prefixes"))) && ((array_key_exists("widget_remove_btn", $context)) ? (_twig_default_filter((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : null), null)) : (null))) && (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent", array(), "any", false, true), "vars", array(), "any", false, true), "allow_delete", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent", array(), "any", false, true), "vars", array(), "any", false, true), "allow_delete"), false)) : (false)))) {
                // line 471
                echo "            ";
                $this->displayBlock("form_widget_remove_btn", $context, $blocks);
                echo "
        ";
            }
            // line 473
            $this->displayBlock("widget_form_group_end", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 480
    public function block_form_message($context, array $blocks = array())
    {
        // line 481
        ob_start();
        // line 482
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
    ";
        // line 483
        $this->displayBlock("form_help", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 489
    public function block_form_help($context, array $blocks = array())
    {
        // line 490
        ob_start();
        // line 491
        if ((isset($context["help_block"]) ? $context["help_block"] : null)) {
            echo "<p class=\"help-block\">";
            echo $this->env->getExtension('translator')->trans((isset($context["help_block"]) ? $context["help_block"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null));
            echo "</p>";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 495
    public function block_form_widget_add_btn($context, array $blocks = array())
    {
        // line 496
        ob_start();
        // line 497
        echo "    ";
        if (((array_key_exists("widget_add_btn", $context)) ? (_twig_default_filter((isset($context["widget_add_btn"]) ? $context["widget_add_btn"] : null), null)) : (null))) {
            // line 498
            echo "        ";
            $context["button_type"] = "add";
            // line 499
            echo "        ";
            $context["button_values"] = (isset($context["widget_add_btn"]) ? $context["widget_add_btn"] : null);
            // line 500
            echo "        ";
            $this->displayBlock("collection_button", $context, $blocks);
            echo "
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 505
    public function block_form_widget_remove_btn($context, array $blocks = array())
    {
        // line 506
        ob_start();
        // line 507
        echo "    ";
        if (($this->getAttribute((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : null), "wrapper_div", array(), "any", true, true) && (!($this->getAttribute((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : null), "wrapper_div") === false)))) {
            // line 508
            echo "        <div class=\"form-group\">
            <div class=\"";
            // line 509
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : null), "wrapper_div"), "class"), "html", null, true);
            echo " col-sm-offset-3\">
    ";
        }
        // line 511
        echo "    ";
        if (((array_key_exists("widget_remove_btn", $context)) ? (_twig_default_filter((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : null), null)) : (null))) {
            // line 512
            echo "    ";
            $context["button_type"] = "remove";
            // line 513
            echo "    ";
            $context["button_values"] = (isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : null);
            // line 514
            echo "    ";
            $this->displayBlock("collection_button", $context, $blocks);
            echo "
    ";
        }
        // line 516
        echo "    ";
        if (($this->getAttribute((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : null), "wrapper_div", array(), "any", true, true) && (!($this->getAttribute((isset($context["widget_remove_btn"]) ? $context["widget_remove_btn"] : null), "wrapper_div") === false)))) {
            // line 517
            echo "            </div>
        </div>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 523
    public function block_collection_button($context, array $blocks = array())
    {
        // line 524
        echo "<a ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : null), "attr"));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : null), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : null), "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " data-collection-";
        echo twig_escape_filter($this->env, (isset($context["button_type"]) ? $context["button_type"] : null), "html", null, true);
        echo "-btn=\".";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "id", array(), "array"), "html", null, true);
        echo "_form_group\">
";
        // line 525
        if ((!(null === $this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : null), "icon")))) {
            // line 526
            echo "    ";
            echo $this->env->getExtension('mopa_bootstrap_icon')->renderIcon($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : null), "icon"), (($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : null), "icon_inverted", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : null), "icon_inverted"), false)) : (false)));
            echo "
";
        }
        // line 528
        if ($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : null), "label", array(), "any", true, true)) {
            // line 529
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["button_values"]) ? $context["button_values"] : null), "label"), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
            echo "
";
        }
        // line 531
        echo "</a>

";
    }

    // line 535
    public function block_label_asterisk($context, array $blocks = array())
    {
        // line 536
        if ((isset($context["required"]) ? $context["required"] : null)) {
            // line 537
            if ((isset($context["render_required_asterisk"]) ? $context["render_required_asterisk"] : null)) {
                echo "&nbsp;<span class=\"asterisk\">*</span>";
            }
        } else {
            // line 539
            if ((isset($context["render_optional_text"]) ? $context["render_optional_text"] : null)) {
                echo "&nbsp;<span>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("(optional)", array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
                echo "</span>";
            }
        }
    }

    // line 543
    public function block_widget_addon($context, array $blocks = array())
    {
        // line 544
        ob_start();
        // line 545
        $context["widget_addon_icon"] = (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "icon", array(), "any", true, true)) ? ($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "icon")) : (null));
        // line 546
        $context["widget_addon_icon_inverted"] = (($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "icon_inverted", array(), "any", true, true)) ? ($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "icon_inverted")) : (false));
        // line 547
        echo "    <span class=\"input-group-addon\">";
        echo (((($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "text", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "text"), false)) : (false))) ? ($this->env->getExtension('translator')->trans($this->getAttribute((isset($context["widget_addon"]) ? $context["widget_addon"] : null), "text"), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null))) : ($this->env->getExtension('mopa_bootstrap_icon')->renderIcon((isset($context["widget_addon_icon"]) ? $context["widget_addon_icon"] : null), (isset($context["widget_addon_icon_inverted"]) ? $context["widget_addon_icon_inverted"] : null))));
        echo "</span>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 553
    public function block_form_errors($context, array $blocks = array())
    {
        // line 554
        ob_start();
        // line 555
        if ((isset($context["error_delay"]) ? $context["error_delay"] : null)) {
            // line 556
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 557
                echo "        ";
                if (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index") == 1)) {
                    // line 558
                    echo "            ";
                    if ($this->getAttribute((isset($context["child"]) ? $context["child"] : null), "set", array(0 => "errors", 1 => (isset($context["errors"]) ? $context["errors"] : null)), "method")) {
                    }
                    // line 559
                    echo "        ";
                }
                // line 560
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 562
            echo "    ";
            if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null)) > 0)) {
                // line 563
                echo "        ";
                if (($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") == null)) {
                    // line 564
                    echo "            ";
                    $context["__internal_038ae30009db939d8410582f4dc13eebfccc040e8806df2ff5f5bd2700a6c450"] = $this->env->loadTemplate("MopaBootstrapBundle::flash.html.twig");
                    // line 565
                    echo "            ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                        // line 566
                        echo "                ";
                        echo $context["__internal_038ae30009db939d8410582f4dc13eebfccc040e8806df2ff5f5bd2700a6c450"]->getflash("danger", (((null === $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messagePluralization"))) ? ($this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageTemplate"), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageParameters"), "validators")) : ($this->env->getExtension('translator')->transchoice($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageTemplate"), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messagePluralization"), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageParameters"), "validators"))));
                        // line 571
                        echo "
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 573
                    echo "        ";
                } else {
                    // line 574
                    echo "            <span class=\"help-";
                    $this->displayBlock("error_type", $context, $blocks);
                    echo "\">
            ";
                    // line 575
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                        // line 576
                        echo "                ";
                        echo twig_escape_filter($this->env, (((null === $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messagePluralization"))) ? ($this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageTemplate"), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageParameters"), "validators")) : ($this->env->getExtension('translator')->transchoice($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageTemplate"), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messagePluralization"), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageParameters"), "validators"))), "html", null, true);
                        // line 580
                        echo " <br>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 582
                    echo "            </span>
        ";
                }
                // line 584
                echo "    ";
            }
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 591
    public function block_error_type($context, array $blocks = array())
    {
        // line 592
        ob_start();
        // line 593
        if ((isset($context["error_type"]) ? $context["error_type"] : null)) {
            // line 594
            echo "    ";
            echo twig_escape_filter($this->env, (isset($context["error_type"]) ? $context["error_type"] : null), "html", null, true);
            echo "
";
        } elseif (($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") == null)) {
            // line 596
            echo "    ";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "error_type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array(), "any", false, true), "error_type"), "inline")) : ("inline")), "html", null, true);
            echo "
";
        } else {
            // line 598
            echo "    block
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 605
    public function block_widget_form_group_start($context, array $blocks = array())
    {
        // line 606
        if ((((array_key_exists("widget_form_group", $context)) ? (_twig_default_filter((isset($context["widget_form_group"]) ? $context["widget_form_group"] : null), false)) : (false)) || ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") == null))) {
            // line 607
            echo "    ";
            if (((!(null === $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"))) && twig_in_filter("collection", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent"), "vars"), "block_prefixes")))) {
                echo " ";
                // line 608
                echo "        ";
                if ((!(isset($context["omit_collection_item"]) ? $context["omit_collection_item"] : null))) {
                    // line 609
                    echo "            ";
                    // line 610
                    echo "        \t";
                    $context["widget_form_group_attr"] = twig_array_merge((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), array("class" => "collection-item"));
                    // line 611
                    echo "        ";
                }
                // line 612
                echo "    ";
            }
            // line 613
            echo "    ";
            if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null)) > 0)) {
                // line 614
                echo "\t    ";
                // line 615
                echo "\t    ";
                $context["widget_form_group_attr"] = twig_array_merge((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), array("class" => ((($this->getAttribute((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null), "class"), "")) : ("")) . " has-error")));
                // line 616
                echo "    ";
            }
            // line 617
            echo "    ";
            if (($this->getAttribute((isset($context["help_widget_popover"]) ? $context["help_widget_popover"] : null), "selector") === null)) {
                // line 618
                echo "        ";
                $context["help_widget_popover"] = twig_array_merge((isset($context["help_widget_popover"]) ? $context["help_widget_popover"] : null), array("selector" => ("#" . (isset($context["id"]) ? $context["id"] : null))));
                // line 619
                echo "    ";
            }
            // line 620
            echo "    <div";
            if ((!($this->getAttribute((isset($context["help_widget_popover"]) ? $context["help_widget_popover"] : null), "title") === null))) {
                $this->displayBlock("help_widget_popover", $context, $blocks);
            }
            echo " ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["widget_form_group_attr"]) ? $context["widget_form_group_attr"] : null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : null), "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, (isset($context["attrvalue"]) ? $context["attrvalue"] : null), "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">
    ";
            // line 622
            echo "    ";
            if (((((twig_length_filter($this->env, (isset($context["form"]) ? $context["form"] : null)) > 0) && ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") != null)) && !twig_in_filter("field", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "block_prefixes"))) && !twig_in_filter("date", $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars"), "block_prefixes")))) {
                // line 625
                echo "        ";
                if ((isset($context["show_child_legend"]) ? $context["show_child_legend"] : null)) {
                    // line 626
                    echo "            ";
                    $this->displayBlock("form_legend", $context, $blocks);
                    echo "
        ";
                } elseif ((isset($context["label_render"]) ? $context["label_render"] : null)) {
                    // line 628
                    echo "            ";
                    echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter((isset($context["label"]) ? $context["label"] : null), null)) : (null))) ? array() : array("label" => $_label_)));
                    echo "
        ";
                } else {
                    // line 630
                    echo "        ";
                }
                // line 631
                echo "    ";
            } else {
                // line 632
                echo "        ";
                if ((isset($context["label_render"]) ? $context["label_render"] : null)) {
                    // line 633
                    echo "            ";
                    echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter((isset($context["label"]) ? $context["label"] : null), null)) : (null))) ? array() : array("label" => $_label_)));
                    echo "
        ";
                }
                // line 635
                echo "    ";
            }
        } else {
            // line 637
            echo "    ";
            if ((isset($context["label_render"]) ? $context["label_render"] : null)) {
                // line 638
                echo "        ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter((isset($context["label"]) ? $context["label"] : null), null)) : (null))) ? array() : array("label" => $_label_)));
                echo "
    ";
            }
        }
    }

    // line 643
    public function block_help_widget_popover($context, array $blocks = array())
    {
        // line 644
        echo " ";
        ob_start();
        // line 645
        echo " ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["help_widget_popover"]) ? $context["help_widget_popover"] : null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 646
            echo " data-";
            echo twig_escape_filter($this->env, (isset($context["attrname"]) ? $context["attrname"] : null), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["attrvalue"]) ? $context["attrvalue"] : null), array(), ((array_key_exists("domain", $context)) ? (_twig_default_filter((isset($context["domain"]) ? $context["domain"] : null), "messages")) : ("messages"))), "html", null, true);
            echo "\"
 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 648
        echo " ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 651
    public function block_widget_form_group_end($context, array $blocks = array())
    {
        // line 652
        ob_start();
        // line 653
        if ((((array_key_exists("widget_form_group", $context)) ? (_twig_default_filter((isset($context["widget_form_group"]) ? $context["widget_form_group"] : null), false)) : (false)) || ($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parent") == null))) {
            // line 654
            echo "    </div>
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "MopaBootstrapBundle:Form:fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1876 => 654,  1874 => 653,  1872 => 652,  1869 => 651,  1864 => 648,  1853 => 646,  1848 => 645,  1845 => 644,  1842 => 643,  1833 => 638,  1830 => 637,  1826 => 635,  1820 => 633,  1817 => 632,  1814 => 631,  1811 => 630,  1805 => 628,  1799 => 626,  1796 => 625,  1793 => 622,  1773 => 620,  1770 => 619,  1767 => 618,  1764 => 617,  1761 => 616,  1758 => 615,  1756 => 614,  1753 => 613,  1750 => 612,  1747 => 611,  1744 => 610,  1742 => 609,  1739 => 608,  1735 => 607,  1733 => 606,  1730 => 605,  1723 => 598,  1717 => 596,  1711 => 594,  1709 => 593,  1707 => 592,  1704 => 591,  1697 => 584,  1693 => 582,  1686 => 580,  1683 => 576,  1679 => 575,  1674 => 574,  1671 => 573,  1664 => 571,  1661 => 566,  1656 => 565,  1653 => 564,  1650 => 563,  1647 => 562,  1632 => 560,  1629 => 559,  1625 => 558,  1622 => 557,  1604 => 556,  1602 => 555,  1600 => 554,  1597 => 553,  1589 => 547,  1587 => 546,  1585 => 545,  1583 => 544,  1580 => 543,  1571 => 539,  1566 => 537,  1564 => 536,  1561 => 535,  1555 => 531,  1549 => 529,  1547 => 528,  1541 => 526,  1539 => 525,  1519 => 524,  1516 => 523,  1508 => 517,  1505 => 516,  1499 => 514,  1496 => 513,  1493 => 512,  1490 => 511,  1485 => 509,  1482 => 508,  1479 => 507,  1477 => 506,  1474 => 505,  1465 => 500,  1462 => 499,  1459 => 498,  1456 => 497,  1454 => 496,  1451 => 495,  1442 => 491,  1440 => 490,  1437 => 489,  1430 => 483,  1425 => 482,  1423 => 481,  1420 => 480,  1412 => 473,  1406 => 471,  1404 => 470,  1401 => 469,  1397 => 467,  1395 => 466,  1392 => 465,  1386 => 463,  1383 => 462,  1381 => 461,  1372 => 459,  1369 => 458,  1363 => 456,  1361 => 455,  1358 => 454,  1355 => 453,  1352 => 452,  1350 => 451,  1347 => 450,  1345 => 449,  1339 => 447,  1333 => 445,  1330 => 444,  1328 => 443,  1325 => 442,  1316 => 438,  1310 => 436,  1306 => 435,  1301 => 434,  1295 => 431,  1292 => 430,  1289 => 429,  1287 => 428,  1284 => 427,  1277 => 424,  1274 => 423,  1266 => 419,  1262 => 418,  1257 => 417,  1254 => 416,  1248 => 412,  1242 => 410,  1239 => 409,  1233 => 407,  1231 => 406,  1223 => 405,  1220 => 404,  1217 => 403,  1211 => 399,  1205 => 397,  1202 => 396,  1196 => 394,  1194 => 393,  1188 => 392,  1185 => 391,  1182 => 390,  1175 => 387,  1172 => 386,  1164 => 380,  1158 => 378,  1155 => 377,  1149 => 375,  1146 => 374,  1140 => 372,  1137 => 371,  1131 => 369,  1129 => 368,  1125 => 367,  1123 => 366,  1107 => 365,  1104 => 364,  1101 => 363,  1098 => 362,  1095 => 361,  1092 => 360,  1089 => 359,  1086 => 358,  1083 => 357,  1080 => 356,  1077 => 355,  1074 => 354,  1071 => 353,  1069 => 352,  1067 => 351,  1064 => 350,  1052 => 346,  1049 => 345,  1046 => 344,  1043 => 343,  1041 => 342,  1038 => 341,  1028 => 333,  1025 => 332,  1022 => 331,  1020 => 330,  1013 => 329,  1007 => 327,  1004 => 326,  1001 => 325,  999 => 324,  997 => 323,  995 => 322,  992 => 321,  984 => 317,  981 => 316,  979 => 315,  976 => 314,  968 => 310,  965 => 309,  963 => 308,  960 => 307,  951 => 301,  947 => 300,  943 => 299,  939 => 298,  934 => 297,  931 => 296,  928 => 295,  922 => 293,  916 => 290,  908 => 289,  891 => 288,  882 => 287,  879 => 286,  876 => 285,  873 => 284,  871 => 283,  868 => 282,  861 => 277,  855 => 275,  853 => 274,  849 => 273,  844 => 272,  841 => 271,  838 => 270,  831 => 267,  825 => 264,  819 => 263,  802 => 262,  795 => 261,  792 => 260,  789 => 259,  787 => 258,  785 => 257,  782 => 256,  774 => 250,  772 => 249,  771 => 248,  770 => 247,  769 => 246,  766 => 245,  763 => 244,  756 => 241,  750 => 238,  744 => 237,  721 => 236,  714 => 235,  711 => 234,  708 => 233,  706 => 232,  704 => 231,  701 => 230,  694 => 225,  692 => 224,  689 => 223,  684 => 220,  678 => 218,  675 => 217,  672 => 216,  670 => 215,  657 => 214,  649 => 212,  647 => 211,  644 => 210,  640 => 208,  638 => 207,  634 => 205,  632 => 204,  630 => 203,  627 => 202,  618 => 198,  614 => 196,  612 => 195,  607 => 193,  603 => 192,  587 => 191,  581 => 189,  578 => 188,  573 => 187,  570 => 186,  567 => 185,  564 => 184,  561 => 183,  558 => 182,  556 => 181,  553 => 180,  544 => 175,  541 => 174,  532 => 172,  527 => 171,  524 => 170,  509 => 168,  506 => 167,  503 => 166,  501 => 165,  498 => 164,  495 => 163,  492 => 162,  490 => 161,  487 => 160,  484 => 155,  481 => 154,  478 => 153,  475 => 152,  472 => 151,  470 => 150,  467 => 149,  460 => 145,  444 => 144,  441 => 143,  438 => 142,  432 => 138,  421 => 134,  417 => 133,  413 => 132,  406 => 131,  402 => 130,  397 => 129,  395 => 128,  392 => 127,  385 => 123,  383 => 122,  380 => 121,  374 => 117,  369 => 116,  367 => 115,  362 => 113,  359 => 112,  355 => 110,  353 => 109,  348 => 107,  345 => 106,  338 => 103,  336 => 102,  333 => 101,  330 => 100,  325 => 99,  320 => 98,  317 => 97,  315 => 96,  312 => 95,  305 => 90,  299 => 88,  296 => 87,  293 => 86,  290 => 85,  284 => 83,  278 => 81,  275 => 80,  272 => 79,  269 => 78,  266 => 77,  263 => 76,  260 => 75,  254 => 73,  251 => 72,  249 => 71,  246 => 70,  243 => 69,  240 => 68,  238 => 67,  235 => 66,  229 => 62,  223 => 60,  220 => 59,  217 => 58,  215 => 57,  204 => 54,  198 => 52,  195 => 51,  193 => 50,  190 => 49,  187 => 48,  184 => 47,  181 => 46,  174 => 43,  171 => 42,  168 => 41,  163 => 36,  157 => 33,  154 => 32,  147 => 28,  132 => 27,  129 => 26,  126 => 25,  120 => 23,  117 => 22,  109 => 18,  103 => 16,  101 => 15,  94 => 14,  91 => 13,  88 => 12,  83 => 10,  70 => 5,  54 => 12,  48 => 10,  42 => 8,  37 => 6,  34 => 5,  26 => 2,  20 => 1,  39 => 7,  216 => 119,  210 => 56,  207 => 55,  176 => 89,  170 => 86,  134 => 52,  128 => 50,  125 => 49,  123 => 24,  119 => 47,  111 => 41,  105 => 38,  102 => 37,  100 => 36,  96 => 35,  93 => 34,  85 => 11,  80 => 9,  73 => 6,  67 => 4,  62 => 19,  57 => 13,  55 => 17,  49 => 15,  45 => 14,  32 => 4,  29 => 2,);
    }
}
