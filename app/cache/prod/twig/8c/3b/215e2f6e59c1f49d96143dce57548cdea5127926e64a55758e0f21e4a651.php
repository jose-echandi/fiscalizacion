<?php

/* CentrohipicoBundle:DataEmpresa:formLegal.html.twig */
class __TwigTemplate_8c3b215e2f6e59c1f49d96143dce57548cdea5127926e64a55758e0f21e4a651 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group\">
    <div class=\"col-md-12\">
        ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formLegal"]) ? $context["formLegal"] : null), 'errors');
        echo "
    </div>
</div>
<table id=\"tabla_reporte2\" class=\"tabla_reporte2\">
<tbody>
";
        // line 9
        echo "    ";
        // line 10
        echo "    ";
        // line 11
        echo "        ";
        // line 12
        echo "            ";
        // line 13
        echo "                ";
        // line 14
        echo "            ";
        // line 15
        echo "        ";
        // line 16
        echo "
    ";
        // line 19
        echo "<tr id=\"table_header2\">
    <td colspan=\"4\">Datos del apoderado o representante legal</td>
</tr>
<tr>
    <td>";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "nombre"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
    <td>";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "nombre"), 'widget', array("attr" => array("class" => " ")));
        echo "
        ";
        // line 25
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "nombre", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 26
            echo "            <span class=\"help-block \">
                                ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "nombre"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
        ";
        }
        // line 30
        echo "
    </td>
    <td>";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "apellido"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
    <td>";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "apellido"), 'widget', array("attr" => array("class" => " ")));
        echo "
        ";
        // line 34
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "apellido", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 35
            echo "            <span class=\"help-block \">
                                ";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "apellido"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
        ";
        }
        // line 39
        echo "
    </td>

</tr>
<tr>
    <td>";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ci"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>
        ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "tipoci"), 'widget', array("attr" => array("class" => " input-mini")));
        echo "
        - ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ci"), 'widget', array("attr" => array("class" => " ", "style" => "width: 110px;")));
        echo "
        ";
        // line 48
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ci", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 49
            echo "            <span class=\"help-block \">
                        ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ci"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 53
        echo "
    </td>
    <td>";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "rif"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>
        ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "persJuridica"), 'widget', array("attr" => array("class" => "input-mini ")));
        echo "
        - ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "rif"), 'widget', array("attr" => array("class" => " ", "style" => "width: 110px;")));
        echo "
        ";
        // line 59
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "rif", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 60
            echo "            <span class=\"help-block \">
                        ";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "rif"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 64
        echo "
    </td>

</tr>
<tr>
    <td>";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "estado"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "estado"), 'widget', array("attr" => array("class" => "dlestado2", "style" => "width:160px !important")));
        echo "
        ";
        // line 71
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "estado", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 72
            echo "            <span class=\"help-block \">
                        ";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "estado"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 76
        echo "
    </td>
    <td>";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "municipio"), 'label', array("label_attr" => array("class" => " col-md-12 text-left control-label")));
        echo "</td>
    <td>";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "municipio"), 'widget', array("attr" => array("class" => "dlmunicipio2 ", "disabled" => "disabled", "style" => "width:160px !important")));
        echo "
        ";
        // line 80
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "municipio", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 81
            echo "            <span class=\"help-block \">
                        ";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "municipio"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 85
        echo "    </td>

</tr>
<tr>
    <td>";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ciudad"), 'label', array("label_attr" => array("class" => " col-md-12 text-left control-label")));
        echo "</td>
    <td>";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ciudad"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 91
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ciudad", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 92
            echo "            <span class=\"help-block \">
                        ";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ciudad"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 96
        echo "    </td>
    <td>";
        // line 97
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "parroquia"), 'label', array("label_attr" => array("class" => " col-md-12 text-left control-label")));
        echo "</td>
    <td>";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "parroquia"), 'widget', array("attr" => array("class" => "dlparroquia2", "disabled" => "disabled", "style" => "width:160px !important")));
        echo "
        ";
        // line 99
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "parroquia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 100
            echo "            <span class=\"help-block \">
                        ";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "parroquia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 104
        echo "    </td>
</tr>
<tr>
    <td>";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "urbanSector"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 108
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "urbanSector"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 109
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "urbanSector", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 110
            echo "            <span class=\"help-block \">
                        ";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "urbanSector"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 114
        echo "    </td>
    <td>";
        // line 115
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "avCalleCarrera"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>
        ";
        // line 117
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "avCalleCarrera"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 118
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "avCalleCarrera", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 119
            echo "            <span class=\"help-block \">
                        ";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "avCalleCarrera"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 123
        echo "
    </td>
</tr>
<tr>
    <td>";
        // line 127
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "edifCasa"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "edifCasa"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 129
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "edifCasa", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 130
            echo "            <span class=\"help-block \">
                        ";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "edifCasa"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 134
        echo "    </td>
    <td>";
        // line 135
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ofcAptoNum"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ofcAptoNum"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 137
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ofcAptoNum", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 138
            echo "            <span class=\"help-block \">
                        ";
            // line 139
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "ofcAptoNum"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 142
        echo "

    </td>
    ";
        // line 146
        echo "</tr>
<tr>
    <td>";
        // line 148
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "puntoReferencia"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "puntoReferencia"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 150
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "puntoReferencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 151
            echo "            <span class=\"help-block \">
                        ";
            // line 152
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "puntoReferencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 155
        echo "
    </td>

    <td>";
        // line 158
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "codigoPostal"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 159
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "codigoPostal"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
        ";
        // line 160
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "codigoPostal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 161
            echo "            <span class=\"help-block \">
                        ";
            // line 162
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "codigoPostal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 165
        echo "
    </td>
</tr>
<tr>
    <td>";
        // line 169
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "email"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "email"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 171
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "email", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 172
            echo "            <span class=\"help-block \">
                        ";
            // line 173
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "email"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 176
        echo "    </td>
    <td>";
        // line 177
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "fax"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 178
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "codFax"), 'widget', array("attr" => array("class" => " col-md-4")));
        echo "
        ";
        // line 179
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "fax"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
        ";
        // line 180
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "fax", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 181
            echo "            <span class=\"help-block \">
                        ";
            // line 182
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "fax"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 185
        echo "    </td>
</tr>
<tr>
    <td>";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "tflFijo"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 189
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "codTlfFijo"), 'widget', array("attr" => array("class" => " col-md-4")));
        echo "
        ";
        // line 190
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "tflFijo"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
        ";
        // line 191
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "tflFijo", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 192
            echo "            <span class=\"help-block \">
                        ";
            // line 193
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "tflFijo"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 196
        echo "    </td>
    <td>";
        // line 197
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "tflCelular"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 198
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "codTlfCelular"), 'widget', array("attr" => array("class" => " col-md-4 ")));
        echo "
        ";
        // line 199
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "tflCelular"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
        ";
        // line 200
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "tflCelular", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 201
            echo "            <span class=\"help-block \">
                        ";
            // line 202
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "tflCelular"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 205
        echo "    </td>
</tr>
<tr>
    <td>";
        // line 208
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "pagWeb"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td colspan=\"3\">";
        // line 209
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "pagWeb"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 210
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "pagWeb", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 211
            echo "            <span class=\"help-block \">
                        ";
            // line 212
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "pagWeb"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 215
        echo "
    </td>
</tr>
";
        // line 232
        echo "</tbody>
</table>
<div class=\"block-separator col-md-12\"></div>
";
        // line 235
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formLegal"]) ? $context["formLegal"] : null), "_token"), 'widget');
        echo "
<script type=\"text/javascript\" >
    \$(document).ready(function(){
        \$(\".form-horizontal\").removeClass('form-horizontal');
        \$(\".form-control\").removeClass('form-control');
        \$(\".dlestado2\").change(function() {
            estado = \$('.dlestado2 option:selected').val();
            var Rmunicipio=Routing.generate('municipios', {estado_id: estado});
            getSelect(Rmunicipio,'.dlmunicipio2',\"Municipio\");
            \$(\".dlmunicipio2\").removeAttr('disabled');
            //var Rciudad=Routing.generate('ciudades', {estado_id: estado, municipio_id: null});
            //console.log(Rciudad);
            //getSelect(Rciudad,'#dlciudad',\"Ciudad\");
        });
        \$(\".dlmunicipio2\").change(function() {
            estado = \$('.dlestado2 option:selected').val();
            municipio = \$('.dlmunicipio2 option:selected').val();
            var Rparroquia=Routing.generate('parroquias', {municipio_id: municipio});
            \$(\".dlparroquia2\").removeAttr('disabled');
            getSelect(Rparroquia,'.dlparroquia2',\"Parroquia\");
        });
    });

</script>";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataEmpresa:formLegal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  536 => 235,  531 => 232,  526 => 215,  520 => 212,  517 => 211,  515 => 210,  511 => 209,  507 => 208,  502 => 205,  496 => 202,  493 => 201,  491 => 200,  487 => 199,  483 => 198,  476 => 196,  470 => 193,  467 => 192,  461 => 190,  457 => 189,  453 => 188,  448 => 185,  442 => 182,  439 => 181,  437 => 180,  433 => 179,  429 => 178,  425 => 177,  422 => 176,  416 => 173,  413 => 172,  411 => 171,  407 => 170,  403 => 169,  397 => 165,  391 => 162,  388 => 161,  386 => 160,  373 => 155,  364 => 151,  362 => 150,  354 => 148,  350 => 146,  345 => 142,  339 => 139,  334 => 137,  330 => 136,  326 => 135,  317 => 131,  314 => 130,  312 => 129,  298 => 123,  292 => 120,  289 => 119,  283 => 117,  275 => 114,  266 => 110,  264 => 109,  256 => 107,  251 => 104,  245 => 101,  242 => 100,  240 => 99,  236 => 98,  232 => 97,  223 => 93,  218 => 91,  210 => 89,  204 => 85,  198 => 82,  195 => 81,  193 => 80,  189 => 79,  172 => 72,  170 => 71,  162 => 69,  155 => 64,  149 => 61,  146 => 60,  140 => 58,  136 => 57,  131 => 55,  127 => 53,  121 => 50,  116 => 48,  112 => 47,  108 => 46,  96 => 39,  90 => 36,  87 => 35,  85 => 34,  81 => 33,  77 => 32,  73 => 30,  67 => 27,  64 => 26,  58 => 24,  54 => 23,  48 => 19,  43 => 15,  41 => 14,  37 => 12,  494 => 214,  492 => 213,  490 => 212,  488 => 211,  486 => 210,  484 => 209,  479 => 197,  475 => 204,  471 => 199,  465 => 191,  462 => 195,  460 => 194,  456 => 193,  452 => 192,  447 => 189,  441 => 186,  438 => 185,  436 => 184,  432 => 183,  428 => 182,  424 => 181,  421 => 180,  415 => 177,  412 => 176,  410 => 175,  406 => 174,  402 => 173,  398 => 172,  393 => 169,  387 => 166,  384 => 165,  382 => 159,  378 => 158,  374 => 162,  370 => 161,  367 => 152,  361 => 157,  358 => 149,  356 => 155,  352 => 154,  348 => 153,  342 => 149,  336 => 138,  333 => 145,  331 => 144,  327 => 143,  323 => 134,  319 => 140,  313 => 137,  310 => 136,  308 => 128,  304 => 127,  300 => 133,  293 => 128,  287 => 118,  284 => 124,  282 => 123,  278 => 115,  274 => 121,  269 => 111,  263 => 115,  260 => 108,  258 => 113,  254 => 112,  250 => 111,  244 => 107,  238 => 104,  235 => 103,  233 => 102,  229 => 96,  224 => 99,  220 => 92,  214 => 90,  211 => 93,  209 => 92,  205 => 91,  201 => 90,  196 => 87,  190 => 84,  187 => 83,  185 => 78,  181 => 76,  175 => 73,  169 => 74,  166 => 70,  164 => 72,  160 => 71,  156 => 70,  150 => 66,  144 => 59,  141 => 62,  139 => 61,  135 => 60,  129 => 56,  120 => 52,  118 => 49,  114 => 50,  110 => 49,  97 => 41,  94 => 40,  92 => 39,  88 => 38,  84 => 37,  78 => 34,  71 => 29,  65 => 26,  62 => 25,  60 => 24,  51 => 21,  39 => 13,  35 => 11,  33 => 10,  31 => 9,  27 => 6,  25 => 5,  23 => 3,  21 => 3,  103 => 44,  52 => 20,  45 => 16,  42 => 15,  40 => 14,  36 => 12,  34 => 11,  22 => 2,  19 => 1,  123 => 53,  82 => 41,  61 => 23,  56 => 23,  53 => 21,  49 => 18,  32 => 3,  29 => 7,);
    }
}
