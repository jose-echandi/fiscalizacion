<?php

/* ExportBundle:Pagos:index.html.twig */
class __TwigTemplate_dcbd6a25dfcfefdd9372141ce718402b00cc774508b9e7ff2dad2c9db5d8d8b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("ExportBundle::export_pdf.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'addHeader' => array($this, 'block_addHeader'),
            'addCol' => array($this, 'block_addCol'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ExportBundle::export_pdf.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "<div id=\"right_side\" class=\"col-md-11\">
    <div class=\"block-separator col-sm-12\"></div>
    <h1>";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</h1>
    <table class=\"table table-striped table-condensed\">
        <thead>
            <tr>
                <th>Nº</th>
                <th>D. Comercial</th>
                <th>Nº de Licencia</th>
                <th>RIF</th>
                <th>Monto</th>
                <th>Creado</th>
                <th>Estatus</th>
                ";
        // line 17
        $this->displayBlock('addHeader', $context, $blocks);
        // line 20
        echo "            </tr>
        </thead>
        <tbody>
        
        ";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 25
            echo "        ";
            $context["centro"] = (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"))) : ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora")));
            // line 26
            echo "            <tr>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["centro"]) ? $context["centro"] : null), "denominacionComercial"), "html", null, true);
            echo "</td>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getSolicitud", array(), "any", false, true), "getNumLicenciaAdscrita", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getSolicitud", array(), "any", false, true), "getNumLicenciaAdscrita"), " --- ")) : (" --- ")), "html", null, true);
            echo "</td>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["centro"]) ? $context["centro"] : null), "persJuridica"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["centro"]) ? $context["centro"] : null), "rif"), "html", null, true);
            echo "</td>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "monto"), 2, ",", "."), "html", null, true);
            echo " Bs</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaCreacion"), "d/m/Y"), "html", null, true);
            echo "</td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status"), "html", null, true);
            echo "</td>
                ";
            // line 34
            $this->displayBlock('addCol', $context, $blocks);
            // line 37
            echo "            </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "        </tbody>
    </table>
</div>
";
    }

    // line 17
    public function block_addHeader($context, array $blocks = array())
    {
        // line 18
        echo "                  <th> </th>
                ";
    }

    // line 34
    public function block_addCol($context, array $blocks = array())
    {
        // line 35
        echo "                 <td>-</td>
                ";
    }

    public function getTemplateName()
    {
        return "ExportBundle:Pagos:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 35,  144 => 34,  139 => 18,  136 => 17,  129 => 39,  114 => 37,  112 => 34,  108 => 33,  104 => 32,  100 => 31,  94 => 30,  90 => 29,  86 => 28,  82 => 27,  79 => 26,  76 => 25,  59 => 24,  53 => 20,  51 => 17,  37 => 6,  33 => 4,  30 => 3,);
    }
}
