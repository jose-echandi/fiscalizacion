<?php

/* PagosBundle:Banco:index.html.twig */
class __TwigTemplate_5ffcd75cbdcfc04a2db95714ac5ccf87e87ce85101787477ebcdfdf6cf30c08d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_base' => array($this, 'block_script_base'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>
            Bancos
        </h1>
    </div>

    <div id=\"action\">
        <div class=\"left\">
            ";
        // line 14
        echo "                ";
        // line 15
        echo "                    ";
        // line 16
        echo "                ";
        // line 17
        echo "            ";
        // line 18
        echo "        </div>
        <div class=\"right\">
            <a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("banco_new");
        echo "\" class=\"btn btn-primary\" style=\"float:right; margin: 20px 25px 10px 0px;height: 32px\"> Agregar nuevo banco </a>
        </div>
    </div>


<div class=\"col-md-12\">
    <table class=\"table table-condensed table-striped\">
        <thead>
            <tr>
                <th>No</th>
                <th>Banco</th>
                <th>Numero de cuenta</th>
                <th>Estatus</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 38
            echo "            <tr>
                <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "</td>
                <td align=\"center\">";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "banco"), "html", null, true);
            echo "</td>
                <td align=\"center\">";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "numeroCuenta"), "html", null, true);
            echo "</td>
                <td align=\"center\">";
            // line 42
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "activo") == 1)) {
                echo " Activo ";
            } else {
                echo " Inactivo ";
            }
            echo "</td>
                <td align=\"center\">
                    ";
            // line 45
            echo "                    <a class=\"btn btn-sm btn-primary\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("banco_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
            echo "\">
                        <i class=\"fa fa-pencil\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\"></i>
                    </a>
                    <a class=\"btn btn-sm btn-primary\" data-href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("banco_delete", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
            echo "\" data-toggle=\"modal\" data-target=\"#confirm-delete\" href=\"#\">
                        <i class=\"fa fa-trash-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Eliminar\"></i>
                    </a>
                </td>
            </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "        </tbody>
    </table>
</div>

    <div class=\"modal fade\" id=\"confirm-delete\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Eliminar banco
                </div>
                <div class=\"modal-body\">
                    Está seguro de eliminar la cuenta de banco?
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" class=\"btn btn-danger danger\">Eliminar</a>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 75
        $this->displayBlock('script_base', $context, $blocks);
        // line 83
        echo "


";
    }

    // line 75
    public function block_script_base($context, array $blocks = array())
    {
        // line 76
        echo "        ";
        $this->displayParentBlock("script_base", $context, $blocks);
        echo "
        <script type=\"text/javascript\">
            \$('#confirm-delete').on('show.bs.modal', function(e) {
                \$(this).find('.danger').attr('href', \$(e.relatedTarget).data('href'));
            });
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "PagosBundle:Banco:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 76,  175 => 75,  168 => 83,  166 => 75,  143 => 54,  123 => 48,  116 => 45,  107 => 42,  103 => 41,  99 => 40,  95 => 39,  92 => 38,  75 => 37,  55 => 20,  51 => 18,  49 => 17,  47 => 16,  45 => 15,  43 => 14,  32 => 4,  29 => 3,);
    }
}
