<?php

/* CentrohipicoBundle:DataOperadora:index.html.twig */
class __TwigTemplate_d57502f49e07a93160c823a66e830174abb338da35cee1e5e18f592e4a46ab50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1 class=\"tit_principal\">Lista de Operadoras</h1>
    </div>
    <div class=\"block-separator col-sm-12\"></div>
    ";
        // line 8
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 9
            echo "        <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
            ";
            // line 10
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : null));
            echo "
        </article>
    ";
        }
        // line 13
        echo "    <div class=\"col-md-12\">
    <table class=\"table table-condensed table-striped\">
        <thead>
        <tr>
                <th>Nº</th>
                <th>RIF</th>
                <th>Operadora</th>
                <th>Fecha de afiliacion</th>
                <th>Tipo de licencia</th>
                <th>No de licencia</th>
                <th>No Afiliados</th>
                <th>Aporte mensual</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 29
            echo "            <tr>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "</td>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
            echo "</td>
                <td>
                    <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" class=\"show_detail\" onclick=\"getInfo(";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
            echo ")\">
                        ";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
            echo "
                    </a>
                </td>
                <td>";
            // line 37
            if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaRegistro")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaRegistro"), "d-m-Y"), "html", null, true);
            }
            echo "</td>
                <td>
                    ";
            // line 39
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"));
            foreach ($context['_seq'] as $context["_key"] => $context["lic"]) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "clasfLicencia"), "html", null, true);
                echo " &nbsp; ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 40
            echo "                    ";
            if (twig_test_empty($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"))) {
                echo "Sin Registro ";
            }
            echo "    
                </td>
                <td>
                    ";
            // line 43
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"));
            foreach ($context['_seq'] as $context["_key"] => $context["lic"]) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "numLicencia"), "html", null, true);
                echo " &nbsp; ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "                    ";
            if (twig_test_empty($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"))) {
                echo "Sin Registro ";
            }
            echo "    
                </td>
                <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "afiliados"), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 48
            if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "aporte", array(), "any", true, true)) {
                // line 49
                echo "                        ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "aporte"), 0, array(), "array"), "monto"), 2, ",", "."), "html", null, true);
                echo "
                    ";
            } else {
                // line 51
                echo "                        Sin Registro
                    ";
            }
            // line 53
            echo "                </td>
            </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "       ";
        if (twig_test_empty((isset($context["entities"]) ? $context["entities"] : null))) {
            echo " 
                <tr>
                  <td colspan=\"9\" class=\"text-center text-uppercase text-info\">No hay Resultados</td>
                </tr>       
         ";
        }
        // line 60
        echo "       
        </tbody>
    </table>
    </div>
    <!-- Modal -->
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">
                        <span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span>
                    </button>
                </div>
                 <div class=\"modal-body\" id=\"datos_operadora\"></div>
                 <div class=\"modal-footer\"></div>
            </div>
        </div>
    </div>
 ";
    }

    // line 79
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 80
        echo "    ";
        $this->displayParentBlock("foot_script_assetic", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        function getInfo(id)
        {
            //console.info(\"entro\");
            var url = Routing.generate('operadora_info',{ 'id': id });
            \$.get( url, function( data ) {
                \$(\"#datos_operadora\").html(data);
            });
        }
    </script>

";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataOperadora:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 80,  218 => 79,  196 => 60,  187 => 56,  171 => 53,  167 => 51,  161 => 49,  159 => 48,  154 => 46,  146 => 44,  135 => 43,  126 => 40,  115 => 39,  108 => 37,  102 => 34,  98 => 33,  91 => 31,  87 => 30,  84 => 29,  67 => 28,  50 => 13,  44 => 10,  41 => 9,  39 => 8,  32 => 3,  29 => 2,);
    }
}
