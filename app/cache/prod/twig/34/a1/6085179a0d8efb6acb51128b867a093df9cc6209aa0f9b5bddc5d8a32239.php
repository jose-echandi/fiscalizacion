<?php

/* SolicitudesCitasBundle:DataSolicitudes:List_juegos.html.twig */
class __TwigTemplate_34a16085179a0d8efb6acb51128b867a093df9cc6209aa0f9b5bddc5d8a32239 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["porJuego"]) ? $context["porJuego"] : null) == 1)) {
            // line 2
            echo "    <select id=\"Sjuegose\" name=\"juegose[]\" multiple>
        ";
            // line 3
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                echo "   
           <option value=\"";
                // line 4
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["row"]) ? $context["row"] : null), "id"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["row"]) ? $context["row"] : null), "juego"), "html", null, true);
                echo "</option>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 6
            echo "    </select>
<br>Si desea seleccionar varios mantenga presionado la tecla CRTL
";
            // line 8
            if (((isset($context["tipo"]) ? $context["tipo"] : null) == "edit")) {
                // line 9
                echo "    <button class=\"btn btn-info\" onclick=\"cargaJuegos('juegoe');\"> Actualizar</button>
    <button class=\"btn btn-danger\" onclick=\"cambiar('juegoe',false);\">Cancelar</button>
";
            }
            // line 12
            echo " ";
        } else {
            // line 13
            echo "     No Requiere Selección de Juegos
     ";
        }
        // line 16
        echo "
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:List_juegos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 16,  54 => 13,  51 => 12,  46 => 9,  44 => 8,  40 => 6,  30 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
