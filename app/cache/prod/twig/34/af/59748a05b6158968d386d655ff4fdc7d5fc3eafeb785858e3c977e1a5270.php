<?php

/* LicenciaBundle:AdmJuegosExplotados:edit.html.twig */
class __TwigTemplate_34af59748a05b6158968d386d655ff4fdc7d5fc3eafeb785858e3c977e1a5270 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/genstyles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"print\">

";
    }

    // line 10
    public function block_content_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"block-separator col-sm-12\"></div>
\t<div class=\"tit_principal\">Editar Juego Explotado</div>
\t<div class=\"row col-lg-12\">
\t\t<div class=\"form-horizontal\">
\t\t\t";
        // line 15
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_start');
        echo "
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'errors');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12\">Los campos con <span class=\"oblig\">(*)</span> son obligatorios</div><br/>
\t\t\t\t</div>

\t\t\t\t<div class=\"form-group";
        // line 23
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "juego", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "juego"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "juego"), 'widget', array("attr" => array("class" => "form-control")));
        echo "

\t\t\t\t\t\t";
        // line 28
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "juego", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 29
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tIngrese una Licencia V&aacute;lida
\t\t\t\t\t\t\t\t";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "juego"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 34
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>

\t\t\t\t<div class=\"form-group";
        // line 37
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "status", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "status"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "status"), 'widget', array("attr" => array("class" => "form-control")));
        echo "

\t\t\t\t\t\t";
        // line 42
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "status", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 43
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tSeleccionar
\t\t\t\t\t\t\t\t";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "status"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 48
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>

\t\t\t\t<div class=\"block-separator col-md-12\"></div>

\t\t\t\t<div class=\"col-md-12 col-md-offset-2 form-group btn-group\">
\t\t\t\t\t<div class=\"col-md-6\" style=\"text-align:center\"><a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("admjuegosexplotados");
        echo "\" class=\"btn btn-primary btn-sm \">Regresar</a></div>";
        // line 56
        echo "\t\t\t\t\t<div class=\"col-md-6\" style=\"text-align:center\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "submit"), 'widget');
        echo "</div>
\t\t\t\t</div>
\t\t\t";
        // line 58
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_end');
        echo "
\t\t</div>
\t</div>

\t<!-- Modal -->
\t<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t<div class=\"modal-dialog\">
\t\t\t<div class=\"modal-content\">
\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
\t\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Eliminar el Registro</h4>
\t\t\t\t</div>
\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\tRealmente desea eliminar el registro <b>\"";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "vars"), "value"), "juego"), "html", null, true);
        echo "\"</b>?
\t\t\t\t</div>
\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t";
        // line 74
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start');
        echo "
\t\t\t\t\t";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : null), "submit"), 'widget');
        echo "
\t\t\t\t\t";
        // line 76
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "LicenciaBundle:AdmJuegosExplotados:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 76,  175 => 75,  171 => 74,  165 => 71,  149 => 58,  143 => 56,  140 => 54,  132 => 48,  126 => 45,  122 => 43,  120 => 42,  115 => 40,  110 => 38,  104 => 37,  99 => 34,  93 => 31,  89 => 29,  87 => 28,  82 => 26,  77 => 24,  71 => 23,  63 => 18,  57 => 15,  51 => 11,  48 => 10,  41 => 6,  37 => 5,  32 => 4,  29 => 3,);
    }
}
