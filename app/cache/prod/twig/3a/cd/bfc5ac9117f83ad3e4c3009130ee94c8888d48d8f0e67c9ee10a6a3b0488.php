<?php

/* LicenciaBundle:AdmClasfLicencias:edit.html.twig */
class __TwigTemplate_3acdbfc5ac9117f83ad3e4c3009130ee94c8888d48d8f0e67c9ee10a6a3b0488 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/genstyles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"print\">

";
    }

    // line 10
    public function block_content_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"block-separator col-sm-12\"></div>
\t<div class=\"tit_principal\">Editar Clasificación de Licencia</div>
\t<div class=\"row col-lg-12\">
\t\t<div class=\"form-horizontal\">
\t\t\t";
        // line 15
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_start');
        echo "
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'errors');
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12\">Los campos con <span class=\"oblig\">(*)</span> son obligatorios</div><br/>
\t\t\t\t</div>

\t\t\t\t<div class=\"form-group";
        // line 23
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "clasfLicencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "clasfLicencia"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "clasfLicencia"), 'widget', array("attr" => array("class" => "form-control")));
        echo "

\t\t\t\t\t\t";
        // line 28
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "clasfLicencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 29
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tIngrese una Licencia V&aacute;lida
\t\t\t\t\t\t\t\t";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "clasfLicencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 34
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>

\t\t\t\t<div class=\"form-group";
        // line 37
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admTiposLicencias", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admTiposLicencias"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admTiposLicencias"), 'widget', array("attr" => array("class" => "form-control")));
        echo "

\t\t\t\t\t\t";
        // line 42
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admTiposLicencias", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 43
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tSeleccionar
\t\t\t\t\t\t\t\t";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "admTiposLicencias"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 48
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>

\t\t\t\t<div class=\"form-group";
        // line 51
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admRecaudosLicencias", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admRecaudosLicencias"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admRecaudosLicencias"), 'widget', array("attr" => array("class" => "form-control")));
        echo "

\t\t\t\t\t\t";
        // line 56
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admRecaudosLicencias", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 57
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tSeleccionar
\t\t\t\t\t\t\t\t";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "admRecaudosLicencias"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 62
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>

\t\t\t\t<div class=\"form-group";
        // line 65
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admJuegosExplotados", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admJuegosExplotados"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admJuegosExplotados"), 'widget', array("attr" => array("class" => "form-control")));
        echo "

\t\t\t\t\t\t";
        // line 70
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "admJuegosExplotados", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 71
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tSeleccionar
\t\t\t\t\t\t\t\t";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "admJuegosExplotados"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 76
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>

\t\t\t\t<div class=\"form-group";
        // line 79
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "solicitudUt", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "solicitudUt"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "solicitudUt"), 'widget', array("attr" => array("class" => "form-control", "min" => "0")));
        echo "

\t\t\t\t\t\t";
        // line 84
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "solicitudUt", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 85
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tIngrese un monto V&aacute;lida
\t\t\t\t\t\t\t\t";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "solicitudUt"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 90
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>

\t\t\t\t<div class=\"form-group";
        // line 93
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "otorgamientoUt", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "otorgamientoUt"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "otorgamientoUt"), 'widget', array("attr" => array("class" => "form-control", "min" => "0")));
        echo "

\t\t\t\t\t\t";
        // line 98
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "otorgamientoUt", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 99
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tIngrese un monto V&aacute;lida
\t\t\t\t\t\t\t\t";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "otorgamientoUt"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 104
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>
                                
                                <div class=\"form-group";
        // line 107
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "codLicencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 108
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "codLicencia"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 110
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "codLicencia"), 'widget', array("attr" => array("class" => "form-control", "min" => "0")));
        echo "

\t\t\t\t\t\t";
        // line 112
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "otorgamientoUt", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 113
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tIngrese un código válido
\t\t\t\t\t\t\t\t";
            // line 115
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codLicencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 118
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>
                        
                                <div class=\"form-group";
        // line 121
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "hasOperadora", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "hasOperadora"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 124
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "hasOperadora"), 'widget', array("attr" => array("class" => "form-control")));
        echo "

\t\t\t\t\t\t";
        // line 126
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "hasOperadora", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 127
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tSeleccionar 
\t\t\t\t\t\t\t\t";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hasOperadora"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 132
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>
                        
                                <div class=\"form-group";
        // line 135
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "hasHipodromo", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "hasHipodromo"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "hasHipodromo"), 'widget', array("attr" => array("class" => "form-control")));
        echo "

\t\t\t\t\t\t";
        // line 140
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "hasHipodromo", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 141
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tSeleccionar 
\t\t\t\t\t\t\t\t";
            // line 143
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "hasHipodromo"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 146
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>

\t\t\t\t<div class=\"form-group";
        // line 149
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "status", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            echo " has-error";
        }
        echo "\">
\t\t\t\t\t";
        // line 150
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "status"), 'label', array("label_attr" => array("class" => " col-md-3 control-label text-left")));
        echo "
\t\t\t\t\t<div class=\"col-md-8\">
\t\t\t\t\t\t";
        // line 152
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "status"), 'widget', array("attr" => array("class" => "form-control")));
        echo "

\t\t\t\t\t\t";
        // line 154
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "status", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 155
            echo "\t\t\t\t\t\t\t<span class=\"help-block \">
\t\t\t\t\t\t\t\tSeleccionar
\t\t\t\t\t\t\t\t";
            // line 157
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "status"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
        }
        // line 160
        echo "\t\t\t\t\t</div >
\t\t\t\t</div>

\t\t\t\t<div class=\"block-separator col-md-12\"></div>

\t\t\t\t<div class=\"col-md-12 col-md-offset-2 form-group btn-group\">
\t\t\t\t\t<div class=\"col-md-6\" style=\"text-align:center\"><a href=\"";
        // line 166
        echo $this->env->getExtension('routing')->getPath("admclasflicencias");
        echo "\" class=\"btn btn-primary btn-sm \">Regresar</a></div>
\t\t\t\t\t";
        // line 168
        echo "\t\t\t\t\t<div class=\"col-md-6\" style=\"text-align:center\">";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "submit"), 'widget');
        echo "</div>
\t\t\t\t</div>
\t\t\t";
        // line 170
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : null), 'form_end');
        echo "
\t\t</div>
\t</div>

\t<!-- Modal -->
\t<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t\t<div class=\"modal-dialog\">
\t\t\t<div class=\"modal-content\">
\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
\t\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Eliminar el Registro</h4>
\t\t\t\t</div>
\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\tRealmente desea eliminar el registro <b>\"";
        // line 183
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["edit_form"]) ? $context["edit_form"] : null), "vars"), "value"), "clasfLicencia"), "html", null, true);
        echo "\"</b>?
\t\t\t\t</div>
\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t";
        // line 186
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start');
        echo "
\t\t\t\t\t";
        // line 187
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : null), "submit"), 'widget');
        echo "
\t\t\t\t\t";
        // line 188
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "LicenciaBundle:AdmClasfLicencias:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  444 => 188,  440 => 187,  436 => 186,  430 => 183,  414 => 170,  408 => 168,  404 => 166,  396 => 160,  390 => 157,  386 => 155,  384 => 154,  379 => 152,  374 => 150,  368 => 149,  363 => 146,  357 => 143,  353 => 141,  351 => 140,  346 => 138,  341 => 136,  335 => 135,  330 => 132,  324 => 129,  320 => 127,  318 => 126,  313 => 124,  308 => 122,  302 => 121,  297 => 118,  291 => 115,  287 => 113,  285 => 112,  280 => 110,  275 => 108,  269 => 107,  264 => 104,  258 => 101,  254 => 99,  252 => 98,  247 => 96,  242 => 94,  236 => 93,  231 => 90,  225 => 87,  221 => 85,  219 => 84,  214 => 82,  209 => 80,  203 => 79,  198 => 76,  192 => 73,  188 => 71,  186 => 70,  181 => 68,  176 => 66,  170 => 65,  165 => 62,  159 => 59,  155 => 57,  153 => 56,  148 => 54,  143 => 52,  137 => 51,  132 => 48,  126 => 45,  122 => 43,  120 => 42,  115 => 40,  110 => 38,  104 => 37,  99 => 34,  93 => 31,  89 => 29,  87 => 28,  82 => 26,  77 => 24,  71 => 23,  63 => 18,  57 => 15,  51 => 11,  48 => 10,  41 => 6,  37 => 5,  32 => 4,  29 => 3,);
    }
}
