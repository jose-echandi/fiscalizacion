<?php

/* SolicitudesCitasBundle:DataSolicitudes:hipodromos.html.twig */
class __TwigTemplate_3a1fc450ada2583e7a4f63dc7930033e4b40c3fd0195a4275952464f5961d7ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["hasHipodromo"]) ? $context["hasHipodromo"] : null) == "true")) {
            // line 2
            echo "<div class=\"col-md-4\"> Hipódromos Internacionales <span class=\"oblig\">(*)</span>:</div>
<div class=\"col-md-8\">
    <textarea cols=\"60\" rows=\"6\" id=\"hipointer\" class=\"hipointer form-control\" required=\"required\" name=\"sunahip_solicitudescitasbundle_datasolicitudes[hipodromoInter]\"></textarea><br>
    Ejemplo:<br>Argentina: Hipodromo, Estados Unidos: Hipódromo, ...    
</div>
";
        } else {
            // line 8
            echo "<div class=\"col-md-4\"><p>No Aplica para este tipo de Licencia</p></div>
<div class=\"col-md-8\">&nbsp;</div>
<input type=\"hidden\" id=\"hipointer\" class=\"hipointer form-control\" name=\"sunahip_solicitudescitasbundle_datasolicitudes[hipodromoInter]\">
";
        }
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:hipodromos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 8,  21 => 2,  19 => 1,);
    }
}
