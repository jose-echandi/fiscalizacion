<?php

/* CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig */
class __TwigTemplate_7c7b553671d7d96b9ee2afcc86248620195958885165777136bc4cf20678c0b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a title=\"Ver Documento Adjunto\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute((isset($context["media"]) ? $context["media"] : null), "path")), "html", null, true);
        echo "\" target=\"_blank\">Documento</a>";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  258 => 139,  255 => 138,  244 => 130,  224 => 113,  208 => 99,  205 => 98,  198 => 93,  195 => 92,  193 => 91,  185 => 86,  179 => 82,  176 => 81,  170 => 78,  166 => 76,  164 => 75,  159 => 73,  155 => 71,  153 => 70,  148 => 68,  141 => 64,  134 => 60,  129 => 58,  122 => 54,  117 => 52,  110 => 48,  105 => 46,  98 => 42,  93 => 40,  86 => 36,  81 => 34,  75 => 30,  71 => 28,  65 => 26,  63 => 25,  53 => 20,  46 => 16,  32 => 4,  29 => 3,);
    }
}
