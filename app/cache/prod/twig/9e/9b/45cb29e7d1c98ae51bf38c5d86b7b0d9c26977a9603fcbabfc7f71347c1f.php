<?php

/* CentrohipicoBundle:DataCentrohipico:newEstablishment.html.twig */
class __TwigTemplate_9e9b45cb29e7d1c98ae51bf38c5d86b7b0d9c26977a9603fcbabfc7f71347c1f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "
    ";
        // line 4
        if ((!array_key_exists("message", $context))) {
            // line 5
            echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"col-md-12\">
        <h1 class=\"tit_principal\">Centro Hipico</h1>
    </div>
    <div class=\"col-md-12\"><p>Los campos con <span class=\"oblig\">(*)</span> son obligatorios. Por favor ingrese los todos los Datos de Establecimiento para guardar</p></div>
    <div class=\"row col-lg-12\">
        ";
            // line 11
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "form_dch")));
            echo "
        ";
            // line 12
            $this->env->loadTemplate("CentrohipicoBundle:DataCentrohipico:CHtable_form.html.twig")->display($context);
            // line 13
            echo "        <div class=\"col-md-12 form-group btn-group\">
            <div style=\"float: right\">
                <button id=\"form_btn\" type=\"submit\" class=\"btn btn-primary btn-sm\">Guardar</button>
            </div>
        </div>
        ";
            // line 18
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
            echo "
    </div>

    ";
        } else {
            // line 22
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>";
            // line 25
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
            echo "</h5> <a href=\"";
            echo $this->env->getExtension('routing')->getPath("data_empresa_new");
            echo "\">Crear Empresa</a></li>
                </ul>
            </div>
        </div>
    ";
        }
    }

    // line 32
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 33
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/DataCentroHipico/validate.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/resource.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" >
        \$(document).ready(function(){
            \$('#tabs').tabs();
            \$(\".form-horizontal\").removeClass('form-horizontal');
            \$(\".form-control\").removeClass('form-control');
            \$(\"#datach #datalegal\").show();
            \$(\"#temp\").hide();
            \$(\"#addDL\").click(function(){
                agregarDL();
            });
        });

        function agregarDL(){
            \$(\"#btnDL\").hide();
            \$(\"#fDL\").show();
            \$(\"#fDL\").html(getGifLoading());
            //var route=Routing.generate('datalegal_new');
            \$.get('";
        // line 52
        echo $this->env->getExtension('routing')->getPath("datalegal_new");
        echo "').success(function(data) {
                if (data.message) {
                    message = data.message;
                } else {
                    \$('#datalegal').html(data);
                }
            }).error(function(data, status, headers, config) {
                        if (status === '500') {
                            message = \"No hay conexión con el servidor\";
                        }
                    });
        }
        function sFormDL(){
            var datach=\$(\"#form_dl\").serialize();
            \$(\"#form_dl\").hide();
            \$(\"#fDL\").hide();
            \$(\"#btnDL\").show();
            //\$.post()
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataCentrohipico:newEstablishment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 52,  90 => 34,  85 => 33,  82 => 32,  70 => 25,  65 => 22,  58 => 18,  51 => 13,  49 => 12,  45 => 11,  37 => 5,  35 => 4,  32 => 3,  29 => 2,);
    }
}
