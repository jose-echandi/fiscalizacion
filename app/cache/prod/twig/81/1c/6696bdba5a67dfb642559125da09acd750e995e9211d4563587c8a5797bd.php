<?php

/* CentrohipicoBundle:DataOperadora:DLtable_form.html.twig */
class __TwigTemplate_811c6696bdba5a67dfb642559125da09acd750e995e9211d4563587c8a5797bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<input type=\"hidden\" id=\"validaCiudMunParro\" value=\"-1\">
<table id=\"tabla_reporte2\" class=\"tabla_reporte2\">\t
    <tbody>
      <tr>
            <td>";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombreComercial"), 'label', array("label_attr" => array("class" => "text-left col-md-12")));
        echo "</td>
            <td colspan=\"3\">";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombreComercial"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                ";
        // line 7
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombreComercial", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 8
            echo "                <span class=\"help-block \">
                    ";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombreComercial"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                </span>
                ";
        }
        // line 12
        echo "            </td>
        </tr>\t
        <tr id=\"table_header2\">
                 <td colspan=\"4\">Datos del apoderado o representante legal</td>
        </tr>
    <tr>
             <td>";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
             <td>";
        // line 19
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), 'widget', array("attr" => array("class" => " ")));
        echo "
                            ";
        // line 20
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 21
            echo "                            <span class=\"help-block \">
                                ";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 25
        echo "             </td>
             <td>";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
             <td>";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), 'widget', array("attr" => array("class" => " ")));
        echo "
                            ";
        // line 28
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 29
            echo "                            <span class=\"help-block \">
                                ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 33
        echo "             </td>
        </tr>
        <tr>
             <td>";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>
                  ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tipoci"), 'widget', array("attr" => array("class" => " input-mini")));
        echo "
                  - ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), 'widget');
        echo "
                    ";
        // line 40
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 41
            echo "                    <span class=\"help-block \">
                        ";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 45
        echo "             </td>
             <td>";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rif"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>
                 ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "persJuridica"), 'widget', array("attr" => array("class" => "input-mini ")));
        echo "
                  - ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rif"), 'widget');
        echo "
                    ";
        // line 50
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rif", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 51
            echo "                    <span class=\"help-block \">
                        ";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rif"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 55
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'widget', array("attr" => array("class" => "col-md-10 dlestado")));
        echo "
                    ";
        // line 60
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 61
            echo "                    <span class=\"help-block \">
                        ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 65
        echo "             </td>
        <td>";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
        <td>";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), 'widget', array("attr" => array("class" => "dlmunicipio")));
        echo "
            ";
        // line 68
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 69
            echo "                <span class=\"help-block \">
                        ";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
            ";
        }
        // line 73
        echo "        </td>
    </tr>
    <tr>
             <td><label class=\"col-md-12 control-label text-left required\" for=\"ciudad\">Ciudad <span class=\"asterisk\">*</span></label></td>
             <td>";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 78
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 79
            echo "                    <span class=\"help-block \">
                        ";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 83
        echo "             </td>
        <td>";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
        <td>";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), 'widget', array("attr" => array("class" => "dlparroquia")));
        echo "
            ";
        // line 86
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquiaparroquia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 87
            echo "                <span class=\"help-block \">
                        ";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
            ";
        }
        // line 91
        echo "        </td>
    </tr>
    <tr>
             <td>";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 96
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 97
            echo "                    <span class=\"help-block \">
                        ";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 101
        echo "             </td>
             <td>";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>
                 ";
        // line 104
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 105
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 106
            echo "                    <span class=\"help-block \">
                        ";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 110
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 113
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 114
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 115
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 116
            echo "                    <span class=\"help-block \">
                        ";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 120
        echo "             </td>
             <td>";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 123
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 124
            echo "                    <span class=\"help-block \">
                        ";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 128
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 132
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 133
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 134
            echo "                    <span class=\"help-block \">
                        ";
            // line 135
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 138
        echo "             </td>
             <td>";
        // line 139
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 140
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
                    ";
        // line 141
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 142
            echo "                    <span class=\"help-block \">
                        ";
            // line 143
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 146
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 150
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 151
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 152
            echo "                    <span class=\"help-block \">
                        ";
            // line 153
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 156
        echo "             </td>
        <td>";
        // line 157
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
        <td>";
        // line 158
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codFax"), 'widget', array("attr" => array("class" => " col-md-3")));
        echo "
            ";
        // line 159
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
            ";
        // line 160
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 161
            echo "                <span class=\"help-block \">
                        ";
            // line 162
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
            ";
        }
        // line 165
        echo "        </td>
    </tr>
    <tr>
             <td>";
        // line 168
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 169
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codTlfFijo"), 'widget', array("attr" => array("class" => " col-md-3")));
        echo "
                 ";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
                    ";
        // line 171
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 172
            echo "                    <span class=\"help-block \">
                        ";
            // line 173
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 176
        echo "             </td>
             <td>";
        // line 177
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 178
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codTlfCelular"), 'widget', array("attr" => array("class" => " col-md-3 ")));
        echo "
                 ";
        // line 179
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
                    ";
        // line 180
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 181
            echo "                    <span class=\"help-block \">
                        ";
            // line 182
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 185
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td colspan=\"3\">";
        // line 189
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 190
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 191
            echo "                    <span class=\"help-block \">
                        ";
            // line 192
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 195
        echo "             </td>
    </tr>
   ";
        // line 208
        echo "    ";
        // line 211
        echo "  </tbody>
</table>
 <div class=\"block-separator col-md-12\"></div>
<script type=\"text/javascript\" >
          \$(document).ready(function(){
                \$(\".form-horizontal\").removeClass('form-horizontal');
                \$(\".form-control\").removeClass('form-control');
                \$(\".dlestado\").change(function() {
                    estado = \$('.dlestado option:selected').val();
                      var Rmunicipio=Routing.generate('municipios', {estado_id: estado});
                      getSelect(Rmunicipio,'.dlmunicipio',\"Municipio\");
                    \$(\".dlmunicipio\").removeAttr('disabled');
                });
                 \$(\".dlmunicipio\").change(function() {
                     estado = \$('.dlestado option:selected').val();
                     municipio = \$('.dlmunicipio option:selected').val();
                     var Rparroquia=Routing.generate('parroquias', {municipio_id: municipio});
                      getSelect(Rparroquia,'.dlparroquia',\"Parroquia\");
                     \$(\".dlparroquia\").removeAttr('disabled');
                });
            });
          
 </script>      
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataOperadora:DLtable_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  511 => 211,  509 => 208,  505 => 195,  499 => 192,  496 => 191,  494 => 190,  490 => 189,  486 => 188,  481 => 185,  475 => 182,  472 => 181,  470 => 180,  466 => 179,  462 => 178,  458 => 177,  455 => 176,  449 => 173,  446 => 172,  444 => 171,  440 => 170,  436 => 169,  432 => 168,  427 => 165,  421 => 162,  418 => 161,  416 => 160,  412 => 159,  408 => 158,  404 => 157,  401 => 156,  395 => 153,  392 => 152,  390 => 151,  386 => 150,  382 => 149,  377 => 146,  371 => 143,  368 => 142,  366 => 141,  362 => 140,  358 => 139,  355 => 138,  349 => 135,  346 => 134,  344 => 133,  340 => 132,  336 => 131,  331 => 128,  325 => 125,  322 => 124,  320 => 123,  316 => 122,  312 => 121,  309 => 120,  303 => 117,  300 => 116,  298 => 115,  294 => 114,  290 => 113,  285 => 110,  279 => 107,  276 => 106,  274 => 105,  270 => 104,  265 => 102,  262 => 101,  256 => 98,  253 => 97,  251 => 96,  247 => 95,  243 => 94,  232 => 88,  229 => 87,  227 => 86,  223 => 85,  219 => 84,  216 => 83,  210 => 80,  207 => 79,  201 => 77,  195 => 73,  189 => 70,  186 => 69,  180 => 67,  173 => 65,  167 => 62,  164 => 61,  162 => 60,  154 => 58,  149 => 55,  143 => 52,  140 => 51,  138 => 50,  134 => 49,  130 => 48,  125 => 46,  122 => 45,  116 => 42,  113 => 41,  111 => 40,  107 => 39,  103 => 38,  98 => 36,  93 => 33,  87 => 30,  84 => 29,  82 => 28,  78 => 27,  71 => 25,  65 => 22,  62 => 21,  60 => 20,  56 => 19,  52 => 18,  44 => 12,  38 => 9,  35 => 8,  33 => 7,  25 => 5,  19 => 1,  308 => 161,  288 => 144,  283 => 143,  280 => 142,  275 => 139,  264 => 131,  260 => 129,  258 => 128,  255 => 127,  245 => 120,  238 => 91,  231 => 112,  224 => 108,  217 => 104,  211 => 100,  205 => 78,  203 => 97,  196 => 93,  190 => 89,  184 => 68,  182 => 86,  176 => 66,  170 => 80,  168 => 79,  158 => 59,  151 => 68,  142 => 64,  133 => 60,  126 => 56,  119 => 52,  112 => 48,  105 => 44,  96 => 40,  76 => 26,  74 => 26,  69 => 22,  64 => 20,  57 => 15,  55 => 14,  50 => 13,  48 => 12,  37 => 6,  32 => 3,  29 => 6,);
    }
}
