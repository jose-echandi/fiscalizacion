<?php

/* FiscalizacionBundle:Providencia:index.html.twig */
class __TwigTemplate_81f172bf6143578fb895bd4c3e1926b78aef239ee9be6a11180b7a3a538b10bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "<div id=\"right_side\" class=\"col-md-11\">
    <div class=\"block-separator col-sm-12\"></div>
    <h1>Gestionar Providencias</h1>
    <div id=\"action\">
        <div class=\"left\">
            ";
        // line 9
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 10
            echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
            // line 11
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : null));
            echo "
                </article>
            ";
        }
        // line 14
        echo "        </div>
        <div class=\"right\">
            ";
        // line 16
        if ($this->env->getExtension('security')->isGranted("ROLE_GERENTE")) {
            // line 17
            echo "                <a style=\"float:right;margin-right:10px; margin-top: 20px; margin-bottom: 10px; height: 32px\"  href=\"";
            echo $this->env->getExtension('routing')->getPath("providencia_new");
            echo "\" class=\"btn btn-primary\">
                    Incluir
                </a>
            ";
        }
        // line 21
        echo "        </div>
    </div>
     <br/>
     ";
        // line 24
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 25
            echo "    ";
            echo twig_include($this->env, $context, "ExportBundle::iconslink.html.twig", array("pdf" => "exportpdf_providencia", "xcel" => "exportxls_providencia"));
            echo "
    <br/>
    
    <table class=\"table table-striped table-condensed\">
        <thead>
            <tr>
                <th>No</th>
                <th>No Providencia</th>
                <th>Fecha de inicio / Fecha de fin</th>
                <th>Estatus</th>
                <th>Motivo</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        ";
            // line 40
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 41
                echo "            <tr>
                <td><a href=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("providencia_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                echo "</a></td>
                <td>";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "num"), "html", null, true);
                echo "</td>
                <td>
                    ";
                // line 45
                if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "finicio")) {
                    // line 46
                    echo "                        ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "finicio"), "Y-m-d"), "html", null, true);
                    echo "
                    ";
                }
                // line 48
                echo "                    ";
                if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ffinal")) {
                    // line 49
                    echo "                        / ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ffinal"), "Y-m-d"), "html", null, true);
                    echo "
                    ";
                }
                // line 50
                echo "</td>
                <td>";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status"), "html", null, true);
                echo "</td>
                <td>";
                // line 52
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "motivo"), "html", null, true);
                echo "</td>
                <td>
                    <a class=\"btn btn-info btn-sm\" href=\"";
                // line 54
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("providencia_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
                echo "\">Ver</a>

                </td>
            </tr>
        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "        </tbody>
    </table>
        ";
        } else {
            // line 62
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>No se encontraron resultados</h5></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 70
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Providencia:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 70,  173 => 62,  168 => 59,  149 => 54,  144 => 52,  140 => 51,  137 => 50,  131 => 49,  128 => 48,  122 => 46,  120 => 45,  115 => 43,  109 => 42,  106 => 41,  89 => 40,  70 => 25,  68 => 24,  63 => 21,  55 => 17,  53 => 16,  49 => 14,  43 => 11,  40 => 10,  38 => 9,  31 => 4,  28 => 3,);
    }
}
