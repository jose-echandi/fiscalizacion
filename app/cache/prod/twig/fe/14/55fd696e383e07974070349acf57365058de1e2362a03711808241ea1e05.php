<?php

/* SolicitudesCitasBundle:DataSolicitudes:List_recaudos_edf.html.twig */
class __TwigTemplate_fe1455fd696e383e07974070349acf57365058de1e2362a03711808241ea1e05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "form_drecaudos")));
        echo "   
               <div class=\"form-group\">
                    <div class=\"col-md-12\">
                        ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
                        ";
        // line 5
        if ((array_key_exists("errors", $context) && (!(null === (isset($context["errors"]) ? $context["errors"] : null))))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null), "html", null, true);
            echo " ";
        }
        // line 6
        echo "                    </div>
                     ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "flashbag"), "all", array(), "method"));
        foreach ($context['_seq'] as $context["type"] => $context["flashMessage"]) {
            // line 8
            echo "                        <div class=\"alert alert-";
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
            echo " \">
                            <button class=\"close\" data-dismiss=\"alert\" type=\"button\"></button>
                            ";
            // line 10
            if ($this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : null), "title", array(), "any", true, true)) {
                // line 11
                echo "                            <strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : null), "title"), "html", null, true);
                echo "</strong>
                            ";
                // line 12
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["flashMessage"]) ? $context["flashMessage"] : null), "message"), "html", null, true);
                echo "
                            ";
            } else {
                // line 14
                echo "                            ";
                echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
                echo "
                            ";
            }
            // line 16
            echo "                        </div>
                     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "                </div>
<table id=\"tabla_reporte2\">\t\t\t\t
        <tr id=\"table_header2\">
                 <td>Tipo de Documento</td>
                 <td>Buscar Archivo<span class=\"oblig\">(*)</span></td>
                 <td>Fecha de Vencimiento<span class=\"oblig\">(*)</span></td>
                 <td>Acción<span class=\"oblig\">(*)</span></td>
        </tr>
          <tr>
                 <td class=\"text-left\">";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "recaudoLicencia"), "recaudo"), "html", null, true);
        echo "</td>
                 <td class=\"text-center\">";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mediarecaudo"), 'widget', array("attr" => array("class" => "mediarecaudo")));
        echo "
                                ";
        // line 29
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mediarecaudo", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 30
            echo "                                <span class=\"help-block \">
                                    ";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mediarecaudo"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                                </span>
                                ";
        }
        // line 34
        echo "                  </td>
                 <td class=\"text-center \">";
        // line 35
        $context["disable"] = ((($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "recaudoLicencia"), "fechaVencimiento") == 0)) ? ("disabled") : (" "));
        // line 36
        echo "                                ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fechaVencimiento"), 'widget', array("attr" => array("class" => ("date fechavencimiento " . (isset($context["disable"]) ? $context["disable"] : null)))));
        echo "
                                ";
        // line 37
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fechaVencimiento", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 38
            echo "                                <span class=\"help-block \">
                                    ";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fechaVencimiento"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                                </span>
                                ";
        }
        // line 42
        echo "                  </td>
                 <td class=\"text-center\">
                     ";
        // line 45
        echo "                     <button class=\"btn btn-sm btn-info\" onclick=\"updateRecaudo('";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
        echo "');\">Actualizar</button>
                     <button class=\"btn btn-sm btn-warning\" onclick=\"cambiar('recaudos',false);\">Cancelar</button>
                     ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tipodoc"), 'widget', array("attr" => array("class" => "tipodoc"), "data" => "D"));
        echo "
                     ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "status"), 'widget', array("attr" => array("class" => "status")));
        echo "
                     ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "reciboN"), 'widget', array("attr" => array("class" => "hide")));
        echo "
                 </td>
        </tr>
</table>
";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token"), 'widget');
        echo "
</form>                 
<script type=\"text/javascript\">
    \$(document).ready(function(){
        \$(\".disabled\").prop('disabled',true);       
            var Options2={
                autoSize: true,
                dateFormat: 'dd/mm/yy',
                minDate: '";
        // line 61
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
        echo "',
                onSelect: function(dateText, inst) { 
                   var datets = Date.parse(inst.selectedYear+'-'+inst.selectedMonth+'-'+inst.selectedDay);
                   \$(this).val(dateText);                
               } 
            };
        \$(\".date\").each(function(index,elem){
            \$(this).datepicker(Options2); 
        });
    });
    function updateRecaudo(id){
        dataF=\$(\"#form_drecaudos\").serializeArray();
        //dataF=new FormData( \$(\"#form_drecaudos\") );
        route=Routing.generate('recaudoscargados_update',{id:id});
        \$( \"#form_drecaudos\" ).submit(function( event ) {
            event.preventDefault();
            //dataF=\$(\"#form_drecaudos\").serializeArray();
            dataF=new FormData(this);
            postRoute(\"recaudos\",route,dataF);
          });
    }
    
</script>
    
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:List_recaudos_edf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 61,  149 => 53,  142 => 49,  138 => 48,  134 => 47,  128 => 45,  124 => 42,  118 => 39,  115 => 38,  113 => 37,  108 => 36,  106 => 35,  103 => 34,  97 => 31,  94 => 30,  92 => 29,  88 => 28,  84 => 27,  73 => 18,  66 => 16,  60 => 14,  55 => 12,  50 => 11,  48 => 10,  42 => 8,  38 => 7,  35 => 6,  29 => 5,  25 => 4,  19 => 1,);
    }
}
