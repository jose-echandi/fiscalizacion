<?php

/* CentrohipicoBundle:DataEmpresa:tabs.html.twig */
class __TwigTemplate_572e19c36428d4e040204aa3bd5f93e46acc9503da26c82f737e94e4824a920f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row col-lg-12\">
    ";
        // line 2
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "form_dch")));
        echo "
    <div id=\"tabs\">
        <ul>
            <li><a href=\"#data\">Datos Empresa</a></li>
            <li><a href=\"#dataLegal\">Datos Legales</a></li>
            <li><a href=\"#dataPartner\">Socios</a></li>
        </ul>
        <div id=\"temp\"><h3> Espere un momento</h3> </div>
        <div id=\"data\" style=\"display:none\">
            ";
        // line 11
        $this->env->loadTemplate("CentrohipicoBundle:DataEmpresa:form.html.twig")->display($context);
        // line 12
        echo "        </div>
        <div id=\"dataLegal\" style=\"display:none;\">
            ";
        // line 14
        $this->env->loadTemplate("CentrohipicoBundle:DataEmpresa:formLegal.html.twig")->display($context);
        // line 15
        echo "        </div>
    ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token"), 'widget');
        echo "
        <div id=\"divBtnSubmit\">
            <div class=\"col-md-12 form-group btn-group\" id=\"btnSubmitABack\">
                <div style=\"float: left\">
                    <a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"btn btn-success btn-sm\">Regresar</a>
                </div>
                <div style=\"float: right\">
                    <button onclick=\"validateForm()\" type=\"button\" class=\"btn btn-primary btn-sm\">Guardar</button>
                </div>
            </div>
        </div>
    </form>
        <div id=\"dataPartner\" style=\"display:none;\">
            <table id=\"tabla_reporte2\" class=\"socios tabla_reporte2\" style=\"\">
                <tr id=\"table_header2\">
                    <td colspan=\"3\">Socios</td>
                </tr>
                <tr id=\"rowPartner\">
                    <td><a href=\"#\">Nombre</a></td>
                    <td><a href=\"#\">Tipo</a></td>
                    <td><a href=\"#\"># Socios</a></td>
                </tr>
            </table>
            <div style=\"float: right;margin-right: 20px; margin-top: 10px;\">
                <button class=\"btn\" id=\"addPartner\" type=\"button\">Agregar socio</button>
            </div>
            <br><br>
            <div>
                <br>
                <form id=\"partnerAddForm\">
                    <table id=\"tabla_reporte2\" class=\"tabla_reporte2 dataPartner\" style=\"display: none\">
                        <tbody>
                        <tr id=\"table_header2\">
                            <td colspan=\"4\">Datos del socio</td>
                        </tr>
                        <tr id=\"zone_select_partner\">
                        </tr>
                    </tbody>
                    </table>
                    <div id=\"zone_form_partner\"></div>
                </form>
            </div>
        </div>

    </div>
    <div class=\"col-md-12 form-group btn-group\">
        (*) Campos obligatorios
    </div>
    <div id=\"divBtnSubmitPartner\" style=\"display: none;\">

        <div class=\"col-md-12 form-group btn-group\">
            <div style=\"float: left\">
                <a href=\"";
        // line 68
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"btn btn-success btn-sm\">Regresar</a>
            </div>
            <div style=\"float: right\">
                <button onclick=\"validateForm()\" type=\"button\" class=\"btn btn-primary btn-sm\">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" id=\"buttonCloseModal\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Notificación</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"col-md-12 alert text-left\" id=\"myMessage\">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataEmpresa:tabs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 68,  52 => 20,  45 => 16,  42 => 15,  40 => 14,  36 => 12,  34 => 11,  22 => 2,  19 => 1,  123 => 79,  82 => 41,  61 => 23,  56 => 22,  53 => 21,  49 => 18,  32 => 3,  29 => 2,);
    }
}
