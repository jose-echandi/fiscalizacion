<?php

/* CentrohipicoBundle:DataEmpresa:addPartner.html.twig */
class __TwigTemplate_08873e98dc2087f0245d32e38ac038ebead81a131099e23ae8af83ef4c64ac40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <style>
        em{
            color: #e70203;
            font-size: 11px;
        }
    </style>
    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"col-md-12\">
        <h1 class=\"tit_principal\">Empresas | Agregar socios </h1>
    </div>
    <div class=\"col-md-12\">
        <p>Los campos con <span class=\"oblig\">(*)</span> son obligatorios. Por favor ingrese los todos los Datos de la empresa para guardar.</p>
    </div>
    <div class=\"col-md-12 form-group btn-group\">
        <table id=\"tabla_reporte2\" class=\"sociosTable tabla_reporte2\" style=\"display:none\">
            <tr id=\"table_header2\">
                <td colspan=\"3\">Nuevos socios</td>
            </tr>
            <tr id=\"rowPartner\">
                <td><a href=\"#\">Nombre</a></td>
                <td><a href=\"#\">Tipo</a></td>
                <td><a href=\"#\"># Socios</a></td>
            </tr>
        </table>
        <div id=\"btnAddMore\" style=\"display:none;float: right;margin-right: 20px; margin-top: 10px;\">
            <button class=\"btn\" id=\"addPartner\" type=\"button\">Agregar socio</button>
        </div>
        <br>
            <br>
            <form id=\"partnerAddForm\">
                <input type=\"hidden\" name=\"c_id\" id=\"company_id\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
        echo "\">
                <table id=\"tabla_reporte2\" class=\"tabla_reporte2 dataPartner\">
                    <tbody>
                    <tr id=\"table_header2\">
                        <td colspan=\"4\">Datos del socio</td>
                    </tr>
                    <tr id=\"zone_select_partner\">
                        <td>
                            <label>Rif *</label>
                        </td>
                        <td>
                            <select onchange='addFormPartner()' id='rifTypePartner' class='input-mini'>
                                <option value='J'>J</option>
                                <option value='G'>G</option>
                                <option value='V'>V</option>
                                <option value='E'>E</option>
                            </select>-
                            <input type='text' id='rifNumberPartner' required='required' maxlength='9' onkeypress='return isNumericInteger(event)'>
                            &nbsp;<a href='#' class='btn btn-primary btn-sm' id='btnPartner' onclick='addFormPartner()'>Agregar</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div id=\"zone_form_partner\"></div>
            </form>
            </br>
    </div>
    <div class=\"col-md-12 form-group btn-group\">
        <div style=\"float: left\">
            <a href=\"";
        // line 62
        echo $this->env->getExtension('routing')->getPath("data_empresa_list");
        echo "\" class=\"btn btn-success btn-sm\">Regresar</a>
        </div>
        <div style=\"float: right\">
            <button id=\"form_btn\" type=\"submit\" disabled=\"disabled\" class=\"btn btn-primary btn-sm\">Guardar</button>
        </div>
    </div>

    <!-- Modal -->
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" id=\"buttonCloseModal\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Notificación</h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 alert text-left\" id=\"myMessage\">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
    }

    // line 90
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 91
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/resource.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" >
        \$(document).ready(function(){
            \$('#tabs').tabs();
            \$(\".form-horizontal\").removeClass('form-horizontal');
            \$(\".form-control\").removeClass('form-control');
            \$(\"#datach #datalegal\").show();
            \$(\"#temp\").hide();
            \$(\"#addDL\").click(function(){
                agregarDL();
            });
        });

        function agregarDL(){
            \$(\"#btnDL\").hide();
            \$(\"#fDL\").show();
            \$(\"#fDL\").html(getGifLoading());
            //var route=Routing.generate('datalegal_new');
            \$.get('";
        // line 109
        echo $this->env->getExtension('routing')->getPath("datalegal_new");
        echo "').success(function(data) {
                if (data.message) {
                    message = data.message;
                } else {
                    \$('#datalegal').html(data);
                }
            }).error(function(data, status, headers, config) {
                        if (status === '500') {
                            message = \"No hay conexión con el servidor\";
                        }
                    });
        }
        function sFormDL(){
            var datach=\$(\"#form_dl\").serialize();
            \$(\"#form_dl\").hide();
            \$(\"#fDL\").hide();
            \$(\"#btnDL\").show();
            //\$.post()
        }

        \$(document).ready(function() {
            \$(\".estados\").change(function() {
                estado = \$(this).val();
                var Rmunicipio=Routing.generate('municipios', {estado_id: estado||0});
                getSelect(Rmunicipio,'.municipio',\"Municipio\");
                \$(\".municipio\").val('');

            });

            \$(\".municipio\").change(function() {
                municipio = \$(this).val();
                \$(\".parroquia\").val('');
                var Rparroquia=Routing.generate('parroquias', {municipio_id: municipio||0});
                getSelect(Rparroquia,'.parroquia',\"Parroquia\");
            });
        });

    </script>
    <script src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/DataEmpresa/partner.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataEmpresa:addPartner.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 147,  152 => 109,  130 => 91,  127 => 90,  96 => 62,  64 => 33,  32 => 3,  29 => 2,);
    }
}
