<?php

/* CentrohipicoBundle::centroh_base.html.twig */
class __TwigTemplate_70bc29c87fb1cb111ebb43dc1fa2083b18660f40b6722a5853c3e625cf44f457 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 6
    public function block_content_content($context, array $blocks = array())
    {
        echo "   
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 6,  32 => 3,  29 => 2,  186 => 76,  178 => 71,  173 => 68,  163 => 60,  157 => 57,  154 => 56,  152 => 55,  144 => 49,  126 => 45,  122 => 44,  115 => 40,  111 => 39,  104 => 37,  100 => 36,  95 => 35,  78 => 34,  64 => 22,  62 => 21,  55 => 17,  51 => 15,  45 => 12,  42 => 11,  40 => 10,  31 => 3,  28 => 2,);
    }
}
