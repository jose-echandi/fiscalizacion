<?php

/* FiscalizacionBundle:Citas:index.html.twig */
class __TwigTemplate_8fb0901a25407a1e962e1fe598c44393a58c2e40d6e9f7fb191b44314e2e20d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle::listar.html.twig");

        $this->blocks = array(
            'main_title' => array($this, 'block_main_title'),
            'headers' => array($this, 'block_headers'),
            'export' => array($this, 'block_export'),
            'columns' => array($this, 'block_columns'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle::listar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_main_title($context, array $blocks = array())
    {
        echo "Citas";
    }

    // line 3
    public function block_headers($context, array $blocks = array())
    {
        // line 4
        echo "    <th>Nº</th>
    <th>RIF</th>   
    <th>D. Comercial </th>
    <th>Tipo de Licencia</th>
    <th>Fecha Cita</th>
    <th>Bloque</th>
    <th>Tipo Operación</th>
    <th>F. Creación</th>
    <th>F. Actualización</th>
    <th>Estatus</th>

";
    }

    // line 16
    public function block_export($context, array $blocks = array())
    {
        // line 17
        echo "      ";
        echo twig_include($this->env, $context, "ExportBundle::iconslink.html.twig", array("pdf" => "exportpdf_citas", "xcel" => "exportxls_citas"));
        echo "
";
    }

    // line 19
    public function block_columns($context, array $blocks = array())
    {
        // line 20
        echo "    ";
        $context["centro"] = (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"))) : ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora")));
        // line 21
        echo "    <tr>
        <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
        echo "</td>
        <td>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["centro"]) ? $context["centro"] : null), "persJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["centro"]) ? $context["centro"] : null), "rif"), "html", null, true);
        echo "</td>
        <td>";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["centro"]) ? $context["centro"] : null), "denominacionComercial"), "html", null, true);
        echo "</td>
        <td>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ClasLicencia"), "html", null, true);
        echo "</td>
        <td>";
        // line 26
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "fechaSolicitud"), "d/m/Y"), "html", null, true);
        echo " </td>
        <td>";
        // line 27
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "fechaSolicitud"), "A"), "html", null, true);
        echo " </td>
        <td>";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tipoSolicitud"), "html", null, true);
        echo "</td>
        <td>";
        // line 29
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaSolicitud"), "d/m/Y"), "html", null, true);
        echo "</td>
        <td>";
        // line 30
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaSolicitud"), "d/m/Y"), "html", null, true);
        echo "</td>
        <td>";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status"), "html", null, true);
        echo "</td>

    </tr>
";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Citas:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 31,  108 => 30,  104 => 29,  100 => 28,  96 => 27,  92 => 26,  88 => 25,  84 => 24,  78 => 23,  74 => 22,  71 => 21,  68 => 20,  65 => 19,  58 => 17,  55 => 16,  40 => 4,  37 => 3,  31 => 2,);
    }
}
