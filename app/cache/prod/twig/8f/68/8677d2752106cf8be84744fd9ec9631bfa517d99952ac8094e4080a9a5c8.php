<?php

/* LicenciaBundle:AdmTiposLicencias:show.html.twig */
class __TwigTemplate_8f688677d2752106cf8be84744fd9ec9631bfa517d99952ac8094e4080a9a5c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/genstyles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"print\">

";
    }

    // line 9
    public function block_content_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"tit_principal\">Tipo de Licencia</div>

    <div class=\"col-md-12\">
        <table class=\"record_properties table table-condensed\">
            <thead>
                <tr>
                    <th colspan=\"2\" >Datos del Tipo de Licencia</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Tipo de Licencia</th>
                    <td>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tipoLicencia"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th>Tipo Usuario</th>
                    <td>";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "roleType"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th>Estatus</th>
                    <td>";
        // line 31
        echo ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == 1)) ? ("Activo") : ("Inactivo"));
        echo "</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class=\"col-md-6 col-md-offset-2 form-group btn-group\">
        <div class=\"col-md-6\" style=\"text-align:center\"><a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("admtiposlicencias");
        echo "\" class=\"btn btn-primary btn-sm \">Regresar</a></div>";
        // line 39
        echo "        <div class=\"col-md-6\" style=\"text-align:center\"><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admtiposlicencias_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
        echo "\" class=\"btn btn-success btn-sm \">Modificar</a></div>
    </div>

    <!-- Modal -->
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Eliminar el Registro</h4>
                </div>
                <div class=\"modal-body\">
                    Realmente desea eliminar el registro <b>\"";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tipoLicencia"), "html", null, true);
        echo "\"</b>?
                </div>
                <div class=\"modal-footer\">
                    ";
        // line 54
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_start');
        echo "
                    ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["delete_form"]) ? $context["delete_form"] : null), "submit"), 'widget');
        echo "
                    ";
        // line 56
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'form_end');
        echo "
                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "LicenciaBundle:AdmTiposLicencias:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 56,  118 => 55,  114 => 54,  108 => 51,  92 => 39,  89 => 37,  80 => 31,  73 => 27,  66 => 23,  51 => 10,  48 => 9,  41 => 5,  37 => 4,  32 => 3,  29 => 2,);
    }
}
