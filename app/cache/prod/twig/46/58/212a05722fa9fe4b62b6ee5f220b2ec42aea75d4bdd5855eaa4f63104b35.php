<?php

/* CentrohipicoBundle:DataOperadora:newOffice.html.twig */
class __TwigTemplate_4658212a05722fa9fe4b62b6ee5f220b2ec42aea75d4bdd5855eaa4f63104b35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <br/><br/>

<div class=\"tab-content\">
    <div class=\"tab-pane ";
        // line 6
        if ((array_key_exists("tab", $context) && (isset($context["tab"]) ? $context["tab"] : null))) {
            echo "active";
        }
        echo "\" id=\"form\">
    <div class=\"col-md-12\">
        <h1 class=\"tit_principal\">Datos Operadora</h1>
    </div>
    <div class=\"col-md-12\"><p>Los campos con <span class=\"oblig\">(*)</span> son obligatorios. Por favor ingrese los todos los Datos de la oficina principal para guardar</p></div>
    <div class=\"row col-lg-12\">
        ";
        // line 12
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "form_dop")));
        echo "
        ";
        // line 13
        $this->env->loadTemplate("CentrohipicoBundle:DataOperadora:DOpertable_form.html.twig")->display($context);
        // line 14
        echo "        <div class=\"col-md-12 form-group btn-group\">
            <div style=\"float: right\">
                <button id=\"form_btn\" type=\"submit\" class=\"btn btn-primary btn-sm\">Guardar</button>
            </div>
        </div>
    </div>   
    ";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
    </div>

  ";
        // line 23
        if ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id")) {
            // line 24
            echo "
      ";
            // line 25
            $this->env->loadTemplate("CentrohipicoBundle:DataOperadora:verDatos.html.twig")->display($context);
            // line 26
            echo "
    ";
            // line 28
            echo "        ";
            // line 29
            echo "            ";
            // line 30
            echo "        ";
            // line 31
            echo "        ";
            // line 32
            echo "            ";
            // line 33
            echo "                ";
            // line 34
            echo "                ";
            // line 35
            echo "                    ";
            // line 36
            echo "                ";
            // line 37
            echo "                ";
            // line 38
            echo "                ";
            // line 39
            echo "                ";
            // line 40
            echo "                    ";
            // line 41
            echo "                    ";
            // line 42
            echo "                ";
            // line 43
            echo "                ";
            // line 44
            echo "                    ";
            // line 45
            echo "                    ";
            // line 46
            echo "                ";
            // line 47
            echo "                ";
            // line 48
            echo "                    ";
            // line 49
            echo "                    ";
            // line 50
            echo "                ";
            // line 51
            echo "                ";
            // line 52
            echo "                    ";
            // line 53
            echo "                    ";
            // line 54
            echo "                ";
            // line 55
            echo "                ";
            // line 56
            echo "                    ";
            // line 57
            echo "                    ";
            // line 58
            echo "                ";
            // line 59
            echo "                ";
            // line 60
            echo "                    ";
            // line 61
            echo "                    ";
            // line 62
            echo "                ";
            // line 63
            echo "                ";
            // line 64
            echo "                    ";
            // line 65
            echo "                    ";
            // line 66
            echo "                ";
            // line 67
            echo "                ";
            // line 68
            echo "                    ";
            // line 69
            echo "                    ";
            // line 70
            echo "                ";
            // line 71
            echo "                ";
            // line 72
            echo "                    ";
            // line 73
            echo "                    ";
            // line 74
            echo "                ";
            // line 75
            echo "                ";
            // line 76
            echo "                    ";
            // line 77
            echo "                    ";
            // line 78
            echo "                ";
            // line 79
            echo "                ";
            // line 80
            echo "                    ";
            // line 81
            echo "                    ";
            // line 82
            echo "                ";
            // line 83
            echo "                ";
            // line 84
            echo "                    ";
            // line 85
            echo "                ";
            // line 86
            echo "                ";
            // line 87
            echo "                    ";
            // line 88
            echo "                    ";
            // line 89
            echo "                            ";
            // line 90
            echo "                        ";
            // line 91
            echo "                    ";
            // line 92
            echo "                ";
            // line 93
            echo "                ";
            // line 94
            echo "                    ";
            // line 95
            echo "                    ";
            // line 96
            echo "                            ";
            // line 97
            echo "                        ";
            // line 98
            echo "                    ";
            // line 99
            echo "                ";
            // line 100
            echo "                ";
            // line 101
            echo "                    ";
            // line 102
            echo "                    ";
            // line 103
            echo "                ";
            // line 104
            echo "                ";
            // line 105
            echo "                    ";
            // line 106
            echo "                    ";
            // line 107
            echo "                            ";
            // line 108
            echo "                        ";
            // line 109
            echo "                    ";
            // line 110
            echo "                ";
            // line 111
            echo "                ";
            // line 112
            echo "                    ";
            // line 113
            echo "                    ";
            // line 114
            echo "                ";
            // line 115
            echo "                ";
            // line 116
            echo "                    ";
            // line 117
            echo "                    ";
            // line 118
            echo "                ";
            // line 119
            echo "                ";
            // line 120
            echo "                    ";
            // line 121
            echo "                    ";
            // line 122
            echo "                ";
            // line 123
            echo "                ";
            // line 124
            echo "                    ";
            // line 125
            echo "                    ";
            // line 126
            echo "                ";
            // line 127
            echo "                ";
            // line 128
            echo "                    ";
            // line 129
            echo "                    ";
            // line 130
            echo "                ";
            // line 131
            echo "                ";
            // line 132
            echo "            ";
            // line 133
            echo "        ";
            // line 134
            echo "     ";
        } elseif (array_key_exists("message", $context)) {
            // line 135
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>";
            // line 138
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
            echo "</h5> <a href=\"";
            echo $this->env->getExtension('routing')->getPath("operadora_new");
            echo "\">Agregar Representante legal</a></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 143
        echo "    </div>


";
        // line 146
        if ((!(isset($context["tab"]) ? $context["tab"] : null))) {
            // line 147
            echo "        <div class=\"col-md-12\">
            <div style=\"float: left\">
                <a href=\"";
            // line 149
            echo $this->env->getExtension('routing')->getPath("operadora");
            echo "\" class=\"btn btn-success btn-sm\">Regresar</a>
            </div>
            <div style=\"float: right\">
                <a href=\"#datos\"  class=\"btn btn-default\" role=\"tab\" data-toggle=\"tab\">Ver</a>
                <a href=\"#form\" class=\"btn btn-primary\" role=\"tab\" data-toggle=\"tab\">Editar</a>
            </div>
        </div>
";
        }
        // line 157
        echo "    </div>

    ";
    }

    // line 160
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 161
        echo "    <script type=\"text/javascript\" >
        \$(document).ready(function(){
            \$('#tabs').tabs();
            \$(\"#dataoper #datalegal\").show();
        });
    </script>
    <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/resource.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/DataOperadora/validate.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" >
        \$(document).ready(function(){
            //\$('#tabs').tabs();
            \$(\".form-horizontal\").removeClass('form-horizontal');
            \$(\".form-control\").removeClass('form-control');
            \$(\"#dataoper #datalegal\").show();
            \$(\"#temp\").hide();
            \$(\"#addDL\").click(function(){
                agregarDL();
            });
        });

        function agregarDL(){
            \$(\"#btnDL\").hide();
            \$(\"#fDL\").show();
            \$(\"#fDL\").html(getGifLoading());
            //var route=Routing.generate('datalegal_new');
            \$.get('";
        // line 186
        echo $this->env->getExtension('routing')->getPath("datalegal_new");
        echo "').success(function(data) {
                if (data.message) {
                    message = data.message;
                } else {
                    \$('#datalegal').html(data);
                }
            }).error(function(data, status, headers, config) {
                        if (status === '500') {
                            message = \"No hay conexión con el servidor\";
                        }
                    });
        }
        function sFormDL(){
            var datach=\$(\"#form_dl\").serialize();
            \$(\"#form_dl\").hide();
            \$(\"#fDL\").hide();
            \$(\"#btnDL\").show();
            //\$.post()
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataOperadora:newOffice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  372 => 186,  351 => 168,  347 => 167,  339 => 161,  336 => 160,  330 => 157,  319 => 149,  315 => 147,  313 => 146,  308 => 143,  298 => 138,  293 => 135,  290 => 134,  288 => 133,  286 => 132,  284 => 131,  282 => 130,  280 => 129,  278 => 128,  276 => 127,  274 => 126,  272 => 125,  270 => 124,  268 => 123,  266 => 122,  264 => 121,  262 => 120,  260 => 119,  258 => 118,  256 => 117,  254 => 116,  252 => 115,  250 => 114,  248 => 113,  246 => 112,  244 => 111,  242 => 110,  240 => 109,  238 => 108,  236 => 107,  234 => 106,  232 => 105,  230 => 104,  228 => 103,  226 => 102,  224 => 101,  222 => 100,  220 => 99,  218 => 98,  216 => 97,  214 => 96,  212 => 95,  210 => 94,  208 => 93,  206 => 92,  204 => 91,  202 => 90,  200 => 89,  198 => 88,  196 => 87,  194 => 86,  192 => 85,  190 => 84,  188 => 83,  186 => 82,  184 => 81,  182 => 80,  180 => 79,  178 => 78,  176 => 77,  174 => 76,  172 => 75,  170 => 74,  168 => 73,  166 => 72,  164 => 71,  162 => 70,  160 => 69,  158 => 68,  156 => 67,  154 => 66,  152 => 65,  150 => 64,  148 => 63,  146 => 62,  144 => 61,  142 => 60,  140 => 59,  138 => 58,  136 => 57,  134 => 56,  132 => 55,  130 => 54,  128 => 53,  126 => 52,  124 => 51,  122 => 50,  120 => 49,  118 => 48,  116 => 47,  114 => 46,  112 => 45,  110 => 44,  108 => 43,  106 => 42,  104 => 41,  102 => 40,  100 => 39,  98 => 38,  96 => 37,  94 => 36,  92 => 35,  90 => 34,  88 => 33,  86 => 32,  84 => 31,  82 => 30,  80 => 29,  78 => 28,  75 => 26,  73 => 25,  70 => 24,  68 => 23,  62 => 20,  54 => 14,  52 => 13,  48 => 12,  37 => 6,  32 => 3,  29 => 2,);
    }
}
