<?php

/* CentrohipicoBundle:DataOperadora:new.html.twig */
class __TwigTemplate_a82bd2a598d3751e9c6b0d4439dc7d7cccf559b4c992601a755b8ec6a68b7e1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "     <br/><br/>

<div class=\"tab-content\">
    <div class=\"tab-pane ";
        // line 6
        if ((array_key_exists("tab", $context) && (isset($context["tab"]) ? $context["tab"] : null))) {
            echo "active";
        }
        echo "\" id=\"form\">
    <div class=\"col-md-12 \">
        <h1 class=\"tit_principal\">Datos del Representante Legal</h1>
    </div>
    <div class=\"col-md-12\"><p>Los campos con <span class=\"oblig\">(*)</span> son obligatorios. Por favor ingrese los todos los Datos Legales para guardar</p></div>
    <div class=\"row col-lg-12\">
        ";
        // line 12
        if (array_key_exists("form", $context)) {
            // line 13
            echo "            ";
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "form_dch")));
            echo "
            ";
            // line 14
            $this->env->loadTemplate("CentrohipicoBundle:DataOperadora:DLtable_form.html.twig")->display($context);
            // line 15
            echo "            <div class=\"col-md-12 form-group btn-group\">
                <div style=\"float: right\">
                    <button id=\"form_btn\" type=\"submit\" class=\"btn btn-primary btn-sm\">Guardar</button>
                </div>
            </div>
            ";
            // line 20
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
            echo "
        ";
        }
        // line 22
        echo "    </div>    
    </div>

    ";
        // line 25
        if (array_key_exists("entity", $context)) {
            // line 26
            echo "    <div class=\"tab-pane ";
            if (((!array_key_exists("tab", $context)) || (!(isset($context["tab"]) ? $context["tab"] : null)))) {
                echo "active";
            }
            echo "\" id=\"datos\">
            <div class=\"col-md-12\">
                <h1 class=\"tit_principal\">Representante Legal</h1>
            </div>
            <div class=\"col-md-12\">
                <table  class=\"record_properties table table-condensed\">
                    <thead>
                    <tr>
                        <th colspan=\"2\" >Detalles Legales</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>RIF</th>
                        <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Cedula</th>
                        <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ci"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Nombre</th>
                        <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nombre"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Apellido</th>
                        <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "apellido"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Fax</th>
                        <td>";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fax"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Teléfono Fijo</th>
                        <td>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfFijo"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tflFijo"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Teléfono Celular</th>
                        <td>";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfCelular"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tflCelular"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "email"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Página Web</th>
                        <td>";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "pagWeb"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th colspan=\"2\" >Datos Legales Direccion </th>
                    </tr>
                    <tr>
                        <th>Estado</th>
                        <td>";
            // line 79
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "estado")) > 0)) {
                // line 80
                echo "                                ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "estado"), "nombre"), "html", null, true);
                echo "
                            ";
            }
            // line 82
            echo "                        </td>
                    </tr>
                    <tr>
                        <th>Municipio</th>
                        <td>";
            // line 86
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "municipio")) > 0)) {
                // line 87
                echo "                                ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "municipio"), "nombre"), "html", null, true);
                echo "
                            ";
            }
            // line 89
            echo "                        </td>
                    </tr>
                    <tr>
                        <th>Ciudad</th>
                        <td>";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ciudad"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Parroquia</th>
                        <td>";
            // line 97
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "parroquia")) > 0)) {
                // line 98
                echo "                                ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "parroquia"), "nombre"), "html", null, true);
                echo "
                            ";
            }
            // line 100
            echo "                        </td>
                    </tr>
                    <tr>
                        <th>Av/Calle/Carretera</th>
                        <td>";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "avCalleCarrera"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Edif/Casa</th>
                        <td>";
            // line 108
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "edifCasa"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Oficina/Apto/No</th>
                        <td>";
            // line 112
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ofcAptoNum"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Punto Referencia</th>
                        <td>";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "puntoReferencia"), "html", null, true);
            echo "</td>
                    </tr>
                    <tr>
                        <th>Código Postal</th>
                        <td>";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codigoPostal"), "html", null, true);
            echo "</td>
                    </tr>
                    </tbody>
                </table>
            </div>
    </div>
    ";
        }
        // line 127
        echo "</div>
    ";
        // line 128
        if ((!(isset($context["tab"]) ? $context["tab"] : null))) {
            // line 129
            echo "        <div class=\"col-md-12 form-group btn-group\">
            <div style=\"float: left\">
                <a href=\"";
            // line 131
            echo $this->env->getExtension('routing')->getPath("operadora");
            echo "\" class=\"btn btn-success btn-sm\">Regresar</a>
            </div>
            <div style=\"float: right\">
                <a href=\"#datos\"  class=\"btn btn-default\" role=\"tab\" data-toggle=\"tab\">Ver</a>
                <a href=\"#form\" class=\"btn btn-primary\" role=\"tab\" data-toggle=\"tab\">Editar</a>
            </div>
        </div>
    ";
        }
        // line 139
        echo "
";
    }

    // line 142
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 143
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/resource.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/DataOperadora/validate.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" >
        \$(document).ready(function(){
            \$(\".form-horizontal\").removeClass('form-horizontal');
            \$(\".form-control\").removeClass('form-control');
            \$(\"#dataoper #datalegal\").show();
            \$(\"#temp\").hide();
            \$(\"#addDL\").click(function(){
                agregarDL();
            });
        });

        function agregarDL(){
            \$(\"#btnDL\").hide();
            \$(\"#fDL\").show();
            \$(\"#fDL\").html(getGifLoading());
            //var route=Routing.generate('datalegal_new');
            \$.get('";
        // line 161
        echo $this->env->getExtension('routing')->getPath("datalegal_new");
        echo "').success(function(data) {
                if (data.message) {
                    message = data.message;
                } else {
                    \$('#datalegal').html(data);
                }
            }).error(function(data, status, headers, config) {
                        if (status === '500') {
                            message = \"No hay conexión con el servidor\";
                        }
                    });
        }
        function sFormDL(){
            var datach=\$(\"#form_dl\").serialize();
            \$(\"#form_dl\").hide();
            \$(\"#fDL\").hide();
            \$(\"#btnDL\").show();
            //\$.post()
        }
    </script>


";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataOperadora:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  308 => 161,  288 => 144,  283 => 143,  280 => 142,  275 => 139,  264 => 131,  260 => 129,  258 => 128,  255 => 127,  245 => 120,  238 => 116,  231 => 112,  224 => 108,  217 => 104,  211 => 100,  205 => 98,  203 => 97,  196 => 93,  190 => 89,  184 => 87,  182 => 86,  176 => 82,  170 => 80,  168 => 79,  158 => 72,  151 => 68,  142 => 64,  133 => 60,  126 => 56,  119 => 52,  112 => 48,  105 => 44,  96 => 40,  76 => 26,  74 => 25,  69 => 22,  64 => 20,  57 => 15,  55 => 14,  50 => 13,  48 => 12,  37 => 6,  32 => 3,  29 => 2,);
    }
}
