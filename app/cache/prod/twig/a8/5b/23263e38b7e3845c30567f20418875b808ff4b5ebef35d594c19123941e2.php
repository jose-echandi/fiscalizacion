<?php

/* CentrohipicoBundle:SolicitudAfiliacion:detalles.html.twig */
class __TwigTemplate_a85b23263e38b7e3845c30567f20418875b808ff4b5ebef35d594c19123941e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_base' => array($this, 'block_script_base'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"page-header\">
    <h1>Detalles de la solicitud de afiliado</h1>
</div>

<div class=\"padding-15\" id=\"contenido\">

    <table id=\"tabla_reporte2\">
        <tbody><tr id=\"table_header2\">
            <td colspan=\"4\">Datos de la solicitud</td>
        </tr>
        <tr>
            <td>Fecha de Solicitud:</td>
            <td colspan=\"3\">";
        // line 16
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "m/d/Y"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td>RIF Afiliado:</td>
            <td colspan=\"3\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "persJuridica"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "rif"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td>Licencia a Afiliar:</td>
            <td colspan=\"3\">
                ";
        // line 25
        if ((!(null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getClasLicencia")))) {
            // line 26
            echo "                ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getClasLicencia"), "clasfLicencia"), "html", null, true);
            echo "
                ";
        } else {
            // line 28
            echo "                    Sin detalle
                ";
        }
        // line 30
        echo "            </td>
        </tr>
        <tr>
            <td>Denominación Comercial:</td>
            <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "denominacionComercial"), "html", null, true);
        echo "</td>
            <td>Clasificación del Establecimiento:</td>
            <td>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "clasificacionLocal"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td style=\"width:25%;\">Estado:</td>
            <td style=\"width:25%;\">";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "estado"), "nombre"), "html", null, true);
        echo "</td>
            <td style=\"width:25%;\">Ciudad:</td>
            <td style=\"width:25%;\">";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "ciudad"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td style=\"width:25%;\">Municipio:</td>
            <td style=\"width:25%;\">";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "municipio"), "nombre"), "html", null, true);
        echo "</td>
            <td style=\"width:25%;\">Urbanización/Sector:</td>
            <td style=\"width:25%;\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "urbanSector"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td style=\"width:25%;\">Avenida/Calle/Carrera:</td>
            <td style=\"width:25%;\">";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "avCalleCarrera"), "html", null, true);
        echo "</td>
            <td style=\"width:25%;\">Edificio/Casa:</td>
            <td style=\"width:25%;\">";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "edifCasa"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td style=\"width:25%;\">Oficina/Apto/No.:</td>
            <td style=\"width:25%;\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "ofcAptoNum"), "html", null, true);
        echo "</td>
            <td style=\"width:25%;\">Punto de Referencia:</td>
            <td style=\"width:25%;\">";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "puntoReferencia"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td>Contrato firmado con el Afiliado:</td>
            <td colspan=\"3\"> ";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "contratoFirmado"), "CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig"), "html", null, true);
        echo " </td>
        </tr>
        <tr>
            <td>Buena Pro:</td>
            <td colspan=\"3\"> ";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "buenaPro"), "CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig"), "html", null, true);
        echo " </td>
        </tr>
        ";
        // line 70
        if ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == "Rechazado") || ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == "Aprobado"))) {
            // line 71
            echo "            <tr>
                <td>Estatus:</td>
                <td colspan=\"3\"> ";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status"), "html", null, true);
            echo "</td>
            </tr>
            ";
            // line 75
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == "Rechazado")) {
                // line 76
                echo "                <tr>
                    <td>Nota:</td>
                    <td colspan=\"3\"> <textarea rows=\"4\" cols=\"100\" disabled=\"disabled\"> ";
                // line 78
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "notas"), "html", null, true);
                echo " </textarea></td>
                </tr>
            ";
            }
            // line 81
            echo "        ";
        }
        // line 82
        echo "        </tbody>
    </table>
    <div class=\"block-separator col-md-12\"></div>
    <div class=\"col-md-offset-8 col-md-4 form-group text-right\">
        <a href=\"";
        // line 86
        echo $this->env->getExtension('routing')->getPath("Centrohipico_solicitud_afiliacion_listado");
        echo "\" class=\"btn btn-warning btn-sm\">Regresar</a>
        &nbsp; &nbsp; &nbsp; &nbsp;
    </div>
    

    ";
        // line 91
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method")) {
            // line 92
            echo "        ";
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == "Por aprobar")) {
                // line 93
                echo "            <div>
                <input type=\"submit\" value=\"Aceptar solicitud\" class=\"\" id=\"form_btttn\" style=\"float: right;height: auto;margin: 10px;padding: 5px;\">
                <input type=\"submit\" value=\"Rechazar solicitud\" data-toggle=\"modal\" data-target=\"#confirmar-rechazar\" class=\"\" id=\"form_btttn\" style=\"float: right;height: auto;margin: 10px;padding: 5px;\">
            </div>
        ";
            }
            // line 98
            echo "    ";
        }
        // line 99
        echo "
    <div class=\"modal fade\" id=\"confirmar-rechazar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Rechazar Solicitud
                </div>
                <div class=\"modal-body\">
                    <label for=\"form_rechazar_nota\"> Agregue el motivo por el cual se rechaza la solicitud:</label><br>
                    <textarea rows=\"5\" cols=\"65\" id=\"form_rechazar_nota\" name=\"form_rechazar[nota]\"> </textarea>
                    <div style=\"color: #E70202;display: none\" id=\"nota_minimum\"></div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" onclick=\"reject(";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
        echo ")\" class=\"btn btn-danger danger\">Rechazar</a>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"confirm-active\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Aprobar solicitudes
                </div>
                <div class=\"modal-body\">
                    Está seguro de aprobar la solicitud?
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" onclick=\"approve(";
        // line 130
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
        echo ")\" class=\"btn btn-info info\">Aprobar</a>
                </div>
            </div>
        </div>
    </div>

    ";
    }

    // line 138
    public function block_script_base($context, array $blocks = array())
    {
        // line 139
        echo "        <script>

            function reject(id)
            {
                var nota  = \$(\"#form_rechazar_nota\").val();

                if(nota.length > 3)
                {
                    var url = Routing.generate('Centrohipico_solicitud_afiliacion_rechazar');
                    \$.post( url, { id: id, nota: nota } );
                    \$(\"#nota_minimum\").hide();
                    \$('#confirmar-rechazar').modal('hide');
                    var url = Routing.generate('Centrohipico_solicitud_afiliacion_detalles', {'id': id} );
                    window.location = url;
                }else{
                    if(nota.length == 1){
                        var html = \"La nota no puede estar vac&iacute;a\";
                        \$(\"#nota_minimum\").show();
                        \$(\"#nota_minimum\").html(html);
                    }
                    var html = \"La nota debe contener al menos 4 digitos\";
                    \$(\"#nota_minimum\").show();
                    \$(\"#nota_minimum\").html(html);
                }
            }


            function approve(id)
            {
                var arrayData= new Array();
                arrayData.push(id);
                \$('#confirm-active').modal('hide');
                var url = Routing.generate('Centrohipico_solicitud_afiliacion_aprobar');
                \$.post(url, {'ids': arrayData}, function(){
                }).done(function() {
                    var url = Routing.generate('Centrohipico_solicitud_afiliacion_detalles', {'id': id} );
                    window.location = url;
                });
            }


        </script>
    ";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:SolicitudAfiliacion:detalles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 139,  255 => 138,  244 => 130,  224 => 113,  208 => 99,  205 => 98,  198 => 93,  195 => 92,  193 => 91,  185 => 86,  179 => 82,  176 => 81,  170 => 78,  166 => 76,  164 => 75,  159 => 73,  155 => 71,  153 => 70,  148 => 68,  141 => 64,  134 => 60,  129 => 58,  122 => 54,  117 => 52,  110 => 48,  105 => 46,  98 => 42,  93 => 40,  86 => 36,  81 => 34,  75 => 30,  71 => 28,  65 => 26,  63 => 25,  53 => 20,  46 => 16,  32 => 4,  29 => 3,);
    }
}
