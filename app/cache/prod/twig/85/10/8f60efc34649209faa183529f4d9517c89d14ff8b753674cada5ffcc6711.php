<?php

/* ExportBundle:Operadoras:operafiliados.html.twig */
class __TwigTemplate_85108f60efc34649209faa183529f4d9517c89d14ff8b753674cada5ffcc6711 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("ExportBundle::export_pdf.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ExportBundle::export_pdf.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        ";
        // line 5
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method")) {
            echo "<h1>Aprobar Incorporaci&oacute;n de Afiliados</h1> ";
        } else {
            echo " <h1>Listado de solicitudes </h1> ";
        }
        // line 6
        echo "    </div>
    <div class=\"block-separator col-sm-12\"></div>
    ";
        // line 8
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 9
            echo "    <div class=\"col-md-12\">
        ";
            // line 10
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method")) {
                // line 11
                echo "            <input style=\"float: right;height: auto;margin: 0 15px 5px 20px;padding: 5px;\" disabled=\"disabled\" type=\"button\" id=\"activeMultiple\" value=\"Aplicar\">
        ";
            }
            // line 13
            echo "        <table class=\"table table-condensed table-striped\">
            <thead>
            <tr>
                <th>Nº</th>
                <th>RIF</th>
                <th>Denominación Comercial</th>
                ";
            // line 20
            echo "                <th>Licencia</th>
                <th>Operadora</th>
                <th>Estatus Adscrito</th>
            </tr>
            </thead>
            <tbody>
            ";
            // line 26
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 27
                echo "            <tr id=\"request-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                echo "\">
                <td>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                echo "</td>
                <td>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
                echo "-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
                echo "</td>
                <td>";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
                echo "</td>
                ";
                // line 32
                echo "                <td>";
                if ((!(null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licencia")))) {
                    // line 33
                    echo "                    ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licencia"), "html", null, true);
                    echo "
                    ";
                }
                // line 35
                echo "                </td>
                <td>";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "html", null, true);
                echo "</td>
                <td>";
                // line 37
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status"), "html", null, true);
                echo "</td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 40
            echo "            </tbody>
        </table>
    </div>
        ";
        } else {
            // line 44
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>";
            // line 47
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method")) {
                echo "No existen solicitudes por aprobar";
            } else {
                echo "No se encontraron resultados.";
            }
            echo "</h5></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 52
        echo "    <div class=\"block-separator col-sm-12\"></div>

";
    }

    public function getTemplateName()
    {
        return "ExportBundle:Operadoras:operafiliados.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 52,  131 => 47,  126 => 44,  120 => 40,  111 => 37,  107 => 36,  104 => 35,  98 => 33,  95 => 32,  91 => 30,  85 => 29,  81 => 28,  76 => 27,  72 => 26,  64 => 20,  56 => 13,  52 => 11,  50 => 10,  47 => 9,  45 => 8,  41 => 6,  35 => 5,  31 => 3,  28 => 2,);
    }
}
