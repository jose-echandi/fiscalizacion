<?php

/* UserBundle:Notification:resetting.email.html.twig */
class __TwigTemplate_ac516aa12cc1573eb6bc0c5de9850f8898cccd0ae2fcfd6e1673266f3cd87ddd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("UserBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UserBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    Estimado(a) ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "getFullname"), "html", null, true);
        echo ",<br><br>
    Para restablecer su contraseña, haga clic en el siguiente enlace: ";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "UserBundle:Notification:resetting.email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,  53 => 12,  50 => 11,  45 => 8,  39 => 4,  36 => 4,  32 => 11,  29 => 10,  27 => 8,  24 => 7,  22 => 2,);
    }
}
