<?php

/* SolicitudesCitasBundle:DataSolicitudes:editCitaOk.html.twig */
class __TwigTemplate_407b95bcbffd16bc2f27e3ec246b01e7febeb6656ed285e0cd9c9500f9944e1f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SolicitudesCitasBundle::solicitud_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"contendor\">
        <div class=\"block-separator col-md-12\"></div>
\t<div class=\"tit_principal\">Modificar Fecha de Cita</div>
            <br /><br />
            <div class=\"col-md-12\">
               <div class=\"col-md-4\"> 
                   <h3>Su cita ha Sido Actualizada</h3>
               </div>  
               <div class=\"block-separator col-md-12\"></div>
                ";
        // line 14
        echo " 
            </div>
    </div>
 ";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:editCitaOk.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 14,  31 => 3,  28 => 2,  73 => 38,  69 => 37,  52 => 23,  48 => 22,  44 => 21,  22 => 2,  19 => 1,);
    }
}
