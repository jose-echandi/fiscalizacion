<?php

/* NewTemplateBundle:Menu:mnu_fiscal.html.twig */
class __TwigTemplate_ef089e82682130e59d22c58742b172e70f71a47492503573ccced1befae21cb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3>Licencias</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("fiscal_citas_listado", array("tipo" => "Solicitada"));
        echo "\" title=\"Citas asignadas\">Citas asignadas</a></li>
    ";
        // line 5
        echo "</ul>
<h3>Fiscalizaciones</h3>
<ul class=\"menu2\">
        <li><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("providencia");
        echo "\" title=\"Providencias\">Providencias</a></li>
        <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("fiscalizacion");
        echo "\" title=\"Fiscalización\">Generar Fiscalización</a></li>
        ";
        // line 11
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle:Menu:mnu_fiscal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 11,  36 => 9,  32 => 8,  27 => 5,  23 => 3,  19 => 1,);
    }
}
