<?php

/* SolicitudesCitasBundle::solicitud_base.html.twig */
class __TwigTemplate_5aeb1895fcdf1b49c9c0c0b027cb8192e29293d1c4225b5bd6d00a432fa4dab2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 6
    public function block_content_content($context, array $blocks = array())
    {
        echo "    
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 6,  216 => 119,  210 => 117,  207 => 116,  176 => 89,  170 => 86,  134 => 52,  128 => 50,  125 => 49,  123 => 48,  119 => 47,  111 => 41,  105 => 38,  102 => 37,  100 => 36,  96 => 35,  93 => 34,  85 => 28,  80 => 25,  73 => 23,  67 => 21,  62 => 19,  57 => 18,  55 => 17,  49 => 15,  45 => 14,  32 => 3,  29 => 2,);
    }
}
