<?php

/* ReporteBundle:Reporte:reportePorMes.html.twig */
class __TwigTemplate_665bb691585769d395f2e89a40fc74331026b623e19223db3b2c24d4f439d540 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("ReporteBundle::base.html.twig");

        $this->blocks = array(
            'content_reporte' => array($this, 'block_content_reporte'),
            'script_reporte' => array($this, 'block_script_reporte'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ReporteBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_reporte($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-5\">
    <canvas id=\"grafico\" width=\"300\" height=\"300\"></canvas>
</div>
<div class=\"col-md-7\">
    <div id=\"dataTable\">
        <table id=\"tablaDatos\" class=\"table table-condensed table-striped\">
            <caption><b><b></caption>
            <thead>
                <tr>
                    <th class=\"col-md-1\"></th>
                    <th class=\"col-md-5 text-left\"></th>
                    <th class=\"col-md-3\"></th>
                    <th class=\"col-md-3\"></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th class=\"text-left\">Totales</th>
                    <th class=\"text-right\"></th>
                    <th class=\"text-right\"></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
";
    }

    // line 34
    public function block_script_reporte($context, array $blocks = array())
    {
        // line 35
        echo "    
            \$('.filtroPorMes').show();
            \$('#porMes').prop('checked', true).parent('label').addClass('btn-info');
            \$(\"#xMes\").hide();
            \$(\"#xEstado\").show();
            \$(\"#xAnio\").show();

            var d = new Date();
            for(var a=d.getFullYear(); a>=1990; a--){
                \$('#anio').append('<option value=\"'+a+'\">'+a+'</option>');
            }
            
            ";
        // line 47
        if ((array_key_exists("anio", $context) && (!twig_test_empty((isset($context["anio"]) ? $context["anio"] : null))))) {
            // line 48
            echo "                \$('#anio > option:contains(\"";
            echo twig_escape_filter($this->env, (isset($context["anio"]) ? $context["anio"] : null), "html", null, true);
            echo "\")').prop('selected', true);
            ";
        }
        // line 50
        echo "
            ";
        // line 51
        if ((array_key_exists("mes", $context) && (!twig_test_empty((isset($context["mes"]) ? $context["mes"] : null))))) {
            // line 52
            echo "                \$('#mes > option:eq(";
            echo twig_escape_filter($this->env, ((isset($context["mes"]) ? $context["mes"] : null) - 1), "html", null, true);
            echo ")').prop('selected', true);
            ";
        } else {
            // line 54
            echo "                \$('#mes > option:eq('+d.getMonth()+')').prop('selected', true);
            ";
        }
        // line 56
        echo "            
            var encab = ";
        // line 57
        echo (isset($context["encabezado"]) ? $context["encabezado"] : null);
        echo ";
            var report = ";
        // line 58
        echo (isset($context["data"]) ? $context["data"] : null);
        echo ";
            var data = report.data;
            var total = report.total;
            
            var chart = new Chart(\$(\"#grafico\").get(0).getContext(\"2d\"));
            chart.Pie(data);
            
            \$('#tablaDatos > caption > b').html( \$('#mes > option:selected').html() + ' ' + \$('#anio').val() );

            \$('#tablaDatos > thead > tr > th:eq(1)').html(encab.descripcion);
            \$('#tablaDatos > thead > tr > th:eq(2)').html(encab.cantidad);
            \$('#tablaDatos > thead > tr > th:eq(3)').html(encab.porcentaje);
            
            var tabla = \$('#tablaDatos > tbody');
            \$.each(data, function(i,o){
                tabla.append('<tr></tr>');
                var row = \$('#tablaDatos > tbody > tr:last');
                row.append('<td><span style=\"background-color:'+o.color+'\">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>');
                row.append('<td>'+o.label+'</td>');
                ";
        // line 77
        if (((isset($context["reporte"]) ? $context["reporte"] : null) != "ingresos")) {
            // line 78
            echo "                    row.append('<td class=\"text-right\">'+o.value+'</td>');
                ";
        } else {
            // line 80
            echo "                    row.append('<td class=\"text-right\">'+o.value.toFixed(2)+'</td>');
                ";
        }
        // line 82
        echo "                row.append('<td class=\"text-right\">'+o.percent.toFixed(2)+'%</td>');
            });
            
            ";
        // line 85
        if (((isset($context["reporte"]) ? $context["reporte"] : null) != "ingresos")) {
            // line 86
            echo "                \$('#tablaDatos > tfoot > tr > th:eq(2)').html(total.cantidad);
            ";
        } else {
            // line 88
            echo "                \$('#tablaDatos > tfoot > tr > th:eq(2)').html(total.cantidad.toFixed(2));
            ";
        }
        // line 90
        echo "            \$('#tablaDatos > tfoot > tr > th:eq(3)').html(total.porcentaje+'%');

";
    }

    public function getTemplateName()
    {
        return "ReporteBundle:Reporte:reportePorMes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 90,  154 => 88,  150 => 86,  148 => 85,  143 => 82,  139 => 80,  135 => 78,  133 => 77,  111 => 58,  107 => 57,  104 => 56,  100 => 54,  94 => 52,  92 => 51,  89 => 50,  83 => 48,  81 => 47,  67 => 35,  64 => 34,  32 => 4,  29 => 3,);
    }
}
