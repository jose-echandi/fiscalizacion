<?php

/* ReporteBundle::base.html.twig */
class __TwigTemplate_898bb3eb5ee926ac5677e8075eaa28f122da152a53cb61d162b1eba65f6d4ea0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'content_reporte' => array($this, 'block_content_reporte'),
            'script_base' => array($this, 'block_script_base'),
            'script_reporte' => array($this, 'block_script_reporte'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/reporte/css/style.css"), "html", null, true);
        echo "\"/>
<style>
    #table_header td{
        color:white;
    }
</style>

<div id=\"contenido\">
    <div class=\"row col-sm-12\">
        <h1>
            Informes
        </h1>
    </div>

    <form class=\"form-inline\" id=\"form-query\" role=\"form\" action=\"\" method=\"post\">
    <div class=\"informe\">
        <h2>";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["tituloReporte"]) ? $context["tituloReporte"] : null), "html", null, true);
        echo "</h2>
        ";
        // line 21
        $this->displayBlock('content_reporte', $context, $blocks);
        // line 22
        echo "        <div class=\"rango\">
            <ul class=\"select_date\">
                <li>
                    Vista por Mes
                    <select id=\"mes\" name=\"mes\" class=\"form-control\">
                        <option value=\"1\">Enero</option>
                        <option value=\"2\">Febrero</option>
                        <option value=\"3\">Marzo</option>
                        <option value=\"4\">Abril</option>
                        <option value=\"5\">Mayo</option>
                        <option value=\"6\">Junio</option>
                        <option value=\"7\">Julio</option>
                        <option value=\"8\">Agosto</option>
                        <option value=\"9\">Septiembre</option>
                        <option value=\"10\">Octubre</option>
                        <option value=\"11\">Noviembre</option>
                        <option value=\"12\">Diciembre</option>
                    </select>
                </li>
                <li>Año:
                    <select id=\"anio\" name=\"anio\" class=\"form-control\"></select>
                </li>
            </ul>
            <input type=\"button\" id=\"xMes\" style=\"color: black;padding: 4px\" value=\"Ver por mes\" data-url=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("informe_xmes", array("reporte" => "@reporte"));
        echo "\" class=\"btn_informe btn_anio\" title=\"ver por mes\" onclick=\"onClickTipo(this);\" style=\"display: none;\" >
            <input type=\"button\" id=\"xAnio\" style=\"color: black;padding: 4px\" value=\"Ver por año\" data-url=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("informe_xanio", array("reporte" => "@reporte"));
        echo "\" class=\"btn_informe btn_anio\" title=\"ver por año\" onclick=\"onClickTipo(this);\" style=\"display: none;\" >
            <input type=\"button\" id=\"xEstado\" style=\"color: black;padding: 4px\" value=\"Ver por estado\" data-url=\"";
        // line 47
        echo $this->env->getExtension('routing')->getPath("informe_xestado", array("reporte" => "@reporte"));
        echo "\" class=\"btn_informe btn_anio\" title=\"ver por estado\" onclick=\"onClickTipo(this);\" style=\"display: none;\" >

            <input type=\"radio\" class=\"btn_informe btn_anio\" id=\"porMes\" name=\"tipo\" value=\"mes\" data-url=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("informe_xmes", array("reporte" => "@reporte"));
        echo "\" onclick=\"onClickTipo(this);\" style=\"display: none;\" />
            <input type=\"radio\" class=\"btn_informe btn_anio\" id=\"porAnio\" name=\"tipo\" value=\"anio\" data-url=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("informe_xanio", array("reporte" => "@reporte"));
        echo "\" onclick=\"onClickTipo(this);\" style=\"display: none;\" />
            <input type=\"radio\" class=\"btn_informe btn_anio\" id=\"porEstado\" name=\"tipo\" value=\"estado\" data-url=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("informe_xestado", array("reporte" => "@reporte"));
        echo "\" onclick=\"onClickTipo(this);\" style=\"display: none;\" />

            <ul id=\"datepickerList\" >
                <li> <strong style=\"margin-top: 10px; margin-right: 12px; padding:10px\">Vista por Rango</strong></li>
                <li> Desde:
                    <input readonly=\"readonly\"  style=\"color: black\" type=\"text\" ";
        // line 56
        if (array_key_exists("desde", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, (isset($context["desde"]) ? $context["desde"] : null), "html", null, true);
            echo "\" ";
        } else {
            echo " value=\"\" ";
        }
        echo " name=\"desde\" id=\"datepicker\">
                </li>
                <li> Hasta:
                    <input readonly=\"readonly\"  style=\"color: black\" type=\"text\" ";
        // line 59
        if (array_key_exists("hasta", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, (isset($context["hasta"]) ? $context["hasta"] : null), "html", null, true);
            echo "\" ";
        } else {
            echo " value=\"\" ";
        }
        echo " name=\"hasta\" id=\"datepickerEnd\">
                </li>
                <li>
                    <button  id=\"btnReset\" type=\"button\" onclick=\"reset()\" style=\"color: black;padding: 2px\" >Limpiar</button>
                </li>
            </ul>

            <input type=\"submit\" style=\"color: black;padding: 4px\" value=\"Consultar\" class=\"consultar\">

            <div class=\"div_class\">
                <p><strong style=\"color: white;\">Ver por:</strong></p>
                <ul>
                    ";
        // line 71
        if (((isset($context["reporte"]) ? $context["reporte"] : null) == "licencias")) {
            // line 72
            echo "                        ";
            $this->env->loadTemplate("ReporteBundle:Filtro:licencias.html.twig")->display($context);
            // line 73
            echo "                    ";
        } elseif (((isset($context["reporte"]) ? $context["reporte"] : null) == "fiscalizaciones")) {
            // line 74
            echo "                        ";
            $this->env->loadTemplate("ReporteBundle:Filtro:fiscalizaciones.html.twig")->display($context);
            // line 75
            echo "                    ";
        } elseif (((isset($context["reporte"]) ? $context["reporte"] : null) == "ingresos")) {
            // line 76
            echo "                        ";
            $this->env->loadTemplate("ReporteBundle:Filtro:ingresos.html.twig")->display($context);
            // line 77
            echo "                    ";
        } elseif (((isset($context["reporte"]) ? $context["reporte"] : null) == "usuarios")) {
            // line 78
            echo "                        ";
            $this->env->loadTemplate("ReporteBundle:Filtro:usuarios.html.twig")->display($context);
            // line 79
            echo "
                    ";
        }
        // line 81
        echo "                </ul>
            </div>
        </div>
    </div>
    </form>
</div>

";
    }

    // line 21
    public function block_content_reporte($context, array $blocks = array())
    {
    }

    // line 90
    public function block_script_base($context, array $blocks = array())
    {
        // line 91
        echo "    ";
        $this->displayParentBlock("script_base", $context, $blocks);
        echo "

    <script>
        function reset()
        {
            getElementById(\"form-query\").reset();
            \$('#datepicker').val(' ');
            \$('#datepickerEnd').val(' ');
        }
    </script>


    <script src=\"//code.jquery.com/ui/1.11.1/jquery-ui.js\"></script>
    <script>
        \$(document).ready(function(){
            \$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
            \$('#datepickerEnd').datepicker({ dateFormat: 'yy-mm-dd' });
        });
    </script>


    ";
        // line 112
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "79dc8c9_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_79dc8c9_0") : $this->env->getExtension('assets')->getAssetUrl("js/79dc8c9_Chart.min_1.js");
            // line 113
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "79dc8c9"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_79dc8c9") : $this->env->getExtension('assets')->getAssetUrl("js/79dc8c9.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        // line 115
        echo "
    <script>
        \$(document).ready(function(){
            Chart.defaults.global = {
                animation: true,
                animationSteps: 30,
                animationEasing: \"easeOutQuart\",
                showScale: true,
                scaleOverride: false,
                scaleSteps: null,
                scaleStepWidth: null,
                scaleStartValue: null,
                scaleLineColor: \"rgba(0,0,0,.1)\",
                scaleLineWidth: 1,
                scaleShowLabels: true,
                scaleLabel: \"<%=value%>\",
                scaleIntegersOnly: true,
                scaleBeginAtZero: false,
                scaleFontFamily: \"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif\",
                scaleFontSize: 12,
                scaleFontStyle: \"normal\",
                scaleFontColor: \"#666\",
                responsive: false,
                maintainAspectRatio: true,
                showTooltips: true,
                tooltipEvents: [\"mousemove\", \"touchstart\", \"touchmove\"],
                tooltipFillColor: \"rgba(0,0,0,0.8)\",
                tooltipFontFamily: \"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif\",
                tooltipFontSize: 14,
                tooltipFontStyle: \"normal\",
                tooltipFontColor: \"#fff\",
                tooltipTitleFontFamily: \"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif\",
                tooltipTitleFontSize: 14,
                tooltipTitleFontStyle: \"bold\",
                tooltipTitleFontColor: \"#fff\",
                tooltipYPadding: 6,
                tooltipXPadding: 6,
                tooltipCaretSize: 8,
                tooltipCornerRadius: 6,
                tooltipXOffset: 10,
                tooltipTemplate: \"<% if (label){ %><%= label %>: <% } %><%= value %>\",
                multiTooltipTemplate: \"<%= value %>\",
                onAnimationProgress: function(){},
                onAnimationComplete: function(){}
            };

            \$('#grafico').prop('width', \$(\"#grafico\").parent('div').width());

            ";
        // line 163
        if ((array_key_exists("tipo", $context) && (!twig_test_empty((isset($context["tipo"]) ? $context["tipo"] : null))))) {
            // line 164
            echo "            if('";
            echo twig_escape_filter($this->env, (isset($context["tipo"]) ? $context["tipo"] : null), "html", null, true);
            echo "' === 'mes'){
                \$(\"#xMes\").hide();
                \$(\"#xAnio\").show();
                \$(\"#xEstado\").show();
                \$('#porMes').prop('checked', true).parent('label').addClass('btn-info');
            }else if('";
            // line 169
            echo twig_escape_filter($this->env, (isset($context["tipo"]) ? $context["tipo"] : null), "html", null, true);
            echo "' === 'anio'){
                \$(\"#xMes\").show();
                \$(\"#xAnio\").hide();
                \$(\"#xEstado\").show();
                \$('#porAnio').prop('checked', true).parent('label').addClass('btn-info');
            }else if('";
            // line 174
            echo twig_escape_filter($this->env, (isset($context["tipo"]) ? $context["tipo"] : null), "html", null, true);
            echo "' === 'estado'){
                \$(\"#xMes\").show();
                \$(\"#xAnio\").show();
                \$(\"#xEstado\").hide();
                \$('#porEstado').prop('checked', true).parent('label').addClass('btn-info');
            }
            ";
        }
        // line 181
        echo "

            ";
        // line 183
        $this->displayBlock('script_reporte', $context, $blocks);
        // line 184
        echo "        });

        function onClickTipo(obj){
            \$('input[name=\"tipo\"]').parent('label').removeClass('btn-info');
            \$(obj).parent('label').addClass('btn-info');
            var url = \$(obj).data('url');
            url = url.replace('@reporte','";
        // line 190
        echo twig_escape_filter($this->env, (isset($context["reporte"]) ? $context["reporte"] : null), "html", null, true);
        echo "');
            \$('form').prop('action', url).submit();
        }
    </script>

";
    }

    // line 183
    public function block_script_reporte($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "ReporteBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  339 => 183,  329 => 190,  321 => 184,  319 => 183,  315 => 181,  305 => 174,  297 => 169,  288 => 164,  286 => 163,  236 => 115,  222 => 113,  218 => 112,  193 => 91,  190 => 90,  185 => 21,  174 => 81,  170 => 79,  167 => 78,  164 => 77,  161 => 76,  155 => 74,  152 => 73,  149 => 72,  147 => 71,  126 => 59,  114 => 56,  106 => 51,  102 => 50,  98 => 49,  93 => 47,  85 => 45,  60 => 22,  58 => 21,  54 => 20,  34 => 4,  31 => 3,  158 => 75,  154 => 88,  150 => 86,  148 => 85,  143 => 82,  139 => 80,  135 => 78,  133 => 77,  111 => 58,  107 => 57,  104 => 56,  100 => 54,  94 => 52,  92 => 51,  89 => 46,  83 => 48,  81 => 47,  67 => 35,  64 => 34,  32 => 4,  29 => 3,);
    }
}
