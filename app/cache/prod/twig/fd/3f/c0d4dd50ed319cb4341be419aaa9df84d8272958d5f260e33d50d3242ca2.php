<?php

/* FOSUserBundle:Resetting:request_content.html.twig */
class __TwigTemplate_fd3fc0d4dd50ed319cb4341be419aaa9df84d8272958d5f260e33d50d3242ca2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<div class=\"padding-15\">
    ";
        // line 3
        if (array_key_exists("invalid_username", $context)) {
            // line 4
            echo "        <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span></button>
            ";
            // line 6
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.invalid_username", array("%username%" => (isset($context["invalid_username"]) ? $context["invalid_username"] : null)), "FOSUserBundle"), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 9
        echo "
    <form action=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" class=\"fos_user_resetting_request form-inline\" role=\"form\" id=\"fos_user_resetting_request\">
        <fieldset>
            <legend>Recuperar contraseña</legend>
            <div class=\"form-group\">
                <select class=\"form-control rif\" name=\"rif\" id=\"rif\" required=\"required\">
                    <option value=\"\"></option>
                    <option value=\"J\">J</option>
                    <option value=\"G\">G</option>
                    <option value=\"V\">V</option>
                    <option value=\"E\">E</option>
                </select>
            </div>
            <div class=\"form-group\">
                <label class=\"sr-only\" for=\"fake_username\">";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                <input type=\"hidden\" id=\"username\" name=\"username\" class=\"username\"  />
                <input type=\"text\" class=\"form-control col-xs-6 numeric fake_username\" id=\"fake_username\" name=\"fake_username\" required=\"required\" placeholder=\"Escriba su RIF\" />
            </div>
            <button type=\"submit\" id=\"reset_btn\" class=\"btn btn-default pull-right\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "</button>
            <br><br>
        </fieldset>
    </form>
</div>

<script type=\"text/javascript\">
    \$(document).ready(function() {
        \$(\"#fos_user_resetting_request\").submit(function(e) {
            e.preventDefault();
            \$.ajax({
                type: \"POST\",
                cache: false,
                url: \$(\"#fos_user_resetting_request\").attr('action'),
                data: \$(this).serializeArray(),
                success: function(data) {
                    \$.fancybox(data);
                }
            });
        });

        function result() {
            var s = \$('.rif').find(\":selected\").val(); 
            var i = \$('.fake_username').val();
            \$('.username').val(s + i);
        }

        \$('.rif').on('change', result);
        \$('.fake_username').on('blur', result);

        \$(\".numeric\").keydown(function(e) {
            if (\$.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }

            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 27,  53 => 23,  37 => 10,  34 => 9,  28 => 6,  24 => 4,  22 => 3,  19 => 2,  26 => 2,  20 => 1,);
    }
}
