<?php

/* UserBundle:Notification:confirm.email.html.twig */
class __TwigTemplate_4e2fc20da72a326392382a87d6eb49dc66840a1b19c0c81af711206645e85a04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("UserBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UserBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    Estimado(a) ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "getFullname"), "html", null, true);
        echo ",<br><br>
    Gracias por registrarse en el Sistema Nacional de Licencias y Fiscalización de la SUNAHIP.<br><br>
    Para completar la validación de su correo electrónico y activar su cuenta de usuario, debe hacer clic en el siguiente enlace: ";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null), "html", null, true);
        echo " este enlace solo puede usarse una sola vez. 
";
    }

    public function getTemplateName()
    {
        return "UserBundle:Notification:confirm.email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 5,  31 => 3,  28 => 2,  53 => 12,  50 => 11,  45 => 8,  39 => 4,  36 => 2,  32 => 11,  29 => 10,  27 => 8,  24 => 7,  22 => 2,);
    }
}
