<?php

/* CentrohipicoBundle:SolicitudAfiliacion:listado.html.twig */
class __TwigTemplate_4ebdf36ddfc960e806f3312d3d299a825a08bfe82d6dc595c26cd3e5283846b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_base' => array($this, 'block_script_base'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((((isset($context["ajax"]) ? $context["ajax"] : null) == false)) ? ("NewTemplateBundle::base.html.twig") : ("NewTemplateBundle::base_ajax.html.twig")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        ";
        // line 6
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method")) {
            echo "<h1>Aprobar Incorporaci&oacute;n / Desafiliación de Afiliados</h1> ";
        } else {
            echo " <h1>Listado de Solicitudes </h1> ";
        }
        // line 7
        echo "    </div>
    <div class=\"block-separator col-sm-12\"></div>
    
    <div class=\"col-md-7\" style=\"float:right\">
        <form action=\"\" method=\"get\" class=\"form-horizontal\">                
            <input type=\"search\" name=\"keyword\" style=\"float: left; margin-right: 10px; height: 32px\" id=\"txtKeyword\" aria-controls=\"dt_basic\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "keyword"), "method"), "html", null, true);
        echo "\">
            <select class=\"form-control\" name=\"estatus\" style=\"height: 32px; margin-right: 10px; width: 160px !important; display: inline !important\">
                <option value=\"\">Todos</option>
                <option value=\"Aprobado\" ";
        // line 15
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "estatus"), "method") == "Aprobado")) {
            echo " selected ";
        }
        echo ">Aprobados</option>
                <option value=\"Por Aprobar\" ";
        // line 16
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "estatus"), "method") == "Por Aprobar")) {
            echo " selected ";
        }
        echo ">Por Aprobar</option>
                <option value=\"Desafiliado\" ";
        // line 17
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "estatus"), "method") == "Desafiliado")) {
            echo " selected ";
        }
        echo ">Desafiliados</option>
                <option value=\"Por Desafiliar\" ";
        // line 18
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "estatus"), "method") == "Por Desafiliar")) {
            echo " selected ";
        }
        echo ">Por Desafiliar</option>
            </select>
            <input type=\"submit\" style=\"float:right; height: 32px\" class=\"btn btn-primary\" value=\"Buscar\">
        </form>
        <div class=\"col-sm-5 block-separator\"></div>
    </div>  
    
    <div id=\"action\">
        <div class=\"left\">
        ";
        // line 27
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 28
            echo "            <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                ";
            // line 29
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : null));
            echo "
            </article>
            ";
        }
        // line 32
        echo "        </div>
    </div>    
    
    ";
        // line 35
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 36
            echo "   <div class=\"col-md-2 col-md-offset-10\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("exportpdf_operafiliados");
            echo "\" target='_blank'><i class=\"icon-pdf\"></i></a>&nbsp;<a href=\"";
            echo $this->env->getExtension('routing')->getPath("exportxls_operafilliados");
            echo "\" target='_blank'><i class=\"icon-excel\"></i></a></div>
    <div class=\"col-md-12\">
        ";
            // line 38
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method")) {
                // line 39
                echo "            <input style=\"float: right;height: auto;margin: 0 15px 5px 20px;padding: 5px;\" disabled=\"disabled\" type=\"button\" id=\"activeMultiple\" value=\"Aplicar\">
        ";
            }
            // line 41
            echo "        <table class=\"table table-condensed table-striped table-hover\">
            <thead>
            <tr>
                <th>Nº</th>
                <th>RIF</th>
                <th>Denominación Comercial</th>                
                <th>Licencia</th>
                <th>Operadora</th>
                <th>Estatus</th>
                <th colspan=\"3\">Acciones</th>
            </tr>
            </thead>
            <tbody>            
            ";
            // line 54
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 55
                echo "            <tr id=\"request-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                echo "\">
                <td>";
                // line 56
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                echo "</td>
                <td>";
                // line 57
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
                echo "-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
                echo "</td>
                <td>";
                // line 58
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
                echo "</td>
                <td>";
                // line 59
                if ((!(null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licencia")))) {
                    // line 60
                    echo "                    ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licencia"), "html", null, true);
                    echo "
                    ";
                }
                // line 62
                echo "                </td>
                <td>";
                // line 63
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "html", null, true);
                echo "</td>
                <td>";
                // line 64
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status"), "html", null, true);
                echo "</td>
                <td>
                    <a data-href=\"\" href=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("Centrohipico_solicitud_afiliacion_detalles", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
                echo "\">
                        <i class=\"fa fa-search\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Ver Detalles\"></i>Ver
                    </a>
                </td>
                ";
                // line 70
                if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method")) {
                    // line 71
                    echo "                    ";
                    if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == "Por Aprobar")) {
                        // line 72
                        echo "                        <td><input type=\"checkbox\" class=\"activeRequest\" id=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                        echo "\"> Aprobar</td>
                        <td>
                            <a data-href=\"\" data-toggle=\"modal\" data-target=\"#confirmar-rechazar\" onclick=\"setId('";
                        // line 74
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                        echo "')\" href=\"#\">
                                <i class=\"fa fa-times-circle-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Rechazar\"></i>Rechazar
                            </a>
                        </td>
                    ";
                    } elseif (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == "Por Desafiliar")) {
                        // line 79
                        echo "                        <td colspan=\"2\">
                            <a data-href=\"\" data-toggle=\"modal\" data-target=\"#aprobar-desafiliar\" onclick=\"setId('";
                        // line 80
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                        echo "')\" href=\"#\">
                                <i class=\"fa fa-plus-square-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Aprobar Desafiliación\"></i>Aprobar Desafiliación
                            </a>
                        </td>
                    ";
                    } else {
                        // line 85
                        echo "                        <td colspan=\"2\">&nbsp;</td>
                    ";
                    }
                    // line 87
                    echo "                ";
                } elseif ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_OPERADOR"), "method")) {
                    // line 88
                    echo "                    ";
                    if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == "Aprobado")) {
                        // line 89
                        echo "                         <td colspan=\"2\">
                             <a data-href=\"\" data-toggle=\"modal\" data-target=\"#confirmar-desafiliar\" onclick=\"setId('";
                        // line 90
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                        echo "')\" href=\"#\">
                                <i class=\"fa fa-times-circle-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Desafiliar\"></i>Desafiliar
                            </a>
                         </td>
                    ";
                    } else {
                        // line 95
                        echo "                         <td colspan=\"2\">&nbsp;</td>
                    ";
                    }
                    // line 97
                    echo "                ";
                } else {
                    // line 98
                    echo "                    <td colspan=\"2\">&nbsp;</td>
                ";
                }
                // line 100
                echo "            </tr>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 102
            echo "            </tbody>
        </table>
    </div>
        ";
        } else {
            // line 106
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>";
            // line 109
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method")) {
                echo "No se encontraron resultados.";
            } else {
                echo "No se encontraron resultados.";
            }
            echo "</h5></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 114
        echo "    <div class=\"block-separator col-sm-12\"></div>

    <div class=\"modal fade\" id=\"confirmar-rechazar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Rechazar Solicitud
                </div>
                <div class=\"modal-body\">
                    <label for=\"form_rechazar_nota\"> Agregue el motivo por el cual se rechaza la solicitud:</label><br>
                    <textarea rows=\"5\" cols=\"65\" id=\"form_rechazar_nota\" name=\"form_rechazar[nota]\"> </textarea>
                    <div style=\"color: #E70202;display: none\" id=\"nota_minimum\"></div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" onclick=\"reject()\" class=\"btn btn-danger danger\">Rechazar</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class=\"modal fade\" id=\"confirmar-desafiliar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Solicitud de Desafiliación
                </div>
                <div class=\"modal-body\" id=\"bodyDesafiliacion\">
                    La solicitud se debe realizar los primeros 10 días del mes y debe ser aprobado por funcionarios del sunahip.<br>
                    Si la solicitud de desafiliación no es aprobada por SUNAHIP, deberá cancelar el monto asociado al Centro Hípico de todo el mes.<br><br>
                    ¿Está seguro que desea continuar?
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" onclick=\"solicitarDesafiliacion()\" class=\"btn btn-danger danger\" id=\"aceptarDesafiliacion\">Aceptar</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class=\"modal fade\" id=\"aprobar-desafiliar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Aprobación de Desafiliación
                </div>
                <div class=\"modal-body\" id=\"bodyApDesafiliacion\">
                    Si la solicitud de desafiliación es aprobada después del 10 de cada mes. La desafiliación se verá reflejada el siguiente mes.
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" onclick=\"aprobarDesafiliacion()\" class=\"btn btn-danger danger\" id=\"aceptarApDesafiliacion\">Aceptar</a>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"confirm-active\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Aprobar solicitudes
                </div>
                <div class=\"modal-body\">
                    Está seguro de aprobar las solicitudes seleccionadas?
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" onclick=\"approve()\" class=\"btn btn-info info\">Aprobar</a>
                </div>
            </div>
        </div>
    </div>

";
    }

    // line 190
    public function block_script_base($context, array $blocks = array())
    {
        // line 191
        echo "    ";
        $this->displayParentBlock("script_base", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        var id = 0;

        function setId(id)
        {
            window.id = id;
        }

        function reject()
        {
            var nota  = \$(\"#form_rechazar_nota\").val();

            if(nota.length > 3)
            {
                var url = Routing.generate('Centrohipico_solicitud_afiliacion_rechazar');
                \$.post( url, { id: window.id, nota: nota } );
                \$(\"#nota_minimum\").hide();
                \$('#confirmar-rechazar').modal('hide');
                var url = Routing.generate('Centrohipico_solicitud_afiliacion_listado');
                window.location = url;
            }else{
                if(nota.length == 1){
                    var html = \"La nota no puede estar vac&iacute;a\";
                    \$(\"#nota_minimum\").show();
                    \$(\"#nota_minimum\").html(html);
                }
                var html = \"La nota debe contener al menos 4 digitos\";
                \$(\"#nota_minimum\").show();
                \$(\"#nota_minimum\").html(html);
            }
        }
        
        function solicitarDesafiliacion() 
        {
            var url = Routing.generate('Centrohipico_solicitud_desafiliacion', { id: window.id });
            \$.ajax({
                async: false,
                type: \"POST\",
                cache: false,
                url: url,
                dataType: \"json\",
                success: function(response) {
                    if(response.status == 'true') {
                       var url = Routing.generate('Centrohipico_solicitud_afiliacion_listado');
                       window.location = url;                        
                    } else {
                        \$(\"#bodyDesafiliacion\").html(response.mensaje);
                        \$(\"#aceptarDesafiliacion\").attr(\"disabled\", \"disabled\");
                    }
                },
                error: function(){
                    \$(\"#bodyDesafiliacion\").html(\"Error al tratar de hacer la petición.\");
                    \$(\"#aceptarDesafiliacion\").attr(\"disabled\", \"disabled\");
                }
            });           
        }
        
        function aprobarDesafiliacion() 
        {
            var url = Routing.generate('Centrohipico_solicitud_desafiliacion', { id: window.id });
            \$.ajax({
                async: false,
                type: \"POST\",
                cache: false,
                url: url,
                dataType: \"json\",
                success: function(response) {
                    if(response.status == 'true') {
                       var url = Routing.generate('Centrohipico_solicitud_afiliacion_listado');
                       window.location = url;                        
                    } else {
                        \$(\"#bodyApDesafiliacion\").html(response.mensaje);
                        \$(\"#aceptarApDesafiliacion\").attr(\"disabled\", \"disabled\");
                    }
                },
                error: function(){
                    \$(\"#bodyApDesafiliacion\").html(\"Error al tratar de hacer la petición.\");
                    \$(\"#aceptarApDesafiliacion\").attr(\"disabled\", \"disabled\");
                }
            });           
        }

        var arrayData= new Array();


        \$('.activeRequest').on('change', function() {
            id = parseInt(\$(this).attr('id'));
            var index = window.arrayData.indexOf(id);

            if(index > -1)
            {
                if (\$(this).is(':checked')) {
                    console.info(\"in\");
                }else{
                    window.arrayData.splice(index, 1);
                }
            }else{
                window.arrayData.push(id);
            }

            if (window.arrayData.length > 0)
                \$(\"#activeMultiple\").removeAttr(\"disabled\");

        });

        \$('#activeMultiple').click(function() {
            if (window.arrayData.length > 0)
            {
                \$('#confirm-active').modal('show');
            }
        });

        function approve()
        {
            if (window.arrayData.length > 0)
            {
                \$('#confirm-active').modal('hide');
                var url = Routing.generate('Centrohipico_solicitud_afiliacion_aprobar');
                \$.post(url, {'ids': window.arrayData}, function(){
                }).done(function() {
                  var url = Routing.generate('Centrohipico_solicitud_afiliacion_listado');
                  window.location = url;
                });

            }
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:SolicitudAfiliacion:listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  376 => 191,  373 => 190,  295 => 114,  283 => 109,  278 => 106,  272 => 102,  257 => 100,  253 => 98,  250 => 97,  246 => 95,  238 => 90,  235 => 89,  232 => 88,  229 => 87,  225 => 85,  217 => 80,  214 => 79,  206 => 74,  200 => 72,  197 => 71,  195 => 70,  188 => 66,  183 => 64,  179 => 63,  176 => 62,  170 => 60,  168 => 59,  164 => 58,  158 => 57,  154 => 56,  149 => 55,  132 => 54,  117 => 41,  113 => 39,  111 => 38,  103 => 36,  101 => 35,  96 => 32,  90 => 29,  87 => 28,  85 => 27,  71 => 18,  65 => 17,  59 => 16,  53 => 15,  47 => 12,  40 => 7,  34 => 6,  30 => 4,  27 => 3,);
    }
}
