<?php

/* FiscalizacionBundle::listar.html.twig */
class __TwigTemplate_63ba929c7b286722b4180c8ccd53de08cb8fc62791bddc89b898692ccbb46ca3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'main_title' => array($this, 'block_main_title'),
            'buttons' => array($this, 'block_buttons'),
            'headers' => array($this, 'block_headers'),
            'columns' => array($this, 'block_columns'),
            'footlistar' => array($this, 'block_footlistar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        echo " 
<div class=\"col-md-11\">
    <div class=\"block-separator col-sm-11\"></div>

    <h1>";
        // line 7
        $this->displayBlock('main_title', $context, $blocks);
        echo "</h1>

    ";
        // line 9
        $this->displayBlock('buttons', $context, $blocks);
        // line 12
        echo "    
    <div class=\"left\">
            ";
        // line 14
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 15
            echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
            // line 16
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : null));
            echo "
                </article>
            ";
        }
        // line 19
        echo "        </div>
    <br/><br>
    ";
        // line 21
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 22
            echo "    ";
            if (array_key_exists("tipo", $context)) {
                // line 23
                echo "        ";
                echo twig_include($this->env, $context, "ExportBundle::iconslink.html.twig", array("pdf" => ("exportpdf_pagos_" . (isset($context["tipo"]) ? $context["tipo"] : null)), "xcel" => ("exportxls_pagos_" . (isset($context["tipo"]) ? $context["tipo"] : null))));
                echo "
    ";
            }
            // line 25
            echo "    <table class=\"table table-condensed table-striped table-hover\">
        <thead>
            <tr>
                ";
            // line 28
            $this->displayBlock('headers', $context, $blocks);
            // line 31
            echo "            </tr>
        </thead>
        <tbody>
        ";
            // line 34
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 35
                echo "            ";
                $this->displayBlock('columns', $context, $blocks);
                // line 38
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "        </tbody>
    </table>
    ";
        } else {
            // line 42
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>No se encontraron resultados</h5></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 50
        echo "</div>

";
        // line 52
        $this->displayBlock('footlistar', $context, $blocks);
    }

    // line 7
    public function block_main_title($context, array $blocks = array())
    {
    }

    // line 9
    public function block_buttons($context, array $blocks = array())
    {
        // line 10
        echo "
    ";
    }

    // line 28
    public function block_headers($context, array $blocks = array())
    {
        // line 29
        echo "                
                ";
    }

    // line 35
    public function block_columns($context, array $blocks = array())
    {
        // line 36
        echo "            
            ";
    }

    // line 52
    public function block_footlistar($context, array $blocks = array())
    {
        echo " ";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle::listar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 52,  171 => 36,  168 => 35,  163 => 29,  160 => 28,  152 => 9,  147 => 7,  143 => 52,  139 => 50,  124 => 39,  110 => 38,  90 => 34,  85 => 31,  78 => 25,  72 => 23,  69 => 22,  67 => 21,  63 => 19,  57 => 16,  48 => 12,  46 => 9,  41 => 7,  33 => 3,  181 => 53,  178 => 52,  174 => 48,  170 => 46,  167 => 45,  161 => 42,  158 => 41,  155 => 10,  149 => 38,  146 => 37,  140 => 34,  137 => 33,  135 => 32,  132 => 31,  129 => 42,  126 => 29,  121 => 49,  119 => 29,  115 => 28,  111 => 27,  107 => 35,  101 => 25,  97 => 24,  93 => 23,  89 => 22,  86 => 21,  83 => 28,  80 => 19,  73 => 17,  70 => 16,  66 => 14,  61 => 13,  58 => 12,  54 => 15,  52 => 14,  43 => 5,  40 => 4,  34 => 2,);
    }
}
