<?php

/* ReporteBundle:Filtro:licencias.html.twig */
class __TwigTemplate_030617da2a2437c0dd00ef4941bd1436376b7d47bf3cf13a3e3e5a9d46aa1dd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "    ";
        // line 3
        echo "    ";
        // line 6
        echo "    ";
        // line 7
        echo "    ";
        // line 10
        echo "    ";
        // line 11
        echo "    ";
        // line 14
        echo "    ";
        // line 15
        echo "    ";
        // line 18
        echo "    ";
        // line 19
        echo "    ";
        // line 22
        echo "    ";
        // line 23
        echo "    ";
        // line 25
        echo "<li>
    Solicitadas:
    <input type=\"radio\" name=\"status\" value=\"Solicitada\" ";
        // line 27
        if (array_key_exists("status", $context)) {
            echo " ";
            if (((isset($context["status"]) ? $context["status"] : null) == "Solicitada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo " >
</li>
<li>
    Aprobadas:
    <input type=\"radio\" name=\"status\" value=\"Aprobada\" ";
        // line 31
        if (array_key_exists("status", $context)) {
            echo " ";
            if (((isset($context["status"]) ? $context["status"] : null) == "Aprobada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo ">
</li>
<li>
    Rechazadas:
    <input type=\"radio\" name=\"status\" value=\"Rechazada\" ";
        // line 35
        if (array_key_exists("status", $context)) {
            echo " ";
            if (((isset($context["status"]) ? $context["status"] : null) == "Rechazada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo ">
</li>";
    }

    public function getTemplateName()
    {
        return "ReporteBundle:Filtro:licencias.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 35,  47 => 27,  43 => 25,  41 => 23,  39 => 22,  37 => 19,  35 => 18,  33 => 15,  27 => 10,  25 => 7,  23 => 6,  21 => 3,  19 => 2,  339 => 183,  329 => 190,  321 => 184,  319 => 183,  315 => 181,  305 => 174,  297 => 169,  288 => 164,  286 => 163,  236 => 115,  222 => 113,  218 => 112,  193 => 91,  190 => 90,  185 => 21,  174 => 81,  170 => 79,  167 => 78,  164 => 77,  161 => 76,  155 => 74,  152 => 73,  149 => 72,  147 => 71,  126 => 59,  114 => 56,  106 => 51,  102 => 50,  98 => 49,  93 => 47,  85 => 45,  60 => 31,  58 => 21,  54 => 20,  34 => 4,  31 => 14,  158 => 75,  154 => 88,  150 => 86,  148 => 85,  143 => 82,  139 => 80,  135 => 78,  133 => 77,  111 => 58,  107 => 57,  104 => 56,  100 => 54,  94 => 52,  92 => 51,  89 => 46,  83 => 48,  81 => 47,  67 => 35,  64 => 34,  32 => 4,  29 => 11,);
    }
}
