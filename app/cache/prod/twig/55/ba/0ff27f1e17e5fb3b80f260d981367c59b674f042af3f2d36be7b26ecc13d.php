<?php

/* SolicitudesCitasBundle:DataSolicitudes:List_recaudos_edf2.html.twig */
class __TwigTemplate_55ba0ff27f1e17e5fb3b80f260d981367c59b674f042af3f2d36be7b26ecc13d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table id=\"tabla_reporte2\">\t\t\t\t
        <tr id=\"table_header2\">
                 <td>N#</td>
                 <td>Tipo de Documento</td>
                 <td>Archivo</td>
                 <td>Fecha de Vencimiento</td>
                 <td>Cargar</td>
        </tr>
        ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["recaudo"]) {
            echo "   
          <tr id=\"tr_recaudos";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "\">
                 <td class=\"text-center\">";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "</td>
                 <td class=\"text-left\">";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["recaudo"]) ? $context["recaudo"] : null), "recaudo"), "html", null, true);
            echo "</td>
                 <td class=\"text-left\">Archivo</td>
                 <td class=\"text-left\">Fecha: (dd/mm/aaaa) </td>
                 <td class=\"text-center\">
                     <input type=\"hidden\" id=\"recaudoLicencia_";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "\" name=\"recaudoLicencia";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "\" class=\"hide\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["recaudo"]) ? $context["recaudo"] : null), "id"), "html", null, true);
            echo "\"/>
                     <div class=\"col-md-2 col-md-offset-1\"> <button class=\"btn btn-sm btn-info\" onclick=\"cRecaudo('";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "');\">Cargar</button></div>
                 </td>
        </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recaudo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "    </table>
<script type=\"text/javascript\">
    \$(document).ready(function(){
        var Options={
            autoSize: true,
            dateFormat: 'dd/mm/yy',
            maxDate: '";
        // line 27
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
        echo "',
            onSelect: function(dateText, inst) { 
               var datets = Date.parse(inst.selectedYear+'-'+inst.selectedMonth+'-'+inst.selectedDay);
               //GenerarSolicitud(datets);
               \$(this).val(dateText);                
           } 
        };
        var Options2={
            autoSize: true,
            dateFormat: 'dd/mm/yy',
            minDate: '";
        // line 37
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
        echo "',
            onSelect: function(dateText, inst) { 
               var datets = Date.parse(inst.selectedYear+'-'+inst.selectedMonth+'-'+inst.selectedDay);
               //GenerarSolicitud(datets);
               \$(this).val(dateText);                
           } 
        };
      \$(\".date\").each(function(index,elem){
          \$(this).datepicker(Options2); 
      });
      \$(\".datePago\").each(function(index,elem){
          \$(this).datepicker(Options); 
      });      
    });
</script>
    
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:List_recaudos_edf2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 37,  97 => 27,  89 => 21,  71 => 17,  63 => 16,  56 => 12,  52 => 11,  48 => 10,  29 => 9,  19 => 1,);
    }
}
