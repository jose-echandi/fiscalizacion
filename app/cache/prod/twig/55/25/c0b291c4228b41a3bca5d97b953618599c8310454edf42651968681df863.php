<?php

/* SolicitudesCitasBundle:AdminLicencias:consulta.html.twig */
class __TwigTemplate_5525c0b291c4228b41a3bca5d97b953618599c8310454edf42651968681df863 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>
            Consulta de licencias
        </h1>
    </div>
    <div class=\"col-md-12\">
        ";
        // line 12
        ob_start();
        // line 13
        echo "        <form action=\"\" method=\"post\" id=\"licensesQuery\">
            <p>Seleccione los parámetros a buscar:</p>
            <table style=\"width:100%\">
                <tr>
                    <td>
                        <label for=\"correo\">RIF:</label>
                    </td>
                    <td>
                        <select name=\"rif_type\" id=\"\">
                            <option value=\"V\">V</option>
                            <option value=\"J\">J</option>
                        </select> -
                        <input type=\"text\" ";
        // line 25
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "rif_number", array(), "any", true, true)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "rif_number"), "html", null, true);
            echo "\" ";
        } else {
            echo " value=\"\" ";
        }
        echo " name=\"data[rif_number]\"  maxlength=\"9\" class=\"corto\" title=\"No colocar ni puntos, ni guiones\">
                    </td>
                    <td>
                        <label for=\"correo\">Denominación Comercial:</label>
                    </td>
                    <td>
                        <p><input type=\"text\" name=\"data[denominacion_comercial]\" ";
        // line 31
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "denominacion_comercial", array(), "any", true, true)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "numero_licencia"), "html", null, true);
            echo "\" ";
        } else {
            echo " value=\"\" ";
        }
        echo "  title=\"Coloque un Nombre o Razón Social\" class=\"corto\" style=\"width: 155px;\"></p>
                    </td>

                </tr>
                <tr>
                    <td>
                        <label for=\"correo\">Clasificación de Licencia:</label>
                    </td>
                    <td>
                        <select name=\"data[claf_licencia]\" id=\"tipos\">
                            <option value=\"0\" ";
        // line 41
        if ((!$this->getAttribute((isset($context["data"]) ? $context["data"] : null), "claf_licencia", array(), "any", true, true))) {
            echo "  selected ";
        }
        echo "> Seleccione</option>
                            ";
        // line 42
        if (array_key_exists("claf_licenses", $context)) {
            // line 43
            echo "                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["claf_licenses"]) ? $context["claf_licenses"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["lic"]) {
                // line 44
                echo "                                    <option ";
                if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "claf_licencia", array(), "any", true, true)) {
                    echo " ";
                    if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "claf_licencia") == $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "id"))) {
                        echo " selected=\"selected\" ";
                    }
                    echo " ";
                }
                echo " value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "id"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "clasfLicencia"), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "                            ";
        }
        // line 47
        echo "                        </select>
                    </td>
                    <td>
                        <label for=\"correo\">Licencias:</label>
                    </td>
                    <td>
                        <select name=\"data[tipo_licencia]\">
                            <option value=\"0\"  ";
        // line 54
        if ((!$this->getAttribute((isset($context["data"]) ? $context["data"] : null), "tipo_licencia", array(), "any", true, true))) {
            echo "  selected ";
        }
        echo "> Seleccione</option>
                            ";
        // line 55
        if (array_key_exists("licenses", $context)) {
            // line 56
            echo "                                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["licenses"]) ? $context["licenses"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["lic"]) {
                // line 57
                echo "                                    <option ";
                if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "tipo_licencia", array(), "any", true, true)) {
                    echo " ";
                    if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "tipo_licencia") == $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "id"))) {
                        echo " selected=\"selected\" ";
                    }
                    echo " ";
                }
                echo " value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "id"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "tipoLicencia"), "html", null, true);
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "                            ";
        }
        // line 60
        echo "                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for=\"correo\">Número de Licencia:</label>
                    </td>
                    <td>
                        <p><input type=\"text\" name=\"data[numero_licencia]\" ";
        // line 68
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "numero_licencia", array(), "any", true, true)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "numero_licencia"), "html", null, true);
            echo "\" ";
        } else {
            echo " value=\"\"  ";
        }
        echo "   title=\"Coloque un Número de Licencia válido\" class=\"corto\" style=\"width: 155px;\"></p>
                    </td>
                    <td>
                        <label for=\"correo\">Número de Solicitud:</label>
                    </td>
                    <td>
                        <p><input type=\"text\" name=\"data[numero_solicitud]\" ";
        // line 74
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "numero_solicitud", array(), "any", true, true)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "numero_solicitud"), "html", null, true);
            echo "\" ";
        } else {
            echo " value=\"\" ";
        }
        echo "   title=\"Coloque un Número de Solicitud válido\" class=\"corto\" style=\"width: 155px;\"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for=\"estados\">Estado:</label>
                    </td>
                    <td>
                        <select name=\"data[estados]\" id=\"estados\">
                            <option value=\"0\" ";
        // line 83
        if ((!$this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estados", array(), "any", true, true))) {
            echo "  selected ";
        }
        echo "> Seleccione</option>
                            ";
        // line 84
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["states"]) ? $context["states"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["state"]) {
            // line 85
            echo "                                <option ";
            if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estados", array(), "any", true, true)) {
                echo " ";
                if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estados") == $this->getAttribute((isset($context["state"]) ? $context["state"] : null), "id"))) {
                    echo " selected=\"selected\" ";
                }
                echo " ";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["state"]) ? $context["state"] : null), "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["state"]) ? $context["state"] : null), "nombre"), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['state'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "                        </select>
                    </td>
                    <td>
                        <label for=\"municipios\">Municipio:</label>
                    </td>
                    <td>
                        <select name=\"data[municipios]\" id=\"municipios\">
                            <option value=\"0\" ";
        // line 94
        if ((!$this->getAttribute((isset($context["data"]) ? $context["data"] : null), "municipios", array(), "any", true, true))) {
            echo "  selected ";
        }
        echo "> Seleccione</option>
                            ";
        // line 95
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["towns"]) ? $context["towns"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["town"]) {
            // line 96
            echo "                                <option ";
            if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "municipios", array(), "any", true, true)) {
                echo " ";
                if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "municipios") == $this->getAttribute((isset($context["town"]) ? $context["town"] : null), "nombre"))) {
                    echo " selected=\"selected\" ";
                }
                echo " ";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["town"]) ? $context["town"] : null), "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["town"]) ? $context["town"] : null), "nombre"), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['town'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 98
        echo "                        </select>
                    </td>
                </tr>
            </table>

        ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 104
        echo "            <div id=\"linea\">
                <div class=\"form_line3\">
                    <label for=\"correo\">Estatus de Solicitud:</label><br>
                    <table style=\" float: right; width: 90%;\">
                        <tbody>
                        ";
        // line 110
        echo "                            ";
        // line 111
        echo "                            ";
        // line 112
        echo "                            ";
        // line 113
        echo "                        ";
        // line 114
        echo "                        <tr>
                            <td><input type=\"radio\" name=\"data[estatus_solicitud]\" ";
        // line 115
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud") == "gerencia")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo " value=\"gerencia\">Aprobación Gerencia</td>
                            <td><input type=\"radio\" name=\"data[estatus_solicitud]\" ";
        // line 116
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud") == "asesor")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo " value=\"asesor\">Aprobación Legal</td>
                            <td><input type=\"radio\" name=\"data[estatus_solicitud]\" ";
        // line 117
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud") == "direccion")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo " value=\"direccion\">Aprobación Dirección General</td>
                        </tr>
                        <tr>
                            <td><input type=\"radio\" name=\"data[estatus_solicitud]\" ";
        // line 120
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud") == "Solicitada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo "value=\"Solicitada\">Solicitada</td>
                            <td><input type=\"radio\" name=\"data[estatus_solicitud]\" ";
        // line 121
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud") == "Aprobada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo "value=\"Aprobada\">Aprobada</td>
                            <td><input type=\"radio\" name=\"data[estatus_solicitud]\" ";
        // line 122
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_solicitud") == "Rechazada")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo "value=\"Rechazada\">Rechazada</td>
                        </tr>
                        </tbody></table>
                </div>

                <div id=\"linea\">
                    <div class=\"form_line3\">
                        <label for=\"correo\">Estatus de Licencia:</label><br>
                        <tdstyle=\"width: 25%;\"=\"\">
                        </tdstyle=\"width:><table style=\" float: right; width: 90%;\">
                        <tbody><tr>
                            <td style=\"width: 18%;\"><input type=\"radio\" name=\"data[estatus_licencia]\" ";
        // line 133
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_licencia", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_licencia") == "1")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo "value=\"1\">Activa</td>
                            <td style=\"width: 25%;\"><input type=\"radio\" name=\"data[estatus_licencia]\" ";
        // line 134
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_licencia", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_licencia") == "0")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo "value=\"0\">Inactiva</td>
                            <td style=\"width: 25%;\"></td>
                        </tr>
                        </tbody></table>
                    </div>
                </div>

                <div id=\"linea\">
                    <div class=\"form_line3\">
                        <label for=\"correo\">Estatus de pagos:</label><br>
                        <table style=\" float: right; width: 90%;\">
                            <tbody><tr>
                                <td style=\"width: 36%;\"><input type=\"radio\" name=\"data[estatus_pago]\" ";
        // line 146
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_pago", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_pago") == "PROCESAMIENTO")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo "  value=\"PROCESAMIENTO\">Pagos de Procesamiento</td>
                                <td style=\"width: 36%;\"><input type=\"radio\" name=\"data[estatus_pago]\" ";
        // line 147
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_pago", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_pago") == "OTORGAMIENTO")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo "  value=\"OTORGAMIENTO\">Pagos por Otorgamiento</td>
                            </tr>
                            <tr>
                                <td style=\"width: 36%;\"><input type=\"radio\" name=\"data[estatus_pago]\" ";
        // line 150
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_pago", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_pago") == "MULTA")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo " value=\"MULTA\">Pagos de Multas</td>
                                <td style=\"width: 54%;\"><input type=\"radio\" name=\"data[estatus_pago]\" ";
        // line 151
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_pago", array(), "any", true, true)) {
            echo " ";
            if (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "estatus_pago") == "APORTE_MENSUAL")) {
                echo " checked=\"checked\" ";
            }
            echo " ";
        }
        echo " VALUE=\"APORTE_MENSUAL\">Pagos de Aportes Mensuales</td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>

                <div class=\"col-sm-10 block-separator\"></div>

                <div class=\"col-sm-12\">
                    <div class=\"form_line3\">
                        <input  style=\"float: right\" type=\"button\" onclick=\"reset()\" class=\"btn\" value=\"Limpiar\" style=\"float: right;\">
                        <input  style=\"float: right\" type=\"submit\" class=\"btn btn-primary\" value=\"Buscar\" style=\"float: right;\">
                    </div>
                </div>

            </div>
        </form>\t<!--  FIN FILTRO -->

    </div>
";
        // line 170
        if (array_key_exists("result", $context)) {
            // line 171
            echo "    <div class=\"col-sm-12 block-separator\"></div>

    <div id=\"action\">
        <div class=\"left\">
            ";
            // line 175
            if ((twig_length_filter($this->env, (isset($context["result"]) ? $context["result"] : null)) > 0)) {
                // line 176
                echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
                // line 177
                echo $this->env->getExtension('knp_pagination')->render((isset($context["result"]) ? $context["result"] : null));
                echo "
                </article>
            ";
            }
            // line 180
            echo "        </div>
        <div class=\"right\">
            &nbsp;
        </div>
    </div>


    ";
            // line 187
            if ((twig_length_filter($this->env, (isset($context["result"]) ? $context["result"] : null)) > 0)) {
                // line 188
                echo "        </br>
        <div class=\"col-md-12\">
            <table class=\"table table-condensed table-striped\">
                <thead>
                <tr>
                    <th>Denominaci&oacute;n comercial</th>
                    <th>Codigo de solicitud</th>
                    <th>Fecha solicitud</th>
                    <th>Status</th>
                    <th>N&uacute;mero licencia</th>
                </tr>
                </thead>
                <tbody>
                ";
                // line 201
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["result"]) ? $context["result"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                    // line 202
                    echo "                    <tr>
                        <td>
                            ";
                    // line 204
                    if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cType") == "1")) {
                        // line 205
                        echo "                                <a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" class=\"show_detail\" onclick=\"getInfo(";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cId"), "html", null, true);
                        echo ")\">
                            ";
                    } else {
                        // line 207
                        echo "                                    <a href=\"#\" id=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cId"), "html", null, true);
                        echo "\"  data-toggle=\"modal\" data-target=\"#myModal\" class=\"show_detail2\">
                            ";
                    }
                    // line 209
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
                    echo "
                            </a>
                        </td>
                        <td align=\"center\">";
                    // line 212
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), 0), "codSolicitud"), "html", null, true);
                    echo "</td>
                        <td align=\"center\">";
                    // line 213
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), 0), "fechaSolicitud"), "d/m/Y"), "html", null, true);
                    echo "</td>
                        <td align=\"center\">";
                    // line 214
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), 0), "status"), "html", null, true);
                    echo "</td>
                        <td align=\"center\">";
                    // line 215
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), 0), "numLicenciaAdscrita"), "html", null, true);
                    echo "</td>
                    </tr>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 218
                echo "                </tbody>
            </table>
        </div>
        <div class=\"col-md-12\">
            <div id=\"action\">
                <div class=\"left\">
                    ";
                // line 224
                if ((twig_length_filter($this->env, (isset($context["result"]) ? $context["result"] : null)) > 0)) {
                    // line 225
                    echo "                        <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                            ";
                    // line 226
                    echo $this->env->getExtension('knp_pagination')->render((isset($context["result"]) ? $context["result"] : null));
                    echo "
                        </article>
                    ";
                }
                // line 229
                echo "                </div>
                <div class=\"right\">
                    </br>
                </div>
            </div>
        </div>

    ";
            } elseif ((twig_length_filter($this->env, (isset($context["result"]) ? $context["result"] : null)) == 0)) {
                // line 237
                echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>No se encontraron resultados</h5></li>
                </ul>
            </div>
        </div>
    ";
            }
            // line 245
            echo "    <div class=\"block-separator col-sm-12\"></div>

";
        }
        // line 248
        echo "
    <script>
        function reset()
        {
            \$('input:radio').removeAttr('checked');
             // Refresh the jQuery UI buttonset.
            \$( \"input:radio\" ).buttonset('refresh');
            getElementById(\"licensesQuery\").reset();
        }
    </script>


<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" id=\"btnClose\" class=\"close\" data-dismiss=\"modal\">
                    <span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span>
                </button>
            </div>
            <div class=\"modal-body\" id=\"datos_hipicos\"> Cargando....</div>
            <div class=\"modal-footer\"></div>
        </div>
    </div>
</div>

";
    }

    // line 277
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 278
        echo "     ";
        $this->displayParentBlock("foot_script_assetic", $context, $blocks);
        echo "
     <script type=\"text/javascript\">
         function getInfo(id)
         {
             //console.info(\"entro\");
             var url = Routing.generate('operadora_info',{ 'id': id });
             \$.get( url, function( data ) {
                 console.info('data',data);
                 \$(\"#datos_hipicos\").html(data);
             });
         }

         \$(function() {
             \$(\".show_detail2\").click(function(ev) {
                 var identificador = \$(this).attr(\"id\");
                 \$.get(Routing.generate('datacentrohipico_show', {id: identificador}), function(data) {
                     \$('#datos_hipicos').html(data);// llenarel div llenar del body de la ventana modal
                 });

             });

             \$(\"#btnClose\").click(function(ev) {
                 \$('#datos_hipicos').html(\"Cargando....\");
             });
         });


     </script>

 ";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:AdminLicencias:consulta.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  643 => 278,  640 => 277,  609 => 248,  604 => 245,  594 => 237,  584 => 229,  578 => 226,  575 => 225,  573 => 224,  565 => 218,  556 => 215,  552 => 214,  548 => 213,  544 => 212,  537 => 209,  531 => 207,  525 => 205,  523 => 204,  519 => 202,  515 => 201,  500 => 188,  498 => 187,  489 => 180,  483 => 177,  480 => 176,  478 => 175,  472 => 171,  470 => 170,  442 => 151,  432 => 150,  420 => 147,  410 => 146,  389 => 134,  379 => 133,  359 => 122,  349 => 121,  339 => 120,  327 => 117,  317 => 116,  307 => 115,  304 => 114,  302 => 113,  300 => 112,  298 => 111,  296 => 110,  289 => 104,  281 => 98,  262 => 96,  258 => 95,  252 => 94,  243 => 87,  224 => 85,  220 => 84,  214 => 83,  196 => 74,  181 => 68,  171 => 60,  168 => 59,  149 => 57,  144 => 56,  142 => 55,  136 => 54,  127 => 47,  124 => 46,  105 => 44,  100 => 43,  98 => 42,  92 => 41,  73 => 31,  58 => 25,  44 => 13,  42 => 12,  32 => 4,  29 => 3,);
    }
}
