<?php

/* ExportBundle:AdminCitas:listado.html.twig */
class __TwigTemplate_c421e50c3fb746d94897b0698e90e4f786bfe65c65726cfad684124abdeed75f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("ExportBundle::export_pdf.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ExportBundle::export_pdf.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <style>
        #table_header td{
            color:white;
        }
    </style>
    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>
            ";
        // line 12
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_FISCAL"), "method")) {
            // line 13
            echo "                Citas asignadas
            ";
        } else {
            // line 15
            echo "                ";
            echo "  
                Licencias ";
            // line 16
            echo twig_escape_filter($this->env, ((((isset($context["tipo"]) ? $context["tipo"] : null) == "Verificada")) ? ("Por Aprobar") : ((isset($context["tipo"]) ? $context["tipo"] : null))), "html", null, true);
            echo "  
            ";
        }
        // line 18
        echo "        </h1>
    </div>

    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row\">
        <div class=\"col-sm-2\">
        </div>
        <div class=\"col-sm-10 block-separator\"></div>
    </div>
    ";
        // line 27
        if (array_key_exists("entities", $context)) {
            // line 28
            echo "        <div class=\"col-md-12\">
            <table class=\"table table-condensed table-striped\">
                <thead>
                <tr>
                    <th>Nº</th>
                    <th>RIF</th>
                    <th>Denominación Comercial</th>
                    <th>Licencia</th>
                    <th>Clasificación de Licencia</th>
                    <th>No Solicitud</th>
                    <th>Fecha de cita</th>
                </tr>
                </thead>
                <tbody>
                ";
            // line 42
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 43
                echo "                    <tr id=\"request-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                echo "\">
                        <td>";
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                echo "</td>
                        <td>
                            ";
                // line 46
                if ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora", array(), "any", false, true), "persJuridica", array(), "any", true, true)) {
                    // line 47
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "persJuridica"), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "rif"), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 49
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "persJuridica"), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "rif"), "html", null, true);
                    echo "
                            ";
                }
                // line 51
                echo "                        </td>
                        <td>
                            ";
                // line 53
                if ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora", array(), "any", false, true), "denominacionComercial", array(), "any", true, true)) {
                    // line 54
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "denominacionComercial"), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 56
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "denominacionComercial"), "html", null, true);
                    echo "
                            ";
                }
                // line 58
                echo "                        </td>
                        <td>";
                // line 59
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasLicencia"), "admTiposLicencias"), "tipoLicencia"), "html", null, true);
                echo "</td>
                        <td>";
                // line 60
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasLicencia"), "clasfLicencia"), "html", null, true);
                echo "</td>
                        <td onclick=\"getInfo('";
                // line 61
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                echo "')\"><a href=\"#\" class=\"big-link\" data-reveal-id=\"myModal\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "codsolicitud"), "html", null, true);
                echo "</a></td>
                        <td>";
                // line 62
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "fechaSolicitud"), "d-m-Y"), "html", null, true);
                echo "</td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "                </tbody>
            </table>
        </div>
    ";
        } else {
            // line 69
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>No existen registros</h5></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 77
        echo "    <div class=\"block-separator col-sm-12\"></div>
";
    }

    public function getTemplateName()
    {
        return "ExportBundle:AdminCitas:listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 77,  167 => 69,  161 => 65,  152 => 62,  146 => 61,  142 => 60,  138 => 59,  135 => 58,  129 => 56,  123 => 54,  121 => 53,  117 => 51,  109 => 49,  101 => 47,  99 => 46,  94 => 44,  89 => 43,  85 => 42,  69 => 28,  67 => 27,  56 => 18,  51 => 16,  47 => 15,  43 => 13,  41 => 12,  31 => 4,  28 => 3,);
    }
}
