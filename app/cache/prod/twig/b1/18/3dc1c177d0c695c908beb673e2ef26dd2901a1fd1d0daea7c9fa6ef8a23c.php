<?php

/* CentrohipicoBundle:DataEmpresa:new.html.twig */
class __TwigTemplate_b1183dc1c177d0c695c908beb673e2ef26dd2901a1fd1d0daea7c9fa6ef8a23c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <style>
        em{
            color: #e70203;
            font-size: 11px;
        }
    </style>
    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"col-md-12\">
        <h1 class=\"tit_principal\">Centro Hipico | Empresas</h1>
    </div>
    <div class=\"col-md-12\">
        <p>Los campos con <span class=\"oblig\">(*)</span> son obligatorios. Por favor ingrese los todos los Datos de la empresa y legales para guardar<br/>
        Recuerde llenar los datos de las 3 pestañas (Datos Empresa, Datos Legales, Socios)
        </p>
    </div>
    ";
        // line 18
        $this->env->loadTemplate("CentrohipicoBundle:DataEmpresa:tabs.html.twig")->display($context);
    }

    // line 21
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 22
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/DataEmpresa/validate.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/resource.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" >
        \$(document).ready(function(){
            \$('#tabs').tabs();
            \$(\".form-horizontal\").removeClass('form-horizontal');
            \$(\".form-control\").removeClass('form-control');
            \$(\"#datach #datalegal\").show();
            \$(\"#temp\").hide();
            \$(\"#addDL\").click(function(){
                agregarDL();
            });
        });

        function agregarDL(){
            \$(\"#btnDL\").hide();
            \$(\"#fDL\").show();
            \$(\"#fDL\").html(getGifLoading());
            //var route=Routing.generate('datalegal_new');
            \$.get('";
        // line 41
        echo $this->env->getExtension('routing')->getPath("datalegal_new");
        echo "').success(function(data) {
                if (data.message) {
                    message = data.message;
                } else {
                    \$('#datalegal').html(data);
                }
            }).error(function(data, status, headers, config) {
                        if (status === '500') {
                            message = \"No hay conexión con el servidor\";
                        }
                    });
        }
        function sFormDL(){
            var datach=\$(\"#form_dl\").serialize();
            \$(\"#form_dl\").hide();
            \$(\"#fDL\").hide();
            \$(\"#btnDL\").show();
            //\$.post()
        }

        \$(document).ready(function() {
            \$(\".estados\").change(function() {
                estado = \$(this).val();
                var Rmunicipio=Routing.generate('municipios', {estado_id: estado||0});
                getSelect(Rmunicipio,'.municipio',\"Municipio\");
                \$(\".municipio\").val('');

            });

            \$(\".municipio\").change(function() {
                municipio = \$(this).val();
                \$(\".parroquia\").val('');
                var Rparroquia=Routing.generate('parroquias', {municipio_id: municipio||0});
                getSelect(Rparroquia,'.parroquia',\"Parroquia\");
            });
        });

    </script>
    <script src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/DataEmpresa/form.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataEmpresa:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 79,  82 => 41,  61 => 23,  56 => 22,  53 => 21,  49 => 18,  32 => 3,  29 => 2,);
    }
}
