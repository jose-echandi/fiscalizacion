<?php

/* CentrohipicoBundle:DataOperadora:sucursales.html.twig */
class __TwigTemplate_a4ed2715bf65cf8e9d5f1843397287a02839a82ee866029f8b0bd3f7a7923c63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1 class=\"tit_principal\">Mis Sucursales</h1>
    </div>
";
        // line 7
        $this->env->loadTemplate("NewTemplateBundle::flashMessages.html.twig")->display($context);
        // line 8
        echo " <div class=\"col-md-12\">  
    <table class=\"table table-hover text-center\" id=\"tabla_reporte2\">
        <thead>
            <tr id=\"table_header2\">
                <th class=\"text-center\">Nº</th>
                <th class=\"text-center\">Rif</th>
                <th class=\"text-center\">Denominación Comercial</th>
                <th class=\"text-center\">Clasificación Licencia</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 21
            echo "            <tr>
                <td class=\"text-center bg-white\">";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "</td>
                <td class=\"text-left bg-white\">";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
            echo "  </td>
                <td class=\"text-left bg-white\"><a href=\"#\">";
            // line 24
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial")), "html", null, true);
            echo "</a></td>
                <td class=\"text-left bg-white\">
                    ";
            // line 26
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"));
            foreach ($context['_seq'] as $context["_key"] => $context["lic"]) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "clasfLicencia"), "html", null, true);
                echo " &nbsp; ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lic'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "                    ";
            if (twig_test_empty($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"))) {
                echo "Sin Licencia ";
            }
            // line 28
            echo "                </td>
                <td class=\"text-right  bg-white\"><a href=\"#\" class=\"btn btn-warning btn-sm\" onclick=\"confirmaDel('";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
            echo "','";
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial")), "html", null, true);
            echo "')\"><i class=\"fa fa-times\"></i> Desincorporar</a></td>
            </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "        ";
        if (twig_test_empty((isset($context["entities"]) ? $context["entities"] : null))) {
            echo " 
                <tr>
                  <td colspan=\"5\" class=\"text-center text-uppercase text-info\">No hay Resultados</td>
                </tr>       
         ";
        }
        // line 36
        echo "      
        </tbody>
    </table>
 </div>
        <div class=\"col-md-6 \">
            <a href=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath("operadora_new_sucursal", array("tipo" => "S"));
        echo "\" class=\"btn btn-info btn-sm\">Incorporar Sucursal</a>
        </div>  
      <!-- Modal -->
    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">
                        <span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span>
                    </button>
                     <h4 class=\"modal-title\" id=\"ModalTitle\"></h4>
                </div>
                <div class=\"modal-body\" id=\"Message\"></div>
                 <div class=\"modal-footer\" id=\"Modalfooter\"></div>
            </div>
        </div>
    </div>      
 <form id=\"sucursaldel\" action=\"\" method=\"POST\">
   <input id=\"sucursalid\" name=\"sucursalid\" type=\"hidden\" value=\"\">
</form>
    
  ";
    }

    // line 64
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 65
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/resource.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" >
        function confirmaDel(id,name){
            var msg='<h1 class=\"tit_principal text-center\">Esta Seguro que Desea Eliminar la sucursal? </h1><br/><br/>\\n\\
             <div class=\"col-md-12 text-left\"><h4>'+ name+'</h4><h5>Se Eliminaran los datos de licencia y sucursal.</h5><br/></div><br/><br/>\\n\\
             <br/><div class=\"col-md-9 col-md-offset-3 text-center btn-group\">\\n\\
              <button onclick=\"EjecutarDel(\\''+id+'\\')\" class=\"btn btn-warning\">Desincorporar</button><button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-info\">Cancelar</button>\\n\\
              </div>';
            ModalShow(msg,\"Desincorporar Sucursal\");
        }

    function ModalShow(msg,title){
        \$(\"#Message\").html(msg);
        \$(\"#ModalTitle\").html(title);
        console.log(msg);
        \$(\"#myModal\").modal(\"show\");
    }  
    
    function EjecutarDel(id){
        \$(\"#Message\").html(\"<h3>Espere un momento mientras se Procesa al Petición...<h3>\");
        var action=Routing.generate('operadora_delete_sucursal', {id: id});
        \$(\"#sucursalid\").val(id);
        \$(\"#sucursaldel\").attr('action',action);
        \$(\"#sucursaldel\").submit();
        //window.location=action;
    }
  </script>        
  ";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataOperadora:sucursales.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 65,  169 => 64,  143 => 41,  136 => 36,  127 => 32,  108 => 29,  105 => 28,  100 => 27,  89 => 26,  84 => 24,  78 => 23,  74 => 22,  71 => 21,  54 => 20,  40 => 8,  38 => 7,  32 => 3,  29 => 2,);
    }
}
