<?php

/* SolicitudesCitasBundle:DataSolicitudes:generadaCH_print.html.twig */
class __TwigTemplate_be2092d5b4b329fcae768f6cf967cbb725cfd321621bc63c46eb43eb853582a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"font-family: tahoma; font-size: 13px; text-align: center; width: 698px; height: 952px; padding: 10px; margin: 0px auto;\">
\t\t<div style=\"width: 100%; text-align: right; margin-top: 0px; float: left;\"><img src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/common/images/header_gubernamental.png"), "html", null, true);
        echo "\" alt=\"\"></div>

\t\t<div style=\"width: 698px; text-align: center; float: left; margin-top: 20px;\">
\t\t\t<p style=\" float: left; font-size: 14px; font-weight: bold; margin: 0; width: 100%;\">SISTEMA NACIONAL DE LICENCIAS HÍPICAS (SISNALHP)</p>
\t\t\t<p style=\" float: left; font-size: 14px; font-weight: bold; margin: 0; width: 100%;\">COMPROBANTE DE CITA PARA ENTREGA DE DOCUMENTOS</p>
\t\t\t<p style=\" float: left; font-size: 14px; font-weight: bold; margin: 0; width: 100%;\">CENTRO DE APUESTAS</p>
\t\t</div>

\t\t<div style=\"width: 100%; text-align: center; margin-top: 30px; float: left;\">
\t\t\t<table style=\"width: 100%; border: 1px solid rgb(200, 200, 200);\">
\t\t\t\t<tr style=\"background: none repeat scroll 0% 0% rgb(160, 21, 3); color: rgb(255, 255, 255); text-transform: uppercase;\">
\t\t\t\t\t<td colspan=\"3\">Datos de la Solicitud</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr style=\"background: #efefef;\">
\t\t\t\t\t<td>RIF del Solicitante</td>
\t\t\t\t\t<td>Fecha Cita</td>\t
\t\t\t\t\t<td>Localizador</td>\t\t\t\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "rif"), "html", null, true);
        echo "</td>\t\t\t\t\t
\t\t\t\t\t<td>";
        // line 22
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "fechaSolicitud"), "d-m-Y"), "html", null, true);
        echo "</td>\t
\t\t\t\t\t<td>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codsolicitud"), "html", null, true);
        echo "</td>\t\t\t\t\t
\t\t\t\t</tr>
\t\t\t</table>
\t\t</div>
\t\t<div style=\"width: 100%; text-align: center; margin-top: 30px; float: left;\">
\t\t\t<table style=\"width: 100%; border: 1px solid rgb(200, 200, 200);\">
\t\t\t\t<tr style=\"background: none repeat scroll 0% 0% rgb(160, 21, 3); color: rgb(255, 255, 255); text-transform: uppercase;\">
\t\t\t\t\t<td colspan=\"3\">Detalles de la Solicitud</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr style=\"background: #efefef;\">
\t\t\t\t\t<td>Tipo de Licencia</td>
\t\t\t\t\t<td>Clasificación</td>
\t\t\t\t\t<td>Operadora</td>\t\t\t\t\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tipoSolicitud"), "html", null, true);
        echo "</td>
\t\t\t\t\t<td>";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ClasLicencia"), "clasfLicencia"), "html", null, true);
        echo "</td>
\t\t\t\t\t<td>";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "denominacionComercial"), "html", null, true);
        echo "</td>\t\t\t\t\t\t
\t\t\t\t</tr>
\t\t\t</table>
\t\t</div>\t\t
\t\t<div style=\"width: 100%; text-align: left; margin-top: 30px; float: left;\">
\t\t\t<table style=\"width: 100%; border: 1px solid rgb(200, 200, 200);\">
\t\t\t\t<tr style=\"background: none repeat scroll 0% 0% rgb(160, 21, 3); color: rgb(255, 255, 255); text-transform: uppercase;\">
\t\t\t\t\t<td colspan=\"2\">Documentos a Consignar</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">1.\t</td>
\t\t\t\t\t<td>Persona Natural Copia de la Cédula de Identidad. Persona Jurídica. Acta constitutiva de la empresa, sus modificaciones estatutarias si las hubiere, debidamente registradas. Las empresa debe tener claramente establecido en el objeto de sus estatutos “la operación de actividades hípicas”.</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">2.\t</td>
\t\t\t\t\t<td>Registro de Información Fiscal de la persona solicitante.</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">3.\t</td>
\t\t\t\t\t<td>Persona Jurídica: Cédula de Identidad del Representante Legal y de las persona solicitante.</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">4.\t</td>
\t\t\t\t\t<td>Si la solicitud se realiza a través de un apoderado, deberá consignar copia certificada del poder autenticado que acredita el mandato y la  cédula del apoderado. Si el Inmueble donde se explotará la actividad es propio: Copia del documento de propiedad del Inmueble.</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">5.\t</td>
\t\t\t\t\t<td>Si el Inmueble donde se explotará la actividad es propio: Copia del documento de propiedad del Inmueble. Si el inmueble donde se explotará la actividad es arrendado : Copia del documento de arrendamiento del inmueble y autorización autenticada del propietario del mismo que lo autoriza a utilizar el inmueble arrendado para explotar la actividad hípica.</td>\t
\t\t\t\t</tr>\t
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">6.\t</td>
\t\t\t\t\t<td>Plano de ubicación del establecimiento.</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">7.\t</td>
\t\t\t\t\t<td>Carta de Buena Pro emitida por la empresa Operadora con la que pretende trabajar.</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">8.\t</td>
\t\t\t\t\t<td>Listado de los juegos a explotar.</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">9.\t</td>
\t\t\t\t\t<td>Concesión otorgada por el Instituto Nacional de Hipódromos que lo acredita como Centro de Apuestas autorizado por dicha institución.</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">10.\t</td>
\t\t\t\t\t<td>Declaración Jurada debidamente autenticada, indicando el origen de los recursos económicos del capital, aumentos de capital.</td>\t\t
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td style=\"vertical-align: top;\">11.\t</td>
\t\t\t\t\t<td>Depósito bancario con el que se canceló la tarifa correspondiente al procesamiento de la solicitud.</td>\t\t
\t\t\t\t</tr>
\t\t\t</table>
\t\t</div>\t
\t\t<div style=\"width: 100%; text-align: left; float: left; background: none repeat scroll 0% 0% rgb(160, 21, 3); color: rgb(255, 255, 255); height: 2px; margin-top: 10px; margin-bottom: 2px;\">
\t\t</div>

\t\t<div style=\"width: 100%; text-align: center; float: left; font-size: 12px;\">
\t\t\t<p>Oficina Principal: Hipódromo la rinconada, sede central del Instituto Nacional Hipódromo, Piso 5, parroquia Coche del Municipio Libertador. Teléfonos: 58 (212)681 33 31</p>
\t\t</div>


\t</div>     ";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:generadaCH_print.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 40,  74 => 39,  70 => 38,  52 => 23,  48 => 22,  44 => 21,  22 => 2,  19 => 1,);
    }
}
