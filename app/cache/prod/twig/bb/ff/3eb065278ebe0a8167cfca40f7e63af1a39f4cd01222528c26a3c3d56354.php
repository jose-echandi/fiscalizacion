<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_bbff3eb065278ebe0a8167cfca40f7e63af1a39f4cd01222528c26a3c3d56354 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 10
        echo "
";
        // line 11
        $this->displayBlock('body_html', $context, $blocks);
    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "fullname"), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
    }

    // line 11
    public function block_body_html($context, array $blocks = array())
    {
        // line 12
        $this->env->loadTemplate("UserBundle:Notification:resetting.email.html.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  53 => 12,  50 => 11,  45 => 8,  39 => 4,  36 => 2,  32 => 11,  29 => 10,  27 => 8,  24 => 7,  22 => 2,);
    }
}
