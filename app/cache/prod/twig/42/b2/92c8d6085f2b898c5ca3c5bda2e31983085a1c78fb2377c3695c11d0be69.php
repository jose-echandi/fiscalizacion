<?php

/* FOSUserBundle:DatosBasicos:editDatosBasicos.html.twig */
class __TwigTemplate_42b292c8d6085f2b898c5ca3c5bda2e31983085a1c78fb2377c3695c11d0be69 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_base' => array($this, 'block_script_base'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    
    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"col-md-12\">
            <h1 class=\"tit_principal\">Modificar Datos Básicos</h1>
    </div>
    <div class=\"col-md-12\"><p>Los campos con <span class=\"oblig\">*</span> son obligatorios</p></div>
           <div class=\"col-md-12\">
                ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "  
            </div>
    <div class=\"col-md-12\"> 
    ";
        // line 14
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("class" => "form-horizontal form-datos-basicos", "role" => "form", "style" => "width:85%", "id" => "extra_user_profile")));
        echo "
       <table id=\"tabla_reporte2\" class=\"bg-white text-left table\">\t\t\t\t
           <thead>
            <tr id=\"table_header2\">
              <td colspan=\"4\">Datos Básicos </td>
            </tr> 
           </thead>  
      <tbody>      
        <tr> 
          ";
        // line 23
        echo "  
          <td ><label for=\"ci\" class=\"col-md-12 control-label text-left padding-1\">CI</label></td>
          <td> 
              ";
        // line 31
        echo "              ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "perfil"), 0, array(), "array"), "getPersJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "perfil"), 0, array(), "array"), "getCi"), "html", null, true);
        echo "
        </td> 
          <td ><label for=\"rif_t\" class=\"col-md-12 control-label text-left\">RIF</label></td>
          <td>
              ";
        // line 39
        echo "               ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "perfil"), 0, array(), "array"), "getPersJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "perfil"), 0, array(), "array"), "getRif"), "html", null, true);
        echo "
         </td>  
    </tr>
    <tr>
        <td ><label for=\"nombre\" class=\"col-md-12 padding-1 control-label\">Nombre</label></td>
            <td >";
        // line 45
        echo "                ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "perfil"), 0, array(), "array"), "getNombre"), "html", null, true);
        echo "
            </td>
        <td ><label for=\"ci\" class=\"col-md-12 padding-1 control-label\">Apellido</label></td>
            <td >
                ";
        // line 50
        echo "                ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "perfil"), 0, array(), "array"), "getApellido"), "html", null, true);
        echo "
            </td>
    </tr>  
    <tr>
        <td >";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'label', array("label_attr" => array("class" => "col-sm-12 padding-1")));
        echo "</td>
        <td >";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'widget');
        echo "
            ";
        // line 56
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 57
            echo "                    <span class=\"help-block \">
                        ";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
           ";
        }
        // line 61
        echo "        </td>
    ";
        // line 63
        echo "    <td ><label class=\"col-sm-12 padding-1\">Municipio <span class=\"oblig\">*</span></label>
         <input type=\"hidden\" id=\"validaCiudMunParro\" value=\"-1\">
     </td>           
            <td>
                ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), 'widget', array("attr" => array("class" => "municipio", "required" => "required")));
        echo "
                  ";
        // line 68
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 69
            echo "                    <span class=\"help-block \">
                        ";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                  ";
        }
        // line 72
        echo " 
            </td>             
        </tr> 
        <tr>
            <td>";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
            <td>";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), 'widget', array("attr" => array("required" => "required")));
        echo "
                ";
        // line 78
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 79
            echo "                    <span class=\"help-block \">
                        ";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 83
        echo "            </td>
            <td><label class=\"col-md-12 padding-1\" >Parroquia <span class=\"oblig\">*</span></label></td>
            <td> 
                ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), 'widget', array("attr" => array("class" => "parroquia", "required" => "required")));
        echo " 
            </td>
     </tr> 
     <tr>
         <td> ";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanizacion"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
          <td >";
        // line 91
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanizacion"), 'widget');
        echo "
                 ";
        // line 92
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanizacion", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 93
            echo "                    <span class=\"help-block \">
                        ";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanizacion"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 97
        echo "          </td>
          <td >";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "calle"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
          <td >";
        // line 99
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "calle"), 'widget');
        echo "
                  ";
        // line 100
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "calle", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 101
            echo "                    <span class=\"help-block \">
                        ";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "calle"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 105
        echo "          </td>
      </tr>
      <tr> 
          <td >";
        // line 108
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apartamento"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
            <td >";
        // line 109
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apartamento"), 'widget');
        echo "
                  ";
        // line 110
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apartamento", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 111
            echo "                    <span class=\"help-block \">
                        ";
            // line 112
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apartamento"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 115
        echo "            </td>
            <td >";
        // line 116
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apartamento_no"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
            <td >";
        // line 117
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apartamento_no"), 'widget');
        echo "
                  ";
        // line 118
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apartamento_no", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 119
            echo "                    <span class=\"help-block \">
                        ";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apartamento_no"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 123
        echo "            </td>
       </tr>      
       <tr> 
           <td >";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "referencia"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
            <td >";
        // line 127
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "referencia"), 'widget');
        echo "
                 ";
        // line 128
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "referencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 129
            echo "                    <span class=\"help-block \">
                        ";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "referencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 133
        echo "            </td>
            <td >";
        // line 134
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigo_postal"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
            <td >";
        // line 135
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigo_postal"), 'widget');
        echo "</td>
       </tr>
       <tr>
           <td >";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
            <td >";
        // line 139
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cod_fax"), 'widget', array("attr" => array("class" => "col-md-4")));
        echo "
                 ";
        // line 140
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'widget', array("attr" => array("class" => "col-md-6")));
        echo "
            
            </td>
            <td >";
        // line 143
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cod_telefono_local"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
            <td >";
        // line 144
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cod_telefono_local"), 'widget', array("attr" => array("class" => "col-md-4")));
        echo "
                 ";
        // line 145
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "telefono_local"), 'widget', array("attr" => array("class" => "col-md-6")));
        echo "
             </td>
       </tr>
       <tr>
           <td >";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cod_telefono_movil"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
            <td >";
        // line 150
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cod_telefono_movil"), 'widget', array("attr" => array("class" => "col-md-4")));
        echo "
                 ";
        // line 151
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "telefono_movil"), 'widget', array("attr" => array("class" => "col-md-6")));
        echo "
           </td>
        <td >";
        // line 153
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "correo_alternativo"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
         <td >";
        // line 154
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "correo_alternativo"), 'widget');
        echo "</td>
      </tr> 
      <tr>
          <td >";
        // line 157
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "sitio_web"), 'label', array("label_attr" => array("class" => "col-md-12 padding-1")));
        echo "</td>
          <td colspan=\"3\">";
        // line 158
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "sitio_web"), 'widget', array("attr" => array("size" => "70")));
        echo "</td>
       </tr>      
    </tbody>
   </table>
 <div class=\"col-md-12\">
    <div style=\"float:left\">
        <a href=\"";
        // line 164
        echo $this->env->getExtension('routing')->getPath("datos-basicos");
        echo "\" class=\"btn btn-success btn-sm\">Regresar</a>
    </div>
    <div style=\"float:right\">
        <button id=\"form_btn\" type=\"submit\" class=\"btn btn-primary btn-sm\">Guardar</button>
    </div>
</div>
    ";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token"), 'widget');
        echo "
    ";
        // line 171
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
    <div class=\"block-separator col-md-12\"></div> 
    
</div><!-- Fin Content -->               
  
";
    }

    // line 178
    public function block_script_base($context, array $blocks = array())
    {
        // line 179
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/resource.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/user/js/registro.js"), "html", null, true);
        echo "\"></script>
   <script src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/user/js/DatosBasicos/validate.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" >
\$(document).ready(function(){
  \$(\".form-horizontal\").removeClass('form-horizontal');
  \$(\".form-control\").removeClass('form-control');
  // estado, municipio, parroquia, ciudad
  \$(\".estados\").change(function() {
    estado = \$(this).val();
    ";
        // line 190
        echo "    \$('.parroquia').val('');
    var Rmunicipio=Routing.generate('municipios', {estado_id: estado||0});
    getSelect(Rmunicipio,'.municipio',\"Municipio\");
  });

  \$(\".municipio\").change(function() {
    municipio = \$(this).val();
    var Rparroquia=Routing.generate('parroquias', {municipio_id: municipio||0});
    getSelect(Rparroquia,'.parroquia',\"Parroquia\");
  });
  \$('.estados').change();
  \$('.municipio').change();
});
</script>         
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:DatosBasicos:editDatosBasicos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 190,  405 => 181,  401 => 180,  396 => 179,  393 => 178,  383 => 171,  379 => 170,  370 => 164,  361 => 158,  357 => 157,  351 => 154,  347 => 153,  342 => 151,  338 => 150,  334 => 149,  327 => 145,  323 => 144,  319 => 143,  313 => 140,  309 => 139,  305 => 138,  299 => 135,  295 => 134,  292 => 133,  286 => 130,  283 => 129,  281 => 128,  277 => 127,  273 => 126,  268 => 123,  262 => 120,  259 => 119,  257 => 118,  253 => 117,  249 => 116,  246 => 115,  240 => 112,  237 => 111,  235 => 110,  231 => 109,  227 => 108,  222 => 105,  216 => 102,  213 => 101,  211 => 100,  207 => 99,  203 => 98,  200 => 97,  194 => 94,  191 => 93,  189 => 92,  185 => 91,  181 => 90,  174 => 86,  169 => 83,  163 => 80,  160 => 79,  158 => 78,  154 => 77,  150 => 76,  144 => 72,  138 => 70,  135 => 69,  133 => 68,  129 => 67,  123 => 63,  120 => 61,  114 => 58,  111 => 57,  109 => 56,  105 => 55,  101 => 54,  93 => 50,  85 => 45,  74 => 39,  64 => 31,  59 => 23,  47 => 14,  41 => 11,  32 => 4,  29 => 3,);
    }
}
