<?php

/* NewTemplateBundle:Menu:mnu_usuario.html.twig */
class __TwigTemplate_2ac76680eac4d03027a48170a158ba145a3647ef2d36f3db719267bc897b0e12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3>Datos de Usuario</h3>
<ul class=\"menu2\">
<li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("datos-basicos");
        echo "\" title=\"\">Datos Básicos</a></li>
";
        // line 5
        echo "</ul>

<h3>Mis Empresas</h3>
<ul class=\"menu2\">
<li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("data_empresa_list");
        echo "\" title=\"\">Consultar</a></li>
</ul>    

<h3>Mis Centros Hípicos</h3>
<ul class=\"menu2\">
";
        // line 15
        echo "<li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("datacentrohipico_new_est");
        echo "\" title=\"\">Crear</a></li>
<li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("datacentrohipico_list");
        echo "\" title=\"\">Consultar</a></li>
</ul>

<h3>Mis Solicitudes de Licencia</h3>
<ul class=\"menu2\">
<li><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("solicitudes_citas_create");
        echo "\" title=\"Crear Solicitudes\">Crear</a></li>
<li><a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("solicitudes_list");
        echo "\" title=\"Consulta de Solicitudes\">Consultar</a></li>
";
        // line 24
        echo "</ul>\t

<h3>Mis Pagos</h3>
<ul class=\"menu2\">
<li><a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "PROCESAMIENTO"));
        echo "\" title=\"Pago de Procesamiento\">Procesamiento</a></li>
<li><a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "OTORGAMIENTO"));
        echo "\" title=\"Pago de Otorgamiento\">Otorgamiento</a></li>
<li><a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("pagos", array("tipo" => "MULTA"));
        echo "\" title=\"Pago de Multas\">Multa</a></li>
</ul>

<h3>Mis Fiscalizaciones</h3>
<ul class=\"menu2\">
<li><a href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("fiscalizacion_mis");
        echo "\" title=\"Consulta de Fiscalizaciones\">Consultar</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle:Menu:mnu_usuario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 35,  72 => 29,  68 => 28,  41 => 15,  33 => 9,  27 => 5,  23 => 3,  155 => 55,  151 => 53,  148 => 52,  145 => 51,  142 => 50,  139 => 49,  136 => 48,  133 => 47,  130 => 46,  127 => 45,  124 => 44,  121 => 43,  118 => 42,  115 => 41,  112 => 40,  109 => 39,  106 => 38,  103 => 37,  100 => 36,  97 => 35,  94 => 34,  89 => 32,  82 => 30,  76 => 30,  73 => 28,  70 => 26,  66 => 24,  62 => 24,  58 => 22,  54 => 21,  50 => 16,  46 => 16,  42 => 12,  38 => 10,  35 => 9,  32 => 7,  30 => 6,  26 => 5,  21 => 2,  19 => 1,  52 => 13,  43 => 11,  39 => 10,  31 => 4,  28 => 3,);
    }
}
