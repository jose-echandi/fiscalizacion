<?php

/* NewTemplateBundle:Menu:mnu_admin.html.twig */
class __TwigTemplate_6a1f08f8d4b05ce05d7ca0aff01db4869cf83440b533980687cdaca346f32f1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3>Administración de Usuarios</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("listado");
        echo "\" title=\"Usuarios\">Usuarios</a></li>
</ul>

<h3>Administrador de Licencias</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("admtipoaporte");
        echo "\" title=\"Aportes de Licencias\">Aportes de Licencias</a></li>
    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("admclasflicencias");
        echo "\" title=\"Licencias\">Licencias</a></li>
    <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("admtiposlicencias");
        echo "\" title=\"Tipos de Licencias\">Tipos de Licencias</a></li>
    <li><a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("admrecaudoslicencias");
        echo "\" title=\"Recaudos\">Recaudos</a></li>
    <li><a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("admjuegosexplotados");
        echo "\" title=\"Juegos Explotados\">Juegos Explotados</a></li>
    <li><a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("admclasfestab");
        echo "\" title=\"Clasificación de Establecimientos\">Clasificación de Establecimientos</a></li>    
</ul>

<h3>Bancos</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("banco");
        echo "\" title=\"Bancos\">Consultar</a></li>
</ul>";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle:Menu:mnu_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 18,  51 => 13,  47 => 12,  43 => 11,  39 => 10,  35 => 9,  31 => 8,  23 => 3,  19 => 1,);
    }
}
