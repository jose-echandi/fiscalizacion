<?php

/* SolicitudesCitasBundle:AdminLicencias:pdf/licencia.html.twig */
class __TwigTemplate_dfb28d396347e61653c13f1d0ec9508e7501c50995de9e07d8783a602a5a1057 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"UTF-8\">
        <title>Licencia Hípica</title>
    </head>
    <body>
        <div id=\"licencia\">
            <table width=\"\" align=\"center\" style=\"background-image: url( ";
        // line 9
        echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "schemeAndHttpHost") . $this->env->getExtension('assets')->getAssetUrl("bundles/common/images/bg/licencia.jpg")), "html", null, true);
        echo "); background-size: contain; background-repeat: no-repeat; width: 972px; height: 718px; font-weight: bold;\">
                <tbody>
                    <tr>
                        <td align=\"center\">
                            <div style=\"font-size: 10px; text-align: center; width: 972px; float: left; height: 718px;\"> 

                                <div style=\"float: left; text-align: center; height: 40px; width: 972px; text-transform: uppercase; font-size: 25px; margin-top: 40px;\">
                                    <span>Licencia Hípica</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: 40px; width: 972px; text-transform: uppercase; font-size: 15px;margin-top: 15px;\">
                                    <span>";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["tipo_licencia"]) ? $context["tipo_licencia"] : null), "html", null, true);
        echo "</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: 40px; width: 972px; text-transform: uppercase; font-size: 12px;margin-top: 0px; font-weight: normal; text-transform: uppercase;\">
                                    <span>El Superintendente Nacional de Actividades Hípicas en uso de la competencia exclusiva establecida en el artículo 28 del Decreto Nro. 422 con Rango y Fuerza de Ley que Suprime y Liquida al Instituto Nacional de Hipódromos y Regula las Actividades Hípicas, publicado en la Gaceta Oficial de la República Bolivariana de Venezuela Nro. 5397 extraordinaria de fecha 25 de octubre de 1999, otorga la presente Licencia Nro.</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: 40px; width: 972px; text-transform: uppercase; font-size: 15px; margin-top: 19px;\">
                                    <span>";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["cod_licencia"]) ? $context["cod_licencia"] : null), "html", null, true);
        echo "</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: auto; width: 972px; text-transform: uppercase; font-size: 13px; margin-top:-12px; font-weight: normal;\">
                                    <div style=\"float: left; text-align: center; margin-left: 3%; margin-right: 3%; width: 44%;\">
                                        <span>A la Sociedad Mercantil:</span>
                                    </div>
                                    <div style=\"float: left; text-align: center; margin-left: 3%; margin-right: 3%; width: 44%;\">
                                        <span>RIF:</span>
                                    </div>
                                </div>

                                <div style=\"float: left; text-align: center; height: auto; width: 972px; text-transform: uppercase; font-size: 13px; margin-top: 9px;\">
                                    <div style=\"float: left; text-align: center; margin-left: 3%; margin-right: 3%; width: 44%;\">
                                        <span>";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["establecimiento"]) ? $context["establecimiento"] : null), "html", null, true);
        echo "</span>
                                    </div>
                                    <div style=\"float: left; text-align: center; margin-left: 3%; margin-right: 3%; width: 44%;\">
                                        <span>";
        // line 45
        echo twig_escape_filter($this->env, (isset($context["rif"]) ? $context["rif"] : null), "html", null, true);
        echo "</span>
                                    </div>
                                </div>

                                <div style=\"float: left; text-align: center; height: auto; width: 100%; text-transform: uppercase; font-size: 11px; margin-top: 16px; padding-top: 5px; height: 17px; background-color: rgba(28, 148, 210, 0.5);\">
                                    <span style=\"float: left; text-align: left; width: 220px; padding-left: 50px;  font-weight: normal; color:#283a86;\">Nro. de Providencia Administrativa</span>
                                    <span style=\"float: left; text-align: left; width: 170px; padding-left: 25px;\">";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["num_providencia"]) ? $context["num_providencia"] : null), "html", null, true);
        echo "</span>
                                    <span style=\"float: left; text-align: right; width: 175px; padding-left: 2px; font-weight: normal; color:#283a86;\">Desde</span>
                                    <span style=\"float: left; text-align: left; width: 220px;padding-left: 25px;\">";
        // line 53
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["desde"]) ? $context["desde"] : null), "d/m/Y"), "html", null, true);
        echo "</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: auto; width: 100%; text-transform: uppercase; font-size: 11px; margin-top: 3px; padding-top: 5px; height: 17px; background-color: rgba(28, 148, 210, 0.5);\">
                                    <span style=\"float: left; text-align: left; width: 220px; padding-left: 50px; font-weight: normal; color:#283a86;\">Fecha Providencia</span>
                                    <span style=\"float: left; text-align: left; width: 170px; padding-left: 25px;\">";
        // line 58
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["fecha_providencia"]) ? $context["fecha_providencia"] : null), "d/m/Y"), "html", null, true);
        echo "</span>\t
                                    <span style=\"float: left; text-align: right; width: 175px; padding-left: 2px; font-weight: normal; color:#283a86;\">Hasta</span>
                                    <span style=\"float: left; text-align: left; width: 220px;padding-left: 25px;\">";
        // line 60
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["hasta"]) ? $context["hasta"] : null), "d/m/Y"), "html", null, true);
        echo "</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: 0px; width: 972px; text-transform: uppercase; font-size: 12px; margin-top: 4px;\">
                                    <span>Dirección del Establecimiento</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: 20px; width: 972px; text-transform: uppercase; font-size: 11px;margin-top: 17px; padding-top: 5px; height: 17px; background-color: rgba(28, 148, 210, 0.5);\">
                                    <span style=\"float: left; text-align: left; width: 115px; padding-left: 60px; font-weight: normal; color:#283a86;\">Entidad Federal</span>
                                    <span style=\"float: left; text-align: left; overflow: hidden; padding-left: 15px; width: 270px;\">";
        // line 69
        echo twig_escape_filter($this->env, (isset($context["estado"]) ? $context["estado"] : null), "html", null, true);
        echo "</span>
                                    <span style=\"float: left; text-align: left; width: 70px; padding-left: 80px; font-weight: normal; color:#283a86;\">Municipio</span>
                                    <span style=\"float: left; text-align: left; overflow: hidden; padding-left: 15px; width: 260px;\">";
        // line 71
        echo twig_escape_filter($this->env, (isset($context["municipio"]) ? $context["municipio"] : null), "html", null, true);
        echo "</span>  
                                </div>

                                <div style=\"float: left; text-align: center; height: 20px; width: 972px; text-transform: uppercase; font-size: 11px;margin-top: 3px; padding-top: 5px; height: 17px; background-color: rgba(28, 148, 210, 0.5);\">
                                    <span style=\"float: left; text-align: left; width: 115px; padding-left: 60px; font-weight: normal; color:#283a86;\">Parroquia</span>
                                    <span style=\"float: left; text-align: left; overflow: hidden; padding-left: 15px; width: 270px;\">";
        // line 76
        echo twig_escape_filter($this->env, (isset($context["parroquia"]) ? $context["parroquia"] : null), "html", null, true);
        echo "</span> 
                                    <span style=\"float: left; text-align: left; width: 70px; padding-left: 80px; font-weight: normal; color:#283a86;\">Ciudad</span>
                                    <span style=\"float: left; text-align: left; overflow: hidden; padding-left: 15px; width: 260px;\">";
        // line 78
        echo twig_escape_filter($this->env, (isset($context["ciudad"]) ? $context["ciudad"] : null), "html", null, true);
        echo "</span>\t
                                </div>

                                <div style=\"float: left; text-align: center; height: 20px; width: 972px; text-transform: uppercase; font-size: 11px;margin-top: 3px; padding-top: 5px; height: 17px; background-color: rgba(28, 148, 210, 0.5);\">
                                    <span style=\"float: left; text-align: left; width: 115px; padding-left: 60px; font-weight: normal; color:#283a86;\">Sector</span>
                                    <span style=\"float: left; text-align: left; overflow: hidden; padding-left: 15px; width: 270px;\">";
        // line 83
        echo twig_escape_filter($this->env, (isset($context["sector"]) ? $context["sector"] : null), "html", null, true);
        echo "</span>
                                    <span style=\"float: left; text-align: left; width: 70px; padding-left: 80px; font-weight: normal; color:#283a86;\">Calle</span>
                                    <span style=\"float: left; text-align: left; overflow: hidden; padding-left: 15px; width: 260px;\">";
        // line 85
        echo twig_escape_filter($this->env, (isset($context["calle"]) ? $context["calle"] : null), "html", null, true);
        echo "</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: 20px; width: 972px; text-transform: uppercase; font-size: 11px;margin-top: 3px; padding-top: 5px; height: 17px; background-color: rgba(28, 148, 210, 0.5);\">\t
                                    <span style=\"float: left; text-align: left; width: 115px; padding-left: 60px; font-weight: normal; color:#283a86;\">Casa</span>
                                    <span style=\"float: left; text-align: left; overflow: hidden; padding-left: 15px; width: 270px;\">";
        // line 90
        echo twig_escape_filter($this->env, (isset($context["casa"]) ? $context["casa"] : null), "html", null, true);
        echo "</span>
                                </div>

                                ";
        // line 93
        if ((((isset($context["afiliado"]) ? $context["afiliado"] : null) != "") && ((isset($context["rif_afiliado"]) ? $context["rif_afiliado"] : null) != ""))) {
            // line 94
            echo "
                                <div style=\"float: left; text-align: center; height: 0px; width: 972px; text-transform: uppercase; font-size: 12px; margin-top: 15px;\">
                                    <span>Empresa Operadora</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: auto; width: 972px; text-transform: uppercase; font-size: 11px; margin-top: 18px; padding-top: 5px; height: 17px; background-color: rgba(28, 148, 210, 0.5);\">
                                    <span style=\"float: left; text-align: left; width: 150px; padding-left: 60px; font-weight: normal; color:#283a86;\">Nombre o Razón Social</span>
                                    <span style=\"float: left; text-align: left; padding-left: 10px; width: 660px;  overflow: hidden;\">";
            // line 101
            echo twig_escape_filter($this->env, (isset($context["afiliado"]) ? $context["afiliado"] : null), "html", null, true);
            echo "</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: auto; width: 972px; text-transform: uppercase; font-size: 11px; margin-top: 3px; padding-top: 5px; height: 17px; background-color: rgba(28, 148, 210, 0.5);\">
                                    <span style=\"float: left; text-align: left; width: 150px; padding-left: 60px; font-weight: normal; color:#283a86;\">RNLH</span>
                                    <span style=\"float: left; text-align: left; padding-left: 10px; width: 240px; overflow: hidden;\">0005</span>
                                    <span style=\"float: left; text-align: left; width: 30px; padding-left: 165px; font-weight: normal; color:#283a86;\">RIF</span>
                                    <span style=\"float: left; text-align: left; padding-left: 20px; width: 210px; overflow: hidden;\">";
            // line 108
            echo twig_escape_filter($this->env, (isset($context["rif_afiliado"]) ? $context["rif_afiliado"] : null), "html", null, true);
            echo "</span>
                                </div>
                                ";
            // line 114
            echo "
                                <div style=\"float: left; text-align: center; height: 0px; width: 972px; text-transform: uppercase; font-size: 15px; margin-top: 85px;\">
                                    
                                ";
        } else {
            // line 118
            echo "                                    
                                <div style=\"float: left; text-align: center; height: 0px; width: 972px; text-transform: uppercase; font-size: 15px; margin-top: 165px;\">
                                    
                                ";
        }
        // line 122
        echo "                                                                    
                                    <span>FERNANDO VALENTINO</span>
                                </div>
                                <div style=\"float: left; text-align: center; height: 0px; width: 972px; text-transform: uppercase; font-size: 12px; margin-top: 20px;\">
                                    <span>Superintendente Nacional de Actividades Hipicas (E).</span>
                                </div>

                                <div style=\"float: right; text-align: center; height: 100px; width: 100px; text-transform: uppercase; margin-top: -70px; margin-right: 110px; background-color: #fff;\">
                                    <span><img src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('endroid_qrcode')->qrcodeDataUriFunction((isset($context["data_qr"]) ? $context["data_qr"] : null), null, 100, 10, null), "html", null, true);
        echo "\" /></span>
                                </div>

                                <!--<div style=\"float: left; text-align: center; height: 0px; width: 472px; text-transform: uppercase; font-size: 10px; text-transform: uppercase; margin-top: 25px;padding-left: 245px; font-weight: normal;\">-->
                                <div style=\"float: left; text-align: center; height: 0px; width: 505px; text-transform: uppercase; font-size: 10px; text-transform: uppercase; margin-top: 25px;padding-left: 245px; font-weight: normal;\">
                                    <span>Resolución Nro.074/13 del 23/09/2013, publicada en la Gaceta Oficial de la República Bolivariana de Venezuela Nro. 40.260 de fecha 27 de Septiembre de 2013</span>
                                </div>

                                <div style=\"float: left; text-align: center; height: 0px; width: 972px; text-transform: uppercase; font-size: 9px; text-transform: uppercase; margin-top: 30px; font-weight: normal;\">
                                    <span>Se hace del conocimiento del licenciatario que la Superintendencia Nacional de Actividades Hípicas podrá suspender o revocar esta licencia si verifica el incumplimiento de las disposiciones establecidas en el Decreto Nro° 422 y las normas que establezca esta superintendencia. Esta Licencia es intransferible y servira como instrumento sólo para la explotación de carreras hípicas (nacionales o internacionales según el caso), en el establecimiento aquí descrito.</span>
                                </div>

                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:AdminLicencias:pdf/licencia.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 130,  199 => 122,  193 => 118,  187 => 114,  182 => 108,  172 => 101,  163 => 94,  161 => 93,  155 => 90,  147 => 85,  142 => 83,  134 => 78,  129 => 76,  121 => 71,  116 => 69,  104 => 60,  99 => 58,  91 => 53,  86 => 51,  77 => 45,  71 => 42,  54 => 28,  43 => 20,  29 => 9,  19 => 1,);
    }
}
