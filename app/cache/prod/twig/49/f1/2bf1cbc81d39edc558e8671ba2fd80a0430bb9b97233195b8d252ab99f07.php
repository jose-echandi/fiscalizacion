<?php

/* SolicitudesCitasBundle:AdminCitas:modal.html.twig */
class __TwigTemplate_49f12bf1cbc81d39edc558e8671ba2fd80a0430bb9b97233195b8d252ab99f07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style type=\"text/css\">
    .popover.rigth {
        position: fixed !important;
        z-index: 8600 !important;
        display: inline-table !important;
    }
</style>
    <table id=\"tabla_reporte\">
        <tbody><tr id=\"table_header\">
            <td colspan=\"4\">Denominación Comercial:
                <strong>
                    ";
        // line 12
        if ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora", array(), "any", false, true), "denominacionComercial", array(), "any", true, true)) {
            // line 13
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "denominacionComercial"), "html", null, true);
            echo "
                    ";
        } else {
            // line 15
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "denominacionComercial"), "html", null, true);
            echo "
                    ";
        }
        // line 17
        echo "                </strong>
            </td>
        </tr>
        <tr>
            <td style=\"width: 70%; text-align: right;\" colspan=\"3\">Nª de Solicitud:</td>
            <td><strong>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "codsolicitud"), "html", null, true);
        echo "</strong></td>
        </tr>
        <tr>
            <td style=\"width: 70%; text-align: right;\" colspan=\"3\">Licencia:</td>
            <td><strong>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasLicencia"), "admTiposLicencias"), "tipoLicencia"), "html", null, true);
        echo "</strong></td>
        </tr>
        <tr>
            <td style=\"width: 70%; text-align: right;\" colspan=\"3\">Clasificación de Licencia:</td>
            <td><strong>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasLicencia"), "clasfLicencia"), "html", null, true);
        echo "</strong></td>
        </tr>
        <tr>
            <td style=\"width: 70%; text-align: right;\" colspan=\"3\">Estatus de Solicitud:</td>
            <td><strong>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status"), "html", null, true);
        echo "</strong></td>
        </tr>
        </tbody>
    </table>

    <form id=\"form-info\">
       <input type=\"hidden\" name=\"solicitud_id\" id=\"solicitud_id\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
        echo "\" /> 
       <table id=\"tabla_reporte\">
            <tbody>
                <tr id=\"table_header\">
                    <td>Nº</td>
                    <td>Recaudo</td>
                    <td>Descargar</td>
                    <td>Fecha de Vencimiento</td>
                    <td>Verificado y Aprobado</td>
                </tr>
            ";
        // line 50
        $context["count"] = 0;
        // line 51
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["documents"]) ? $context["documents"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 52
            echo "            <tr>
                <td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "</td>
                ";
            // line 54
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "recaudoLicencia"), "recaudo")) > 100)) {
                // line 55
                echo "                <td data-toggle=\"popover\" data-content=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "recaudoLicencia"), "recaudo"), "html", null, true);
                echo "\"> 
                ";
            } else {
                // line 57
                echo "                <td>
                ";
            }
            // line 58
            echo " 
                    ";
            // line 59
            echo twig_escape_filter($this->env, twig_truncate_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "recaudoLicencia"), "recaudo"), 100), "html", null, true);
            echo "
                </td>
                <td class=\"center\">
                    ";
            // line 62
            if ($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getMediarecaudo", array(), "any", true, true)) {
                // line 63
                echo "                        ";
                echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getMediarecaudo"), "CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig"), "html", null, true);
                echo "
                    ";
            }
            // line 65
            echo "                </td>
                <td class=\"center\" style=\"text-align: center !important\">";
            // line 66
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "fechaVencimiento"), "d-m-Y"), "html", null, true);
            echo "</td>
                <td class=\"center\" style=\"text-align: center !important\">
                    Sí<input type=\"radio\" name=\"option-";
            // line 68
            echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
            echo "\" class=\"option\" value=\"1*";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
            echo "\" required=\"true\"><br>
                    No<input type=\"radio\" name=\"option-";
            // line 69
            echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
            echo "\" class=\"option\" value=\"0*";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
            echo "\" required=\"true\">
                </td>
            </tr>
                ";
            // line 72
            $context["count"] = (1 + (isset($context["count"]) ? $context["count"] : null));
            // line 73
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "            <tr>
                <td>";
        // line 75
        echo twig_escape_filter($this->env, ((isset($context["count"]) ? $context["count"] : null) + 1), "html", null, true);
        echo "</td>
                <td>Pago por Procesamiento</td>
                <td class=\"center\">
                    ";
        // line 78
        if ($this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "getArchivoAdjunto", array(), "any", true, true)) {
            // line 79
            echo "                        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate($this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "getArchivoAdjunto"), "CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig"), "html", null, true);
            echo "
                    ";
        }
        // line 81
        echo "                </td>
                <td class=\"center\" style=\"text-align: center !important\">";
        // line 82
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "fechaDeposito"), "d-m-Y"), "html", null, true);
        echo "</td>
                <td class=\"center\" style=\"text-align: center !important\">
                    Sí<input type=\"radio\" name=\"pago\" id=\"pago_";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
        echo "\" class=\"option\" value=\"1*";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
        echo "\" required=\"true\"><br>
                    No<input type=\"radio\" name=\"pago\" id=\"pago_";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
        echo "\" class=\"option\" value=\"0*";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
        echo "\" required=\"true\">
                </td>
            </tr>            
            <tr>
                <td colspan=\"3\">&nbsp;<input type=\"hidden\" name=\"total_document\" value=\"";
        // line 89
        echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
        echo "\" ></td>
                <td colspan=\"2\" class=\"center\" style=\"text-align: right !important\">
                    <select name=\"status_recaudos\" id=\"statusRecaudos\" required=\"true\">
                        <option value=\"Recaudos incompletos\">Recaudos Incompletos</option>
                        <option value=\"No cumple con las especificaciones\">No Cumple con las especificaciones</option>
                        <option value=\"Recuados vencidos\">Recaudos Vencidos</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan=\"3\">&nbsp;</td>
                <td colspan=\"2\" class=\"center\" style=\"text-align: right !important\">
                    <div class=\"btn_aprob\" style=\"text-align: right !important\">
                        <input type=\"submit\" value=\"Aplicar\" id=\"btnSave\" class=\"btn btn-primary btn-sm\"/>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>

<script type=\"text/javascript\">

    \$('[data-toggle=\"tooltip\"]').tooltip({
        'placement': 'rigth'
    });
    \$('[data-toggle=\"popover\"]').popover({
        trigger: 'hover',
        'placement': 'rigth'
    });
    
    \$(\".option\").click(function() {
        var total = 0;
        var checked = 0;
        \$(\":radio\").each(function(){
            total++;
        });
        \$(\":radio:checked\").each(function(){
            if(\$(this).val().substr(0,1)==1) {
                checked++;
            }
         });
         if(total/2==checked){
            var html =\"<option id='optionComplete' value='Recaudos completos' selected>Recaudos Completos</option>\";
            \$('#statusRecaudos').append(html);
         } else {
            \$('#optionComplete').remove();
         }
    });    
    
    \$('#form-info').on('submit', function(e){
        e.stopPropagation();
        e.preventDefault();
        if(confirm('¿Está seguro que desea continuar?')) {
            \$(\"#btnSave\").attr(\"disabled\",\"disabled\");
            var id = \$(\"#solicitud_id\").val();
            var url = Routing.generate('fiscal_citas_guardar_status',{ 'id': id })
            \$.ajax({
                async: false,
                type: \"POST\",
                cache: false,
                url: url,
                data: \$(\"#form-info\").serialize(),
                dataType: \"json\",
                success: function(data) {
                    window.location = Routing.generate('fiscal_citas_listado',{'tipo':'Revisada'});
                    \$(\"#btnSave\").removeAttr(\"disabled\");
                },
                error: function(){
                    \$(\"#btnSave\").removeAttr(\"disabled\");
                    e.preventDefault();
                }
            });
            e.preventDefault();
        } 
        return false;
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:AdminCitas:modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 89,  225 => 85,  219 => 84,  214 => 82,  211 => 81,  205 => 79,  203 => 78,  197 => 75,  194 => 74,  180 => 73,  178 => 72,  168 => 69,  160 => 68,  155 => 66,  152 => 65,  146 => 63,  144 => 62,  138 => 59,  135 => 58,  131 => 57,  125 => 55,  123 => 54,  119 => 53,  116 => 52,  98 => 51,  96 => 50,  83 => 40,  74 => 34,  67 => 30,  60 => 26,  53 => 22,  46 => 17,  40 => 15,  34 => 13,  32 => 12,  19 => 1,);
    }
}
