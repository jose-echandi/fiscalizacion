<?php

/* CentrohipicoBundle:DataEmpresa:formPartnerLegal.html.twig */
class __TwigTemplate_d414fb105942209a31b28628e0bce05bc6bd1531206f460d068b6ec2f6436f1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table id=\"tabla_reporte2\" class=\"tabla_reporte2\">
<tbody>
";
        // line 4
        echo "    ";
        // line 6
        echo "<input type=\"hidden\" name=\"typeForm\" value=\"2\">
<input type=\"hidden\" id=\"partnerIdField\" name=\"partnerId\" value=\"0\">
<input type=\"hidden\" id=\"parentIdField\" name=\"parentId\" value=\"0\">
<input type=\"hidden\" id=\"pers_juridicaPartner\" name=\"pers_juridicaPartner\" value=\"0\">
<input type=\"hidden\" id=\"rifPartner\" name=\"rifPartner\" value=\"0\">
<tr>
    <td>";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
    <td>";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), 'widget', array("attr" => array("class" => " ")));
        echo "
        ";
        // line 14
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 15
            echo "            <span class=\"help-block \">
                                ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
        ";
        }
        // line 19
        echo "
    </td>
    <td>";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
    <td>";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), 'widget', array("attr" => array("class" => " ")));
        echo "
        ";
        // line 23
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 24
            echo "            <span class=\"help-block \">
                                ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
        ";
        }
        // line 28
        echo "
    </td>

</tr>
<tr>
    <td>";
        // line 33
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td colspan=\"3\">
        ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tipoci"), 'widget', array("attr" => array("class" => " input-mini")));
        echo "
        - ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), 'widget', array("attr" => array("class" => " ")));
        echo "
        ";
        // line 37
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 38
            echo "            <span class=\"help-block \">
                        ";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 42
        echo "
    </td>

</tr>
<tr>
    <td>";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'widget', array("attr" => array("class" => "dlestadoSocio", "style" => "width:156px !important")));
        echo "
        ";
        // line 49
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 50
            echo "            <span class=\"help-block \">
                        ";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 54
        echo "
    </td>
    <td>";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), 'label', array("label_attr" => array("class" => " col-md-12 text-left control-label")));
        echo "</td>
    <td>";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), 'widget', array("attr" => array("class" => "dlmunicipioSocio ", "disabled" => "disabled")));
        echo "
        ";
        // line 58
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 59
            echo "            <span class=\"help-block \">
                        ";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 63
        echo "    </td>

</tr>
<tr>
    <td>";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), 'label', array("label_attr" => array("class" => " col-md-12 text-left control-label")));
        echo "</td>
    <td>";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 69
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 70
            echo "            <span class=\"help-block \">
                        ";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 74
        echo "    </td>
    <td>";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), 'label', array("label_attr" => array("class" => " col-md-12 text-left control-label")));
        echo "</td>
    <td>";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), 'widget', array("attr" => array("class" => "dlparroquiaSocio", "disabled" => "disabled")));
        echo "
        ";
        // line 77
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 78
            echo "            <span class=\"help-block \">
                        ";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 82
        echo "    </td>
</tr>
<tr>
    <td>";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 87
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 88
            echo "            <span class=\"help-block \">
                        ";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 92
        echo "    </td>
    <td>";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>
        ";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 96
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 97
            echo "            <span class=\"help-block \">
                        ";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 101
        echo "
    </td>
</tr>
<tr>
    <td>";
        // line 105
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 107
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 108
            echo "            <span class=\"help-block \">
                        ";
            // line 109
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 112
        echo "    </td>
    <td>";
        // line 113
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 114
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 115
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 116
            echo "            <span class=\"help-block \">
                        ";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 120
        echo "

    </td>
    ";
        // line 124
        echo "</tr>
<tr>
    <td>";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 127
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 128
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 129
            echo "            <span class=\"help-block \">
                        ";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 133
        echo "
    </td>

    <td>";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 137
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
        ";
        // line 138
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 139
            echo "            <span class=\"help-block \">
                        ";
            // line 140
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 143
        echo "
    </td>
</tr>
<tr>
    <td>";
        // line 147
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 148
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 149
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 150
            echo "            <span class=\"help-block \">
                        ";
            // line 151
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 154
        echo "    </td>
    <td>";
        // line 155
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 156
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codFax"), 'widget', array("attr" => array("class" => " col-md-4")));
        echo "
        ";
        // line 157
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
        ";
        // line 158
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 159
            echo "            <span class=\"help-block \">
                        ";
            // line 160
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 163
        echo "    </td>
</tr>
<tr>
    <td>";
        // line 166
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 167
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codTlfFijo"), 'widget', array("attr" => array("class" => " col-md-4")));
        echo "
        ";
        // line 168
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
        ";
        // line 169
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 170
            echo "            <span class=\"help-block \">
                        ";
            // line 171
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 174
        echo "    </td>
    <td>";
        // line 175
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td>";
        // line 176
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codTlfCelular"), 'widget', array("attr" => array("class" => " col-md-4 ")));
        echo "
        ";
        // line 177
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
        ";
        // line 178
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 179
            echo "            <span class=\"help-block \">
                        ";
            // line 180
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 183
        echo "    </td>
</tr>
<tr>
    <td>";
        // line 186
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
    <td colspan=\"3\">";
        // line 187
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
        ";
        // line 188
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 189
            echo "            <span class=\"help-block \">
                        ";
            // line 190
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
        ";
        }
        // line 193
        echo "
    </td>
</tr>
<tr>
    <td colspan=\"4\"> <div class=\"col-md-2 col-md-offset-10\"><button type=\"submit\" class=\"btn btn-primary btn-sm\">Guardar socio</button></div></td>
</tr>

</tbody>
</table>
<div class=\"block-separator col-md-12\"></div>
";
        // line 203
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token"), 'widget');
        echo "
<script type=\"text/javascript\" >
    \$(document).ready(function(){
        \$(\".form-horizontal\").removeClass('form-horizontal');
        \$(\".form-control\").removeClass('form-control');
        \$(\".dlestadoSocio\").change(function() {
            estado = \$('.dlestadoSocio option:selected').val();
            var Rmunicipio=Routing.generate('municipios', {estado_id: estado});
            getSelect(Rmunicipio,'.dlmunicipioSocio',\"Municipio\");
            \$(\".dlmunicipioSocio\").removeAttr('disabled');
            //var Rciudad=Routing.generate('ciudades', {estado_id: estado, municipio_id: null});
            //console.log(Rciudad);
            //getSelect(Rciudad,'#dlciudad',\"Ciudad\");
        });
        \$(\".dlmunicipioSocio\").change(function() {
            estado = \$('.dlestadoSocio option:selected').val();
            municipio = \$('.dlmunicipioSocio option:selected').val();
            var Rparroquia=Routing.generate('parroquias', {municipio_id: municipio});
            \$(\".dlparroquiaSocio\").removeAttr('disabled');
            getSelect(Rparroquia,'.dlparroquiaSocio',\"Parroquia\");
        });
    });

</script>";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataEmpresa:formPartnerLegal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  489 => 203,  477 => 193,  471 => 190,  468 => 189,  466 => 188,  462 => 187,  458 => 186,  453 => 183,  447 => 180,  444 => 179,  442 => 178,  438 => 177,  434 => 176,  430 => 175,  427 => 174,  421 => 171,  418 => 170,  416 => 169,  412 => 168,  408 => 167,  404 => 166,  399 => 163,  393 => 160,  390 => 159,  388 => 158,  384 => 157,  380 => 156,  376 => 155,  373 => 154,  367 => 151,  364 => 150,  362 => 149,  358 => 148,  354 => 147,  348 => 143,  342 => 140,  339 => 139,  337 => 138,  333 => 137,  329 => 136,  324 => 133,  318 => 130,  315 => 129,  313 => 128,  309 => 127,  305 => 126,  301 => 124,  296 => 120,  290 => 117,  287 => 116,  285 => 115,  281 => 114,  277 => 113,  274 => 112,  268 => 109,  265 => 108,  263 => 107,  259 => 106,  255 => 105,  249 => 101,  243 => 98,  240 => 97,  238 => 96,  234 => 95,  229 => 93,  226 => 92,  220 => 89,  217 => 88,  215 => 87,  211 => 86,  207 => 85,  202 => 82,  196 => 79,  193 => 78,  191 => 77,  187 => 76,  183 => 75,  180 => 74,  174 => 71,  171 => 70,  169 => 69,  165 => 68,  161 => 67,  155 => 63,  149 => 60,  146 => 59,  144 => 58,  140 => 57,  136 => 56,  132 => 54,  126 => 51,  123 => 50,  121 => 49,  117 => 48,  113 => 47,  106 => 42,  100 => 39,  97 => 38,  95 => 37,  91 => 36,  87 => 35,  82 => 33,  75 => 28,  69 => 25,  66 => 24,  64 => 23,  60 => 22,  56 => 21,  52 => 19,  46 => 16,  43 => 15,  41 => 14,  37 => 13,  33 => 12,  25 => 6,  23 => 4,  19 => 1,);
    }
}
