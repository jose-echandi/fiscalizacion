<?php

/* CentrohipicoBundle:DataOperadora:verDatos.html.twig */
class __TwigTemplate_f4b0eca0d56957a5ade9e375a4fee514d3a15176130a477d78f0f38dc4c595cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-md-12\">
      <div id=\"tabs\">
          <ul>
            <li><a href=\"#dataoper\" data-toggle=\"tab\">Datos de la Oficina</a></li>
            <li><a href=\"#datalegal\" data-toggle=\"tab\">Datos Legales</a></li>
            <li><a href=\"#datalicencia\" data-toggle=\"tab\">Licencia</a></li>
          </ul>
      <div id=\"dataoper\"  class=\"col-md-12\">
        <table  id=\"tabla_reporte2\" class=\"text-left bg-white table table-condensed\">
             <thead>
                    <tr>
                        <th colspan=\"4\" > Detalles de la Oficina </th>
                    </tr>
                </thead>
            <tbody>
                <tr>
                    <th class=\"text-left bg-white\" >Denominación Comercial del lugar </th>
                    <td colspan=\"3\" class=\"text-left bg-white\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Local Propio o arrendado </th>
                    <td colspan=\"3\" class=\"text-left bg-white\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "estatusLocal"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">RIF</th>
                    <td class=\"text-left bg-white\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Cedula</th>
                    <td class=\"text-left bg-white\">";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tipoci"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ci"), "html", null, true);
        echo "</td>
                </tr>
              <tr>
                    <th class=\"text-left bg-white\">Nombre</th>
                    <td class=\"text-left bg-white\">";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nombre"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Apellido</th>
                    <td class=\"text-left bg-white\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "apellido"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Fax</th>
                    <td class=\"text-left bg-white\">";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fax"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Teléfono Fijo</th>
                    <td class=\"text-left bg-white\">(";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfFijo"), "html", null, true);
        echo ")-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tflFijo"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Teléfono Celular</th>
                    <td class=\"text-left bg-white\">(";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfCelular"), "html", null, true);
        echo ")- ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tflCelular"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Email</th>
                    <td class=\"text-left bg-white\">";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "email"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Página Web</th>
                    <td colspan=\"3\" class=\"text-left bg-white\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "pagWeb"), "html", null, true);
        echo "</td>
                </tr>
                <thead>
                  <tr> 
                      <th colspan=\"4\" >Datos Direccion </th>
                    </tr>
                </thead>    
                <tr>
                    <th class=\"text-left bg-white\">Estado</th>
                    <td class=\"text-left bg-white\">";
        // line 59
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "estado")) > 0)) {
            // line 60
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "estado"), "nombre"), "html", null, true);
            echo "
                        ";
        }
        // line 62
        echo "                    </td>
                    <th class=\"text-left bg-white\">Municipio</th>
                    <td class=\"text-left bg-white\">";
        // line 64
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "municipio")) > 0)) {
            // line 65
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "municipio"), "nombre"), "html", null, true);
            echo "
                        ";
        }
        // line 67
        echo "                    </td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Ciudad</th>
                    <td class=\"text-left bg-white\">";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ciudad"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Parroquia</th>
                    <td class=\"text-left bg-white\">";
        // line 73
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "parroquia")) > 0)) {
            // line 74
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "parroquia"), "nombre"), "html", null, true);
            echo "
                        ";
        }
        // line 76
        echo "                    </td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Av/Calle/Carretera</th>
                    <td class=\"text-left bg-white\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "avCalleCarrera"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Edif/Casa</th>
                    <td class=\"text-left bg-white\">";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "edifCasa"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Oficina/Apto/No</th>
                    <td class=\"text-left bg-white\">";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ofcAptoNum"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Punto Referencia</th>
                    <td class=\"text-left bg-white\">";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "puntoReferencia"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Código Postal</th>
                    <td colspan=\"3\" class=\"text-left bg-white\">";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codigoPostal"), "html", null, true);
        echo "</td>
                </tr>
            </tbody>
        </table>
      </div><!-- tab DataOper -->
     <div id=\"datalegal\" class=\"col-md-12\">
        <table  id=\"tabla_reporte2\" class=\"text-left bg-white table table-condensed\">
             <thead>
                    <tr>
                        <th colspan=\"4\" >Detalles Legales</th>
                    </tr>
                </thead>
            <tbody>
                <tr class=\"text-left bg-white\">
                    <th class=\"text-left bg-white\">RIF</th>
                    <td class=\"text-left bg-white\">";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "persJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "rif"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Cedula</th>
                    <td class=\"text-left bg-white\">";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "ci"), "html", null, true);
        echo "</td>
                </tr>
              <tr class=\"text-left bg-white\">
                    <th class=\"text-left bg-white\">Nombre</th>
                    <td class=\"text-left bg-white\">";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "nombre"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Apellido</th>
                    <td class=\"text-left bg-white\">";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "apellido"), "html", null, true);
        echo "</td>
                </tr>
                <tr class=\"text-left bg-white\">
                    <th class=\"text-left bg-white\">Fax</th>
                    <td class=\"text-left bg-white\">";
        // line 119
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "fax"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Teléfono Fijo</th>
                    <td class=\"text-left bg-white\">";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "codTlfFijo"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "tflFijo"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Teléfono Celular</th>
                    <td class=\"text-left bg-white\">";
        // line 125
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "codTlfCelular"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "tflCelular"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Email</th>
                    <td class=\"text-left bg-white\">";
        // line 127
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "email"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Página Web</th>
                    <td colspan=\"3\" class=\"text-left bg-white\">";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "pagWeb"), "html", null, true);
        echo "</td>
                </tr>
                <thead>
                    <tr> 
                      <th colspan=\"4\">Datos Legales Direccion </th> 
                    </tr>
                 </thead> 
                <tr>
                    <th class=\"text-left bg-white\">Estado</th>
                    <td class=\"text-left bg-white\">";
        // line 140
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "estado")) > 0)) {
            // line 141
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "estado"), "nombre"), "html", null, true);
            echo "
                        ";
        }
        // line 143
        echo "                    </td>
                    <th class=\"text-left bg-white\">Municipio</th>
                    <td class=\"text-left bg-white\">";
        // line 145
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "municipio")) > 0)) {
            // line 146
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "municipio"), "nombre"), "html", null, true);
            echo "
                        ";
        }
        // line 148
        echo "                    </td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Ciudad</th>
                    <td class=\"text-left bg-white\">";
        // line 152
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "ciudad"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Parroquia</th>
                    <td class=\"text-left bg-white\">";
        // line 154
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "parroquia")) > 0)) {
            // line 155
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "parroquia"), "nombre"), "html", null, true);
            echo "
                        ";
        }
        // line 157
        echo "                    </td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Av/Calle/Carretera</th>
                    <td class=\"text-left bg-white\">";
        // line 161
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "avCalleCarrera"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Edif/Casa</th>
                    <td class=\"text-left bg-white\">";
        // line 163
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "edifCasa"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Oficina/Apto/No</th>
                    <td class=\"text-left bg-white\">";
        // line 167
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "ofcAptoNum"), "html", null, true);
        echo "</td>
                    <th class=\"text-left bg-white\">Punto Referencia</th>
                    <td class=\"text-left bg-white\">";
        // line 169
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "puntoReferencia"), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <th class=\"text-left bg-white\">Código Postal</th>
                    <td class=\"text-left bg-white\">";
        // line 173
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "legal"), "codigoPostal"), "html", null, true);
        echo "</td>
                    <td class=\"text-left bg-white\">&nbsp;</td>
                    <td class=\"text-left bg-white\">&nbsp;</td>
                </tr>
            </tbody>
        </table>
      </div> <!-- Fin DataLegal-->
      <div id=\"datalicencia\" class=\"col-md-12\">
           <table  id=\"tabla_reporte2\" class=\"text-left bg-white table table-condensed\">
             <thead>
                <tr>
                    <th colspan=\"4\" > Detalles de Licencia </th>
                </tr>
                <tr id=\"table_header\">
                    <td>Nº Licencia</td>
                    <td>Clase de Licencia</td>
                    <td>Fecha de Vencimiento</td>
                    <td>Estatus</td>
                </tr>
              </thead>  
              <tbody>
              ";
        // line 194
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"));
        foreach ($context['_seq'] as $context["_key"] => $context["lic"]) {
            echo "  
                <tr>
                    <td class=\"text-center\">";
            // line 196
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "numLicencia"), "html", null, true);
            echo "</td>
                    <td class=\"text-center\">";
            // line 197
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "clasfLicencia"), "html", null, true);
            echo "</td>
                    <td class=\"text-center\">";
            // line 198
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "fechaVencimiento"), "html", null, true);
            echo "</td>
                    <td class=\"text-center\">";
            // line 199
            echo ((($this->getAttribute((isset($context["lic"]) ? $context["lic"] : null), "vigente") == 1)) ? ("Vigente") : ("Vencida"));
            echo "</td>
                </tr>
             ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 202
        echo "             ";
        if (twig_test_empty($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "licenciasaprob"))) {
            // line 203
            echo "                 <tr><td colspan=\"4\">Sin Registros</td></tr>
             ";
        }
        // line 204
        echo "    
          </tbody>
           </table>    
      </div>
    </div><!--Fin Tab -->
  </div> <!-- col-12 Container -->";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataOperadora:verDatos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  401 => 204,  397 => 203,  394 => 202,  385 => 199,  381 => 198,  377 => 197,  373 => 196,  366 => 194,  342 => 173,  335 => 169,  330 => 167,  323 => 163,  318 => 161,  312 => 157,  306 => 155,  304 => 154,  299 => 152,  293 => 148,  287 => 146,  285 => 145,  281 => 143,  275 => 141,  273 => 140,  261 => 131,  254 => 127,  247 => 125,  238 => 121,  233 => 119,  226 => 115,  221 => 113,  214 => 109,  207 => 107,  189 => 92,  182 => 88,  177 => 86,  170 => 82,  165 => 80,  159 => 76,  153 => 74,  151 => 73,  146 => 71,  140 => 67,  134 => 65,  132 => 64,  128 => 62,  122 => 60,  120 => 59,  108 => 50,  101 => 46,  94 => 44,  85 => 40,  80 => 38,  73 => 34,  68 => 32,  59 => 28,  52 => 26,  45 => 22,  38 => 18,  19 => 1,);
    }
}
