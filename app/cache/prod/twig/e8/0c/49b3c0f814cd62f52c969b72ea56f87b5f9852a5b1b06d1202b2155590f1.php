<?php

/* FiscalizacionBundle:Fiscalizacion:citados.html.twig */
class __TwigTemplate_e80c49b3c0f814cd62f52c969b72ea56f87b5f9852a5b1b06d1202b2155590f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle:Fiscalizacion:index.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'preheader' => array($this, 'block_preheader'),
            'precols' => array($this, 'block_precols'),
            'preactions' => array($this, 'block_preactions'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle:Fiscalizacion:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "con estatus <em>Citados</em>
";
    }

    // line 7
    public function block_preheader($context, array $blocks = array())
    {
        // line 8
        echo "    <th>F. Citación</th>
";
    }

    // line 11
    public function block_precols($context, array $blocks = array())
    {
        // line 12
        echo "    <td>";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "citacion"), "fecha"), "d/m/Y"), "html", null, true);
        echo "</td>
";
    }

    // line 15
    public function block_preactions($context, array $blocks = array())
    {
        // line 16
        echo "     <a class=\"btn btn-success btn-sm\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fiscalizacion_multar", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
        echo "\">Multar</a>
";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Fiscalizacion:citados.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 16,  57 => 15,  50 => 12,  47 => 11,  42 => 8,  39 => 7,  34 => 4,  31 => 3,);
    }
}
