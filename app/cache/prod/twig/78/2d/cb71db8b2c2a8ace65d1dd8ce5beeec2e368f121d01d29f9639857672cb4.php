<?php

/* UserBundle:Notification:mail.html.twig */
class __TwigTemplate_782dcb71db8b2c2a8ace65d1dd8ce5beeec2e368f121d01d29f9639857672cb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("UserBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UserBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "Estimado(a) ";
        echo twig_escape_filter($this->env, (isset($context["nombre"]) ? $context["nombre"] : null), "html", null, true);
        echo ",<br><br>
";
        // line 4
        echo (isset($context["message"]) ? $context["message"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "UserBundle:Notification:mail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 4,  31 => 3,  28 => 2,  78 => 40,  74 => 39,  70 => 38,  52 => 23,  48 => 22,  44 => 21,  22 => 2,  19 => 1,);
    }
}
