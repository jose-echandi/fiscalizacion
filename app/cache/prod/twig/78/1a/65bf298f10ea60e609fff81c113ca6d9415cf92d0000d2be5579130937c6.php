<?php

/* SolicitudesCitasBundle:AdminCitas:listado.html.twig */
class __TwigTemplate_781a65bf298f10ea60e609fff81c113ca6d9415cf92d0000d2be5579130937c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((((isset($context["ajax"]) ? $context["ajax"] : null) == false)) ? ("NewTemplateBundle::base.html.twig") : ("NewTemplateBundle::base_ajax.html.twig")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <style>
        #table_header td{
            color:white;
        }
    </style>

    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>
            ";
        // line 13
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_FISCAL"), "method")) {
            // line 14
            echo "                Citas asignadas
            ";
        } elseif (((isset($context["tipo"]) ? $context["tipo"] : null) == "Rechazada")) {
            // line 16
            echo "                Licencias Rechazadas
            ";
        } else {
            // line 18
            echo "                ";
            echo twig_escape_filter($this->env, ((((isset($context["tipo"]) ? $context["tipo"] : null) == "Verificada")) ? ("Licencias por aprobar") : (("Licencias " . (isset($context["tipo"]) ? $context["tipo"] : null)))), "html", null, true);
            echo "
            ";
        }
        // line 20
        echo "        </h1>
    </div>

    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row\">
        <div class=\"col-sm-2\">
        </div>
        <div class=\"col-sm-10 block-separator\"></div>
    </div>
    ";
        // line 29
        if (array_key_exists("entities", $context)) {
            // line 30
            echo "       <div class=\"col-md-2 col-md-offset-10\"><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("exportpdf_licencias", array("tipo" => (isset($context["tipo"]) ? $context["tipo"] : null))), "html", null, true);
            echo "\" target='_blank'><i class=\"icon-pdf\"></i></a>&nbsp;<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("exportxls_licencias", array("tipo" => (isset($context["tipo"]) ? $context["tipo"] : null))), "html", null, true);
            echo "\" target='_blank'><i class=\"icon-excel\"></i></a></div>
        <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
            ";
            // line 32
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : null));
            echo "
        </article>

        <div class=\"col-md-12\">
            <table class=\"table table-condensed table-striped\">
                <thead>
                <tr>
                    <th>Nº</th>
                    <th>RIF</th>
                    <th>Denominación Comercial</th>
                    <th>Licencia</th>
                    <th>Clasificación del Licencia</th>
                    <th>No Solicitud</th>
                     ";
            // line 45
            if (((isset($context["tipo"]) ? $context["tipo"] : null) == "Rechazada")) {
                // line 46
                echo "                         <th>F. Rechazo</th>
                     ";
            }
            // line 47
            echo "    
                    <th>F. de Cita</th>
                </tr>
                </thead>
                <tbody>
                ";
            // line 52
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 53
                echo "                    <tr id=\"request-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                echo "\">
                        <td>";
                // line 54
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                echo "</td>
                        <td>
                            ";
                // line 56
                if ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora", array(), "any", false, true), "persJuridica", array(), "any", true, true)) {
                    // line 57
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "persJuridica"), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "rif"), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 59
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "persJuridica"), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "rif"), "html", null, true);
                    echo "
                            ";
                }
                // line 61
                echo "                        </td>
                        <td>
                            ";
                // line 63
                if ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora", array(), "any", false, true), "denominacionComercial", array(), "any", true, true)) {
                    // line 64
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "denominacionComercial"), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 66
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "denominacionComercial"), "html", null, true);
                    echo "
                            ";
                }
                // line 68
                echo "                        </td>
                        <td>";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasLicencia"), "admTiposLicencias"), "tipoLicencia"), "html", null, true);
                echo "</td>
                        <td>";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasLicencia"), "clasfLicencia"), "html", null, true);
                echo "</td>
                        ";
                // line 71
                if (((isset($context["tipo"]) ? $context["tipo"] : null) != "Rechazada")) {
                    // line 72
                    echo "                            <td onclick=\"getInfo('";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
                    echo "')\"><a href=\"#\" class=\"big-link\" data-reveal-id=\"myModal\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "codsolicitud"), "html", null, true);
                    echo "</a></td>
                        ";
                } else {
                    // line 74
                    echo "                            <td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "codsolicitud"), "html", null, true);
                    echo "</td>
                            <td>";
                    // line 75
                    echo twig_escape_filter($this->env, _twig_default_filter(twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaRechazada"), "d-m-Y"), "none"), "html", null, true);
                    echo "</td>
                        ";
                }
                // line 77
                echo "                        <td>";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "fechaSolicitud"), "d-m-Y"), "html", null, true);
                echo "</td>
                    </tr>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 80
            echo "                </tbody>
            </table>
        </div>
    ";
        } else {
            // line 84
            echo "        <div class=\"col-md-12\">
            <div id=\"notificaciones\">
                <ul>
                    <li class=\"n1\"><h5>No existen registros</h5></li>
                </ul>
            </div>
        </div>
    ";
        }
        // line 92
        echo "    <div class=\"block-separator col-sm-12\"></div>

    <div id=\"myModal\" class=\"reveal-modal\" data-animation=\"fade\" style=\"top:-100px\">
        <a class=\"close-reveal-modal\">&#215;</a>
        <div id=\"dataModal\">
            <h3> Cargando..... </h3>
        </div>
    </div>
    <script>
        function getInfo(id)
        {
            ";
        // line 103
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_FISCAL"), "method")) {
            // line 104
            echo "                var url = Routing.generate('fiscal_citas_modal_info',{ 'id': id });
            ";
        } else {
            // line 106
            echo "                var url = Routing.generate('gerente_modal_info',{ 'id': id });
            ";
        }
        // line 108
        echo "            \$.get( url, function( data ) {
                \$(\"#dataModal\").html(data);
            });
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:AdminCitas:listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 108,  253 => 106,  249 => 104,  247 => 103,  234 => 92,  224 => 84,  218 => 80,  200 => 77,  195 => 75,  190 => 74,  182 => 72,  180 => 71,  176 => 70,  172 => 69,  169 => 68,  163 => 66,  157 => 64,  155 => 63,  151 => 61,  143 => 59,  135 => 57,  133 => 56,  128 => 54,  123 => 53,  106 => 52,  99 => 47,  95 => 46,  93 => 45,  77 => 32,  69 => 30,  67 => 29,  56 => 20,  50 => 18,  46 => 16,  42 => 14,  40 => 13,  29 => 4,  26 => 3,);
    }
}
