<?php

/* SolicitudesCitasBundle:AdminLicencias:modal.html.twig */
class __TwigTemplate_787ee9a1499736d98cbb568793eee22dcb7e758fccd90d29cb82dff780e8dc91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style type=\"text/css\">
    .popover {
        position: fixed !important;
        z-index: 8600 !important;
        display: inline-table !important;
    }
</style>
<table id=\"tabla_reporte\">
    <tbody><tr id=\"table_header\">
        <td colspan=\"4\">Denominación Comercial:
            <strong>
                ";
        // line 12
        if ($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora", array(), "any", false, true), "denominacionComercial", array(), "any", true, true)) {
            // line 13
            echo "                    ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"), "denominacionComercial"), "html", null, true);
            echo "
                ";
        } else {
            // line 15
            echo "                    ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), "denominacionComercial"), "html", null, true);
            echo "
                ";
        }
        // line 17
        echo "            </strong>
        </td>
    </tr>
    <tr>
        <td style=\"width: 70%; text-align: right;\" colspan=\"3\">Nª de Solicitud:</td>
        <td><strong>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "cita"), "codsolicitud"), "html", null, true);
        echo "</strong></td>
    </tr>
    <tr>
        <td style=\"width: 70%; text-align: right;\" colspan=\"3\">Licencia:</td>
        <td><strong>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasLicencia"), "admTiposLicencias"), "tipoLicencia"), "html", null, true);
        echo "</strong></td>
    </tr>
    <tr>
        <td style=\"width: 70%; text-align: right;\" colspan=\"3\">Clasificación de Licencia:</td>
        <td><strong>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasLicencia"), "clasfLicencia"), "html", null, true);
        echo "</strong></td>
    </tr>
    <tr>
        <td style=\"width: 70%; text-align: right;\" colspan=\"3\">Estatus de Solicitud:</td>
        <td><strong>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status"), "html", null, true);
        echo "</strong></td>
    </tr>
    </tbody></table>

<form id=\"form-info\">
    <input type=\"hidden\" name=\"solicitud_id\" id=\"solicitud_id\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
        echo "\" /> 
    <table id=\"tabla_reporte\">
        <tbody><tr id=\"table_header\">
            <td>Nº</td>
            <td>Recaudo</td>
            <td>Descargar</td>
            <td>Fecha de Vencimiento</td>
            <td>";
        // line 46
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_FISCAL"), "method")) {
            echo "Verificado y Aprobado ";
        } else {
            echo " Fiscal ";
        }
        echo "</td>
            ";
        // line 47
        if ((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_ASESOR"), "method")) && ((isset($context["aprob_fiscal"]) ? $context["aprob_fiscal"] : null) == true))) {
            // line 48
            echo "                <td>Gerencia</td>
            ";
        }
        // line 50
        echo "            ";
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_ASESOR"), "method")) {
            // line 51
            echo "                <td>Asesor&iacute;a legal</td>
            ";
        }
        // line 53
        echo "        </tr>
        ";
        // line 54
        $context["count"] = 1;
        // line 55
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["documents"]) ? $context["documents"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 56
            echo "            <tr>
                <td>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "</td>
                ";
            // line 58
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getRecaudo"), "recaudoLicencia"), "recaudo")) > 100)) {
                // line 59
                echo "                <td data-toggle=\"popover\" data-content=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getRecaudo"), "recaudoLicencia"), "recaudo"), "html", null, true);
                echo "\"> 
                ";
            } else {
                // line 61
                echo "                <td>
                ";
            }
            // line 63
            echo "                    ";
            echo twig_escape_filter($this->env, twig_truncate_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getRecaudo"), "recaudoLicencia"), "recaudo"), 100), "html", null, true);
            echo "
                </td>
                <td class=\"center\">
                    ";
            // line 66
            if ($this->getAttribute($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getRecaudo", array(), "any", false, true), "getMediarecaudo", array(), "any", true, true)) {
                // line 67
                echo "                        ";
                echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate($this->getAttribute($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getRecaudo"), "getMediarecaudo"), "CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig"), "html", null, true);
                echo "
                    ";
            }
            // line 69
            echo "                </td>
                <td class=\"center\" style=\"text-align: center !important\">";
            // line 70
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getRecaudo"), "fechaVencimiento"), "d-m-Y"), "html", null, true);
            echo "</td>
                ";
            // line 71
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_ASESOR"), "method"))) {
                // line 72
                echo "                    <td class=\"center\" style=\"text-align: center !important\">
                        ";
                // line 73
                if (($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getApobacionFiscal") == true)) {
                    // line 74
                    echo "                            <span class=\"glyphicon glyphicon-ok\"></span>
                        ";
                } else {
                    // line 76
                    echo "                            <span class=\"glyphicon glyphicon-remove\"></span>
                        ";
                }
                // line 78
                echo "                    </td>
                ";
            }
            // line 80
            echo "                ";
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method")) {
                // line 81
                echo "                    <td class=\"center\" style=\"text-align: center !important\">
                        Sí<input type=\"radio\" name=\"option";
                // line 82
                echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
                echo "\" class=\"option\" value=\"1*";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
                echo "\" required=\"true\" ><br>
                        No<input type=\"radio\" name=\"option";
                // line 83
                echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
                echo "\" class=\"option\" value=\"0*";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
                echo "\" required=\"true\">
                    </td>
                ";
            }
            // line 86
            echo "                ";
            if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_ASESOR"), "method")) {
                // line 87
                echo "                    <td class=\"center\" style=\"text-align: center !important\">
                        ";
                // line 88
                if (($this->getAttribute((isset($context["document"]) ? $context["document"] : null), "getAprobGerente") == true)) {
                    // line 89
                    echo "                            <span class=\"glyphicon glyphicon-ok\"></span>
                        ";
                } else {
                    // line 91
                    echo "                            <span class=\"glyphicon glyphicon-remove\"></span>
                        ";
                }
                // line 93
                echo "                    </td>
                    <td class=\"center\" style=\"text-align: center !important\">
                        Sí<input type=\"radio\" name=\"option";
                // line 95
                echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
                echo "\" class=\"option\" value=\"1*";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
                echo "\" required=\"true\" ><br>
                        No<input type=\"radio\" name=\"option";
                // line 96
                echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
                echo "\" id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
                echo "\" class=\"option\" value=\"0*";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["document"]) ? $context["document"] : null), "id"), "html", null, true);
                echo "\" required=\"true\">
                        <textarea rows=\"2\" name=\"desc";
                // line 97
                echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
                echo "\" id=\"c";
                echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
                echo "\" cols=\"10\" placeholder=\"coloque su comentario\"></textarea>
                    </td>
                ";
            }
            // line 100
            echo "            </tr>
            ";
            // line 101
            $context["count"] = (1 + (isset($context["count"]) ? $context["count"] : null));
            // line 102
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "            <tr>
                <td>";
        // line 104
        echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
        echo "</td>
                <td>Pago por Procesamiento</td>
                <td class=\"center\">
                    ";
        // line 107
        if ($this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "getArchivoAdjunto", array(), "any", true, true)) {
            // line 108
            echo "                        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate($this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "getArchivoAdjunto"), "CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig"), "html", null, true);
            echo "
                    ";
        }
        // line 110
        echo "                </td>
                <td class=\"center\" style=\"text-align: center !important\">";
        // line 111
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "fechaDeposito"), "d-m-Y"), "html", null, true);
        echo "</td>
                ";
        // line 112
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method") || $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_ASESOR"), "method"))) {
            // line 113
            echo "                    <td class=\"center\" style=\"text-align: center !important\">
                        ";
            // line 114
            if (((isset($context["aprob_fiscal"]) ? $context["aprob_fiscal"] : null) == true)) {
                // line 115
                echo "                            <span class=\"glyphicon glyphicon-ok\"></span>
                        ";
            } else {
                // line 117
                echo "                            <span class=\"glyphicon glyphicon-remove\"></span>
                        ";
            }
            // line 119
            echo "                    </td>
                ";
        }
        // line 121
        echo "                ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method") && ((isset($context["aprob_fiscal"]) ? $context["aprob_fiscal"] : null) == true))) {
            // line 122
            echo "                <td class=\"center\" style=\"text-align: center !important\">
                    Sí<input type=\"radio\" name=\"pago\" id=\"pago_";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
            echo "\" class=\"option\" value=\"1*";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
            echo "\" required=\"true\"><br>
                    No<input type=\"radio\" name=\"pago\" id=\"pago_";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
            echo "\" class=\"option\" value=\"0*";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
            echo "\" required=\"true\">
                </td>
                ";
        }
        // line 127
        echo "                ";
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_ASESOR"), "method")) {
            // line 128
            echo "                    <td class=\"center\" style=\"text-align: center !important\">
                        ";
            // line 129
            if (((isset($context["aprob_gerente"]) ? $context["aprob_gerente"] : null) == true)) {
                // line 130
                echo "                            <span class=\"glyphicon glyphicon-ok\"></span>
                        ";
            } else {
                // line 132
                echo "                            <span class=\"glyphicon glyphicon-remove\"></span>
                        ";
            }
            // line 134
            echo "                    </td>
                    <td class=\"center\" style=\"text-align: center !important\">
                        Sí<input type=\"radio\" name=\"pago\" id=\"pago_";
            // line 136
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
            echo "\" class=\"option\" value=\"1*";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
            echo "\" required=\"true\"><br>
                        No<input type=\"radio\" name=\"pago\" id=\"pago_";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
            echo "\" class=\"option\" value=\"0*";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pago"]) ? $context["pago"] : null), "id"), "html", null, true);
            echo "\" required=\"true\">
                    </td>
                ";
        }
        // line 140
        echo "            </tr>
            <input type=\"hidden\" name=\"total_document\" value=\"";
        // line 141
        echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : null), "html", null, true);
        echo "\" >            
            <tr>
            ";
        // line 143
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_FISCAL"), "method")) {
            // line 144
            echo "                <td colspan=\"3\">&nbsp;</td>
                <td class=\"center\">
                    <select name=\"status_recaudos\" id=\"statusRecaudos\" required=\"true\">
                        <option value=\"Recaudos incompletos\">Recaudos Incompletos</option>
                        <option value=\"No cumple con las especificaciones\">No Cumple con las especificaciones</option>
                        <option value=\"Recuados vencidos\">Recaudos Vencidos</option>
                    </select>
                </td>
            ";
        }
        // line 153
        echo "            ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_GERENTE"), "method") && ((isset($context["aprob_fiscal"]) ? $context["aprob_fiscal"] : null) == true))) {
            // line 154
            echo "                <td colspan=\"4\">&nbsp;</td>
                <td class=\"center\">
                        ";
            // line 156
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["fecha"]) ? $context["fecha"] : null), "d-m-Y"), "html", null, true);
            echo " <br>
                        ";
            // line 157
            echo twig_escape_filter($this->env, (isset($context["fiscal"]) ? $context["fiscal"] : null), "html", null, true);
            echo "
                </td>
                <td class=\"center\" style=\"width: 15%\">
                    <input type=\"checkbox\" id=\"btnApproved\" disabled=\"true\" name=\"paprob\">Aprobación<br>
                    <input type=\"checkbox\" id=\"btnRechazo\" disabled=\"true\" name=\"prechazo\">Rechazo
                </td>
            ";
        }
        // line 164
        echo "            ";
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_ASESOR"), "method")) {
            // line 165
            echo "                <td colspan=\"4\">&nbsp;</td>
                <td class=\"center\" style=\"text-align: center !important; vertical-align: top !important\">
                    ";
            // line 167
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["fecha"]) ? $context["fecha"] : null), "d-m-Y"), "html", null, true);
            echo " <br>
                    ";
            // line 168
            echo twig_escape_filter($this->env, (isset($context["fiscal"]) ? $context["fiscal"] : null), "html", null, true);
            echo "
                </td>
                <td class=\"center\" style=\"text-align: center !important; vertical-align: top !important\">
                    ";
            // line 171
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["fecha_gerente"]) ? $context["fecha_gerente"] : null), "d-m-Y"), "html", null, true);
            echo " <br>
                    ";
            // line 172
            echo twig_escape_filter($this->env, (isset($context["gerente"]) ? $context["gerente"] : null), "html", null, true);
            echo "
                </td>
                <td class=\"center\" style=\"text-align: left !important; padding-left: 30px;\">
                    <input type=\"hidden\" name=\"paprob\" value=\"0\">
                    <input type=\"checkbox\" id=\"legalApprove\" disabled=\"true\" name=\"paprob\" value=\"1\">Aprobación<br>
                    <input type=\"hidden\" name=\"prechazo\" value=\"0\">
                    <input type=\"checkbox\" id=\"legalRechazo\" disabled=\"true\" name=\"prechazo\" value=\"1\">Rechazo
                </td>
            ";
        }
        // line 181
        echo "        </tr>
        ";
        // line 182
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_FISCAL"), "method")) {
            // line 183
            echo "        <tr>
            <td colspan=\"4\">&nbsp;</td>
            <td class=\"center\" style=\"text-align: right !important\">
                <div class=\"btn_aprob\" style=\"text-align: right !important\">
                    <input type=\"submit\" value=\"Aplicar\" id=\"btnSave\" class=\"btn btn-primary btn-sm\"/>
                </div>
            </td>
         </tr>
         ";
        } elseif ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hasRole", array(0 => "ROLE_ASESOR"), "method")) {
            // line 192
            echo "                <tr>
                    <td colspan=\"5\">&nbsp;</td>
                    <td colspan=\"2\" class=\"center\" style=\"text-align: right !important\">
                        Nº de Providencia<br><input id=\"numPro\" type=\"text\" name=\"numero-prov\" required=\"true\" style=\"width: 130px;\"><br>
                        Fecha de Providencia<br><input id=\"datepicker\" type=\"text\" name=\"fecha-prov\" required=\"true\" style=\"width: 130px;\"><br>
                        Fecha de Inicio<br><input id=\"datestart\" type=\"text\" name=\"fecha-inicio\" required=\"true\" style=\"width: 130px;\"><br>
                        Fecha de Fin<br><input id=\"dateend\" type=\"text\" name=\"fecha-fin\" required=\"true\"style=\"width: 130px;\"><br>
                        <input type=\"submit\" id=\"btnApp\" value=\"Aplicar\" class=\"btn btn-primary btn-sm\">
                    </td>
                </tr>
        ";
        }
        // line 203
        echo "        </tbody>
    </table>
</form>
</div>

<script type=\"text/javascript\">

    \$('[data-toggle=\"tooltip\"]').tooltip({
        'placement': 'left'
    });
    \$('[data-toggle=\"popover\"]').popover({
        trigger: 'hover',
       'placement': 'left'
    });

    \$(function() {
        \$( \"#datepicker\" ).datepicker({ maxDate: \"+1M +10D\" });
        \$( \"#datestart\" ).datepicker({ maxDate: \"+1Y\",
            onSelect: function(dateStr){
            var date = \$(\"#datestart\").val();
            var date = date.split('/');
            var year = parseInt(date[2]) + 1;
                \$(\"#dateend\").val( date[0]+'/'+date[1]+'/'+ year);
            }  });
        \$( \"#dateend\" ).datepicker({ minDate: \"1D\", defaultDate: \$(\"#datestart\").val()+\"1Y\" });
    });
    
    var item = 0;
    var appr = 0;

    \$(\".option\").click(function() {
        var total = 0;
        var checked = 0;
        var totalChecked = 0;
        \$(\":radio\").each(function(){
            total++;
            if(\$(this).is(':checked')) {
                totalChecked++;
                if(\$(this).val().substr(0,1)==1) {
                    checked++;
                }
            }
        });
        if(total/2==totalChecked) {
            if(total/2==checked){
                \$(\"#btnApproved\").removeAttr('disabled');
                \$(\"#legalApprove\").removeAttr('disabled');
                \$(\"#btnRechazo\").attr('disabled','disabled');
                \$(\"#legalRechazo\").attr('disabled','disabled');
            } else {
                \$(\"#btnApproved\").attr('disabled','disabled');
                \$(\"#legalApprove\").attr('disabled','disabled');
                \$(\"#btnRechazo\").removeAttr('disabled');
                \$(\"#legalRechazo\").removeAttr('disabled');
            }
        } else {
            \$(\"#btnApproved\").attr('disabled','disabled');
            \$(\"#legalApprove\").attr('disabled','disabled');
            \$(\"#btnRechazo\").attr('disabled','disabled');
            \$(\"#legalRechazo\").attr('disabled','disabled');
        }
    });

    \$(document).on('submit','#form-info',function(e){
        e.stopPropagation();
        e.preventDefault();
        if(confirm('¿Está seguro que desea continuar?')) {
            \$(\"#myModal\").modal('hide');
            setStatusDocument();
        }
        return true;
    });

    \$('#btnApproved,#btnRechazo').each(function() {
        \$(this).on('change', function() {
            if(\$(this).is(':checked')) {
                if(confirm('¿Está seguro que desea continuar?')) {
                    \$(\"#btnRechazo\").attr('disabled','disabled');
                    var id = \$(\"#solicitud_id\").val();
                    var url = Routing.generate('gerente_guardar_status',{ 'id': id });
                    \$.ajax({
                        async: false,
                        type: \"POST\",
                        cache: false,
                        url: url,
                        data: \$(\"#form-info\").serialize(),
                        dataType: \"json\",
                        success: function(response) {
                            if(response.status == 'true') {
                                window.item = 1;
                                var url = Routing.generate('fiscal_citas_listado',{'tipo':'Revisada'});
                                window.location = url;
                                \$(\"#btnRechazo\").removeAttr('disabled');
                            } else {
                                alert(response.mensaje);
                            }
                        },
                        error: function(){
                            \$(\"#btnSave\").removeAttr(\"disabled\");
                            e.preventDefault();
                        }
                    });
                } else {
                    \$(this).attr('checked', false);
                }
            }
            return true;
        });        
    });
    
    \$('#legalRechazo').on('change', function() {
        if (\$(this).is(':checked')) {
            \$(\"#numPro\").attr('disabled','disabled');
            \$(\"#datepicker\").attr('disabled','disabled');
            \$(\"#datestart\").attr('disabled','disabled');
            \$(\"#dateend\").attr('disabled','disabled');
        } else {
            \$(\"#numPro\").removeAttr('disabled');
            \$(\"#datepicker\").removeAttr('disabled');
            \$(\"#datestart\").removeAttr('disabled');
            \$(\"#dateend\").removeAttr('disabled');
        }
    });

    function setStatusDocument() {
        \$(\"#btnSave\").attr(\"disabled\",\"disabled\");
        var id = \$(\"#solicitud_id\").val();
        var url = Routing.generate('asesor_guardar_status',{ 'id': id });
        \$.ajax({
            async: false,
            type: \"POST\",
            cache: false,
            url: url,
            data: \$(\"#form-info\").serialize(),
            dataType: \"json\",
            success: function(response) {
                if(response.status == 'true') {
                    var url = Routing.generate('fiscal_citas_listado',{'tipo':'Verificada'});
                    \$(\"#btnSave\").removeAttr('disabled');
                    window.location = url;
                } else {
                    alert(response.mensaje);
                }
            },
            error: function(){
                \$(\"#btnSave\").removeAttr(\"disabled\");
                e.preventDefault();
            }
        });
    }

    \$(\".numeric\").keydown(function(e) {
        if (\$.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }

        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:AdminLicencias:modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  484 => 203,  471 => 192,  460 => 183,  458 => 182,  455 => 181,  443 => 172,  439 => 171,  433 => 168,  429 => 167,  425 => 165,  422 => 164,  412 => 157,  408 => 156,  404 => 154,  401 => 153,  390 => 144,  388 => 143,  383 => 141,  380 => 140,  372 => 137,  366 => 136,  362 => 134,  358 => 132,  354 => 130,  352 => 129,  349 => 128,  346 => 127,  338 => 124,  332 => 123,  329 => 122,  326 => 121,  322 => 119,  318 => 117,  314 => 115,  312 => 114,  309 => 113,  307 => 112,  303 => 111,  300 => 110,  294 => 108,  292 => 107,  286 => 104,  283 => 103,  269 => 102,  267 => 101,  264 => 100,  256 => 97,  248 => 96,  240 => 95,  236 => 93,  232 => 91,  228 => 89,  226 => 88,  223 => 87,  220 => 86,  210 => 83,  202 => 82,  199 => 81,  196 => 80,  192 => 78,  188 => 76,  184 => 74,  182 => 73,  179 => 72,  177 => 71,  173 => 70,  170 => 69,  164 => 67,  162 => 66,  155 => 63,  151 => 61,  145 => 59,  143 => 58,  139 => 57,  136 => 56,  118 => 55,  116 => 54,  113 => 53,  109 => 51,  106 => 50,  102 => 48,  100 => 47,  92 => 46,  82 => 39,  74 => 34,  67 => 30,  60 => 26,  53 => 22,  46 => 17,  40 => 15,  34 => 13,  32 => 12,  19 => 1,);
    }
}
