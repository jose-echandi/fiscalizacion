<?php

/* FiscalizacionBundle:Fiscalizacion:noprov.html.twig */
class __TwigTemplate_14090879014f5791a2089ecc2b00ca75ad11c805fd3dd5358b99ac043a182ab1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-12\">
    <div class=\"alert alert-warning\">
        No hay una <strong>providencia vigente</strong> en este momento. Puede crear una en 
        <a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("providencia");
        echo "\" class=\"btn btn-default\">Providencia</a>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Fiscalizacion:noprov.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 7,  31 => 4,  28 => 3,);
    }
}
