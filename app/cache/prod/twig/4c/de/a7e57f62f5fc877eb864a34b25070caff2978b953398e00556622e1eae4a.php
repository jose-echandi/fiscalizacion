<?php

/* FiscalizacionBundle:Pagos:index.html.twig */
class __TwigTemplate_4cdea7e57f62f5fc877eb864a34b25070caff2978b953398e00556622e1eae4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle::listar.html.twig");

        $this->blocks = array(
            'main_title' => array($this, 'block_main_title'),
            'headers' => array($this, 'block_headers'),
            'addHeader' => array($this, 'block_addHeader'),
            'export' => array($this, 'block_export'),
            'columns' => array($this, 'block_columns'),
            'addCol' => array($this, 'block_addCol'),
            'footlistar' => array($this, 'block_footlistar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle::listar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_main_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
    }

    // line 4
    public function block_headers($context, array $blocks = array())
    {
        // line 5
        echo "    <th>Nº</th>
    <th>D. Comercial</th>
    <th>Nº de Licencia</th>
    <th>RIF</th>
    <th>Monto</th>
    <th>Creado</th>
    <th>Estatus</th>
    ";
        // line 12
        $this->displayBlock('addHeader', $context, $blocks);
        // line 15
        echo " ";
    }

    // line 12
    public function block_addHeader($context, array $blocks = array())
    {
        // line 13
        echo "        ";
        if ($this->env->getExtension('security')->isGranted("ROLE_OPERADOR")) {
            echo "<th>Acciones</th>";
        }
        // line 14
        echo "    ";
    }

    // line 16
    public function block_export($context, array $blocks = array())
    {
        // line 17
        echo " ";
        echo twig_include($this->env, $context, "ExportBundle::iconslink.html.twig", array("pdf" => ("exportpdf_pagos_" . (isset($context["tipo"]) ? $context["tipo"] : null)), "xcel" => ("exportxls_pagos_" . (isset($context["tipo"]) ? $context["tipo"] : null))));
        echo "
";
    }

    // line 19
    public function block_columns($context, array $blocks = array())
    {
        // line 20
        echo "    ";
        $context["centro"] = (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "centroHipico"), $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora"))) : ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "operadora")));
        // line 21
        echo "    <tr style=\"height: 45px;\">
        <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
        echo "</td>
        <td>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["centro"]) ? $context["centro"] : null), "denominacionComercial"), "html", null, true);
        echo "</td>
        <td>";
        // line 24
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getSolicitud", array(), "any", false, true), "getNumLicenciaAdscrita", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getSolicitud", array(), "any", false, true), "getNumLicenciaAdscrita"), " --- ")) : (" --- ")), "html", null, true);
        echo "</td>
        <td>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["centro"]) ? $context["centro"] : null), "persJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["centro"]) ? $context["centro"] : null), "rif"), "html", null, true);
        echo "</td>\t
        <td>";
        // line 26
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "monto"), 2, ",", "."), "html", null, true);
        echo " Bs</td>
        <td>";
        // line 27
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "fechaCreacion"), "d/m/Y"), "html", null, true);
        echo "</td>
        <td>";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status"), "html", null, true);
        echo "</td>
        ";
        // line 29
        $this->displayBlock('addCol', $context, $blocks);
        // line 49
        echo "    </tr>      
";
    }

    // line 29
    public function block_addCol($context, array $blocks = array())
    {
        // line 30
        echo "        ";
        if ($this->env->getExtension('security')->isGranted("ROLE_OPERADOR")) {
            // line 31
            echo "        <td>
            ";
            // line 32
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == "CREADA")) {
                // line 33
                echo "                <a class=\"btn btn-info btn-sm show_pago\" href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" 
                   url=\"";
                // line 34
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pagos_pagar", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "tipo" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tipoPago"))), "html", null, true);
                echo "\">Pagar
                </a>
            ";
            } else {
                // line 37
                echo "                ";
                if ((!(null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getArchivoAdjunto")))) {
                    // line 38
                    echo "                    ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('vlabs_media_twig_extension')->displayTemplate($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "getArchivoAdjunto"), "CentrohipicoBundle:SolicitudAfiliacion:ver_doc.html.twig"), "html", null, true);
                    echo "
                ";
                }
                // line 40
                echo "                ";
                if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == "POR VERIFICAR")) {
                    // line 41
                    echo "                    <a title=\"Modificar Pago\" class=\"btn btn-warning show_pago\" href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" 
                        url=\"";
                    // line 42
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pagos_pagar", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "tipo" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tipoPago"))), "html", null, true);
                    echo "\"><i class=\"fa fa-edit\"></i>
                     </a>
                ";
                }
                // line 45
                echo "            ";
            }
            // line 46
            echo "        </td>
        ";
        }
        // line 48
        echo "        ";
    }

    // line 52
    public function block_footlistar($context, array $blocks = array())
    {
        // line 53
        echo "<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">
                    <span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Cerrar</span>
                </button>
            </div>
            <div class=\"modal-body\" id=\"datos_pago\">
            </div>
            <div class=\"modal-footer\">
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">
    \$(function() {
        \$(\".show_pago\").click(function(ev) {
            \$('#datos_pago').html(\"<div style='text-align:center;'>Cargando...<div>\");
            \$.get(\$(this).attr(\"url\"), function(data) {
                \$('#datos_pago').html(data);
            });
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Pagos:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 53,  178 => 52,  174 => 48,  170 => 46,  167 => 45,  161 => 42,  158 => 41,  155 => 40,  149 => 38,  146 => 37,  140 => 34,  137 => 33,  135 => 32,  132 => 31,  129 => 30,  126 => 29,  121 => 49,  119 => 29,  115 => 28,  111 => 27,  107 => 26,  101 => 25,  97 => 24,  93 => 23,  89 => 22,  86 => 21,  83 => 20,  80 => 19,  73 => 17,  70 => 16,  66 => 14,  61 => 13,  58 => 12,  54 => 15,  52 => 12,  43 => 5,  40 => 4,  34 => 2,);
    }
}
