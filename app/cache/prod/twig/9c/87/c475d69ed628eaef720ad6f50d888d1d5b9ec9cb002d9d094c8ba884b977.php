<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_9c87c475d69ed628eaef720ad6f50d888d1d5b9ec9cb002d9d094c8ba884b977 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 28
        echo "
";
    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 6
        echo "        <div class=\"col-md-8 col-md-offset-2\" style=\"text-align: center\">
            <br/><br/>
          ";
        // line 8
        if ((null === $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"))) {
            echo "  
            <h2 class=\"tit_principal\">";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "fullname")), "FOSUserBundle"), "html", null, true);
            echo "</h2>
            ";
            // line 10
            if ((!twig_test_empty($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session")))) {
                // line 11
                echo "                ";
                $context["targetUrl"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "get", array(0 => (("_security." . $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "security"), "token"), "providerKey")) . ".target_path")), "method");
                // line 12
                echo "            ";
            }
            echo "  
            <br/><br/>
            ";
            // line 15
            echo "            <p><a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-bg btn-primary\">Ingrese Aquí</a></p>
            <br/><br/>
          ";
        } else {
            // line 18
            echo "              <h2 class=\"tit_principal\">Bienvenido ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "fullname"), "html", null, true);
            echo " Ingrese al Menú Principal</h2>
              <br/><br/><br/>
              
            <div class=\"col-md-2 col-md-offset-5\">
                ";
            // line 23
            echo "                <p><a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\" class=\"btn btn-bg btn-primary\">Ingrese Aquí</a></p>
            </div>
          ";
        }
        // line 25
        echo "    
        </div>
    ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 25,  81 => 23,  73 => 18,  66 => 15,  60 => 12,  57 => 11,  55 => 10,  51 => 9,  47 => 8,  43 => 6,  40 => 5,  35 => 28,  32 => 5,  29 => 4,);
    }
}
