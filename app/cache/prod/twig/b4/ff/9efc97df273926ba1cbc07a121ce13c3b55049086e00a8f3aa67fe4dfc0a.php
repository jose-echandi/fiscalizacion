<?php

/* VlabsMediaBundle:Form:form_doc.html.twig */
class __TwigTemplate_b4ff9efc97df273926ba1cbc07a121ce13c3b55049086e00a8f3aa67fe4dfc0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute((isset($context["media"]) ? $context["media"] : null), "path")), "html", null, true);
        echo "\" target=\"_blank\">View</a>
";
    }

    public function getTemplateName()
    {
        return "VlabsMediaBundle:Form:form_doc.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 65,  135 => 55,  129 => 51,  123 => 49,  120 => 48,  116 => 47,  114 => 46,  108 => 43,  104 => 41,  98 => 38,  95 => 37,  93 => 36,  89 => 35,  86 => 34,  80 => 31,  77 => 30,  75 => 29,  71 => 28,  68 => 27,  62 => 24,  59 => 23,  57 => 22,  53 => 21,  50 => 20,  44 => 17,  41 => 16,  39 => 15,  35 => 14,  19 => 1,);
    }
}
