<?php

/* NewTemplateBundle:Menu:mnu_asesor.html.twig */
class __TwigTemplate_b4ef2536a5ded81b4a9e9d9463664462c1a9cbcf54593311a8e831640a53dfa9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3>Licencias</h3>
<ul class=\"menu2\">
    <li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("fiscal_citas_listado", array("tipo" => "Verificada"));
        echo "\" title=\"Por aprobar\">Por aprobar</a></li>
    ";
        // line 5
        echo "    <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("solicitudes_aprobadas_listado");
        echo "\" title=\"Aprobadas\">Aprobadas</a></li>
    <li><a href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("fiscal_citas_listado", array("tipo" => "Rechazada"));
        echo "\" title=\"Rechazadas\">Rechazadas</a></li>
</ul>";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle:Menu:mnu_asesor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 6,  27 => 5,  23 => 3,  19 => 1,);
    }
}
