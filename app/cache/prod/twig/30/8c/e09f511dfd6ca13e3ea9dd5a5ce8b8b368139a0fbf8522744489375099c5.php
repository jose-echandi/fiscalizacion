<?php

/* FOSUserBundle:Resetting:reset_content.html.twig */
class __TwigTemplate_308ce09f511dfd6ca13e3ea9dd5a5ce8b8b368139a0fbf8522744489375099c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'foot_script' => array($this, 'block_foot_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<div class=\"padding-15\">
    <div class=\"block-separator col-md-12\"></div>    
    ";
        // line 4
        $this->displayBlock('content_content', $context, $blocks);
        // line 35
        echo "    ";
        $this->displayBlock('foot_script', $context, $blocks);
        // line 38
        echo "</div>";
    }

    // line 4
    public function block_content_content($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"col-sm-offset-3 col-sm-6\">
            ";
        // line 6
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 33
        echo "        </div>
    ";
    }

    // line 6
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 7
        echo "            <form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_reset", array("token" => (isset($context["token"]) ? $context["token"] : null))), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo " method=\"POST\" class=\"form-horizontal\" role=\"form\">
                <h2>Recuperar contraseña</h2>
                <div class=\"form-group\">
                    ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "plainPassword"), "first"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                    <div class=\"col-sm-8\">
                        <div class=\"col-sm-12 padding-1\">
                            ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "plainPassword"), "first"), 'widget', array("attr" => array("onblur" => "validatePassword(this);", "maxlength" => "8")));
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"form-group\">
                    ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "plainPassword"), "second"), 'label', array("label_attr" => array("class" => "col-sm-4 padding-1")));
        echo "
                    <div class=\"col-sm-8\">
                        <div class=\"col-sm-12 padding-1\">
                            ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "plainPassword"), "second"), 'widget', array("attr" => array("onblur" => "validatePassword(this);", "maxlength" => "8")));
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"form-group\">
                    <div class=\"col-sm-offset-10 col-sm-2\">
                        <button type=\"submit\" id=\"reset_btn\" class=\"btn btn-default pull-right\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.reset.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "</button>
                    </div>
                </div>              
                ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token"), 'widget');
        echo "
            </form>
             ";
    }

    // line 35
    public function block_foot_script($context, array $blocks = array())
    {
        // line 36
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/user/js/validaPassword.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  105 => 36,  102 => 35,  95 => 30,  89 => 27,  80 => 21,  74 => 18,  66 => 13,  60 => 10,  51 => 7,  48 => 6,  43 => 33,  41 => 6,  38 => 5,  35 => 4,  26 => 4,  22 => 2,  31 => 38,  28 => 35,);
    }
}
