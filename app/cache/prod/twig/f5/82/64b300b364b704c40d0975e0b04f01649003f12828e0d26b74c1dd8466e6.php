<?php

/* FiscalizacionBundle:Fiscalizacion:multados.html.twig */
class __TwigTemplate_f58264b300b364b704c40d0975e0b04f01649003f12828e0d26b74c1dd8466e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FiscalizacionBundle:Fiscalizacion:index.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FiscalizacionBundle:Fiscalizacion:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_titulo($context, array $blocks = array())
    {
        // line 3
        echo "con estatus <em>Multados</em>
";
    }

    public function getTemplateName()
    {
        return "FiscalizacionBundle:Fiscalizacion:multados.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,);
    }
}
