<?php

/* SolicitudesCitasBundle:DataSolicitudes:List_pagosP_edf.html.twig */
class __TwigTemplate_2b468304d935f02c91825872eb7810b5c53550baff7141bcced0c29ac1f1748a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "form_pagoP")));
        echo "  
<table id=\"tabla_reporte2\">\t\t\t\t
        <tr id=\"table_header2\">
                 <td>Tipo de Documento</td>
                 <td>Buscar Archivo<span class=\"oblig\">(*)</span></td>
                 <td>Fecha de Pago<span class=\"oblig\">(*)</span></td>
                 <td>N de Recibo<span class=\"oblig\">(*)</span></td>
                 <td>Banco<span class=\"oblig\">(*)</span></td>
                 <td>Monto a Pagar<span class=\"oblig\">(*)</span></td>
                 <td>Acción</td>
        </tr>
         <tr>
            <td class=\"text-left\">Pago por Procesamiento</td>
            <td class=\"text-center\">";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "archivoAdjunto"), 'widget', array("attr" => array("class" => "mediarecaudo")));
        echo "
                           ";
        // line 15
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "archivoAdjunto", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 16
            echo "                           <span class=\"help-block \">
                               ";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "archivoAdjunto"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                           </span>
                           ";
        }
        // line 20
        echo "             </td>
            <td class=\"text-center\">";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fechaDeposito"), 'widget', array("attr" => array("class" => "datePago fechapago")));
        echo "
                           ";
        // line 22
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fechaDeposito", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 23
            echo "                           <span class=\"help-block \">
                               ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fechaDeposito"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                           </span>
                           ";
        }
        // line 27
        echo "             </td>
            <td class=\"text-center\">";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "numReferencia"), 'widget', array("attr" => array("class" => "reciboN"), "id" => "reciboNP"));
        echo "
                           ";
        // line 29
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "numReferencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 30
            echo "                           <span class=\"help-block \">
                               ";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "numReferencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                           </span>
                           ";
        }
        // line 34
        echo "             </td>
            <td class=\"text-center\">";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "banco"), 'widget', array("attr" => array("class" => "banco"), "id" => "banco"));
        echo "
                           ";
        // line 36
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "banco", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 37
            echo "                           <span class=\"help-block \">
                               ";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "banco"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                           </span>
                           ";
        }
        // line 41
        echo "             </td>
            <td class=\"text-center\">
                ";
        // line 43
        echo twig_escape_filter($this->env, ((array_key_exists("nuevo", $context)) ? ((isset($context["PP"]) ? $context["PP"] : null)) : ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "monto"))), "html", null, true);
        echo " Bs       
            </td>
            <td>
               ";
        // line 46
        if (array_key_exists("nuevo", $context)) {
            // line 47
            echo "                   <button class=\"btn btn-sm btn-info\" onclick=\"nuevoPago();\">Cargar</button>
                ";
        } else {
            // line 48
            echo "   
                <button class=\"btn btn-sm btn-info\" onclick=\"updatePago('";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"), "html", null, true);
            echo "');\">Actualizar</button>
                <button class=\"btn btn-sm btn-warning\" onclick=\"cambiar('pagoP',false);\">Cancelar</button>
               ";
        }
        // line 51
        echo " 
            </td>
        </tr>
</table>
     ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token"), 'widget');
        echo "
</form>                
<script type=\"text/javascript\">
    \$(document).ready(function(){
        \$( \"#form_pagoP\" ).submit(function( event ) {
            event.preventDefault();
          });   
            var Options={
                autoSize: true,
                dateFormat: 'dd/mm/yy',
                maxDate: '";
        // line 65
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d/m/Y"), "html", null, true);
        echo "',
                onSelect: function(dateText, inst) { 
                   var datets = Date.parse(inst.selectedYear+'-'+inst.selectedMonth+'-'+inst.selectedDay);
                   \$(this).val(dateText);                
               } 
            };
        \$(\".datePago\").each(function(index,elem){
            \$(this).datepicker(Options); 
        });
    });
</script>            
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:List_pagosP_edf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 65,  135 => 55,  129 => 51,  123 => 49,  120 => 48,  116 => 47,  114 => 46,  108 => 43,  104 => 41,  98 => 38,  95 => 37,  93 => 36,  89 => 35,  86 => 34,  80 => 31,  77 => 30,  75 => 29,  71 => 28,  68 => 27,  62 => 24,  59 => 23,  57 => 22,  53 => 21,  50 => 20,  44 => 17,  41 => 16,  39 => 15,  35 => 14,  19 => 1,);
    }
}
