<?php

/* VlabsMediaBundle:Templates:image.html.twig */
class __TwigTemplate_22eba074920f75d30bda1ff1a62d8e2fe31590faa37af94ed9641dda275182e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute((isset($context["media"]) ? $context["media"] : null), "path")), "html", null, true);
        echo "\" />
";
    }

    public function getTemplateName()
    {
        return "VlabsMediaBundle:Templates:image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
