<?php

/* SolicitudesCitasBundle:DataSolicitudes:noempresa.html.twig */
class __TwigTemplate_b739aa5ebf8ca9410f6ef35cd7749bc106beb8a660794ca43516e458790ddf8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("SolicitudesCitasBundle::solicitud_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SolicitudesCitasBundle::solicitud_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-12\">
    <div class=\"alert alert-warning\">
        No hay una <strong>Oficina Principal</strong> creada. Puede crear una en 
        <a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("operadora_new_office");
        echo "\" class=\"btn btn-default\">Crear Oficina Principal</a>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "SolicitudesCitasBundle:DataSolicitudes:noempresa.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 7,  31 => 4,  28 => 3,);
    }
}
