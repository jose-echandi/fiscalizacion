<?php

/* NewTemplateBundle::flashMessages.html.twig */
class __TwigTemplate_09c4271f6fb5d2eb316f4520f04a64e9fb700ffb2f6cdc13a076617c138765b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-md-12\">  
";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "flashbag"), "all"));
        foreach ($context['_seq'] as $context["type"] => $context["flashes"]) {
            // line 3
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["flashes"]) ? $context["flashes"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["flash"]) {
                // line 4
                echo "        <div class=\"alert alert-";
                echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
                echo " adjusted fade in\">
            <a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>
            ";
                // line 6
                echo twig_escape_filter($this->env, (isset($context["flash"]) ? $context["flash"] : null), "html", null, true);
                echo "
        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo " ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['flashes'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " 
</div>

<script type=\"text/javascript\">
 window.setTimeout(function() {
    \$(\".alert\").fadeTo(500, 0).slideUp(500, function(){
        \$(this).remove(); 
    });
}, 5000);

</script> ";
    }

    public function getTemplateName()
    {
        return "NewTemplateBundle::flashMessages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 9,  37 => 6,  31 => 4,  26 => 3,  22 => 2,  19 => 1,  172 => 65,  169 => 64,  143 => 41,  136 => 36,  127 => 32,  108 => 29,  105 => 28,  100 => 27,  89 => 26,  84 => 24,  78 => 23,  74 => 22,  71 => 21,  54 => 20,  40 => 8,  38 => 7,  32 => 3,  29 => 2,);
    }
}
