<?php

/* CentrohipicoBundle:DataEmpresa:edit.html.twig */
class __TwigTemplate_7a220fd949a0502be9ca60ac915427a5a53b0634e8e851e14b38c34ec8effa7e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("CentrohipicoBundle::centroh_base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'foot_script_assetic' => array($this, 'block_foot_script_assetic'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CentrohipicoBundle::centroh_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content_content($context, array $blocks = array())
    {
        // line 3
        echo "    <style>
        em{
            color: #e70203;
            font-size: 11px;
        }
    </style>
    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"col-md-12\">
        <h1 class=\"tit_principal\">Empresas | Edici&oacute;n</h1>
    </div>
    <div class=\"col-md-12\">
        <p>Los campos con <span class=\"oblig\">(*)</span> son obligatorios. Por favor ingrese los todos los Datos de la empresa para guardar.</p>
    </div>
    <div class=\"col-md-12 form-group btn-group\">
        ";
        // line 17
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start', array("attr" => array("id" => "form_dch")));
        echo "
    </div>
        ";
        // line 19
        $this->env->loadTemplate("CentrohipicoBundle:DataEmpresa:form.html.twig")->display($context);
        // line 20
        echo "        <div class=\"col-md-12 form-group btn-group\">
            <div style=\"float: left\">
                <a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("data_empresa_list");
        echo "\" class=\"btn btn-success btn-sm\">Regresar</a>
            </div>
            <div style=\"float: right\">
                <button id=\"form_btn\" type=\"submit\" class=\"btn btn-primary btn-sm\">Guardar</button>
            </div>
        </div>
    ";
        // line 28
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
";
    }

    // line 31
    public function block_foot_script_assetic($context, array $blocks = array())
    {
        // line 32
        echo "    <script type=\"text/javascript\" >
        \$(document).ready(function(){
            \$('#tabs').tabs();
            \$(\".form-horizontal\").removeClass('form-horizontal');
            \$(\".form-control\").removeClass('form-control');
            \$(\"#datach #datalegal\").show();
            \$(\"#temp\").hide();
            \$(\"#addDL\").click(function(){
                agregarDL();
            });
        });

        function agregarDL(){
            \$(\"#btnDL\").hide();
            \$(\"#fDL\").show();
            \$(\"#fDL\").html(getGifLoading());
            //var route=Routing.generate('datalegal_new');
            \$.get('";
        // line 49
        echo $this->env->getExtension('routing')->getPath("datalegal_new");
        echo "').success(function(data) {
                if (data.message) {
                    message = data.message;
                } else {
                    \$('#datalegal').html(data);
                }
            }).error(function(data, status, headers, config) {
                        if (status === '500') {
                            message = \"No hay conexión con el servidor\";
                        }
                    });
        }
        function sFormDL(){
            var datach=\$(\"#form_dl\").serialize();
            \$(\"#form_dl\").hide();
            \$(\"#fDL\").hide();
            \$(\"#btnDL\").show();
            //\$.post()
        }

        \$(document).ready(function() {
            \$(\".estados\").change(function() {
                estado = \$(this).val();
                var Rmunicipio=Routing.generate('municipios', {estado_id: estado||0});
                getSelect(Rmunicipio,'.municipio',\"Municipio\");
                \$(\".municipio\").val('');

            });

            \$(\".municipio\").change(function() {
                municipio = \$(this).val();
                \$(\".parroquia\").val('');
                var Rparroquia=Routing.generate('parroquias', {municipio_id: municipio||0});
                getSelect(Rparroquia,'.parroquia',\"Parroquia\");
            });
        });

    </script>
    <script src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/centrohipico/js/DataEmpresa/form.js"), "html", null, true);
        echo "\"></script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataEmpresa:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 87,  96 => 49,  77 => 32,  74 => 31,  68 => 28,  59 => 22,  55 => 20,  53 => 19,  48 => 17,  32 => 3,  29 => 2,);
    }
}
