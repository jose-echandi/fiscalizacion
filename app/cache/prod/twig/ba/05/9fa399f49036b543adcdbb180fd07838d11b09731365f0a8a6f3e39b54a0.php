<?php

/* CentrohipicoBundle:DataEmpresa:modal.html.twig */
class __TwigTemplate_ba059fa399f49036b543adcdbb180fd07838d11b09731365f0a8a6f3e39b54a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<style>
    .ui-widget-header {
        background: transparent !important;
        color: transparent !important;
        border: none !important;
    }
    .ui-widget-content {
        background: transparent !important;
        color: transparent !important;
        border: none !important;
    }
</style>

<table class=\"record_properties table table-condensed col-lg-12 col-md-12 col-sm-12\" id=\"tabla_reporte\">
    <thead>
    <tr>
        <th colspan=\"2\" > Denominacion Comercial: ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
        echo "</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style=\"width: 60%; text-align: right;\"> RIF </td>
        <th>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
        echo "</th>
    </tr>
    </tbody>
</table>
<div id=\"tabs\">
    <ul>
        <li><a href=\"#dataempresa\" data-toggle=\"tab\">Empresa</a></li>
        <li><a href=\"#datalegal\" data-toggle=\"tab\">Representante Legal</a></li>
        <li><a href=\"#socios\" data-toggle=\"tab\">Socios</a></li>
    </ul>

    <div id=\"dataempresa\"  class=\"col-md-12\">
        <table class=\"table table-condensed col-lg-12 col-md-12 col-sm-12\" id=\"tabla_reporte2\">
            <thead>
            <tr>
                <th colspan=\"4\" >DATOS DE LA EMPRESA</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Denominación Comercial:</td>
                <th colspan=\"3\">";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "denominacionComercial"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Rif</td>
                <th colspan=\"3\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "persJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "rif"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Estado:</td>

                <th>";
        // line 53
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "estado")) > 0)) {
            // line 54
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "estado"), "nombre"), "html", null, true);
            echo "
                    ";
        }
        // line 56
        echo "                </th>
                <td>Ciudad:</td>
                <th>";
        // line 58
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ciudad")) > 0)) {
            // line 59
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ciudad"), "html", null, true);
            echo "
                    ";
        }
        // line 61
        echo "                </th>
            </tr>
            <tr>
                <td>Municipio:</td>
                <th>";
        // line 65
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "municipio")) > 0)) {
            // line 66
            echo "                        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "municipio"), "nombre"), "html", null, true);
            echo "
                    ";
        }
        // line 68
        echo "                </th>
                <td>Urbanización/Sector:</td>
                <th>";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "urbanSector"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Av/Calle/Carrera:</td>
                <th>";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "avCalleCarrera"), "html", null, true);
        echo "</th>
                <td>Edif/Casa:</td>
                <th>";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "edifCasa"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Oficina/Apto/No:</td>
                <th>";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "ofcAptoNum"), "html", null, true);
        echo "</th>
                <td>Punto Referencia</td>
                <th>";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "puntoReferencia"), "html", null, true);
        echo "</th>
            </tr>
            <tr>
                <td>Teléfono Fijo:</td>
                <th>";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfFijo"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tflFijo"), "html", null, true);
        echo "</th>
                <td>Teléfono Celular</td>
                <th>";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "codTlfCelular"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "tflCelular"), "html", null, true);
        echo "</th>
            </tr>
            </tbody>
        </table>
    </div>

    <div id=\"datalegal\" class=\"col-lg-12 col-md-12 col-sm-12\">
    <table class=\" table table-condensed col-lg-12 col-md-12 col-sm-12\" id=\"tabla_reporte\">
    <thead>
    <tr>
    <th colspan=\"4\" > DATOS DEL APODERADO O REPRESENTANTE LEGAL</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>Nombre:</td>
    <th>";
        // line 104
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "nombre"), "html", null, true);
        echo "</th>
    <td>Rif:</td>
    <th>";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "persJuridica"), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "rif"), "html", null, true);
        echo "</th>
    </tr>
    <tr>
    <td>No. Teléfono Fijo:</td>
    <th>";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "codTlfFijo"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "tflFijo"), "html", null, true);
        echo "</th>
    <td>No. Teléfono Celular:</td>
    <th>";
        // line 112
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "codTlfCelular"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "tflCelular"), "html", null, true);
        echo "</th>
    </tr>
    <tr>
    <td>Estado:</td>
    <th>";
        // line 116
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "estado")) > 0)) {
            // line 117
            echo "    ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "estado"), "nombre"), "html", null, true);
            echo "
    ";
        }
        // line 119
        echo "    </th>
    <td>Ciudad:</td>
    <th>";
        // line 121
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "ciudad")) > 0)) {
            // line 122
            echo "    ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "ciudad"), "html", null, true);
            echo "
    ";
        }
        // line 124
        echo "    </th>
    </tr>
    <tr>
    <td>Municipio:</td>
    <th>";
        // line 128
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "municipio")) > 0)) {
            // line 129
            echo "    ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "municipio"), "nombre"), "html", null, true);
            echo "
    ";
        }
        // line 131
        echo "    </th>
    <td>Parroquia:</td>
    <th>";
        // line 133
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "parroquia")) > 0)) {
            // line 134
            echo "    ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "parroquia"), "nombre"), "html", null, true);
            echo "
    ";
        }
        // line 136
        echo "    </th>
    </tr>
    <tr>
    <td>Urbanización/Sector:</td>
    <th>";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "urbanSector"), "html", null, true);
        echo "</th>
    <td>Av/Calle/Carrera:</td>
    <th>";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "avCalleCarrera"), "html", null, true);
        echo "</th>
    </tr>
    <tr>
    <td>Edif/Casa:</td>
    <th>";
        // line 146
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "edifCasa"), "html", null, true);
        echo "</th>
    <td>Oficina/Apto/No:</td>
    <th>";
        // line 148
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "ofcAptoNum"), "html", null, true);
        echo "</th>
    </tr>
    <tr>
    <td>Punto Referencia:</td>
    <th>";
        // line 152
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "puntoReferencia"), "html", null, true);
        echo "</th>
    <td>Código Postal:</td>
    <th>";
        // line 154
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "codigoPostal"), "html", null, true);
        echo "</th>
    </tr>
    <tr>
    <td>Fax:</td>
    <th>";
        // line 158
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "fax"), "html", null, true);
        echo "</th>
    <td>Email:</td>
    <th>";
        // line 160
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "email"), "html", null, true);
        echo "</th>
    </tr>
    <tr>
    <td>Página Web:</td>
    <th colspan=\"3\">";
        // line 164
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["legal"]) ? $context["legal"] : null), "pagWeb"), "html", null, true);
        echo "</th>
    </tr>
    </tbody>
    </table>
    </div>

    <div id=\"socios\"  class=\"col-md-12\">
        <table id=\"tabla_reporte3\" class=\"table table-condensed col-lg-12 col-md-12 col-sm-12\">
            <thead>
            <tr>
                <th colspan=\"4\" > Detalles de los socios </th>
            </tr>
            </thead>
            <tr id=\"table_header\">
                <td><a href=\"#\">Nombre o Denominacion Comercial</a></td>
                <td><a href=\"#\">Rif</a></td>
                <td><a href=\"#\">Fecha de registro</a></td>
                <td><a href=\"#\">Estatus</a></td>
            </tr>
            <tbody>
            ";
        // line 184
        if (array_key_exists("partner2", $context)) {
            // line 185
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["partner2"]) ? $context["partner2"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["socio"]) {
                // line 186
                echo "                    <tr>
                        <td class=\"text-center\">
                            ";
                // line 188
                if ($this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "denominacionComercial", array(), "any", true, true)) {
                    // line 189
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "denominacionComercial"), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 191
                    echo "                                ";
                    if ((twig_length_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "nombreComercial")) > 0)) {
                        // line 192
                        echo "                                    ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "nombreComercial"), "html", null, true);
                        echo "
                                ";
                    } else {
                        // line 194
                        echo "                                    ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "nombre"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "apellido"), "html", null, true);
                        echo "
                                ";
                    }
                    // line 196
                    echo "                            ";
                }
                // line 197
                echo "                        </td>
                        <td class=\"text-center\">";
                // line 198
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "persJuridica"), "html", null, true);
                echo "-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "rif"), "html", null, true);
                echo "</td>
                        <td class=\"text-center\">";
                // line 199
                if ($this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "fechaRegistro", array(), "any", true, true)) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "fechaRegistro"), "d/m/Y"), "html", null, true);
                } else {
                    echo " - ";
                }
                echo "</td>
                        <td class=\"text-center\">Activo</td>

                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['socio'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 204
            echo "            ";
        }
        // line 205
        echo "
            ";
        // line 206
        if (array_key_exists("partner1", $context)) {
            // line 207
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["partner1"]) ? $context["partner1"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["socio"]) {
                // line 208
                echo "                    <tr>
                        <td class=\"text-center\">
                            ";
                // line 210
                if ($this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "denominacionComercial", array(), "any", true, true)) {
                    // line 211
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "denominacionComercial"), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 213
                    echo "                                ";
                    if ((twig_length_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "nombreComercial")) > 0)) {
                        // line 214
                        echo "                                    ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "nombreComercial"), "html", null, true);
                        echo "
                                ";
                    } else {
                        // line 216
                        echo "                                    ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "nombre"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "apellido"), "html", null, true);
                        echo "
                                ";
                    }
                    // line 218
                    echo "                            ";
                }
                // line 219
                echo "                        </td>
                        <td class=\"text-center\">";
                // line 220
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "persJuridica"), "html", null, true);
                echo "-";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "rif"), "html", null, true);
                echo "</td>
                        <td class=\"text-center\">";
                // line 221
                if ($this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "fechaRegistro", array(), "any", true, true)) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["socio"]) ? $context["socio"] : null), "fechaRegistro"), "d/m/Y"), "html", null, true);
                } else {
                    echo " - ";
                }
                echo "</td>
                        <td class=\"text-center\">Activo</td>

                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['socio'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 226
            echo "            ";
        }
        // line 227
        echo "            </tbody>
        </table>
    </div>
</div>
<script type=\"text/javascript\" >
    \$(document).ready(function(){
        \$('#tabs').tabs();
        \$(\"#dataoper #datalegal\").show();
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataEmpresa:modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  474 => 227,  471 => 226,  456 => 221,  450 => 220,  447 => 219,  444 => 218,  436 => 216,  430 => 214,  427 => 213,  421 => 211,  419 => 210,  415 => 208,  410 => 207,  408 => 206,  405 => 205,  402 => 204,  387 => 199,  381 => 198,  378 => 197,  375 => 196,  367 => 194,  361 => 192,  358 => 191,  352 => 189,  350 => 188,  346 => 186,  341 => 185,  339 => 184,  316 => 164,  309 => 160,  304 => 158,  297 => 154,  292 => 152,  285 => 148,  280 => 146,  273 => 142,  268 => 140,  262 => 136,  256 => 134,  254 => 133,  250 => 131,  244 => 129,  242 => 128,  236 => 124,  230 => 122,  228 => 121,  224 => 119,  218 => 117,  216 => 116,  207 => 112,  200 => 110,  191 => 106,  186 => 104,  165 => 88,  158 => 86,  151 => 82,  146 => 80,  139 => 76,  134 => 74,  127 => 70,  123 => 68,  117 => 66,  115 => 65,  109 => 61,  103 => 59,  101 => 58,  97 => 56,  91 => 54,  89 => 53,  79 => 48,  72 => 44,  46 => 23,  37 => 17,  19 => 1,);
    }
}
