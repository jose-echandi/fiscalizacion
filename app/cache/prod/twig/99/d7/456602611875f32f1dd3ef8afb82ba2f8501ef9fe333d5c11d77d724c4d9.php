<?php

/* CentrohipicoBundle:DataOperadora:DOpertable_form.html.twig */
class __TwigTemplate_99d7456602611875f32f1dd3ef8afb82ba2f8501ef9fe333d5c11d77d724c4d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "        ";
        // line 2
        echo "            ";
        // line 3
        echo "                ";
        // line 4
        echo "            ";
        // line 5
        echo "        ";
        // line 6
        echo "<table id=\"tabla_reporte2\" class=\"tabla_reporte2\">\t
    <tbody>
      <tr id=\"table_header2\"><td colspan=\"4\">";
        // line 8
        if (((isset($context["tipo"]) ? $context["tipo"] : null) == "P")) {
            echo "Oficina Principal";
        } else {
            echo "Oficina Sucursal";
        }
        echo "</td></tr>
    <tr>
        <td colspan=\"2\" style=\"width: 50%;\">";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "denominacionComercial"), 'label', array("label_attr" => array("class" => "text-left col-md-12")));
        echo "</td>
        <td colspan=\"2\" style=\"width: 50%;\">
            ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "denominacionComercial"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
            ";
        // line 13
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "denominacionComercial", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 14
            echo "            <span class=\"help-block \">
                ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "denominacionComercial"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
            </span>
            ";
        }
        // line 18
        echo "        </td>
    </tr>\t\t\t
    <tr>
             <td colspan=\"2\" style=\"width: 50%;\">";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estatusLocal"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td colspan=\"2\" style=\"width: 50%;\">
                      ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estatusLocal"), 'widget', array("attr" => array("class" => " ")));
        echo "
                            ";
        // line 24
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estatusLocal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 25
            echo "                            <span class=\"help-block \">
                                ";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estatusLocal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 29
        echo "             </td>
    </tr>
    <tr>
             <td colspan=\"2\" style=\"width: 50%;\">";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "propietarioLocal"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
             <td colspan=\"2\" style=\"width: 50%;\">
                 ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "propietarioLocal"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                            ";
        // line 35
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "propietarioLocal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 36
            echo "                            <span class=\"help-block \">
                                ";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "propietarioLocal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 40
        echo "             </td>
    </tr>\t
    <tr>
             <td>";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
             <td>";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), 'widget', array("attr" => array("class" => " ")));
        echo "
                            ";
        // line 45
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 46
            echo "                            <span class=\"help-block \">
                                ";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nombre"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 50
        echo "                 
             </td>
             <td>";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), 'label', array("label_attr" => array("class" => " col-md-12 control-label text-left")));
        echo "</td>
             <td>";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), 'widget', array("attr" => array("class" => " ")));
        echo "
                            ";
        // line 54
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 55
            echo "                            <span class=\"help-block \">
                                ";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "apellido"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                            </span>
                            ";
        }
        // line 59
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>
                  ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tipoci"), 'widget', array("attr" => array("class" => " input-mini")));
        echo "
                  - ";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), 'widget');
        echo "
                    ";
        // line 66
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 67
            echo "                    <span class=\"help-block \">
                        ";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ci"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 71
        echo "             </td>
             <td>";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rif"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>
                 ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "persJuridica"), 'widget', array("attr" => array("class" => "input-mini ")));
        echo "
                  - ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rif"), 'widget');
        echo "
                    ";
        // line 76
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rif", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 77
            echo "                    <span class=\"help-block \">
                        ";
            // line 78
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "rif"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 81
        echo "             </td>

    </tr>
    <tr>
             <td>";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), 'widget', array("attr" => array("class" => "estados", "style" => "width:156px !important")));
        echo "
                    ";
        // line 87
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 88
            echo "                    <span class=\"help-block \">
                        ";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "estado"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 92
        echo "             </td>
             <td><label class=\"col-md-12 control-label text-left required\" for=\"municipios\">Municipio <span class=\"asterisk\">*</span></label></td>
             <td>
                    ";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "municipio"), 'widget', array("attr" => array("class" => "municipio", "data-selected" => (isset($context["idmunicipio"]) ? $context["idmunicipio"] : null))));
        echo "
             </td>

    </tr>
    <tr>

             <td><label class=\"col-md-12 control-label text-left required\" for=\"ciudad\">Ciudad <span class=\"asterisk\">*</span></label></td>
             <td>";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 103
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 104
            echo "                    <span class=\"help-block \">
                        ";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ciudad"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 108
        echo "             </td>
             <td><label class=\"col-md-12 control-label text-left required\" for=\"parroquia\">Parroquia <span class=\"asterisk\">*</span></label></td>
             <td>
                 ";
        // line 111
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "parroquia"), 'widget', array("attr" => array("class" => "parroquia", "data-selected" => (isset($context["idparroquia"]) ? $context["idparroquia"] : null))));
        echo "
                 
             </td>
             
    </tr>
    <tr>
             <td>";
        // line 117
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 119
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 120
            echo "                    <span class=\"help-block \">
                        ";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "urbanSector"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 124
        echo "             </td>
             <td>";
        // line 125
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>
                 ";
        // line 127
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 128
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 129
            echo "                    <span class=\"help-block \">
                        ";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "avCalleCarrera"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 133
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 137
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 138
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 139
            echo "                    <span class=\"help-block \">
                        ";
            // line 140
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "edifCasa"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 143
        echo "             </td>
             <td>";
        // line 144
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 145
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 146
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 147
            echo "                    <span class=\"help-block \">
                        ";
            // line 148
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "ofcAptoNum"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 151
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 154
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 155
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 156
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 157
            echo "                    <span class=\"help-block \">
                        ";
            // line 158
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "puntoReferencia"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 161
        echo "             </td>
             <td>";
        // line 162
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "<span class=\"asterisk\">*</span></td>
             <td>";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
                    ";
        // line 164
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 165
            echo "                    <span class=\"help-block \">
                        ";
            // line 166
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codigoPostal"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 169
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 172
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 173
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 174
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 175
            echo "                    <span class=\"help-block \">
                        ";
            // line 176
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 179
        echo "             </td>
        <td>";
        // line 180
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
        <td>";
        // line 181
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codFax"), 'widget', array("attr" => array("class" => " col-md-3")));
        echo "
            ";
        // line 182
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
            ";
        // line 183
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 184
            echo "                <span class=\"help-block \">
                        ";
            // line 185
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "fax"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
            ";
        }
        // line 188
        echo "        </td>
    </tr>
    <tr>
             <td>";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 192
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codTlfFijo"), 'widget', array("attr" => array("class" => " col-md-3")));
        echo "
                 ";
        // line 193
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
                    ";
        // line 194
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 195
            echo "                    <span class=\"help-block \">
                        ";
            // line 196
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflFijo"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 199
        echo "             </td>
             <td>";
        // line 200
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td>";
        // line 201
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codTlfCelular"), 'widget', array("attr" => array("class" => " col-md-3 ")));
        echo "
                 ";
        // line 202
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), 'widget', array("attr" => array("class" => "col-md-6 ")));
        echo "
                    ";
        // line 203
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 204
            echo "                    <span class=\"help-block \">
                        ";
            // line 205
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tflCelular"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 208
        echo "             </td>
    </tr>
    <tr>
             <td>";
        // line 211
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), 'label', array("label_attr" => array("class" => " col-md-12 text-left")));
        echo "</td>
             <td colspan=\"3\">";
        // line 212
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), 'widget', array("attr" => array("class" => "col-md-10 ")));
        echo "
                    ";
        // line 213
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb", array(), "any", false, true), "vars", array(), "any", false, true), "errors", array(), "any", false, true), 0, array(), "array", false, true), "message", array(), "any", true, true)) {
            // line 214
            echo "                    <span class=\"help-block \">
                        ";
            // line 215
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "pagWeb"), "vars"), "errors"), 0, array(), "array"), "message"), "html", null, true);
            echo "
                    </span>
                    ";
        }
        // line 218
        echo "             </td>
    </tr>
    ";
        // line 223
        echo "  </tbody>
</table>

 <div class=\"block-separator col-md-12\"></div>
 ";
        // line 228
        echo " ";
        // line 229
        echo " <script type=\"text/javascript\" >
 \$(document).ready(function() {
      \$(\".estados\").change(function() {
            estado = \$('.estados option:selected').val();
              var Rmunicipio=Routing.generate('municipios', {estado_id: estado});
              getSelect(Rmunicipio,'.municipio',\"Municipio\");
        });
        
        \$(\".municipio\").change(function() {
                estado = \$('.estados option:selected').val();
                municipio = \$('.municipio option:selected').val();
                var Rparroquia=Routing.generate('parroquias', {municipio_id: municipio});
                getSelect(Rparroquia,'.parroquia',\"Parroquia\");
        });
     \$(\".estados\").change();
     \$(\".municipio\").change();
    });
    function sFormCH(){
        var datach=\$(\"#form_dch\").serialize();
    }
</script>";
    }

    public function getTemplateName()
    {
        return "CentrohipicoBundle:DataOperadora:DOpertable_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  558 => 229,  556 => 228,  550 => 223,  546 => 218,  540 => 215,  537 => 214,  535 => 213,  531 => 212,  527 => 211,  522 => 208,  516 => 205,  513 => 204,  511 => 203,  507 => 202,  503 => 201,  499 => 200,  496 => 199,  490 => 196,  487 => 195,  485 => 194,  481 => 193,  477 => 192,  473 => 191,  468 => 188,  462 => 185,  459 => 184,  457 => 183,  453 => 182,  449 => 181,  445 => 180,  442 => 179,  436 => 176,  433 => 175,  431 => 174,  427 => 173,  423 => 172,  418 => 169,  412 => 166,  409 => 165,  407 => 164,  403 => 163,  399 => 162,  396 => 161,  390 => 158,  387 => 157,  385 => 156,  381 => 155,  377 => 154,  366 => 148,  363 => 147,  361 => 146,  357 => 145,  353 => 144,  350 => 143,  344 => 140,  341 => 139,  335 => 137,  331 => 136,  326 => 133,  320 => 130,  317 => 129,  311 => 127,  306 => 125,  303 => 124,  297 => 121,  294 => 120,  292 => 119,  275 => 111,  261 => 104,  259 => 103,  255 => 102,  245 => 95,  231 => 88,  229 => 87,  225 => 86,  221 => 85,  215 => 81,  209 => 78,  191 => 72,  179 => 67,  177 => 66,  173 => 65,  169 => 64,  159 => 59,  153 => 56,  127 => 46,  125 => 45,  121 => 44,  117 => 43,  103 => 36,  101 => 35,  97 => 34,  87 => 29,  81 => 26,  76 => 24,  72 => 23,  67 => 21,  56 => 15,  53 => 14,  51 => 13,  47 => 12,  42 => 10,  33 => 8,  27 => 5,  25 => 4,  23 => 3,  21 => 2,  19 => 1,  372 => 151,  351 => 168,  347 => 167,  339 => 138,  336 => 160,  330 => 157,  319 => 149,  315 => 128,  313 => 146,  308 => 143,  298 => 138,  293 => 135,  290 => 134,  288 => 118,  286 => 132,  284 => 117,  282 => 130,  280 => 129,  278 => 128,  276 => 127,  274 => 126,  272 => 125,  270 => 108,  268 => 123,  266 => 122,  264 => 105,  262 => 120,  260 => 119,  258 => 118,  256 => 117,  254 => 116,  252 => 115,  250 => 114,  248 => 113,  246 => 112,  244 => 111,  242 => 110,  240 => 92,  238 => 108,  236 => 107,  234 => 89,  232 => 105,  230 => 104,  228 => 103,  226 => 102,  224 => 101,  222 => 100,  220 => 99,  218 => 98,  216 => 97,  214 => 96,  212 => 95,  210 => 94,  208 => 93,  206 => 77,  204 => 76,  202 => 90,  200 => 75,  198 => 88,  196 => 74,  194 => 86,  192 => 85,  190 => 84,  188 => 71,  186 => 82,  184 => 81,  182 => 68,  180 => 79,  178 => 78,  176 => 77,  174 => 76,  172 => 75,  170 => 74,  168 => 73,  166 => 72,  164 => 62,  162 => 70,  160 => 69,  158 => 68,  156 => 67,  154 => 66,  152 => 65,  150 => 55,  148 => 54,  146 => 62,  144 => 53,  142 => 60,  140 => 52,  138 => 58,  136 => 50,  134 => 56,  132 => 55,  130 => 47,  128 => 53,  126 => 52,  124 => 51,  122 => 50,  120 => 49,  118 => 48,  116 => 47,  114 => 46,  112 => 40,  110 => 44,  108 => 43,  106 => 37,  104 => 41,  102 => 40,  100 => 39,  98 => 38,  96 => 37,  94 => 36,  92 => 32,  90 => 34,  88 => 33,  86 => 32,  84 => 31,  82 => 30,  80 => 29,  78 => 25,  75 => 26,  73 => 25,  70 => 24,  68 => 23,  62 => 18,  54 => 14,  52 => 13,  48 => 12,  37 => 6,  32 => 3,  29 => 6,);
    }
}
