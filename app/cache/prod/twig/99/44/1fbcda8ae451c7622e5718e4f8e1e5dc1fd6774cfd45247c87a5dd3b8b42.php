<?php

/* LicenciaBundle:AdmJuegosExplotados:index.html.twig */
class __TwigTemplate_99441fbcda8ae451c7622e5718e4f8e1e5dc1fd6774cfd45247c87a5dd3b8b42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/genstyles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"print\">

";
    }

    // line 10
    public function block_content_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>Listado de Juegos Explotados</h1>
    </div>

    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row\">
        <div class=\"col-sm-2\">
            <a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("admjuegosexplotados_new");
        echo "\" class=\"btn btn-primary btn-sm \"><i class=\"icon-plus-sign\"></i> Agregar Juegos Explotados</a>
        </div>
        <div class=\"col-sm-10 block-separator\"></div>
    </div>
<div id=\"action\">
        <div class=\"right\">
            ";
        // line 25
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 26
            echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
            // line 27
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : null));
            echo "
                </article>
            ";
        }
        // line 30
        echo "        </div>
    </div>
    <div class=\"col-md-12\">
        <table class=\"table table-condensed table-striped\">
            <thead>
                <tr>
                    <th>Juego</th>
                    <th>Estatus</th>
                    <th width=\"20%\" colspan=\"2\" >Acciones</th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 43
            echo "                <tr>
                    <td><a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admjuegosexplotados_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "juego"), "html", null, true);
            echo "</a></td>
                    <td>";
            // line 45
            echo ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == 1)) ? ("Activo") : ("Inactivo"));
            echo "</td>
                    <td style=\"text-align:center;\">
                        <a href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admjuegosexplotados_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
            echo "\" class=\"btn btn-default btn-sm\">Editar</a>
                        <a href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admjuegosexplotados_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
            echo "\" class=\"btn btn-info btn-sm\">Mostrar</a>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "            </tbody>
        </table>
    </div>

    <div class=\"block-separator col-sm-12\"></div>
    ";
        // line 63
        echo "
";
    }

    public function getTemplateName()
    {
        return "LicenciaBundle:AdmJuegosExplotados:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 63,  127 => 52,  117 => 48,  113 => 47,  108 => 45,  102 => 44,  99 => 43,  95 => 42,  81 => 30,  75 => 27,  72 => 26,  70 => 25,  61 => 19,  51 => 11,  48 => 10,  41 => 6,  37 => 5,  32 => 4,  29 => 3,);
    }
}
