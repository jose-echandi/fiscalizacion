<?php

/* FOSUserBundle:Fiscal:list.html.twig */
class __TwigTemplate_d1304876031bd85f13928db63c8427a7584999561f909087f8ab33752aaaa41d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'content_content' => array($this, 'block_content_content'),
            'script_base' => array($this, 'block_script_base'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>Gesti&oacute;n de fiscales</h1>
    </div>

    <div class=\"block-separator col-sm-12\"></div>

    <div id=\"action\">
        <div class=\"left\">
            ";
        // line 13
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 14
            echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
            // line 15
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : null));
            echo "
                </article>
            ";
        }
        // line 18
        echo "        </div>
    </div>
   ";
        // line 20
        echo twig_include($this->env, $context, "ExportBundle::iconslink.html.twig", array("pdf" => "exportpdf_fiscales", "xcel" => "exportxls_fiscales"));
        echo "
    <div class=\"col-md-12\">
        <table class=\"table table-condensed table-striped\">
            <thead>
            <tr>
                <th>Nº</th>
                <th>Cedula de identidad</th>
                <th>Nombre y apellido</th>
                <th>Fecha de ingreso</th>
                <th>Estatus</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            ";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["usuario"]) {
            // line 35
            echo "                <tr>
                    <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "</td>
                    <th>
                        ";
            // line 38
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "perfil"), 0, array(), "array"), "ci") > 1)) {
                // line 39
                echo "                            ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "perfil"), 0, array(), "array"), "ci"), "html", null, true);
                echo "
                        ";
            } else {
                // line 41
                echo "                            ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "perfil"), 0, array(), "array"), "rif"), "html", null, true);
                echo "
                        ";
            }
            // line 43
            echo "                    </th>
                    <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "perfil"), 0, array(), "array"), "getNombre"));
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "perfil"), 0, array(), "array"), "getApellido"));
            echo "</td>
                    <td>08/09/2014</td>
                    <td class=\"text-center\">";
            // line 46
            if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "enabled") == "1")) {
                echo "<i class=\"fa fa-check-circle-o\"></i> Activo";
            } else {
                echo "<i class=\"fa fa-times-circle-o\"></i> Inactivo";
            }
            echo "</td>
                    <td class=\"text-center\">
                        ";
            // line 48
            if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "enabled") == "1")) {
                // line 49
                echo "                            <a data-href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fiscal-change-status", array("user_id" => $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "id"), "status_id" => 0)), "html", null, true);
                echo "\" data-toggle=\"modal\" data-target=\"#confirm-disable\" href=\"#\"><i class=\"fa fa-minus-square-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Desactivar\"></i></a>
                        ";
            } else {
                // line 51
                echo "                            <a data-href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fiscal-change-status", array("user_id" => $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "id"), "status_id" => 1)), "html", null, true);
                echo "\" data-toggle=\"modal\" data-target=\"#confirm-enable\" href=\"#\"><i class=\"fa fa-plus-square-o\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Activar\"></i></a>
                        ";
            }
            // line 53
            echo "                    </td>
                </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usuario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "            </tbody>
        </table>
    </div>
    <div class=\"block-separator col-sm-12\"></div>

    <div class=\"modal fade\" id=\"confirm-enable\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Activar Fiscal
                </div>
                <div class=\"modal-body\">
                    ¿Está seguro que desea activar el fiscal?
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" class=\"btn btn-danger danger\">Aceptar</a>
                </div>
            </div>
        </div>
    </div>
    <div class=\"modal fade\" id=\"confirm-disable\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    Desactivar Fiscal
                </div>
                <div class=\"modal-body\">
                    ¿Está seguro que desea desactivar el fiscal?
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
                    <a href=\"#\" class=\"btn btn-danger danger\">Aceptar</a>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 94
    public function block_script_base($context, array $blocks = array())
    {
        // line 95
        echo "    ";
        $this->displayParentBlock("script_base", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        \$('#confirm-enable').on('show.bs.modal', function(e) {
            \$(this).find('.danger').attr('href', \$(e.relatedTarget).data('href'));
        });
        \$('#confirm-disable').on('show.bs.modal', function(e) {
            \$(this).find('.danger').attr('href', \$(e.relatedTarget).data('href'));
        });

    </script>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Fiscal:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 95,  204 => 94,  163 => 56,  147 => 53,  141 => 51,  135 => 49,  133 => 48,  124 => 46,  117 => 44,  114 => 43,  108 => 41,  102 => 39,  100 => 38,  95 => 36,  92 => 35,  75 => 34,  58 => 20,  54 => 18,  48 => 15,  45 => 14,  43 => 13,  32 => 4,  29 => 3,);
    }
}
