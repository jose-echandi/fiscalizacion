<?php

/* LicenciaBundle:AdmClasfLicencias:index.html.twig */
class __TwigTemplate_d1ae7b46a47e41a439dbc2b695dabb6f526d49a76296745d2f09ae0ef190459c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("NewTemplateBundle::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content_content' => array($this, 'block_content_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "NewTemplateBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/genstyles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/solicitudescitas/css/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"print\">

";
    }

    // line 10
    public function block_content_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row col-sm-12\">
        <h1>Listado de Clasificaciones de Licencias</h1>
    </div>

    <div class=\"block-separator col-sm-12\"></div>
    <div class=\"row\">
        <div class=\"col-sm-2\">
            <a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("admclasflicencias_new");
        echo "\" class=\"btn btn-primary btn-sm \"><i class=\"icon-plus-sign\"></i> Agregar Licencia</a>
        </div>
        <div class=\"col-sm-10 block-separator\"></div>
    </div>
    <div id=\"action\">
        <div class=\"right\">
            ";
        // line 25
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 26
            echo "                <article class=\"col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable\" style=\"width: 50%\">
                    ";
            // line 27
            echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : null));
            echo "
                </article>
            ";
        }
        // line 30
        echo "        </div>
    </div>
    <div class=\"col-md-12\">
        <table class=\"table table-condensed table-striped\">
            <thead>
                <tr>
                    <th>Licencia</th>
                    <th>Tipo de Licencia</th>
                    <th>Estatus</th>
                    <th width=\"20%\" colspan=\"2\" >Acciones</th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 44
            echo "                <tr>
                    <td><a href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admclasflicencias_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "clasfLicencia"), "html", null, true);
            echo "</a></td>
                    <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "admTiposLicencias"), "tipoLicencia"), "html", null, true);
            echo "</td>
                    <td>";
            // line 47
            echo ((($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status") == 1)) ? ("Activo") : ("Inactivo"));
            echo "</td>
                    <td style=\"text-align:center;\">
                        <a href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admclasflicencias_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
            echo "\" class=\"btn btn-default btn-sm\">Editar</a>
                        <a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admclasflicencias_show", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
            echo "\" class=\"btn btn-info btn-sm\">Mostrar</a>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "            </tbody>
        </table>
    </div>

    <div class=\"block-separator col-sm-12\"></div>
    ";
        // line 65
        echo "
";
    }

    public function getTemplateName()
    {
        return "LicenciaBundle:AdmClasfLicencias:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 65,  132 => 54,  122 => 50,  118 => 49,  113 => 47,  109 => 46,  103 => 45,  100 => 44,  96 => 43,  81 => 30,  75 => 27,  72 => 26,  70 => 25,  61 => 19,  51 => 11,  48 => 10,  41 => 6,  37 => 5,  32 => 4,  29 => 3,);
    }
}
